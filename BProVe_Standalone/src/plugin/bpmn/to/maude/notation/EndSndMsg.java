package plugin.bpmn.to.maude.notation;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@SuppressWarnings("serial")
public class EndSndMsg extends End  implements java.io.Serializable {
	
	//Status Edge Msg
	
	String Status;
	Msg msg;
	
	public EndSndMsg()
	{
		
	}
	
	public EndSndMsg (String endsndmsg)
	{
		ArrayList<String> EndSndThrow = new ArrayList<String>();
		
		EndSndThrow = TokenEnd(endsndmsg);
		this.Status = EndSndThrow.get(0);
		
		Pattern pattern = Pattern.compile("^\"|^[ ]+\"");
		Matcher matcher = pattern.matcher(EndSndThrow.get(1));	
		
		if (matcher.find()) 
		{
			//int startindex = matcher.group().length();
			//int endindex = EndSndThrow.get(1).lastIndexOf("\"");
			//String substring = EndSndThrow.get(1).substring(startindex, endindex);
			//System.out.println("\nsubstring: "+EndSndThrow.get(1));
			this.Edge = new Edge(EndSndThrow.get(1));
		}
		
		//System.out.println("\nEndSndThrow.get(0): "+EndSndThrow.get(0));
		//System.out.println("\nEndSndThrow.get(1): "+EndSndThrow.get(1));
		//System.out.println("\nEndSndThrow.get(2): "+EndSndThrow.get(2));
		
		this.msg = new Msg(EndSndThrow.get(2));
	}
	
	public boolean compareEndSndMsg(EndSndMsg endsndmsg1, EndSndMsg endsndmsg2)
	{
		//System.out.println("\ncompareEndSndMsg");
		//System.out.println("\nendsndmsg1.Edge.EdgeName: "+endsndmsg1.Edge.EdgeName);
		//System.out.println("\nendsndmsg2.Edge.EdgeName: "+endsndmsg2.Edge.EdgeName);
		//System.out.println("\nendsndmsg1.Edge.EdgeToken: "+endsndmsg1.Edge.EdgeToken);
		//System.out.println("\nendsndmsg2.Edge.EdgeToken: "+endsndmsg2.Edge.EdgeToken);
		if(endsndmsg1.Edge.EdgeName.equals(endsndmsg2.Edge.EdgeName)
				&& !endsndmsg1.Edge.EdgeToken.equals(endsndmsg2.Edge.EdgeToken))
		{
		return true;
		}else
		{
			return false;
		}
	}	
	
	/*public boolean compareEndSndMsgName (EndSndMsg endsndmsg1, EndSndMsg endsndmsg2)
	{
		if(endsndmsg1.name.equals(endsndmsg2.name))
		{
			return true;
		}else return false;	
	}*/
	
	public void printEndSndMsg()
	{
		//Status Edge Msg
		//System.out.println("\nEndSndMsg: ");
		//System.out.println("Status: "+this.Status);
		this.Edge.printEdge();
		this.msg.printMsg();
		////System.out.println("OutputToken: "+OutputToken);
	}
}
