package plugin.bpmn.to.maude.notation;


import java.io.StringWriter;
import java.util.*;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;


@SuppressWarnings("serial")
public class CollaborationCompare  implements java.io.Serializable {

	static ArrayList<Proc> DifferentProcess;
	
	
	
	@SuppressWarnings("static-access")
	public CollaborationCompare(String pathbpmn, String result) throws Exception
	{
		this.DifferentProcess = new ArrayList<Proc>();
		////System.out.println("STRINGA RESULT: "+result);
		Proc resultantProcess = ConfrontoColl(result);
		printConf(resultantProcess, pathbpmn);
		
	}
	
	@SuppressWarnings("static-access")
	public Proc ConfrontoColl(String result)
	{
	////System.out.println(result);
	Proc finalprocess = new Proc();
	ArrayList<Proc> processChanger = new ArrayList<Proc>();
	Collaboration coll1 = new Collaboration();
	String input = coll1.Inizializza(result);
	////System.out.println("INPUT: "+input);
	ArrayList<String> arrayStringColl = coll1.extractCollaboration(input);
		
	int k=0;
	
		for(int j=0; j<arrayStringColl.size()-1; j++)
		{	
			k=j+1;
			Collaboration collprova1 = new Collaboration(arrayStringColl.get(j));
			Collaboration collprova2 = new Collaboration(arrayStringColl.get(k));
			processChanger = coll1.compareCollaboration(collprova1, collprova2);
						
				for(int i=0; i<processChanger.size(); i++)
				{				
				this.DifferentProcess.add(processChanger.get(i));
				}
			
				/*if(this.DifferentProcess.get(0) != null)
				{
					finalprocess = this.DifferentProcess.get(0);
					//System.out.println(j);
					finalprocess.printProcess();
					
				}*/
				for(int y=0; y<this.DifferentProcess.size(); y++)
				{
					////System.out.println("DIFFERENT");
					//this.DifferentProcess.get(y).printProcess();
				ProcessAdd(finalprocess ,this.DifferentProcess.get(y));
				}
				
				////System.out.println("FINALPROC"+j);
				//finalprocess.printProcess();
				////System.out.println("FINE FINALPROC");
			}
		
		return finalprocess;			
		}			
	
	//BUILD FINAL PROC
	@SuppressWarnings("unused")
	public void ProcessAdd (Proc finalprocess, Proc yElement)
	{
		//Boolean appoggio = false;
		Boolean selection = false;
		
		//IF compareAndJoinName IS FALSE, finalprocess add yElement.AndJoin 
		int finalsizeAndJoin = finalprocess.AndJoin.size();		
		for(int i=0; i<yElement.AndJoin.size(); i++)
		{
			Boolean appoggio = false;
			AndJoin andjoinprov = new AndJoin();
			for(int k=0; k<finalsizeAndJoin; k++)
			{
				if(finalprocess.AndJoin.get(k).Edge.EdgeName.equals(yElement.AndJoin.get(i).Edge.EdgeName))
				//if(andjoinprov.compareAndJoin(finalprocess.AndJoin.get(k), yElement.AndJoin.get(i)) || finalprocess.AndJoin.contains(yElement.AndJoin.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			
			if(appoggio == false)
			{
				finalprocess.AndJoin.add(yElement.AndJoin.get(i));
				selection = selectelement(yElement.AndJoin.get(i).name);
			}
		}
		
		int finalsizeAndSplit = finalprocess.AndSplit.size();		
		for(int i=0; i<yElement.AndSplit.size(); i++)
		{
			Boolean appoggio = false;
			AndSplit andsplitprov = new AndSplit();
			for(int k=0; k<finalsizeAndSplit; k++)
			{
				
				if(finalprocess.AndSplit.get(k).Edge.EdgeName.equals(yElement.AndJoin.get(i).Edge.EdgeName))
				//if(andsplitprov.compareAndSplit(finalprocess.AndSplit.get(k), yElement.AndSplit.get(i)) || finalprocess.AndSplit.contains(yElement.AndSplit.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.AndSplit.add(yElement.AndSplit.get(i));
				selection = selectelement(yElement.AndSplit.get(i).name);
			}
		}
		
		int finalsizeEnd = finalprocess.End.size();
		////System.out.println("FINALSIZEEND"+finalsizeEnd);
		for(int i=0; i<yElement.End.size(); i++)
		{
			Boolean appoggio = false;
			End endprov = new End();
			for(int k=0; k<finalsizeEnd; k++)
			{
				if(finalprocess.End.get(k).Edge.EdgeName.equals(yElement.End.get(i).Edge.EdgeName))
				//if(endprov.compareEnd(finalprocess.End.get(k), yElement.End.get(i)) || finalprocess.End.contains(yElement.End.get(i)))
				{
				////System.out.println("TRUE");
				appoggio = true;
				break;
				}
			}
			
			if(appoggio == false)
			{
				////System.out.println("FALSE");
				finalprocess.End.add(yElement.End.get(i));
				////System.out.println("finalprocess.End.add(yElement.End.get(i))");
				//selection = selectelement(yElement.End.get(i).name);
				
			}
			
		}
		
		int finalsizeEndSndMsg = finalprocess.EndSndMsg.size();		
		for(int i=0; i<yElement.EndSndMsg.size(); i++)
		{
			Boolean appoggio = false;
			EndSndMsg endsndmsgprov = new EndSndMsg();
			for(int k=0; k<finalsizeEndSndMsg; k++)
			{
				if(finalprocess.EndSndMsg.get(k).Edge.EdgeName.equals(yElement.EndSndMsg.get(i).Edge.EdgeName))
				//if(endsndmsgprov.compareEndSndMsg(finalprocess.EndSndMsg.get(k), yElement.EndSndMsg.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.EndSndMsg.add(yElement.EndSndMsg.get(i));
				//selection = selectelement(yElement.EndSndMsg.get(i).name);
			}
		}
		
		int finalsizeEventBasedgat = finalprocess.EventBasedgat.size();		
		for(int i=0; i<yElement.EventBasedgat.size(); i++)
		{
			Boolean appoggio = false;
			EventSplit eventbasedgatprov = new EventSplit();
			for(int k=0; k<finalsizeEventBasedgat; k++)
			{
				if(finalprocess.EventBasedgat.get(k).Edge.EdgeName.equals(yElement.EventBasedgat.get(i).Edge.EdgeName))
				//if(eventbasedgatprov.compareEventBasedgat(finalprocess.EventBasedgat.get(k), yElement.EventBasedgat.get(i)) || finalprocess.EventBasedgat.contains(yElement.EventBasedgat.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.EventBasedgat.add(yElement.EventBasedgat.get(i));
				//selection = selectelement(yElement.EventBasedgat.get(i).name);
			}
		}
		
		int finalsizeMsgCatchEvent = finalprocess.InterRcv.size();
		////System.out.println("SIZE: "+yElement.InterRcv.size());
		for(int i=0; i<yElement.InterRcv.size(); i++)
		{
			Boolean appoggio = false;
			InterRcv msgcatcheventprov = new InterRcv();
		
			for(int k=0; k<finalsizeMsgCatchEvent; k++)
			{
				//System.out.println("inizio\n");
				
				
				if(finalprocess.InterRcv.get(k).OutputEdge.EdgeName.equals(yElement.InterRcv.get(i).OutputEdge.EdgeName))
				//if(msgcatcheventprov.compareInterRcv(finalprocess.InterRcv.get(k), yElement.InterRcv.get(i)) || finalprocess.InterRcv.contains(yElement.InterRcv.get(i)))
				{
					//System.out.println("VERO");
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				yElement.InterRcv.get(i).printInterRcv();
				//System.out.println("FALSO");
				yElement.InterRcv.get(i).printInterRcv();
				finalprocess.InterRcv.add(yElement.InterRcv.get(i));
				//selection = selectelement(yElement.InterRcv.get(i).name);
			}
		}
		
		
		int finalsizeMsgThrowEvent = finalprocess.InterSnd.size();		
		for(int i=0; i<yElement.InterSnd.size(); i++)
		{
			Boolean appoggio = false;
			InterSnd msgthroweventprov = new InterSnd();
			for(int k=0; k<finalsizeMsgThrowEvent; k++)
			{
				if(finalprocess.InterSnd.get(k).OutputEdge.EdgeName.equals(yElement.InterSnd.get(i).OutputEdge.EdgeName))
				//if(msgthroweventprov.compareInterSnd(finalprocess.InterSnd.get(k), yElement.InterSnd.get(i)) || finalprocess.InterSnd.contains(yElement.InterSnd.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.InterSnd.add(yElement.InterSnd.get(i));
				//selection = InterSnd(yElement.InterSnd.get(i).name);
			}
		}
		
		int finalsizeOrSplit = finalprocess.OrSplit.size();		
		for(int i=0; i<yElement.OrSplit.size(); i++)
		{
			Boolean appoggio = false;
			OrSplit orsplitprov = new OrSplit();
			for(int k=0; k<finalsizeOrSplit; k++)
			{
				if(finalprocess.OrSplit.get(k).Edge.EdgeName.equals(yElement.OrSplit.get(i).Edge.EdgeName))
				//if(orsplitprov.compareOrSplit(finalprocess.OrSplit.get(k), yElement.OrSplit.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.OrSplit.add(yElement.OrSplit.get(i));
				//selection = selectelement(yElement.OrSplit.get(i).name);
			}
		}
		
		int finalsizeReceiveTask = finalprocess.ReceiveTask.size();		
		for(int i=0; i<yElement.ReceiveTask.size(); i++)
		{
			Boolean appoggio = false;
			ReceiveTask receivetaskprov = new ReceiveTask();
			for(int k=0; k<finalsizeReceiveTask; k++)
			{
				if(finalprocess.ReceiveTask.get(k).name.equals(yElement.ReceiveTask.get(i).name))
				//if(receivetaskprov.compareReceiveTask(finalprocess.ReceiveTask.get(k), yElement.ReceiveTask.get(i)) || finalprocess.ReceiveTask.contains(yElement.ReceiveTask.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.ReceiveTask.add(yElement.ReceiveTask.get(i));
				//selection = selectelement(yElement.ReceiveTask.get(i).name);
			}
		}
		
		int finalsizeSendTask = finalprocess.SendTask.size();		
		for(int i=0; i<yElement.SendTask.size(); i++)
		{
			Boolean appoggio = false;
			SendTask sendtaskprov = new SendTask();
			for(int k=0; k<finalsizeSendTask; k++)
			{
				if(finalprocess.SendTask.get(k).name.equals(yElement.SendTask.get(i).name))
				//if(sendtaskprov.compareSendTask(finalprocess.SendTask.get(k), yElement.SendTask.get(i)) || finalprocess.SendTask.contains(yElement.SendTask.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.SendTask.add(yElement.SendTask.get(i));
				//selection = selectelement(yElement.SendTask.get(i).name);
			}			
		}
		
		int finalsizeStart = finalprocess.Start.size();		
		for(int i=0; i<yElement.Start.size(); i++)
		{
			Boolean appoggio = false;
			Start startprov = new Start();
			for(int k=0; k<finalsizeStart; k++)
			{
				if(finalprocess.Start.get(k).Edge.EdgeName.equals(yElement.Start.get(i).Edge.EdgeName))
				//if(startprov.compareStart(finalprocess.Start.get(k), yElement.Start.get(i)) || finalprocess.Start.contains(yElement.Start.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.Start.add(yElement.Start.get(i));
				//selection = selectelement(yElement.Start.get(i).name);
			}			
		}
		
		int finalsizeStartRcvMsg = finalprocess.StartRcvMsg.size();		
		for(int i=0; i<yElement.StartRcvMsg.size(); i++)
		{
			Boolean appoggio = false;
			StartRcvMsg startrcvmsgprov = new StartRcvMsg();
			for(int k=0; k<finalsizeStartRcvMsg; k++)
			{
				if(finalprocess.StartRcvMsg.get(k).Edge.EdgeName.equals(yElement.StartRcvMsg.get(i).Edge.EdgeName))
				//if(startrcvmsgprov.compareStartRcvMsg(finalprocess.StartRcvMsg.get(k), yElement.StartRcvMsg.get(i)) || finalprocess.StartRcvMsg.contains(yElement.StartRcvMsg.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.StartRcvMsg.add(yElement.StartRcvMsg.get(i));
				//selection = selectelement(yElement.StartRcvMsg.get(i).name);
			}			
		}
		
		int finalsizeTask = finalprocess.Task.size();
		
		for(int i=0; i<yElement.Task.size(); i++)
		{
			Boolean appoggio = false;
			Task taskprov = new Task();
			
			for(int k=0; k<finalsizeTask; k++)
			{		
				if(finalprocess.Task.get(k).name.equals(yElement.Task.get(i).name))
				//if(taskprov.compareTask(finalprocess.Task.get(k), yElement.Task.get(i)) || finalprocess.Task.contains(yElement.Task.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			
			if(appoggio == false)
			{
				finalprocess.Task.add(yElement.Task.get(i));
				//selection = selectelement(yElement.Task.get(i).name);
			}
		}
		
		int finalsizeTerminate = finalprocess.Terminate.size();		
		for(int i=0; i<yElement.Terminate.size(); i++)
		{
			Boolean appoggio = false;
			Terminate terminateprov = new Terminate();
			for(int k=0; k<finalsizeTerminate; k++)
			{
				if(finalprocess.Terminate.get(k).Edge.EdgeName.equals(yElement.Terminate.get(i).Edge.EdgeName))
				//if(terminateprov.compareTerminate(finalprocess.Terminate.get(k), yElement.Terminate.get(i)) || finalprocess.Terminate.contains(yElement.Terminate.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.Terminate.add(yElement.Terminate.get(i));
				//selection = selectelement(yElement.Terminate.get(i).name);
			}			
		}
		
		int finalsizeXorJoin = finalprocess.XorJoin.size();		
		for(int i=0; i<yElement.XorJoin.size(); i++)
		{
			Boolean appoggio = false;
			XorJoin xorjoinprov = new XorJoin();
			for(int k=0; k<finalsizeXorJoin; k++)
			{
				if(finalprocess.XorJoin.get(k).Edge.EdgeName.equals(yElement.XorJoin.get(i).Edge.EdgeName))
				//if(xorjoinprov.compareXorJoin(finalprocess.XorJoin.get(k), yElement.XorJoin.get(i)) || finalprocess.XorJoin.contains(yElement.XorJoin.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.XorJoin.add(yElement.XorJoin.get(i));
				//selection = selectelement(yElement.XorJoin.get(i).name);
			}			
		}
		
		int finalsizeXorSplit = finalprocess.XorSplit.size();		
		for(int i=0; i<yElement.XorSplit.size(); i++)
		{
			Boolean appoggio = false;
			XorSplit xorsplitprov = new XorSplit();
			for(int k=0; k<finalsizeXorSplit; k++)
			{
				if(finalprocess.XorSplit.get(k).Edge.EdgeName.equals(yElement.XorSplit.get(i).Edge.EdgeName))
				//if(xorsplitprov.compareXorSplit(finalprocess.XorSplit.get(k), yElement.XorSplit.get(i)) || finalprocess.XorSplit.contains(yElement.XorSplit.get(i)))
				{
				appoggio = true; 
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.XorSplit.add(yElement.XorSplit.get(i));
				//selection = selectelement(yElement.XorSplit.get(i).name);
			}			
		}		
	}
	
	public static boolean selectelement(String selectname)
	{
		
		return false;		 
	}
		
		
	public void printConf(Proc processconf, String Loadpath) throws Exception
	{
		//System.out.println("\nFinalProcess: ");
		processconf.printProcess();
		processconf.colorChanger(Loadpath);				
	}
	
	
	
	
	
	public static Counterexample getCounterexample(String result, String originalModel) throws Exception
	{
		
	ArrayList<Proc> DifferentProcess = new ArrayList<Proc>();
	//System.out.println("\n into getCounterexample\n");
	Proc finalprocess = new Proc();
	ArrayList<Proc> processChanger = new ArrayList<Proc>();
	Collaboration coll1 = new Collaboration();
	String input = coll1.Inizializza(result);
	////System.out.println("INPUT: "+input);
	ArrayList<String> arrayStringColl = coll1.extractCollaboration(input);
		
	int k=0;
	
		for(int j=0; j<arrayStringColl.size()-1; j++)
		{	
			k=j+1;
			//System.out.println("\narrayStringColl.get(j): "+arrayStringColl.get(j));
			Collaboration collprova1 = new Collaboration(arrayStringColl.get(j));
			Collaboration collprova2 = new Collaboration(arrayStringColl.get(k));
			processChanger = coll1.compareCollaboration(collprova1, collprova2);
						
				for(int i=0; i<processChanger.size(); i++)
				{				
				DifferentProcess.add(processChanger.get(i));
				}
			
				/*if(this.DifferentProcess.get(0) != null)
				{
					finalprocess = this.DifferentProcess.get(0);
					//System.out.println(j);
					finalprocess.printProcess();
					
				}*/
				for(int y=0; y<DifferentProcess.size(); y++)
				{
					////System.out.println("DIFFERENT");
					//this.DifferentProcess.get(y).printProcess();
					ProcessAddStatic(finalprocess ,DifferentProcess.get(y));
				}
				
				////System.out.println("FINALPROC"+j);
				//finalprocess.printProcess();
				////System.out.println("FINE FINALPROC");
			}
		
		try{
		
			Document doc = getPrintableCounterexample(finalprocess , originalModel);
			//System.out.println("\n ProcessCounterexample: \n");
			//RITORNARE UN PROC INSIEME AL DOCUMENT COSì DA 
			//AVERE GLI ELEMENTI CAMBIATI NEL PROCòlllllllllllllllllllllllllllllllllllllllllll
			
			finalprocess.printProcess();
	//		 ByteArrayOutputStream baos = new ByteArrayOutputStream();
	//		    PrintStream ps = new PrintStream(baos);
	//		    // IMPORTANT: Save the old //System.out!
	//		    PrintStream old = //System.out;
	//		    // Tell Java to use your special stream
	//		    System.setOut(ps);
	//		    // Print some output: goes to your special stream
	//		    finalprocess.printProcess();
	//		    // Put things back
	//		    //System.out.flush();
	//		    System.setOut(old);
	//		    // Show what happened
	//		    result=baos.toString();
	//		    //System.out.println("Here: " + baos.toString());
			Counterexample counter= new Counterexample();
			counter.setViolatingCounterexample(doc);
			counter.setViolatingProcess(finalprocess);
			return counter;	
		}catch(Exception e){e.printStackTrace(); return null;}			
		}			
	
	
	
	
	@SuppressWarnings("unused")
	public static void ProcessAddStatic (Proc finalprocess, Proc yElement)
	{
		//Boolean appoggio = false;
		Boolean selection = false;
		
		//IF compareAndJoinName IS FALSE, finalprocess add yElement.AndJoin 
		int finalsizeAndJoin = finalprocess.AndJoin.size();		
		for(int i=0; i<yElement.AndJoin.size(); i++)
		{
			Boolean appoggio = false;
			AndJoin andjoinprov = new AndJoin();
			for(int k=0; k<finalsizeAndJoin; k++)
			{
				if(finalprocess.AndJoin.get(k).Edge.EdgeName.equals(yElement.AndJoin.get(i).Edge.EdgeName))
				//if(andjoinprov.compareAndJoin(finalprocess.AndJoin.get(k), yElement.AndJoin.get(i)) || finalprocess.AndJoin.contains(yElement.AndJoin.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			
			if(appoggio == false)
			{
				finalprocess.AndJoin.add(yElement.AndJoin.get(i));
				selection = selectelement(yElement.AndJoin.get(i).name);
			}
		}
		
		int finalsizeAndSplit = finalprocess.AndSplit.size();		
		for(int i=0; i<yElement.AndSplit.size(); i++)
		{
			Boolean appoggio = false;
			AndSplit andsplitprov = new AndSplit();
			for(int k=0; k<finalsizeAndSplit; k++)
			{
				
				if(finalprocess.AndSplit.get(k).Edge.EdgeName.equals(yElement.AndSplit.get(i).Edge.EdgeName))
				//if(andsplitprov.compareAndSplit(finalprocess.AndSplit.get(k), yElement.AndSplit.get(i)) || finalprocess.AndSplit.contains(yElement.AndSplit.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.AndSplit.add(yElement.AndSplit.get(i));
				selection = selectelement(yElement.AndSplit.get(i).name);
			}
		}
		
		int finalsizeEnd = finalprocess.End.size();
		////System.out.println("FINALSIZEEND"+finalsizeEnd);
		for(int i=0; i<yElement.End.size(); i++)
		{
			Boolean appoggio = false;
			End endprov = new End();
			for(int k=0; k<finalsizeEnd; k++)
			{
				if(finalprocess.End.get(k).Edge.EdgeName.equals(yElement.End.get(i).Edge.EdgeName))
				//if(endprov.compareEnd(finalprocess.End.get(k), yElement.End.get(i)) || finalprocess.End.contains(yElement.End.get(i)))
				{
				////System.out.println("TRUE");
				appoggio = true;
				break;
				}
			}
			
			if(appoggio == false)
			{
				////System.out.println("FALSE");
				finalprocess.End.add(yElement.End.get(i));
				////System.out.println("finalprocess.End.add(yElement.End.get(i))");
				//selection = selectelement(yElement.End.get(i).name);
				
			}
			
		}
		
		int finalsizeEndSndMsg = finalprocess.EndSndMsg.size();		
		for(int i=0; i<yElement.EndSndMsg.size(); i++)
		{
			Boolean appoggio = false;
			EndSndMsg endsndmsgprov = new EndSndMsg();
			for(int k=0; k<finalsizeEndSndMsg; k++)
			{
				if(finalprocess.EndSndMsg.get(k).Edge.EdgeName.equals(yElement.EndSndMsg.get(i).Edge.EdgeName))
				//if(endsndmsgprov.compareEndSndMsg(finalprocess.EndSndMsg.get(k), yElement.EndSndMsg.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.EndSndMsg.add(yElement.EndSndMsg.get(i));
				//selection = selectelement(yElement.EndSndMsg.get(i).name);
			}
		}
		
		int finalsizeEventBasedgat = finalprocess.EventBasedgat.size();		
		for(int i=0; i<yElement.EventBasedgat.size(); i++)
		{
			Boolean appoggio = false;
			EventSplit eventbasedgatprov = new EventSplit();
			for(int k=0; k<finalsizeEventBasedgat; k++)
			{
				if(finalprocess.EventBasedgat.get(k).Edge.EdgeName.equals(yElement.EventBasedgat.get(i).Edge.EdgeName))
				//if(eventbasedgatprov.compareEventBasedgat(finalprocess.EventBasedgat.get(k), yElement.EventBasedgat.get(i)) || finalprocess.EventBasedgat.contains(yElement.EventBasedgat.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.EventBasedgat.add(yElement.EventBasedgat.get(i));
				//selection = selectelement(yElement.EventBasedgat.get(i).name);
			}
		}
		
		int finalsizeMsgCatchEvent = finalprocess.InterRcv.size();
		////System.out.println("SIZE: "+yElement.InterRcv.size());
		for(int i=0; i<yElement.InterRcv.size(); i++)
		{
			Boolean appoggio = false;
			InterRcv msgcatcheventprov = new InterRcv();
		
			for(int k=0; k<finalsizeMsgCatchEvent; k++)
			{
				//System.out.println("inizio\n");
				
				
				if(finalprocess.InterRcv.get(k).OutputEdge.EdgeName.equals(yElement.InterRcv.get(i).OutputEdge.EdgeName))
				//if(msgcatcheventprov.compareInterRcv(finalprocess.InterRcv.get(k), yElement.InterRcv.get(i)) || finalprocess.InterRcv.contains(yElement.InterRcv.get(i)))
				{
					//System.out.println("VERO");
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				yElement.InterRcv.get(i).printInterRcv();
				//System.out.println("FALSO");
				yElement.InterRcv.get(i).printInterRcv();
				finalprocess.InterRcv.add(yElement.InterRcv.get(i));
				//selection = selectelement(yElement.InterRcv.get(i).name);
			}
		}
		
		
		int finalsizeMsgThrowEvent = finalprocess.InterSnd.size();		
		for(int i=0; i<yElement.InterSnd.size(); i++)
		{
			Boolean appoggio = false;
			InterSnd msgthroweventprov = new InterSnd();
			for(int k=0; k<finalsizeMsgThrowEvent; k++)
			{
				if(finalprocess.InterSnd.get(k).OutputEdge.EdgeName.equals(yElement.InterSnd.get(i).OutputEdge.EdgeName))
				//if(msgthroweventprov.compareInterSnd(finalprocess.InterSnd.get(k), yElement.InterSnd.get(i)) || finalprocess.InterSnd.contains(yElement.InterSnd.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.InterSnd.add(yElement.InterSnd.get(i));
				//selection = InterSnd(yElement.InterSnd.get(i).name);
			}
		}
		
		int finalsizeOrSplit = finalprocess.OrSplit.size();		
		for(int i=0; i<yElement.OrSplit.size(); i++)
		{
			Boolean appoggio = false;
			OrSplit orsplitprov = new OrSplit();
			for(int k=0; k<finalsizeOrSplit; k++)
			{
				if(finalprocess.OrSplit.get(k).Edge.EdgeName.equals(yElement.OrSplit.get(i).Edge.EdgeName))
				//if(orsplitprov.compareOrSplit(finalprocess.OrSplit.get(k), yElement.OrSplit.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.OrSplit.add(yElement.OrSplit.get(i));
				//selection = selectelement(yElement.OrSplit.get(i).name);
			}
		}
		
		int finalsizeReceiveTask = finalprocess.ReceiveTask.size();		
		for(int i=0; i<yElement.ReceiveTask.size(); i++)
		{
			Boolean appoggio = false;
			ReceiveTask receivetaskprov = new ReceiveTask();
			for(int k=0; k<finalsizeReceiveTask; k++)
			{
				if(finalprocess.ReceiveTask.get(k).name.equals(yElement.ReceiveTask.get(i).name))
				//if(receivetaskprov.compareReceiveTask(finalprocess.ReceiveTask.get(k), yElement.ReceiveTask.get(i)) || finalprocess.ReceiveTask.contains(yElement.ReceiveTask.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.ReceiveTask.add(yElement.ReceiveTask.get(i));
				//selection = selectelement(yElement.ReceiveTask.get(i).name);
			}
		}
		
		int finalsizeSendTask = finalprocess.SendTask.size();		
		for(int i=0; i<yElement.SendTask.size(); i++)
		{
			Boolean appoggio = false;
			SendTask sendtaskprov = new SendTask();
			for(int k=0; k<finalsizeSendTask; k++)
			{
				if(finalprocess.SendTask.get(k).name.equals(yElement.SendTask.get(i).name))
				//if(sendtaskprov.compareSendTask(finalprocess.SendTask.get(k), yElement.SendTask.get(i)) || finalprocess.SendTask.contains(yElement.SendTask.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.SendTask.add(yElement.SendTask.get(i));
				//selection = selectelement(yElement.SendTask.get(i).name);
			}			
		}
		
		int finalsizeStart = finalprocess.Start.size();		
		for(int i=0; i<yElement.Start.size(); i++)
		{
			Boolean appoggio = false;
			Start startprov = new Start();
			for(int k=0; k<finalsizeStart; k++)
			{
				if(finalprocess.Start.get(k).Edge.EdgeName.equals(yElement.Start.get(i).Edge.EdgeName))
				//if(startprov.compareStart(finalprocess.Start.get(k), yElement.Start.get(i)) || finalprocess.Start.contains(yElement.Start.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.Start.add(yElement.Start.get(i));
				//selection = selectelement(yElement.Start.get(i).name);
			}			
		}
		
		int finalsizeStartRcvMsg = finalprocess.StartRcvMsg.size();		
		for(int i=0; i<yElement.StartRcvMsg.size(); i++)
		{
			Boolean appoggio = false;
			StartRcvMsg startrcvmsgprov = new StartRcvMsg();
			for(int k=0; k<finalsizeStartRcvMsg; k++)
			{
				if(finalprocess.StartRcvMsg.get(k).Edge.EdgeName.equals(yElement.StartRcvMsg.get(i).Edge.EdgeName))
				//if(startrcvmsgprov.compareStartRcvMsg(finalprocess.StartRcvMsg.get(k), yElement.StartRcvMsg.get(i)) || finalprocess.StartRcvMsg.contains(yElement.StartRcvMsg.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.StartRcvMsg.add(yElement.StartRcvMsg.get(i));
				//selection = selectelement(yElement.StartRcvMsg.get(i).name);
			}			
		}
		
		int finalsizeTask = finalprocess.Task.size();
		
		for(int i=0; i<yElement.Task.size(); i++)
		{
			Boolean appoggio = false;
			Task taskprov = new Task();
			
			for(int k=0; k<finalsizeTask; k++)
			{		
				if(finalprocess.Task.get(k).name.equals(yElement.Task.get(i).name))
				//if(taskprov.compareTask(finalprocess.Task.get(k), yElement.Task.get(i)) || finalprocess.Task.contains(yElement.Task.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			
			if(appoggio == false)
			{
				finalprocess.Task.add(yElement.Task.get(i));
				//selection = selectelement(yElement.Task.get(i).name);
			}
		}
		
		int finalsizeTerminate = finalprocess.Terminate.size();		
		for(int i=0; i<yElement.Terminate.size(); i++)
		{
			Boolean appoggio = false;
			Terminate terminateprov = new Terminate();
			for(int k=0; k<finalsizeTerminate; k++)
			{
				if(finalprocess.Terminate.get(k).Edge.EdgeName.equals(yElement.Terminate.get(i).Edge.EdgeName))
				//if(terminateprov.compareTerminate(finalprocess.Terminate.get(k), yElement.Terminate.get(i)) || finalprocess.Terminate.contains(yElement.Terminate.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.Terminate.add(yElement.Terminate.get(i));
				//selection = selectelement(yElement.Terminate.get(i).name);
			}			
		}
		
		int finalsizeXorJoin = finalprocess.XorJoin.size();		
		for(int i=0; i<yElement.XorJoin.size(); i++)
		{
			Boolean appoggio = false;
			XorJoin xorjoinprov = new XorJoin();
			for(int k=0; k<finalsizeXorJoin; k++)
			{
				if(finalprocess.XorJoin.get(k).Edge.EdgeName.equals(yElement.XorJoin.get(i).Edge.EdgeName))
				//if(xorjoinprov.compareXorJoin(finalprocess.XorJoin.get(k), yElement.XorJoin.get(i)) || finalprocess.XorJoin.contains(yElement.XorJoin.get(i)))
				{
				appoggio = true;
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.XorJoin.add(yElement.XorJoin.get(i));
				//selection = selectelement(yElement.XorJoin.get(i).name);
			}			
		}
		
		int finalsizeXorSplit = finalprocess.XorSplit.size();		
		for(int i=0; i<yElement.XorSplit.size(); i++)
		{
			Boolean appoggio = false;
			XorSplit xorsplitprov = new XorSplit();
			for(int k=0; k<finalsizeXorSplit; k++)
			{
				if(finalprocess.XorSplit.get(k).Edge.EdgeName.equals(yElement.XorSplit.get(i).Edge.EdgeName))
				//if(xorsplitprov.compareXorSplit(finalprocess.XorSplit.get(k), yElement.XorSplit.get(i)) || finalprocess.XorSplit.contains(yElement.XorSplit.get(i)))
				{
				appoggio = true; 
				break;
				}
			}
			if(appoggio == false)
			{
				finalprocess.XorSplit.add(yElement.XorSplit.get(i));
				//selection = selectelement(yElement.XorSplit.get(i).name);
			}			
		}		
	}
	
	
	
	
	public static Document getPrintableCounterexample(Proc processconf, String originalModel) throws Exception
	{
		//System.out.println("\nFinalProcess: ");
		//processconf.printProcess();
		Document doc=processconf.getColorChangedCounterexample(originalModel);			
		//System.out.println("DOCUMENTCOLOR: "+getStringFromDocument(doc));
		return doc;
	}
	
	public static String getStringFromDocument(Document doc)
	{
	    try
	    {
	       DOMSource domSource = new DOMSource(doc);
	       StringWriter writer = new StringWriter();
	       StreamResult result = new StreamResult(writer);
	       TransformerFactory tf = TransformerFactory.newInstance();
	       javax.xml.transform.Transformer transformer = tf.newTransformer();
	       transformer.transform(domSource, result);
	       return writer.toString();
	    }
	    catch(TransformerException ex)
	    {
	       ex.printStackTrace();
	       return null;
	    }
	} 
	
}



