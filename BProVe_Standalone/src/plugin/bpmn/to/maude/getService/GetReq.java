package plugin.bpmn.to.maude.getService;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpException;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.HttpVersion;
import org.apache.http.ProtocolVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ConnectTimeoutException;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.ExecutionContext;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import plugin.bpmn.to.maude.handlers.PostMultipleParameters;


/**
 * This example demonstrates the use of the {@link ResponseHandler} to simplify
 * the process of processing the HTTP response and releasing associated resources.
 */
@SuppressWarnings("deprecation")
public class GetReq {
	
	static String property=null;
	static String poolName=null;
	static String taskName1=null , taskName2=null;
	static String sndMsgName=null , rcvMsgName=null;
	//LOCAL
	//static String defaultAddress = "http://localhost:8080/";
	static String defaultAddress = "http://pros.unicam.it:8080/";
	
	public static String GetReq_BProve_Maude_WebServie( String property) throws Exception {
        CloseableHttpClient httpclient = HttpClients.createDefault();
        //System.out.println("PROPERTY: "+property);
        
        property = property.replace("\n", "");
		property = property.replace(" ", "%20");
		property = property.replace("\"", "%22");
		property = property.replace("-", "%2D");
		property = property.replace(".", "%2E");
		property = property.replace("<", "%3C");
		property = property.replace(">", "%3E");
		property = property.replace("_", "%5F");
		property = property.replace("{", "%7B");
		property = property.replace("|", "%7C");
		property = property.replace("}", "%7D");
		property = property.replace("~", "%7E");
		property = property.replace("(", "%28");
		property = property.replace(")", "%29");
		property = property.replace("^", "%5E");
		property = property.replace("[", "%5B");
		property = property.replace("]", "%5D"); 
		property = property.replace("/", "%2F");
		property = property.replace("\\", "%5C");
					
		
        String responseBody="no_response";
        
        String address = defaultAddress+"BProVe_WebService/webapi/BPMNOS/"+property;
        
        HttpGet httpget = new HttpGet(address);
            
        /*print headers*/
        //Header[] headers = httpget.getAllHeaders();
        //for (Header header : headers) {
        	//System.out.println("Key : " + header.getName()
        		 //    + " ,Value : " + header.getValue());
        //}

        // Create a custom response handler
        ResponseHandler<String> responseHandler = new ResponseHandler<String>() {

	        public String handleResponse(final HttpResponse response) throws ClientProtocolException, IOException {
	             int status = response.getStatusLine().getStatusCode();
	                    	
	              /*print headers*/
	              //Header[] headers = response.getAllHeaders();
	              //for (Header header : headers) {
	                    //System.out.println("Key : " + header.getName()
	                    //      + " ,Value : " + header.getValue());
	               //}
	             if (status == HttpStatus.SC_BAD_REQUEST ||
	             	  status == HttpStatus.SC_REQUEST_TIMEOUT ||
	             	  status == HttpStatus.SC_LENGTH_REQUIRED ||
	             	  status == HttpStatus.SC_REQUEST_TOO_LONG ||
	             	  status == HttpStatus.SC_REQUEST_URI_TOO_LONG ||
	             	  status == HttpStatus.SC_SERVICE_UNAVAILABLE ||
	             	  status == HttpStatus.SC_NOT_IMPLEMENTED) throw new ClientProtocolException("Unexpected response status: " + status);                           
	
	
	              if ( status >= 200 && status < 300 ) {
	                  HttpEntity entity = response.getEntity();
	                  return entity != null ? EntityUtils.toString(entity) : null;
	              } else {
	                   throw new ClientProtocolException("Unexpected response status: " + status);
	              }
	         }
        };
            
        try {
                responseBody = httpclient.execute(httpget, responseHandler);
        } catch (ConnectTimeoutException e) {            
         	    e.getCause().printStackTrace(System.out);
                 throw e;
        } catch (SocketTimeoutException e) {           	
         	    e.getCause().printStackTrace(System.out);
                 throw e;      
        } catch (Exception e) {
        	    e.getCause().printStackTrace(System.out);
                throw e;
        }
           
        httpclient.close();
        
        return responseBody;
    }
	
	public void process(final HttpResponse response, final HttpContext context) 
	            throws HttpException, IOException {
	        if (response == null) {
	            throw new IllegalArgumentException("HTTP response may not be null");
	        }
	        if (context == null) {
	            throw new IllegalArgumentException("HTTP context may not be null");
	        }
	        // Always drop connection after certain type of responses
	        int status = response.getStatusLine().getStatusCode();
	        if (status == HttpStatus.SC_BAD_REQUEST ||
	                status == HttpStatus.SC_REQUEST_TIMEOUT ||
	                status == HttpStatus.SC_LENGTH_REQUIRED ||
	                status == HttpStatus.SC_REQUEST_TOO_LONG ||
	                status == HttpStatus.SC_REQUEST_URI_TOO_LONG ||
	                status == HttpStatus.SC_SERVICE_UNAVAILABLE ||
	                status == HttpStatus.SC_NOT_IMPLEMENTED) {
	            response.setHeader(HTTP.CONN_DIRECTIVE, HTTP.CONN_CLOSE);
	            return;
	        }
	        // Always drop connection for HTTP/1.0 responses and below
	        // if the content body cannot be correctly delimited
	        HttpEntity entity = response.getEntity();
	        if (entity != null) {
	            ProtocolVersion ver = response.getStatusLine().getProtocolVersion();
	            if (entity.getContentLength() < 0 && 
	                    (!entity.isChunked() || ver.lessEquals(HttpVersion.HTTP_1_0))) {
	                response.setHeader(HTTP.CONN_DIRECTIVE, HTTP.CONN_CLOSE);
	                return;
	            }
	        }
	        // Drop connection if requested by the client
	        HttpRequest request = (HttpRequest)
	            context.getAttribute(ExecutionContext.HTTP_REQUEST);
	        if (request != null) {
	            Header header = request.getFirstHeader(HTTP.CONN_DIRECTIVE);
	            if (header != null) {
	                response.setHeader(HTTP.CONN_DIRECTIVE, header.getValue());
	            }
	        }
	    }

	
	
	public static String class2Json(PostMultipleParameters pModel){
		ObjectMapper mapper = new ObjectMapper();
		String jsonInString = null;
		try {

			// Convert object to JSON string
			jsonInString = mapper.writeValueAsString(pModel);
			
//			Object json = mapper.readValue(jsonInString, Object.class);
//			String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
//
//			System.out.println(indented);//This p

		} catch (JsonGenerationException e123) {
			e123.printStackTrace();
		} catch (JsonMappingException e123) {
			e123.printStackTrace();
		} catch (IOException e123) {
			e123.printStackTrace();
		}
			return jsonInString;
	   }
	
	//METODO PER RICHIESTE JSON
			public static PostMultipleParameters PostReq_BProve_Maude_WebServie_Property_JSON( PostMultipleParameters inputM) throws Exception {
						
				 	String address=null;

			        if(inputM.getProperty()==null) {
			        	address = defaultAddress+"BProVe_WebService/webapi/BPMNOS/parseModel";
			      
			        }else{
			            address = defaultAddress+"BProVe_WebService/webapi/BPMNOS/model/verification";
			          
			        }
					CloseableHttpClient client = HttpClients.createDefault();

					PostMultipleParameters postMultiple = inputM;

					/*** convert class2Json ***/
				    String jsonString = class2Json(postMultiple);
				    //System.out.println("jsonStringProperties: "+jsonString);
				    
				    /****/
				    
				    URL url = new URL(address);
				    URLConnection con = url.openConnection();
				    HttpURLConnection http = (HttpURLConnection)con;
				    http.setRequestMethod("POST"); // PUT is another valid option
				    http.setDoOutput(true);
				    http.setDoInput(true);
				    http.setRequestProperty("Content-Type", "application/json");
				    http.setRequestProperty("Accept", "application/json");
				    http.setRequestMethod("POST");
				     
				    byte[] out = jsonString.getBytes(StandardCharsets.UTF_8);
				    int length = out.length;

				    http.setFixedLengthStreamingMode(length);
				    http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
				    http.connect();
				    try(OutputStream os = http.getOutputStream()) {
				        os.write(out);
				        os.close();
				    }		
				    
				     BufferedReader inB = new BufferedReader(
				                                    new InputStreamReader(
				                                    		http.getInputStream()));
				        String decodedString=new String();
				        for (String line; (line = inB.readLine()) != null; decodedString += line);
				        inB.close();
				    

				    /****/
			        //System.out.println("decodedStringProperties: "+decodedString);
//				        ObjectMapper mapper = new ObjectMapper();
//							
//							Object json = mapper.readValue(decodedString, Object.class);
//							String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
	//
//							System.out.println("ReturnedJSON: "+indented);//This p

			        postMultiple=json2Class(decodedString);	        
			        //postMultiple.printPostMultipleParameters();
			       		   	
		        client.close();
		        
		        return postMultiple;

			}	
			
	   
	   private static PostMultipleParameters json2Class(String jsonString) {
			ObjectMapper mapper = new ObjectMapper();

			PostMultipleParameters jsonPostMultipleParameters = new PostMultipleParameters();
			
			try {
			
				jsonPostMultipleParameters = mapper.readValue(jsonString, PostMultipleParameters.class);
				

			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return jsonPostMultipleParameters;
		}
								
}
