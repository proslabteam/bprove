package unicam.project.WebServiceMaude.serviceV1_2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import plugin.bpmn.to.maude.handlers.PostMultipleParameters;
import plugin.bpmn.to.maude.notation.Collaboration;
import plugin.bpmn.to.maude.notation.Pool;
import plugin.bpmn.to.maude.notation.Task;
import plugin.bpmn.to.maude.notation.SendTask;
import plugin.bpmn.to.maude.notation.ReceiveTask;
import plugin.bpmn.to.maude.notation.Proc;
import unicam.project.WebServiceMaudeV1_2.XMLUtils;


public class BProVe_Maude_WebService {
	
	private static NodeList PoolNodeList;
	private static Integer z;
	private static Boolean notEmpty;
	private static String Spool;
	private static String messageflowEntry;
	private static String messageflowExit;
	private static Map<String, String>  messageflowmessage;
	private static Map<String, String> messageDict;
	private static String PoolMessageIn;
	private static String PoolMessageOut;
	private static int CountTotale = 0;
	private static int Countpoolev = 0;
	private static int Countstart= 0;
	private static int Countmsgstart= 0;
	private static int Countend= 0;
	private static int Countmsgend= 0;
	private static int Counttask= 0;
	private static int Countsendtask= 0;
	private static int Countreceivetask= 0;
	private static int Countterminateend = 0;
	private static int Countintermediatethrow = 0;
	private static int Countintermediatecatch = 0;
	private static int Countxorsplit = 0;
	private static int Countxorjoin = 0;
	private static int Countandsplit = 0;
	private static int Countandjoin = 0;
	private static int Countorsplit = 0;
	private static int Counteventbased = 0;

	private static Map<String, String> messageflowuscita;
	private static Map<String, String> messageflowentrata;

	private static String eventInterRcv;
	private static ArrayList <String> eventInterRcvList;

	private static ArrayList <String> labels;
	
	private long maxTimer=600000;
	
	//LOCAL
	private String path = "/Users/user/BPMNOS/maude.darwin64";
	//PROS-MACHINE
	//private String path = "/home/pros/MaudeBprove/maude.linux64";

	public PostMultipleParameters getMaudeResult(String property, PostMultipleParameters postMultiple){
		
		//property=property.replace("&&", "/"+"\\");		
//		////System.out.println("\nProperty"+property);		
		BufferedWriter pbr =null;
		BufferedReader br = null;
		OutputStream os = null;
		OutputStreamWriter osw = null;
		InputStreamReader isr = null;
		InputStream is = null; 
		Process MaudeProcess = null;	
		String result = "";
		String realTimeMaude= null;
		//DEBUG
		//////System.out.println("\nBefore Launching Maude\n");
		
		//Launch Maude	
		try {
			long currentTime = System.currentTimeMillis();
			
			MaudeProcess = new ProcessBuilder(path,"-no-banner","-no-wrap").start();
			//DEBUG////System.out.println("\nStart Maude\n");
			os = (OutputStream) MaudeProcess.getOutputStream();
			osw = new OutputStreamWriter(os);
			pbr = new BufferedWriter(osw);
			pbr.write(" -random-seed="+currentTime+ "\n");
			pbr.flush();
			pbr.write("load BPMNOS_MODEL_CHECKER.maude \n");
			pbr.flush();
			pbr.write("set show command off .\n");
			pbr.flush();
			//DEBUG////System.out.println("\nLoad Files\n");
		
			is = MaudeProcess.getInputStream();
			isr = new InputStreamReader(is);
			br = new BufferedReader(isr);

//DEBUG
//			////System.out.println("\nMaude Launched in getMaudeResult\n");
//			result+= "lancio maude"+MaudeProcess;

		} catch (IOException e) {

			try {
				pbr.close();			
				os.close();
				osw.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			MaudeProcess.destroy();	
			e.printStackTrace();
		}

		try {
			
			maudeTimer myThread = new maudeTimer(maxTimer,MaudeProcess);
			Thread th = new Thread(myThread);
		    th.start();
		    ////System.out.println("\nTimer Launched...");
			
	        //write property in maude
			pbr.write(property+" \n");
			pbr.flush();	
			pbr.write("reduce true and false and true and false . \n");
			pbr.flush();
			//to test only one line
		   	String line = "";
		   	//line += br.readLine();
	    	while (true){				
				line = "";	
				
				try{	//try to read the line, if I cannot read it it probably means the stream is closed and the timer has been exceeded
				line = br.readLine();
				//DEBUG////System.out.println("\nLine Read\n");
				//////System.out.println("\n"+line);
					
				if(line != null){
					if(line.equals("\n")){
						result="result2\n"+line+"\nERROR";
						try {
							pbr.close();			
							br.close();
							os.close();
							osw.close();
							isr.close();
							is.close(); 
							MaudeProcess.destroy();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
			    		//////System.out.println("Result: "+result);
						try{
							myThread.setStop(true);
						}catch(Exception e){
							e.printStackTrace();
							postMultiple.setResult(result);
							return postMultiple;
						}
						
						postMultiple.setResult(result);
						return postMultiple;
			    	}
					if(line.contains("Warning")){
			    		result="result3\n"+line+"\nERROR";
			    		//////System.out.println("Result: "+result);
			    		try {
							pbr.close();			
							br.close();
							os.close();
							osw.close();
							isr.close();
							is.close(); 
							MaudeProcess.destroy();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						try{
							myThread.setStop(true);
						}catch(Exception e){
							e.printStackTrace();
							postMultiple.setResult(result);
							return postMultiple;
						}
						postMultiple.setResult(result);
						return postMultiple;
			    	}
					//extract time from maude result
					if(line.contains("ms real")){
						realTimeMaude = extractRealTime(line);
						//////System.out.println("realTimeMaude: "+realTimeMaude);
						postMultiple.setPropertyVerificationTime(realTimeMaude);
						
					}
					if(line.contains("result")){											
							line += "\n";
							result +=line;					
							while(true)
							{
								String line2 = br.readLine(); 
								if(line2.contains("Warning")){
						    		result="result4\n"+line2+"\nERROR";
						    		////System.out.println("Result: "+result);
						    		try {
										pbr.close();			
										br.close();
										os.close();
										osw.close();
										isr.close();
										is.close(); 
										MaudeProcess.destroy();
									} catch (IOException e1) {
										e1.printStackTrace();
									}
									try{
										myThread.setStop(true);
									}catch(Exception e){
										e.printStackTrace();
										postMultiple.setResult(result);
										return postMultiple;
									}
									postMultiple.setResult(result);
									return postMultiple;
						    	}
								if(line2.equals("reduce in CHECK-M-PREDS-BPMNOS : true and false and true and false .")||line2.contains("result Bool: false"))
								{
									////System.out.println("Break: ");
									break;
									}
								else
								{
									line2 += " \n";
									result +=line2;
								}
							}//chiusura while	
							try {
								pbr.close();			
								br.close();
								os.close();
								osw.close();
								isr.close();
								is.close(); 
								MaudeProcess.destroy();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							try{
								myThread.setStop(true);
							}catch(Exception e){
								e.printStackTrace();
								postMultiple.setResult(result);
								return postMultiple;
							}
							if(result.contains("counterexample")){
								postMultiple.setCounterexample(result);	
								postMultiple.setResult("False");	
							}else{
								postMultiple.setResult(result);
							}
							return postMultiple;
							
					}//if contains result
				}else{//linea nulla
					result="Error, Maude LTL Model Checker could not handle the model";
					////System.out.println("Result: "+result);
					try {
						pbr.close();			
						br.close();
						os.close();
						osw.close();
						isr.close();
						is.close(); 
						MaudeProcess.destroy();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					try{
						myThread.setStop(true);
					}catch(Exception e){
						e.printStackTrace();
						postMultiple.setResult(result);
						return postMultiple;
					}
					postMultiple.setResult(result);
					return postMultiple;
				}
			 }catch(Exception e){
				 
				 e.printStackTrace();
				 //return "ERROR";
				 result="TimerMaude of "+maxTimer+" exceeded for processing the verification\n"; 	
				 postMultiple.setPropertyVerificationTime(String.valueOf(maxTimer));
					////System.out.println("\nresult=TimerMaude of exceeded for processing the verification\n");
					try {
						pbr.close();			
						br.close();
						os.close();
						osw.close();
						isr.close();
						is.close(); 
						MaudeProcess.destroy();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					postMultiple.setResult(result);
					return postMultiple;		 
			 }
	    	}//chiusura while true
	    	
			} catch (IOException e) {

				result="TimerMaude of "+maxTimer+" exceeded for processing the verification\n"; 	
				postMultiple.setPropertyVerificationTime(String.valueOf(maxTimer));
				////System.out.println("\nresult=TimerMaude of exceeded for processing the verification\n");
				try {
					pbr.close();			
					br.close();
					os.close();
					osw.close();
					isr.close();
					is.close(); 
					MaudeProcess.destroy();
				} catch (IOException e1) {

					e1.printStackTrace();
				}
				e.printStackTrace();

				postMultiple.setResult(result);
				return postMultiple;
			}
}
	
	@SuppressWarnings("unused")
	public PostMultipleParameters getParsedModel (PostMultipleParameters inputM) throws Exception{
		
		inputM.printPostMultipleParameters();
		
		String inputStringModel=inputM.getOriginalModel();

		DocumentBuilder db = DocumentBuilderFactory.newInstance().newDocumentBuilder();
		InputSource is = new InputSource();
		is.setCharacterStream(new StringReader(inputStringModel));

		Document inputModel = db.parse(is);
		
		////System.out.println("\ngetParsedModel\n");

		//START PARSING TIME COUNTER
		long currentTimeSart = System.currentTimeMillis(); 
		
		String outputModel = "RETURNED MODEL";
		String testResult="";
		
		  try {
				testResult = evaluateModel(inputModel);
	    	  } catch (Exception e1) {
				e1.printStackTrace();
	    	  }

		  //////System.out.println("\ntestResult: "+testResult);
		  
		  //if testResult == ""; parse the model
		  if(testResult == ""){
			  	//Model Parsing below
				labels = new ArrayList<String>();
				
		    	eventInterRcvList = new ArrayList <String>();
		    	
		    	CountTotale = 0;
		    	Countpoolev = 0;
		    	Countstart= 0;
		    	Countmsgstart= 0;
		    	Countend= 0;
		    	Countmsgend= 0;
		    	Counttask= 0;
		    	Countsendtask= 0;
		    	Countreceivetask= 0;
		    	Countterminateend = 0;
		    	Countintermediatethrow = 0;
		    	Countintermediatecatch = 0;
		    	Countxorsplit = 0;
		    	Countxorjoin = 0;
		    	Countandsplit = 0;
		    	Countandjoin = 0;
		    	Countorsplit = 0;
		    	Counteventbased = 0;

		        //setting for querying the bpmn file
		        String startQuery = "./*[local-name()='startEvent']";
		        String endQuery = "./*[local-name()='endEvent']";
		        String anyTaskQuery = "./*[local-name()='task']";
			    String manualTaskQuery = "./*[local-name()='manualTask']";
			    String serviceTaskQuery = "./*[local-name()='serviceTask']";
			    String businessRuleTaskQuery = "./*[local-name()='businessRuleTask']";
			    String scriptTaskQuery = "./*[local-name()='scriptTask']";
			    String userTaskQuery = "./*[local-name()='userTask']";
		        String sequenceFlowQuery = "//*[local-name()='sequenceFlow']";
		        String messageFlowQuery = "//*[local-name()='messageFlow']";
		        String partecipantQuery = "//*[local-name()='participant']";
		        String sequenceFlowExcludeFromToElemList = "participant"; //Se ci sono sequence flow da/verso una pool allora non li considero
		        String messageFlowExcludeFromToElemList = "participant"; //Se ci sono messaggi da/verso una pool allora non li considero in quanto

		        String modelId = inputModel.getDocumentElement().getAttribute("id");
		        String Modelpath = inputModel.getDocumentURI();

		        ArrayList<String> listPool = new ArrayList<String>();
		      //Vengono analizzati tutti i nodi, e si ricercheranno tutti gli start, tutti i task e tutti gli
		      // end con i relativi sequence flow in entrata e in uscita

		       //creo un dizionario con tutti i messageflow in uscita e in entrara
		        messageflowuscita = new HashMap<String, String>();
		       messageflowentrata = new HashMap<String, String>();
		        messageflowmessage = new HashMap<String, String>();

		        NodeList messageFlowNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);
		        notEmpty = false;
		        for(int i=0;i<messageFlowNodeList.getLength();i++){
		        	String name = "";
		        	 String id = messageFlowNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
		             String sourceRef = messageFlowNodeList.item(i).getAttributes().getNamedItem("sourceRef").getNodeValue();
		             String targetRef = messageFlowNodeList.item(i).getAttributes().getNamedItem("targetRef").getNodeValue();
		             String messageRef = "";
		             if (messageFlowNodeList.item(i).getAttributes().getNamedItem("messageRef") != null)
		             {
		            	 messageRef = messageFlowNodeList.item(i).getAttributes().getNamedItem("messageRef").getNodeValue();
		             }
		             /*CREO 3 DIZIONARI (MITTENTI/DESTINATARI/MESSAGGI)
		              *LI RICHIAMERO' PIU' TARDI, PASSANDOGLI UN QUALSIASI ID
		              *SE PRESENTE MI RITORNERA' UN RISULTATO
		             */
		             messageflowuscita.put(sourceRef, id);
		            messageflowentrata.put(targetRef, id);
		            messageflowmessage.put(id, messageRef);
		        }

		    //CREO LA STESSA STRUTTURA PER DETERMINARE I NOMI DEI MESSAGE
		        messageDict = new HashMap<String, String>();
		        NodeList messageNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), "//*[local-name()='message']", XPathConstants.NODESET);
		        for(int i=0;i<messageNodeList.getLength();i++){
		        	String name = "";
		        	 String idmessage = messageNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
		        	 if (messageNodeList.item(i).getAttributes().getNamedItem("name")!=null){
		        		 name = messageNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
		        		 name=name.replaceAll("\\n", "");
						 //Control on labels
						 if(messageDict.containsValue(name)){
							 ////System.out.println("\nPlease Correct Duplicated Message Labels: "+name+"\n");
							 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");  		  
					         //return "Please Correct Duplicated Labels: "+name+"\n";
							  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
							  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
				    		  //dialog that tells which elements are present that cannot be translated
				    		  return returnedMultipleParameters;
						 }
						
		        		 messageDict.put(idmessage, name);
		        	 }else{
		        		 messageDict.put(idmessage, name);
		        	 }
		        }

		         //CREO LA STESSA STRUTTURA PER I MESSAGEFLOW MA PER I PARTECIPANT
		        Map<String, String> partecipantDict = new HashMap<String, String>();
		        NodeList partecipantNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), partecipantQuery, XPathConstants.NODESET);
		       ////System.out.println("NUMEROPOOL partecipantNodeList.getLength(): "+partecipantNodeList.getLength());
		        for(int i=0;i<partecipantNodeList.getLength();i++){
		        	String name = "";

		        	 String idprocess = partecipantNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
		             String processRef = "emptyprocess";
		             if (partecipantNodeList.item(i).getAttributes().getNamedItem("processRef") != null)
		             {
		            	 processRef = partecipantNodeList.item(i).getAttributes().getNamedItem("processRef").getNodeValue();
		             }
		            partecipantDict.put(processRef, idprocess);
		        }
		        
		        //INIZIO AD ANALIZZARE TUTTI GLI ELEMENTI
		        Spool = "";
		        String Scollaboration = "collaboration( {noAction}";
		        //SI ANDRA' A VERIFICARE PER GLI ID INVECE DEI NOMI, PIU' UNIVOCI
		        //ANALIZZO IL PRIMO NODO (POOL) PER OGNI POOL ANALIZZO TUTTI I SUOI ELEMENTI
		        //PoolNodeLista prima variabile globale
		        PoolNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(),"//*[local-name()='process']" , XPathConstants.NODESET);
		        //scorro i processi così, devo invece scorrere le pool
		        //int countPool = PoolNodeList.getLength();
		        int countPool = partecipantNodeList.getLength();
		        int countProc = PoolNodeList.getLength();
		        
		        int maxCounter=countPool;
		        if(countPool<countProc)maxCounter=countProc;
		        ////System.out.println("COUNTPOOL: "+countPool);
//scorro processi		        
		        for(z=0;z<maxCounter;z++){
		            notEmpty = false;
		        	String namepool = "";

		        	//proviamo a leggere il nome del processo ma se non c'è il processo nella pool allora gli do quello vuoto 
		        	
		        	try{//try per vedere se c'è il processo se non c'è metto quello vuoto vuol dire che c'è la pool vuota
		        		
		        	////System.out.println("POOOOOL z: "+z);
		        	//////System.out.println("POOOOOL PoolNodeList.item(z).getAttributes().toString(): "+PoolNodeList.item(z).getAttributes().toString());
		        	String idpool = PoolNodeList.item(z).getAttributes().getNamedItem("id").getNodeValue();
		        	////System.out.println("\nidpool POOL: "+idpool);
//		        	////System.out.println("\nname POOL: "+PoolNodeList.item(z).getAttributes().getNamedItem("name").getNodeValue());
		        	
		        	if(PoolNodeList.item(z).getAttributes().getNamedItem("name") == null)
		        	 {
		        		//c'è la pool non c'è il nome del processo ma c'è il processo quindi ne do uno di default
		        		 ////System.out.println("\nPOOL con PROCESSO VUOTO: ");
		                 Random randomGenerator = new Random();
		                 int randomInt = randomGenerator.nextInt(1000);
		          		 namepool = "DefaultProcess"+randomInt;
		        	 }
		          	 else
		          		 {
		          		 namepool = PoolNodeList.item(z).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
		          		namepool=namepool.replaceAll("\\n", "");
		          		 }
				   	     CountTotale += 1;
				   	     Countpoolev+= 1;
		        		listPool.add(namepool);
		          		 String finalstart = "";
		          		Spool = "\npool( "+'"'+namepool+'"'+" , \nproc( {emptyAction}";
		                PoolMessageIn = "in: ";
		                PoolMessageOut = "out: ";
		                String idProcess = partecipantDict.get(idpool);
		                messageflowEntry= messageflowentrata.get(idProcess);
			            messageflowExit= messageflowuscita.get(idProcess);
			            if (messageflowExit!= null)
			            {
			                String messageExit= messageflowmessage.get(messageflowExit);
				            if (messageExit!= null){
				                String messageName= messageDict.get(messageExit);
				                if((messageName!=null)){
				                messageflowExit = messageName;
				                }
				            	PoolMessageOut += '"'+messageflowExit+'"'+" .msg 0 andmsg ";
				            }
				            else
				            {
				            	PoolMessageOut += '"'+messageflowExit+'"'+" .msg 0 andmsg ";
				            }
			            }
			            if (messageflowEntry!= null)
			            {
			                String messageEntry= messageflowmessage.get(messageflowEntry);
				            if (messageEntry!= null){
				                String messageName= messageDict.get(messageEntry);
				                if((messageName!=null)){
				                messageflowEntry = messageName;
				                }
				                PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
				            }
				            else
				            {
			            	PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
				            }
			            }

		            //START EVENT
			        NodeList startEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), startQuery, XPathConstants.NODESET);
			        for(int i=0;i<startEventNodeList.getLength();i++){

			            String id = startEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = "";
			            if (startEventNodeList.item(i).getAttributes().getNamedItem("name") != null)
			            	{
			            	name = startEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            	name=name.replaceAll("\\n", "");
			            	}
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(startEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);
			            notEmpty = true;
			            boolean isMessageStart = false;
			            NodeList StartMessageNodeList = null;
			            //CERCO SE LO START E' UN MESSAGESTART
			            StartMessageNodeList = (NodeList) XMLUtils.execXPath(startEventNodeList.item(i), "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
			            int countmessagestart = StartMessageNodeList.getLength();
			            String StartMessage = "";
				        for(int c=0;c<countmessagestart;c++){
				        	//MODIFICA QUI
				        	if(StartMessageNodeList.item(i).getAttributes().getNamedItem("id") != null){
			            	StartMessage = StartMessageNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            	isMessageStart = true;
				        	}
			            }
			            String elemType = "start";
			            if(isMessageStart)
			                elemType += "Rcv";
			            String outgoing = "";
			            //LO START, PER REGOLA DELLA SINTASSI, PUO' AVERE SOLO SEQUENCEFLOW IN USCITA
			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //VERIFICO QUANTI SEQUENCEFLOW HA IN USCITA LO START
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {

			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                // String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+andsplitname+" , "+ "edges("+  outgoinglist+" )) | ";
				            String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
				            
				            Spool +="\n"+ additionalAndSplit;
			              }

			            //VERIFICO SE LO STARTEVENT HA MESSAGEFLOW IN ENTRATA, USCITA E MESSAGGI
			            //PASSANDO IL SUO ID AI SEGUENTI DIZIONARI
			            messageflowEntry= messageflowentrata.get(id);
			            if (messageflowEntry!= null)
			            {
			            	String messageEntry = null;
			            	messageEntry= messageflowmessage.get(messageflowEntry);
			            	if ((messageEntry!= null)){
				                String messageName= messageDict.get(messageEntry);
				                if((messageName!=null)){
				                messageflowEntry = messageName;
				                }
				                PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
				            }
				            else
				            {
			            	PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
				            }
			            }
			            messageflowExit= messageflowuscita.get(id);
			        	String message = messageflowmessage.get(id);
			        	//CREO LA SINTASSI PER MAUDE
			            String start = " ";

			            if(isMessageStart){
			            	CountTotale += 1;
			            	Countmsgstart += 1;
			            	//start = elemType+"(1 , "+'"'+ name+'"' +" , "+'"'+messageflowEntry+'"'+" .msg 0 , "+'"' + outgoing+'"'  +" . 0 ) | ";
			            	start = elemType+"(enabled , "+'"' + outgoing+'"'  +" . 0 , "+'"'+messageflowEntry+'"'+" .msg 0) | ";
			            	finalstart += start;
			            }
			            else
			            {
			            	CountTotale += 1;
			            	Countstart += 1;
			            	//start = elemType+"( 1 , "+'"'+ name+'"' +" , "+'"' + outgoing+'"'  +" . 0 ) | ";
			            	start = elemType+"( enabled , "+'"' +  outgoing+'"'  +" . 0 ) | ";
			            	finalstart += start;
			            }
			            if(finalstart.isEmpty()){
				              testResult="element linked to the startEvent may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ finalstart;
			            //SINTASSI START CORRETTA
			        }





			        //END EVENT
			        NodeList endEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), endQuery, XPathConstants.NODESET);
			        for(int i=0;i<endEventNodeList.getLength();i++){

			            String id = endEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = "";
			            if (endEventNodeList.item(i).getAttributes().getNamedItem("name") != null)
			            {
			            name = endEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            }
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(endEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);
			            notEmpty = true;
			            boolean isMessageEnd = false;
			            NodeList EndMessageNodeList = null;
			            EndMessageNodeList = (NodeList) XMLUtils.execXPath(endEventNodeList.item(i), "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
			            int countmessageEnd = EndMessageNodeList.getLength();
			            String EndMessage = "";
				        for(int c=0;c<countmessageEnd;c++)
			            {
			               	EndMessage = EndMessageNodeList.item(c).getAttributes().getNamedItem("id").getNodeValue();
			            	isMessageEnd = true;
			            }
			            boolean isTerminatedEnd = false;
			            NodeList EndTerminateNodeList = null;
			            EndTerminateNodeList = (NodeList) XMLUtils.execXPath(endEventNodeList.item(i), "./*[local-name()='terminateEventDefinition']", XPathConstants.NODESET);

			            int countterminateEnd = EndTerminateNodeList.getLength();
			            String EndTerminate = "";
				        for(int c=0;c<countterminateEnd;c++)
			            {
			               	EndTerminate= EndTerminateNodeList.item(c).getAttributes().getNamedItem("id").getNodeValue();
			               	isTerminatedEnd = true;
			            }
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = '"'+"incoming" + randomInt+'"' ;

			            }else
			            if (lenghtincoming == 1)
			            	//SE NON MULTI INCOMING
			            	incoming =   '"'+incomingNodeList.item(0).getTextContent()+'"';
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = '"'+"incoming" + randomInt+'"' ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	//CORREGGERE XORJOIN
			            	//String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	//String xorjoinname = '"'+"xorJoin"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 //String additionalXorSplit = "xorSplit"+"("+ "edges("+  incominglist+" )"+", "+xorsplitname+" , "+ incoming+" . 0  ) | ";
				             String additionalXorJoin = "xorJoin"+"("+ "edges("+  incominglist+" )"+", "+ incoming+" . 0  ) | ";
			                 Spool +="\n"+ additionalXorJoin;

			            }
			            messageflowEntry= messageflowentrata.get(id);
			            messageflowExit= messageflowuscita.get(id);
			            if (messageflowExit!= null)
			            {

			            	String messageExit= messageflowmessage.get(messageflowExit);
			            	if (messageExit!= null){
				                String messageName= messageDict.get(messageExit);
				                if((messageName!=null)){
				                messageflowExit =messageName;
				                }
				            	PoolMessageOut += '"'+messageflowExit+'"'+" .msg 0 andmsg ";
				            }
				            else
				            {
			            	PoolMessageOut += '"'+messageflowExit+'"'+" .msg 0 andmsg ";
				            }
			            }
			        	String message = messageflowmessage.get(id);

			            String elemType = "end";
			            String finalend = "";

			            String end = " ";
			            if(isMessageEnd){
			                elemType += "Snd";
			            	CountTotale += 1;
			            	Countmsgend += 1;
			            	//end = elemType+"( "+incoming+" . 0 , "+'"'+name+'"'+" , "+'"'+messageflowExit+'"'+" .msg 0 , unset , 0"+") | ";
			            	end = elemType+"( disabled , "+incoming+" . 0 , "+'"'+messageflowExit+'"'+" .msg 0 ) | ";
			            	finalend += end;
			            }
			            else
			            {


			            if(isTerminatedEnd){
			                elemType = "terminate";
			            	CountTotale += 1;
			            	Countterminateend += 1;
			            	end = elemType+"( disabled , "+incoming+" . 0 ) | ";
			            	finalend += end;
			            }
			            else
			            {
			            	CountTotale += 1;
			            	Countend += 1;
			            	end = elemType+"( disabled , "+incoming+" . 0 ) | ";
			            	finalend += end;
			            }
			            }
			            if(finalend.isEmpty()){
				              testResult="element linked to the endEvent may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool += "\n"+finalend;
			           // SINTASSI END OK
			        }


			        //controllo nodo TASK
			        NodeList taskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), anyTaskQuery, XPathConstants.NODESET);
			        for(int i=0;i<taskEventNodeList.getLength();i++){

			            String id = taskEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = taskEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            ////System.out.println("questooo2: "+name);
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(taskEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));
			            notEmpty = true;
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	//String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                // String additionalXorSplit = "xorSplit"+ "("+ "edges("+  incominglist+" )"+", "+'"'+ incoming+'"'+" . 0  ) | ";
			               //  String additionalXorSplit = "xorSplit"+ "("+'"'+ incoming+'"'+" . 0  , "+"edges("+  incominglist+" )) | ";
				            String additionalXorJoin = "xorJoin"+ "("+"edges("+  incominglist+" ),"+ '"'+ incoming+'"'+" . 0  ) | ";
			                  
				            Spool +="\n"+ additionalXorJoin;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(taskEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String outgoing = "";

			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }

			            //tutto uguale sopra
			          //DEVO CONTROLLARE SE IL TASK E' REFERENZIATO DA QUALCHE MESSAGEFLOW, SE SI NON è UN SEMPLICE TASK MA UN SEND O RECEIVE
			            //check message
			            NodeList messageNodeListAppoggio = (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);
			            //////System.out.println("QUESTOOOOOO messageNodeList.getLength(): "+messageNodeListAppoggio.getLength());
			            boolean isSndTask = false;
			            boolean isRcvTask = false;
			            for(int im=0;im<messageNodeListAppoggio.getLength();im++){
		
				        	 //String idmessage = messageNodeList.item(im).getAttributes().getNamedItem("id").getNodeValue();
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(sourceRef): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(TARGETREF): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO id: "+id);
				        	 
			            	
			            	//if ((messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue()!=null)&&(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue()!=null)){
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue().equals(id)){
				        			 //////System.out.println("QUESTO E' UN SEND TASK MODIFICARE DI CONSEGUENZA");
				        			 isSndTask=true;
				        			 
				        		 }
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue().equals(id)){
				        			// ////System.out.println("QUESTO E' UN RECEIVE TASK MODIFICARE DI CONSEGUENZA");
				        			 isRcvTask=true;
				        		 }
				        		 
				        }//check message
			            
			            if(isSndTask){
			            	//va gestito come sndtask
			            	//////System.out.println("\nva gestito come sndtask: ");
			            	String messageflow = messageflowuscita.get(id);

				            if (messageflow!= null)
				            {
				            	//////System.out.println("\nmessageflow!= null: ");
				            	String messageExit= messageflowmessage.get(messageflow);
				            	if (messageExit!= null){
				            		//////System.out.println("\nmessageExit!= null: ");
					                String messageName= messageDict.get(messageExit);
					                if((messageName!=null)){
					                messageflow =messageName;
					                }
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
					            else
					            {
					            	//////System.out.println("\nmessageExit== null: ");
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
				            }
				            
				            //////System.out.println("\nDOPO messageflow!= null: ");
			            	String elemType = "taskSnd";
				            CountTotale +=1;
				            Countsendtask +=1;

						     if (!labels.contains(name))labels.add(name);
						     else { 
						    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
						    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
						         //return "Please Correct Duplicated Labels: "+name+"\n";
						    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
								  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
					    		  //dialog that tells which elements are present that cannot be translated
					    		  return returnedMultipleParameters;
						     }

						    
				            String sendtask = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 , " + '"'+name+'"'+") | " ;
				            if(sendtask.isEmpty()){
					              testResult="element linked to the sendtask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
					              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
					   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
					       		  //dialog that tells which elements are present that cannot be translated
					       		  return returnedMultipleParameters;
					            }
				            Spool +="\n"+ sendtask;
			            	
			            	
			            	isSndTask=false;
			            }else{
			            
				            if(isRcvTask){
				            	//////System.out.println("va gestito come rcvtask");
				            	   String messageflow = messageflowentrata.get(id);
						        	String message = messageflowmessage.get(id);
				            	//va gestito come rcvtask
					            String elemType = "taskRcv";
					            if (messageflow!= null)
					            {
					            	String messageEntry= messageflowmessage.get(messageflow);
					            	if (messageEntry!= null){
						                String messageName= messageDict.get(messageEntry);
						                if((messageName!=null)){
						                messageflow =messageName;
						                }
						                PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
						            else
						            {
					            	PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
					            }
					            CountTotale +=1;
					            Countreceivetask +=1;

							     if (!labels.contains(name))labels.add(name);
							     else { 
							    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
							    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
							         //return "Please Correct Duplicated Labels: "+name+"\n";
							    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
									  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
						    		  //dialog that tells which elements are present that cannot be translated
						    		  return returnedMultipleParameters;
							     }

							    
					            String receivetask = elemType+"( disabled ,"+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 ,"+'"'+name+'"'+") | " ;
					            if(receivetask.isEmpty()){
						              testResult="element linked to the receivetask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
						              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
						   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
						       		  //dialog that tells which elements are present that cannot be translated
						       		  return returnedMultipleParameters;
						            }
					            Spool += "\n"+receivetask;
				            	
				            	isRcvTask=false;
				            }else{
			            
			            
			            
			            String elemType = "task";
			            CountTotale += 1;
			            Counttask += 1;

					     if (!labels.contains(name))labels.add(name);
					     else { 
					    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
					    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
					    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
							  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
				    		  //dialog that tells which elements are present that cannot be translated
				    		  return returnedMultipleParameters;
					     }

					    
			            String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name+'"'+ ") | " ;
			            //ATTENTION!!! .replaceAll("\\W", "") andrebbe fatto ovunque ma poi va modificata la gestione del controesempio perchè avremo nomi differenti...
			            //String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name.replaceAll("\\W", "")+'"'+ ") | " ;
			            if(task.isEmpty()){
				              testResult="element linked to the task may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ task;
			            // SINTASSI TASK OK
				            }
				        }
			        }

			     //RICERCO TUTTI I SEND TASK
			        NodeList sendtaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), "./*[local-name()='sendTask']", XPathConstants.NODESET);
			        for(int i=0;i<sendtaskEventNodeList.getLength();i++){

			            String id = sendtaskEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = sendtaskEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            ////System.out.println("questooo: "+name);
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(sendtaskEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));
			            notEmpty = true;
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

				            String additionalXorJoin = "xorJoin"+ "("+"edges("+  incominglist+" ),"+ '"'+ incoming+'"'+" . 0  ) | ";			                  
				            Spool +="\n"+ additionalXorJoin;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(sendtaskEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String outgoing = "";



			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //////System.out.println("OUTGOING TASKSND"+lenghtoutgoing);
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dargli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";

				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }
			            //ALGORITMO PER ASSEGNARE I MESSAGE FLOW IN USCITA AL SENDTASK
			        	String messageflow = messageflowuscita.get(id);

			            if (messageflow!= null)
			            {
			            	String messageExit= messageflowmessage.get(messageflow);
			            	if (messageExit!= null){
				                String messageName= messageDict.get(messageExit);
				                if((messageName!=null)){
				                messageflow =messageName;
				                }
				            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
				            }
				            else
				            {
			            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
				            }
			            }
			            String elemType = "taskSnd";
			            CountTotale +=1;
			            Countsendtask +=1;

					     if (!labels.contains(name))labels.add(name);
					     else { 
					    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
					    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
					         //return "Please Correct Duplicated Labels: "+name+"\n";
					    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
							  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
				    		  //dialog that tells which elements are present that cannot be translated
				    		  return returnedMultipleParameters;
					     }

					    
			            String sendtask = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 , " + '"'+name+'"'+") | " ;
			            if(sendtask.isEmpty()){
				              testResult="element linked to the sendtask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ sendtask;
			            //SINTASSI OK

			        }

			        

			        //RICERCO TUTTI I RECEIVE TASK
			        NodeList receivetaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), "./*[local-name()='receiveTask']", XPathConstants.NODESET);
			        for(int i=0;i<receivetaskEventNodeList.getLength();i++){

			            String id = receivetaskEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = receivetaskEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            ////System.out.println("questooo: "+name);
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(receivetaskEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String incoming = "";
			            notEmpty = true;
			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

				            String additionalXorJoin = "xorJoin"+ "("+"edges("+  incominglist+" ),"+ '"'+ incoming+'"'+" . 0  ) | ";
			                  
				            Spool +="\n"+ additionalXorJoin;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(receivetaskEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String outgoing = "";



			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }
			            String messageflow = messageflowentrata.get(id);
			        	String message = messageflowmessage.get(id);
			            String elemType = "taskRcv";
			            if (messageflow!= null)
			            {
			            	String messageEntry= messageflowmessage.get(messageflow);
			            	if (messageEntry!= null){
				                String messageName= messageDict.get(messageEntry);
				                if((messageName!=null)){
				                messageflow =messageName;
				                }
				                PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
				            }
				            else
				            {
			            	PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
				            }
			            }
			            CountTotale +=1;
			            Countreceivetask +=1;

					     if (!labels.contains(name))labels.add(name);
					     else { 
					    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
					    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
					         //return "Please Correct Duplicated Labels: "+name+"\n";
					    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
							  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
				    		  //dialog that tells which elements are present that cannot be translated
				    		  return returnedMultipleParameters;
					     }

					    
			            String receivetask = elemType+"( disabled ,"+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 ,"+'"'+name+'"'+") | " ;
			            if(receivetask.isEmpty()){
				              testResult="element linked to the receivetask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool += "\n"+receivetask;
			            //SINTASSI OK


			        }
			        //TROVO TUTTI GLI intermediateThrowEvent
			        NodeList intermediateThrowEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), "./*[local-name()='intermediateThrowEvent']", XPathConstants.NODESET);
			        for(int i=0;i<intermediateThrowEventNodeList.getLength();i++){

			            String id = intermediateThrowEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = intermediateThrowEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(intermediateThrowEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			            notEmpty = true;
			            boolean isMessage = false;
			            NodeList intermediateThrowEventMessageNodeList = null;
			            intermediateThrowEventMessageNodeList = (NodeList) XMLUtils.execXPath(intermediateThrowEventNodeList.item(i), "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
			            int countmessage = intermediateThrowEventMessageNodeList.getLength();
			            String intermediateThrowEventMessage = "";
				        for(int c=0;c<countmessage;c++){
			            	intermediateThrowEventMessage = intermediateThrowEventMessageNodeList.item(c).getAttributes().getNamedItem("id").getNodeValue();
			            	isMessage = true;
			            }
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String xorsplitname = '"'+"xorsplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalXorSplit = "xorSplit"+"("+ "edges("+  incominglist+" )"+", "+'"'+ incoming+'"'+" . 0  ) | ";
			                 Spool +="\n"+ additionalXorSplit;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(intermediateThrowEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String outgoing = "";



			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }
			            String messageflowentry = messageflowentrata.get(id);
			            String messageflowexit = messageflowuscita.get(id);
			        	String message = messageflowmessage.get(id);
			            if (messageflowexit!= null)
			            {
			            	String messageOut= messageflowmessage.get(messageflowexit);
			            	if (messageOut!= null){
				                String messageName= messageDict.get(messageOut);
				                if((messageName!=null)){
				                messageflowexit =messageName;
				                }
				            	PoolMessageOut += '"'+messageflowexit+'"'+" .msg 0 andmsg ";
				            }
				            else
				            {
			            	PoolMessageOut += '"'+messageflowexit+'"'+" .msg 0 andmsg ";
				            }
			            }
			            String elemType = "interSnd";
			            CountTotale +=1;
			            Countintermediatethrow += 1;
			            String interSnd = elemType+"( disabled ,"+'"'+incoming+'"'+" . 0 , "+'"'+outgoing+'"'+" . 0 , " +'"'+messageflowexit+'"'+" .msg 0 ) | " ;
			            if(interSnd.isEmpty()){
				              testResult="element linked to the Intermediate Send Event may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ interSnd;
			            //SINTASSI OK

			        }
		        
//RICERCO TUTTI GLI ALTRI TIPI DI TASK (MANUAL, USER, SERVICE, SCRIPT, BUSINESS)

//controllo nodo MANUAL TASK
			        NodeList manualtaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), manualTaskQuery, XPathConstants.NODESET);
			        for(int i=0;i<manualtaskEventNodeList.getLength();i++){

			            String id = manualtaskEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = manualtaskEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            ////System.out.println("questooo2: "+name);
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(manualtaskEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));
			            notEmpty = true;
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	//String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                // String additionalXorSplit = "xorSplit"+ "("+ "edges("+  incominglist+" )"+", "+'"'+ incoming+'"'+" . 0  ) | ";
			               //  String additionalXorSplit = "xorSplit"+ "("+'"'+ incoming+'"'+" . 0  , "+"edges("+  incominglist+" )) | ";
				            String additionalXorJoin = "xorJoin"+ "("+"edges("+  incominglist+" ),"+ '"'+ incoming+'"'+" . 0  ) | ";
			                  
				            Spool +="\n"+ additionalXorJoin;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(manualtaskEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String outgoing = "";

			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }

			            //tutto uguale sopra
			          //DEVO CONTROLLARE SE IL TASK E' REFERENZIATO DA QUALCHE MESSAGEFLOW, SE SI NON è UN SEMPLICE TASK MA UN SEND O RECEIVE
			            //check message
			            NodeList messageNodeListAppoggio = (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);
			            //////System.out.println("QUESTOOOOOO messageNodeList.getLength(): "+messageNodeListAppoggio.getLength());
			            boolean isSndTask = false;
			            boolean isRcvTask = false;
			            for(int im=0;im<messageNodeListAppoggio.getLength();im++){
		
				        	 //String idmessage = messageNodeList.item(im).getAttributes().getNamedItem("id").getNodeValue();
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(sourceRef): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(TARGETREF): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO id: "+id);
				        	 
			            	
			            	//if ((messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue()!=null)&&(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue()!=null)){
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue().equals(id)){
				        			 //////System.out.println("QUESTO E' UN SEND TASK MODIFICARE DI CONSEGUENZA");
				        			 isSndTask=true;
				        			 
				        		 }
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue().equals(id)){
				        			// ////System.out.println("QUESTO E' UN RECEIVE TASK MODIFICARE DI CONSEGUENZA");
				        			 isRcvTask=true;
				        		 }
				        		 
				        }//check message
			            
			            if(isSndTask){
			            	//va gestito come sndtask
			            	//////System.out.println("\nva gestito come sndtask: ");
			            	String messageflow = messageflowuscita.get(id);

				            if (messageflow!= null)
				            {
				            	//////System.out.println("\nmessageflow!= null: ");
				            	String messageExit= messageflowmessage.get(messageflow);
				            	if (messageExit!= null){
				            		//////System.out.println("\nmessageExit!= null: ");
					                String messageName= messageDict.get(messageExit);
					                if((messageName!=null)){
					                messageflow =messageName;
					                }
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
					            else
					            {
					            	//////System.out.println("\nmessageExit== null: ");
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
				            }
				            
				            //////System.out.println("\nDOPO messageflow!= null: ");
			            	String elemType = "taskSnd";
				            CountTotale +=1;
				            Countsendtask +=1;

						     if (!labels.contains(name))labels.add(name);
						     else { 
						    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
						    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
						         //return "Please Correct Duplicated Labels: "+name+"\n";
						    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
								  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
					    		  //dialog that tells which elements are present that cannot be translated
					    		  return returnedMultipleParameters;
						     }

						    
				            String sendtask = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 , " + '"'+name+'"'+") | " ;
				            if(sendtask.isEmpty()){
					              testResult="element linked to the sendtask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
					              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
					   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
					       		  //dialog that tells which elements are present that cannot be translated
					       		  return returnedMultipleParameters;
					            }
				            Spool +="\n"+ sendtask;
			            	
			            	
			            	isSndTask=false;
			            }else{
			            
				            if(isRcvTask){
				            	//////System.out.println("va gestito come rcvtask");
				            	   String messageflow = messageflowentrata.get(id);
						        	String message = messageflowmessage.get(id);
				            	//va gestito come rcvtask
					            String elemType = "taskRcv";
					            if (messageflow!= null)
					            {
					            	String messageEntry= messageflowmessage.get(messageflow);
					            	if (messageEntry!= null){
						                String messageName= messageDict.get(messageEntry);
						                if((messageName!=null)){
						                messageflow =messageName;
						                }
						                PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
						            else
						            {
					            	PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
					            }
					            CountTotale +=1;
					            Countreceivetask +=1;

							     if (!labels.contains(name))labels.add(name);
							     else { 
							    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
							    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
							         //return "Please Correct Duplicated Labels: "+name+"\n";
							    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
									  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
						    		  //dialog that tells which elements are present that cannot be translated
						    		  return returnedMultipleParameters;
							     }

							    
					            String receivetask = elemType+"( disabled ,"+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 ,"+'"'+name+'"'+") | " ;
					            if(receivetask.isEmpty()){
						              testResult="element linked to the receivetask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
						              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
						   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
						       		  //dialog that tells which elements are present that cannot be translated
						       		  return returnedMultipleParameters;
						            }
					            Spool += "\n"+receivetask;
				            	
				            	isRcvTask=false;
				            }else{
			            
			            
			            
			            String elemType = "task";
			            CountTotale += 1;
			            Counttask += 1;

					     if (!labels.contains(name))labels.add(name);
					     else { 
					    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
					    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
					    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
							  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
				    		  //dialog that tells which elements are present that cannot be translated
				    		  return returnedMultipleParameters;
					     }

					    
			            String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name+'"'+ ") | " ;
			            //ATTENTION!!! .replaceAll("\\W", "") andrebbe fatto ovunque ma poi va modificata la gestione del controesempio perchè avremo nomi differenti...
			            //String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name.replaceAll("\\W", "")+'"'+ ") | " ;
			            if(task.isEmpty()){
				              testResult="element linked to the task may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ task;
			            // SINTASSI TASK OK
				            }
				        }
			        }
			        
			        
//controllo nodo SERVICE TASK
			        NodeList servicetaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), serviceTaskQuery, XPathConstants.NODESET);
			        for(int i=0;i<servicetaskEventNodeList.getLength();i++){

			            String id = servicetaskEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = servicetaskEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            ////System.out.println("questooo2: "+name);
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(servicetaskEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));
			            notEmpty = true;
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	//String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                // String additionalXorSplit = "xorSplit"+ "("+ "edges("+  incominglist+" )"+", "+'"'+ incoming+'"'+" . 0  ) | ";
			               //  String additionalXorSplit = "xorSplit"+ "("+'"'+ incoming+'"'+" . 0  , "+"edges("+  incominglist+" )) | ";
				            String additionalXorJoin = "xorJoin"+ "("+"edges("+  incominglist+" ),"+ '"'+ incoming+'"'+" . 0  ) | ";
			                  
				            Spool +="\n"+ additionalXorJoin;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(servicetaskEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String outgoing = "";

			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }

			            //tutto uguale sopra
			          //DEVO CONTROLLARE SE IL TASK E' REFERENZIATO DA QUALCHE MESSAGEFLOW, SE SI NON è UN SEMPLICE TASK MA UN SEND O RECEIVE
			            //check message
			            NodeList messageNodeListAppoggio = (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);
			            //////System.out.println("QUESTOOOOOO messageNodeList.getLength(): "+messageNodeListAppoggio.getLength());
			            boolean isSndTask = false;
			            boolean isRcvTask = false;
			            for(int im=0;im<messageNodeListAppoggio.getLength();im++){
		
				        	 //String idmessage = messageNodeList.item(im).getAttributes().getNamedItem("id").getNodeValue();
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(sourceRef): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(TARGETREF): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO id: "+id);
				        	 
			            	
			            	//if ((messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue()!=null)&&(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue()!=null)){
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue().equals(id)){
				        			 //////System.out.println("QUESTO E' UN SEND TASK MODIFICARE DI CONSEGUENZA");
				        			 isSndTask=true;
				        			 
				        		 }
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue().equals(id)){
				        			// ////System.out.println("QUESTO E' UN RECEIVE TASK MODIFICARE DI CONSEGUENZA");
				        			 isRcvTask=true;
				        		 }
				        		 
				        }//check message
			            
			            if(isSndTask){
			            	//va gestito come sndtask
			            	//////System.out.println("\nva gestito come sndtask: ");
			            	String messageflow = messageflowuscita.get(id);

				            if (messageflow!= null)
				            {
				            	//////System.out.println("\nmessageflow!= null: ");
				            	String messageExit= messageflowmessage.get(messageflow);
				            	if (messageExit!= null){
				            		//////System.out.println("\nmessageExit!= null: ");
					                String messageName= messageDict.get(messageExit);
					                if((messageName!=null)){
					                messageflow =messageName;
					                }
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
					            else
					            {
					            	//////System.out.println("\nmessageExit== null: ");
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
				            }
				            
				            //////System.out.println("\nDOPO messageflow!= null: ");
			            	String elemType = "taskSnd";
				            CountTotale +=1;
				            Countsendtask +=1;

						     if (!labels.contains(name))labels.add(name);
						     else { 
						    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
						    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
						         //return "Please Correct Duplicated Labels: "+name+"\n";
						    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
								  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
					    		  //dialog that tells which elements are present that cannot be translated
					    		  return returnedMultipleParameters;
						     }

						    
				            String sendtask = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 , " + '"'+name+'"'+") | " ;
				            if(sendtask.isEmpty()){
					              testResult="element linked to the sendtask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
					              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
					   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
					       		  //dialog that tells which elements are present that cannot be translated
					       		  return returnedMultipleParameters;
					            }
				            Spool +="\n"+ sendtask;
			            	
			            	
			            	isSndTask=false;
			            }else{
			            
				            if(isRcvTask){
				            	//////System.out.println("va gestito come rcvtask");
				            	   String messageflow = messageflowentrata.get(id);
						        	String message = messageflowmessage.get(id);
				            	//va gestito come rcvtask
					            String elemType = "taskRcv";
					            if (messageflow!= null)
					            {
					            	String messageEntry= messageflowmessage.get(messageflow);
					            	if (messageEntry!= null){
						                String messageName= messageDict.get(messageEntry);
						                if((messageName!=null)){
						                messageflow =messageName;
						                }
						                PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
						            else
						            {
					            	PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
					            }
					            CountTotale +=1;
					            Countreceivetask +=1;

							     if (!labels.contains(name))labels.add(name);
							     else { 
							    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
							    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
							         //return "Please Correct Duplicated Labels: "+name+"\n";
							    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
									  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
						    		  //dialog that tells which elements are present that cannot be translated
						    		  return returnedMultipleParameters;
							     }

							    
					            String receivetask = elemType+"( disabled ,"+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 ,"+'"'+name+'"'+") | " ;
					            if(receivetask.isEmpty()){
						              testResult="element linked to the receivetask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
						              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
						   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
						       		  //dialog that tells which elements are present that cannot be translated
						       		  return returnedMultipleParameters;
						            }
					            Spool += "\n"+receivetask;
				            	
				            	isRcvTask=false;
				            }else{
			            
			            
			            
			            String elemType = "task";
			            CountTotale += 1;
			            Counttask += 1;

					     if (!labels.contains(name))labels.add(name);
					     else { 
					    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
					    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
					    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
							  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
				    		  //dialog that tells which elements are present that cannot be translated
				    		  return returnedMultipleParameters;
					     }

					    
			            String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name+'"'+ ") | " ;
			            //ATTENTION!!! .replaceAll("\\W", "") andrebbe fatto ovunque ma poi va modificata la gestione del controesempio perchè avremo nomi differenti...
			            //String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name.replaceAll("\\W", "")+'"'+ ") | " ;
			            if(task.isEmpty()){
				              testResult="element linked to the task may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ task;
			            // SINTASSI TASK OK
				            }
				        }
			        }		        
			        
			        
			        
//controllo nodo BUSINESS TASK
			        NodeList businessRuleTaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), businessRuleTaskQuery, XPathConstants.NODESET);
			        for(int i=0;i<businessRuleTaskEventNodeList.getLength();i++){

			            String id = businessRuleTaskEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = businessRuleTaskEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            ////System.out.println("questooo2: "+name);
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(businessRuleTaskEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));
			            notEmpty = true;
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	//String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                // String additionalXorSplit = "xorSplit"+ "("+ "edges("+  incominglist+" )"+", "+'"'+ incoming+'"'+" . 0  ) | ";
			               //  String additionalXorSplit = "xorSplit"+ "("+'"'+ incoming+'"'+" . 0  , "+"edges("+  incominglist+" )) | ";
				            String additionalXorJoin = "xorJoin"+ "("+"edges("+  incominglist+" ),"+ '"'+ incoming+'"'+" . 0  ) | ";
			                  
				            Spool +="\n"+ additionalXorJoin;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(businessRuleTaskEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String outgoing = "";

			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }

			            //tutto uguale sopra
			          //DEVO CONTROLLARE SE IL TASK E' REFERENZIATO DA QUALCHE MESSAGEFLOW, SE SI NON è UN SEMPLICE TASK MA UN SEND O RECEIVE
			            //check message
			            NodeList messageNodeListAppoggio = (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);
			            //////System.out.println("QUESTOOOOOO messageNodeList.getLength(): "+messageNodeListAppoggio.getLength());
			            boolean isSndTask = false;
			            boolean isRcvTask = false;
			            for(int im=0;im<messageNodeListAppoggio.getLength();im++){
		
				        	 //String idmessage = messageNodeList.item(im).getAttributes().getNamedItem("id").getNodeValue();
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(sourceRef): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(TARGETREF): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO id: "+id);
				        	 
			            	
			            	//if ((messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue()!=null)&&(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue()!=null)){
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue().equals(id)){
				        			 //////System.out.println("QUESTO E' UN SEND TASK MODIFICARE DI CONSEGUENZA");
				        			 isSndTask=true;
				        			 
				        		 }
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue().equals(id)){
				        			// ////System.out.println("QUESTO E' UN RECEIVE TASK MODIFICARE DI CONSEGUENZA");
				        			 isRcvTask=true;
				        		 }
				        		 
				        }//check message
			            
			            if(isSndTask){
			            	//va gestito come sndtask
			            	//////System.out.println("\nva gestito come sndtask: ");
			            	String messageflow = messageflowuscita.get(id);

				            if (messageflow!= null)
				            {
				            	//////System.out.println("\nmessageflow!= null: ");
				            	String messageExit= messageflowmessage.get(messageflow);
				            	if (messageExit!= null){
				            		//////System.out.println("\nmessageExit!= null: ");
					                String messageName= messageDict.get(messageExit);
					                if((messageName!=null)){
					                messageflow =messageName;
					                }
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
					            else
					            {
					            	//////System.out.println("\nmessageExit== null: ");
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
				            }
				            
				            //////System.out.println("\nDOPO messageflow!= null: ");
			            	String elemType = "taskSnd";
				            CountTotale +=1;
				            Countsendtask +=1;

						     if (!labels.contains(name))labels.add(name);
						     else { 
						    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
						    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
						         //return "Please Correct Duplicated Labels: "+name+"\n";
						    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
								  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
					    		  //dialog that tells which elements are present that cannot be translated
					    		  return returnedMultipleParameters;
						     }

						    
				            String sendtask = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 , " + '"'+name+'"'+") | " ;
				            if(sendtask.isEmpty()){
					              testResult="element linked to the sendtask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
					              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
					   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
					       		  //dialog that tells which elements are present that cannot be translated
					       		  return returnedMultipleParameters;
					            }
				            Spool +="\n"+ sendtask;
			            	
			            	
			            	isSndTask=false;
			            }else{
			            
				            if(isRcvTask){
				            	//////System.out.println("va gestito come rcvtask");
				            	   String messageflow = messageflowentrata.get(id);
						        	String message = messageflowmessage.get(id);
				            	//va gestito come rcvtask
					            String elemType = "taskRcv";
					            if (messageflow!= null)
					            {
					            	String messageEntry= messageflowmessage.get(messageflow);
					            	if (messageEntry!= null){
						                String messageName= messageDict.get(messageEntry);
						                if((messageName!=null)){
						                messageflow =messageName;
						                }
						                PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
						            else
						            {
					            	PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
					            }
					            CountTotale +=1;
					            Countreceivetask +=1;

							     if (!labels.contains(name))labels.add(name);
							     else { 
							    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
							    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
							         //return "Please Correct Duplicated Labels: "+name+"\n";
							    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
									  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
						    		  //dialog that tells which elements are present that cannot be translated
						    		  return returnedMultipleParameters;
							     }

							    
					            String receivetask = elemType+"( disabled ,"+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 ,"+'"'+name+'"'+") | " ;
					            if(receivetask.isEmpty()){
						              testResult="element linked to the receivetask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
						              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
						   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
						       		  //dialog that tells which elements are present that cannot be translated
						       		  return returnedMultipleParameters;
						            }
					            Spool += "\n"+receivetask;
				            	
				            	isRcvTask=false;
				            }else{
			            
			            
			            
			            String elemType = "task";
			            CountTotale += 1;
			            Counttask += 1;

					     if (!labels.contains(name))labels.add(name);
					     else { 
					    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
					    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
					    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
							  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
				    		  //dialog that tells which elements are present that cannot be translated
				    		  return returnedMultipleParameters;
					     }

					    
			            String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name+'"'+ ") | " ;
			            //ATTENTION!!! .replaceAll("\\W", "") andrebbe fatto ovunque ma poi va modificata la gestione del controesempio perchè avremo nomi differenti...
			            //String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name.replaceAll("\\W", "")+'"'+ ") | " ;
			            if(task.isEmpty()){
				              testResult="element linked to the task may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ task;
			            // SINTASSI TASK OK
				            }
				        }
			        }				        
			        
			        
//controllo nodo SCRIPT TASK
			        NodeList scriptTaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), scriptTaskQuery, XPathConstants.NODESET);
			        for(int i=0;i<scriptTaskEventNodeList.getLength();i++){

			            String id = scriptTaskEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = scriptTaskEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            ////System.out.println("questooo2: "+name);
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(scriptTaskEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));
			            notEmpty = true;
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	//String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                // String additionalXorSplit = "xorSplit"+ "("+ "edges("+  incominglist+" )"+", "+'"'+ incoming+'"'+" . 0  ) | ";
			               //  String additionalXorSplit = "xorSplit"+ "("+'"'+ incoming+'"'+" . 0  , "+"edges("+  incominglist+" )) | ";
				            String additionalXorJoin = "xorJoin"+ "("+"edges("+  incominglist+" ),"+ '"'+ incoming+'"'+" . 0  ) | ";
			                  
				            Spool +="\n"+ additionalXorJoin;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(scriptTaskEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String outgoing = "";

			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }

			            //tutto uguale sopra
			          //DEVO CONTROLLARE SE IL TASK E' REFERENZIATO DA QUALCHE MESSAGEFLOW, SE SI NON è UN SEMPLICE TASK MA UN SEND O RECEIVE
			            //check message
			            NodeList messageNodeListAppoggio = (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);
			            //////System.out.println("QUESTOOOOOO messageNodeList.getLength(): "+messageNodeListAppoggio.getLength());
			            boolean isSndTask = false;
			            boolean isRcvTask = false;
			            for(int im=0;im<messageNodeListAppoggio.getLength();im++){
		
				        	 //String idmessage = messageNodeList.item(im).getAttributes().getNamedItem("id").getNodeValue();
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(sourceRef): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(TARGETREF): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO id: "+id);
				        	 
			            	
			            	//if ((messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue()!=null)&&(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue()!=null)){
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue().equals(id)){
				        			 //////System.out.println("QUESTO E' UN SEND TASK MODIFICARE DI CONSEGUENZA");
				        			 isSndTask=true;
				        			 
				        		 }
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue().equals(id)){
				        			// ////System.out.println("QUESTO E' UN RECEIVE TASK MODIFICARE DI CONSEGUENZA");
				        			 isRcvTask=true;
				        		 }
				        		 
				        }//check message
			            
			            if(isSndTask){
			            	//va gestito come sndtask
			            	//////System.out.println("\nva gestito come sndtask: ");
			            	String messageflow = messageflowuscita.get(id);

				            if (messageflow!= null)
				            {
				            	//////System.out.println("\nmessageflow!= null: ");
				            	String messageExit= messageflowmessage.get(messageflow);
				            	if (messageExit!= null){
				            		//////System.out.println("\nmessageExit!= null: ");
					                String messageName= messageDict.get(messageExit);
					                if((messageName!=null)){
					                messageflow =messageName;
					                }
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
					            else
					            {
					            	//////System.out.println("\nmessageExit== null: ");
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
				            }
				            
				            //////System.out.println("\nDOPO messageflow!= null: ");
			            	String elemType = "taskSnd";
				            CountTotale +=1;
				            Countsendtask +=1;

						     if (!labels.contains(name))labels.add(name);
						     else { 
						    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
						    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
						         //return "Please Correct Duplicated Labels: "+name+"\n";
						    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
								  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
					    		  //dialog that tells which elements are present that cannot be translated
					    		  return returnedMultipleParameters;
						     }

						    
				            String sendtask = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 , " + '"'+name+'"'+") | " ;
				            if(sendtask.isEmpty()){
					              testResult="element linked to the sendtask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
					              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
					   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
					       		  //dialog that tells which elements are present that cannot be translated
					       		  return returnedMultipleParameters;
					            }
				            Spool +="\n"+ sendtask;
			            	
			            	
			            	isSndTask=false;
			            }else{
			            
				            if(isRcvTask){
				            	//////System.out.println("va gestito come rcvtask");
				            	   String messageflow = messageflowentrata.get(id);
						        	String message = messageflowmessage.get(id);
				            	//va gestito come rcvtask
					            String elemType = "taskRcv";
					            if (messageflow!= null)
					            {
					            	String messageEntry= messageflowmessage.get(messageflow);
					            	if (messageEntry!= null){
						                String messageName= messageDict.get(messageEntry);
						                if((messageName!=null)){
						                messageflow =messageName;
						                }
						                PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
						            else
						            {
					            	PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
					            }
					            CountTotale +=1;
					            Countreceivetask +=1;

							     if (!labels.contains(name))labels.add(name);
							     else { 
							    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
							    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
							         //return "Please Correct Duplicated Labels: "+name+"\n";
							    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
									  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
						    		  //dialog that tells which elements are present that cannot be translated
						    		  return returnedMultipleParameters;
							     }

							    
					            String receivetask = elemType+"( disabled ,"+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 ,"+'"'+name+'"'+") | " ;
					            if(receivetask.isEmpty()){
						              testResult="element linked to the receivetask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
						              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
						   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
						       		  //dialog that tells which elements are present that cannot be translated
						       		  return returnedMultipleParameters;
						            }
					            Spool += "\n"+receivetask;
				            	
				            	isRcvTask=false;
				            }else{
			            
			            
			            
			            String elemType = "task";
			            CountTotale += 1;
			            Counttask += 1;

					     if (!labels.contains(name))labels.add(name);
					     else { 
					    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
					    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
					    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
							  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
				    		  //dialog that tells which elements are present that cannot be translated
				    		  return returnedMultipleParameters;
					     }

					    
			            String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name+'"'+ ") | " ;
			            //ATTENTION!!! .replaceAll("\\W", "") andrebbe fatto ovunque ma poi va modificata la gestione del controesempio perchè avremo nomi differenti...
			            //String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name.replaceAll("\\W", "")+'"'+ ") | " ;
			            if(task.isEmpty()){
				              testResult="element linked to the task may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ task;
			            // SINTASSI TASK OK
				            }
				        }
			        }					        
			        
			        
			        
//controllo nodo USER TASK
			        NodeList userTaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), userTaskQuery, XPathConstants.NODESET);
			        for(int i=0;i<userTaskEventNodeList.getLength();i++){

			            String id = userTaskEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = userTaskEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            ////System.out.println("questooo2: "+name);
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(userTaskEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));
			            notEmpty = true;
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	//String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                // String additionalXorSplit = "xorSplit"+ "("+ "edges("+  incominglist+" )"+", "+'"'+ incoming+'"'+" . 0  ) | ";
			               //  String additionalXorSplit = "xorSplit"+ "("+'"'+ incoming+'"'+" . 0  , "+"edges("+  incominglist+" )) | ";
				            String additionalXorJoin = "xorJoin"+ "("+"edges("+  incominglist+" ),"+ '"'+ incoming+'"'+" . 0  ) | ";
			                  
				            Spool +="\n"+ additionalXorJoin;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(userTaskEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

			           // List myList = new ArrayList().add(startEventNodeList.item(i));

			            String outgoing = "";

			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }

			            //tutto uguale sopra
			          //DEVO CONTROLLARE SE IL TASK E' REFERENZIATO DA QUALCHE MESSAGEFLOW, SE SI NON è UN SEMPLICE TASK MA UN SEND O RECEIVE
			            //check message
			            NodeList messageNodeListAppoggio = (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);
			            //////System.out.println("QUESTOOOOOO messageNodeList.getLength(): "+messageNodeListAppoggio.getLength());
			            boolean isSndTask = false;
			            boolean isRcvTask = false;
			            for(int im=0;im<messageNodeListAppoggio.getLength();im++){
		
				        	 //String idmessage = messageNodeList.item(im).getAttributes().getNamedItem("id").getNodeValue();
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(sourceRef): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO messageNodeList.item(im).getAttributes().getNamedItem(TARGETREF): "+messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue());
			            	//////System.out.println("QUESTOOOOOO id: "+id);
				        	 
			            	
			            	//if ((messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue()!=null)&&(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue()!=null)){
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("sourceRef").getNodeValue().equals(id)){
				        			 //////System.out.println("QUESTO E' UN SEND TASK MODIFICARE DI CONSEGUENZA");
				        			 isSndTask=true;
				        			 
				        		 }
				        		 if(messageNodeListAppoggio.item(im).getAttributes().getNamedItem("targetRef").getNodeValue().equals(id)){
				        			// ////System.out.println("QUESTO E' UN RECEIVE TASK MODIFICARE DI CONSEGUENZA");
				        			 isRcvTask=true;
				        		 }
				        		 
				        }//check message
			            
			            if(isSndTask){
			            	//va gestito come sndtask
			            	//////System.out.println("\nva gestito come sndtask: ");
			            	String messageflow = messageflowuscita.get(id);

				            if (messageflow!= null)
				            {
				            	//////System.out.println("\nmessageflow!= null: ");
				            	String messageExit= messageflowmessage.get(messageflow);
				            	if (messageExit!= null){
				            		//////System.out.println("\nmessageExit!= null: ");
					                String messageName= messageDict.get(messageExit);
					                if((messageName!=null)){
					                messageflow =messageName;
					                }
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
					            else
					            {
					            	//////System.out.println("\nmessageExit== null: ");
					            	PoolMessageOut += '"'+messageflow+'"'+" .msg 0 andmsg ";
					            }
				            }
				            
				            //////System.out.println("\nDOPO messageflow!= null: ");
			            	String elemType = "taskSnd";
				            CountTotale +=1;
				            Countsendtask +=1;

						     if (!labels.contains(name))labels.add(name);
						     else { 
						    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
						    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
						         //return "Please Correct Duplicated Labels: "+name+"\n";
						    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
								  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
					    		  //dialog that tells which elements are present that cannot be translated
					    		  return returnedMultipleParameters;
						     }

						    
				            String sendtask = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 , " + '"'+name+'"'+") | " ;
				            if(sendtask.isEmpty()){
					              testResult="element linked to the sendtask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
					              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
					   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
					       		  //dialog that tells which elements are present that cannot be translated
					       		  return returnedMultipleParameters;
					            }
				            Spool +="\n"+ sendtask;
			            	
			            	
			            	isSndTask=false;
			            }else{
			            
				            if(isRcvTask){
				            	//////System.out.println("va gestito come rcvtask");
				            	   String messageflow = messageflowentrata.get(id);
						        	String message = messageflowmessage.get(id);
				            	//va gestito come rcvtask
					            String elemType = "taskRcv";
					            if (messageflow!= null)
					            {
					            	String messageEntry= messageflowmessage.get(messageflow);
					            	if (messageEntry!= null){
						                String messageName= messageDict.get(messageEntry);
						                if((messageName!=null)){
						                messageflow =messageName;
						                }
						                PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
						            else
						            {
					            	PoolMessageIn += '"'+messageflow+'"'+" .msg 0 andmsg ";
						            }
					            }
					            CountTotale +=1;
					            Countreceivetask +=1;

							     if (!labels.contains(name))labels.add(name);
							     else { 
							    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
							    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
							         //return "Please Correct Duplicated Labels: "+name+"\n";
							    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
									  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
						    		  //dialog that tells which elements are present that cannot be translated
						    		  return returnedMultipleParameters;
							     }

							    
					            String receivetask = elemType+"( disabled ,"+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , "+'"'+messageflow+'"'+" .msg 0 ,"+'"'+name+'"'+") | " ;
					            if(receivetask.isEmpty()){
						              testResult="element linked to the receivetask may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
						              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
						   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
						       		  //dialog that tells which elements are present that cannot be translated
						       		  return returnedMultipleParameters;
						            }
					            Spool += "\n"+receivetask;
				            	
				            	isRcvTask=false;
				            }else{
			            
			            
			            
			            String elemType = "task";
			            CountTotale += 1;
			            Counttask += 1;

					     if (!labels.contains(name))labels.add(name);
					     else { 
					    	 ////System.out.println("\nPlease Correct Duplicated Labels: "+name+"\n");
					    	 //JOptionPane.showMessageDialog(parentFrame, "\nPlease Correct Duplicated Message Labels: "+name+"\n");
					    	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
							  returnedMultipleParameters.setParsedModel("Please Correct Duplicated Labels: "+name+"\n");	
				    		  //dialog that tells which elements are present that cannot be translated
				    		  return returnedMultipleParameters;
					     }

					    
			            String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name+'"'+ ") | " ;
			            //ATTENTION!!! .replaceAll("\\W", "") andrebbe fatto ovunque ma poi va modificata la gestione del controesempio perchè avremo nomi differenti...
			            //String task = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"'+name.replaceAll("\\W", "")+'"'+ ") | " ;
			            if(task.isEmpty()){
				              testResult="element linked to the task may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ task;
			            // SINTASSI TASK OK
				            }
				        }
			        }		        

			        //DA MODIFICARE MA NON IMMEDIATO
			        //controllo eventBasedGateway
			        NodeList eventBasedGateway =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), "./*[local-name()='eventBasedGateway']", XPathConstants.NODESET);
			        for(int i=0;i<eventBasedGateway.getLength();i++){

			            String id = eventBasedGateway.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = "";
			            if (eventBasedGateway.item(i).getAttributes().getNamedItem("name") != null)
			            {
			                name = eventBasedGateway.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			                name=name.replaceAll("\\n", "");
			            }
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            String gatewayDirection = eventBasedGateway.item(i).getAttributes().getNamedItem("gatewayDirection").getNodeValue();
			           // String eventGatewayType = eventBasedGateway.item(i).getAttributes().getNamedItem("eventGatewayType").getNodeValue();

			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(eventBasedGateway.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);
			            notEmpty = true;

			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1)
			            	incoming =   incomingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalXorSplit = "xorSplit"+"("+ "edges("+  incominglist+" )"+", "+'"'+ incoming+'"'+" . 0  ) | ";
			                 Spool +="\n"+ additionalXorSplit;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(eventBasedGateway.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);


			            String outgoing = "";
			            String outgoinglist = "eventRcvSplit( ";


			            eventInterRcv = "";
			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            {



			            eventInterRcvList.add(outgoingNodeList.item(0).getTextContent());
			            System.out.println("priamelse eventInterRcvList: "+eventInterRcvList);
			            eventInterRcv = eventInterRcv(outgoingNodeList.item(0).getTextContent());
			            outgoinglist+=   eventInterRcv;
			            }
			            else
			            {


			            	   eventInterRcvList.add(outgoingNodeList.item(0).getTextContent());
			            	   System.out.println("inelse eventInterRcvList: "+eventInterRcvList);
			            	  // ////System.out.println("\nPRIMO outgoingNodeList.item(0).getTextContent(): "+outgoingNodeList.item(0).getTextContent());
					             
				               eventInterRcv = eventInterRcv(outgoingNodeList.item(0).getTextContent());
				               //////System.out.println("\neventInterRcv: "+eventInterRcv);
				               if(eventInterRcv==null){//c'è un problema con il parsing elementi non adatti collegati all'eventInterRcv
				            	  testResult="a problem may be present with the element connected to the Event Based Gateway (only events are admitted) e.g. the Intermediate Catch Event may have multiple incoming sequence flow (not allowed) or an Intermediate Catch Event is missing";
				            	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				     			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				         		  //dialog that tells which elements are present that cannot be translated
				         		  return returnedMultipleParameters;
				               }
				              // outgoing +=   interRcv;
				               outgoinglist+=   eventInterRcv;
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoing +=   outgoingNodeList.item(j).getTextContent();

				              // ////System.out.println("\nHERE outgoingNodeList.item(j).getTextContent(): "+outgoingNodeList.item(j).getTextContent());
				               eventInterRcvList.add(outgoingNodeList.item(j).getTextContent());
				               System.out.println("inFor eventInterRcvList: "+eventInterRcvList);
				               //////System.out.println("neventInterRcv outgoingNodeList.item(j).getTextContent(): "+outgoingNodeList.item(j).getTextContent());
				               eventInterRcv = eventInterRcv(outgoingNodeList.item(j).getTextContent());
				               //////System.out.println("\neventInterRcv: "+eventInterRcv);
				               if(eventInterRcv==null){//c'è un problema con il parsing elementi non adatti collegati all'eventInterRcv
					            	  testResult="a problem may be present with the element connected to the Event Based Gateway (only events are admitted) e.g. the Intermediate Catch Event may have multiple incoming sequence flow (not allowed) or an Intermediate Catch Event is missing";
					            	  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
					     			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
					         		  //dialog that tells which elements are present that cannot be translated
					         		  return returnedMultipleParameters;
					               }
				               outgoinglist= outgoinglist+" ^ "+eventInterRcv ;
				               //////System.out.println("\nHERE outgoinglist: "+outgoinglist + " j: "+j);
					              
				            }
			            }
			            outgoinglist += " ) ";
			            String elemType = "eventSplit";
			            CountTotale +=1;
			            Counteventbased +=1;
			            String eventbasedgat = "";

			            eventbasedgat = elemType+"( "+'"'+incoming+'"'+" . 0 , "+ outgoinglist+" ) | ";

			            if(eventbasedgat.isEmpty()){
				              testResult="element linked to the Event Based Gateway may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool +="\n"+ eventbasedgat;
			            //SINTASSI OK


			            }//EventBased
	


			        //TROVO TUTTI GLI intermediateCatchEvent (valutare prime eventsplit)
			        NodeList intermediateCatchEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), "./*[local-name()='intermediateCatchEvent']", XPathConstants.NODESET);
			        for(int i=0;i<intermediateCatchEventNodeList.getLength();i++){

			            String id = intermediateCatchEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
			            String name = intermediateCatchEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
			            name=name.replaceAll("\\n", "");
			            if ((name == "") || (name == "null"))
			            {
			            	name = id;
			            }
			            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(intermediateCatchEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

			            notEmpty = true;
			            boolean isMessage = false;
			            NodeList intermediateCatchEventMessageNodeList = null;
			            intermediateCatchEventMessageNodeList = (NodeList) XMLUtils.execXPath(intermediateCatchEventNodeList.item(i), "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
			            int countmessage = intermediateCatchEventMessageNodeList.getLength();
			            String intermediateThrowEventMessage = "";
				        for(int c=0;c<countmessage;c++)
			            {
			            	intermediateThrowEventMessage = intermediateCatchEventMessageNodeList.item(c).getAttributes().getNamedItem("id").getNodeValue();
			            	isMessage = true;
			            }
			            String incoming = "";

			            int lenghtincoming = incomingNodeList.getLength();
			            //////System.out.println("NUMERO INCOMING PER INTERRCV"+incomingNodeList.getLength());
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtincoming == 0){
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;

			            }else
			            if (lenghtincoming == 1){
			            	incoming =   incomingNodeList.item(0).getTextContent();
		            	//se è collegato ad eventsplit salto tutto
			            	if (eventInterRcvList.contains(incoming) ) continue;

			            }else
			            {
			            	//////System.out.println("NUMERO INCOMING PER INTERRCV"+incomingNodeList.getLength());
			            	//SE MULTIPLE INCOMING
			            	//MULTIPLE INCOMIG DA VEDERE PERCHE' SE NON E' MULTIPLE LE "" SBALLANO?!
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	incoming = "incoming" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String xorsplitname = '"'+"xorSplit"+randomInt+'"';
			            	String incominglist = "";


			            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
			            	//se è collegato ad eventsplit salto tutto
			            	if (eventInterRcvList.contains(incomingNodeList.item(0).getTextContent())) continue;
				            for (int j= 1; j<incomingNodeList.getLength(); j++)
				            {
				            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalXorSplit = "xorSplit"+"("+ "edges("+  incominglist+" )"+","+'"'+ incoming+'"'+" . 0  ) | ";
			                 Spool +="\n"+ additionalXorSplit;
			            }
			            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(intermediateCatchEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);
			            String outgoing = "";
			            int lenghtoutgoing = outgoingNodeList.getLength();
			            //controllo se la lista ha solo un sequence flow in uscita
			            if (lenghtoutgoing == 0){

			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;

			            }else
			            if (lenghtoutgoing == 1)
			            		outgoing =   outgoingNodeList.item(0).getTextContent();
			            else
			            {
			            	//SE MULTIPLE OUTGOING
			            	Random randomGenerator = new Random();
			            	int randomInt = randomGenerator.nextInt(1000);
			            	outgoing = "outgoing" + randomInt ;
			            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

			            	String andsplitname = '"'+"andSplit"+randomInt+'"';
			            	String outgoinglist = "";


				            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
				            for (int j= 1; j<outgoingNodeList.getLength(); j++)
				            {
				               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

				            }

			                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
			                 Spool +="\n"+ additionalAndSplit;
			            }
			            messageflowEntry= messageflowentrata.get(id);
			            messageflowExit= messageflowuscita.get(id);
			            if (messageflowEntry!= null)
			            {

			            	String messageIn= messageflowmessage.get(messageflowEntry);
			            	if (messageIn!= null){
				                String messageName= messageDict.get(messageIn);
				                if((messageName!=null)){
				                messageflowEntry =messageName;
				                }
				                PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
				            }
				            else
				            {

			            	PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
			            }
			            }
			            CountTotale +=1;
			            Countintermediatecatch += 1;
			            String elemType = "interRcv";

			            String interRcv = elemType+"( disabled , "+'"'+incoming+'"'+" . 0 , "+ '"'+outgoing+'"'+" . 0 , " + '"' +messageflowEntry+'"'+" .msg 0 "+ ") | " ;
			            if(interRcv.isEmpty()){
				              testResult="element linked to the Intermediate Receive Event may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
				              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
				       		  //dialog that tells which elements are present that cannot be translated
				       		  return returnedMultipleParameters;
				            }
			            Spool += "\n"+interRcv;
			            //SINTASSI OK

			        }



		        //controllo exclusiveGateway(XOR)
		        NodeList exclusiveGateway =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), "./*[local-name()='exclusiveGateway']", XPathConstants.NODESET);
		        for(int i=0;i<exclusiveGateway.getLength();i++){

		            String id = exclusiveGateway.item(i).getAttributes().getNamedItem("id").getNodeValue();
		            String name = "";
		            if (exclusiveGateway.item(i).getAttributes().getNamedItem("name") != null)
		            {
		            name = exclusiveGateway.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
		            name=name.replaceAll("\\n", "");
		            }
		            if ((name == "") || (name == "null"))
		            {
		            	name = id;
		            }
		            String gatewayDirection = exclusiveGateway.item(i).getAttributes().getNamedItem("gatewayDirection").getNodeValue();
		            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(exclusiveGateway.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

		           // List myList = new ArrayList().add(startEventNodeList.item(i));
		            notEmpty = true;
		            String incoming = "";
		            String incominglist = "edges( ";
		            int lenghtincoming = incomingNodeList.getLength();
		            //controllo se la lista ha solo un sequence flow in uscita
		            if (lenghtincoming == 0){
		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	incoming = "incoming" + randomInt ;

		            }else
		            if (lenghtincoming == 1)
		            	incoming =   incomingNodeList.item(0).getTextContent();
		            else
		            {
		            	incoming +=   incomingNodeList.item(0).getTextContent();
		            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
			            for (int j= 1; j<incomingNodeList.getLength(); j++)
			            {
			            	incoming +=   incomingNodeList.item(j).getTextContent();
			            	incominglist +=" and "+ '"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0 ";

			            }
			            incominglist += " ) ";
		            }

		            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(exclusiveGateway.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

		           // List myList = new ArrayList().add(startEventNodeList.item(i));

		            String outgoinglist = "edges( ";

		            String outgoing = "";

		            int lenghtoutgoing = outgoingNodeList.getLength();

		            //controllo se la lista ha solo un sequence flow in uscita
		            if (lenghtoutgoing == 0){

		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	outgoing = "outgoing" + randomInt ;

		            }else
		            if (lenghtoutgoing == 1)
		            {
		            outgoing =   outgoingNodeList.item(0).getTextContent();
		            }
		            else
		            {
			               outgoing +=   outgoingNodeList.item(0).getTextContent();
			               outgoinglist +=   '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
			            for (int j= 1; j<outgoingNodeList.getLength(); j++)
			            {
			               outgoing +=   outgoingNodeList.item(j).getTextContent();
			               outgoinglist +=" and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

			            }
			            outgoinglist += " ) ";
		            }


		            String elemType = "";
		            String xor = "";


		          //controllo ExclusiveGateway(XOR)
		            //DEVO CONTROLLARE CASI IN CUI INCOMING SIANO 1 E OUTGOING SIANO 1 se è mixed ma con 1 in e 1 out è diverging per me
		            if ((lenghtincoming == 1 && lenghtoutgoing == 1)){
		            	if(gatewayDirection.equals("Converging"))
		             	{
		                 	elemType = "xorJoin";
		                    	CountTotale +=1;
		                 	Countxorjoin +=1;
		                 	xor = elemType+"( "+'"'+incoming+'"'+" . 0 , "+'"'+ outgoing+'"'+" . 0 ) | ";
		             	}
		            	else
		             	{
		             	elemType = "xorSplit";
		             	CountTotale +=1;
		             	Countxorsplit +=1;
		             	xor = elemType+"( "+'"'+incoming+'"'+" . 0 , "+'"'+outgoing+'"'+" . 0 ) | ";
		             	}
		            } else

		            //MULTIPLI INCOMING AND OUTGOING
		            if ((gatewayDirection.equals("Diverging") && lenghtincoming==1 && lenghtoutgoing>1))
		            	{
		            	elemType = "xorSplit";
		            	CountTotale +=1;
		            	Countxorsplit +=1;
		            	xor = elemType+"( "+'"'+incoming+'"'+" . 0 , "+ outgoinglist+" ) | ";
		            	}
		            else if((gatewayDirection.equals("Converging") && lenghtincoming>1 && lenghtoutgoing==1))
		            	{
		                	elemType = "xorJoin";
		                   	CountTotale +=1;
		                	Countxorjoin +=1;
		                	xor = elemType+"( "+incominglist+" , "+'"'+ outgoing+'"'+" . 0 ) | ";

		            	}//PARTE AGGIUNTA PER GESTIRE MIXED GATEWAYS
		            else if((gatewayDirection.equals("Mixed") && lenghtincoming>1 && lenghtoutgoing>1)){
		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	String linkgat = "xorGat"+randomInt;
		            	xor = "xorJoin( "+incominglist+" , "+'"'+ linkgat+'"'+" . 0 ) | ";
		            	randomInt++;
		            	xor+= "xorSplit( "+'"'+ linkgat+'"'+" . 0 "+" , "+outgoinglist+" ) | ";
		            }
		            if(xor.isEmpty()){
		              testResult="element linked to the xorGateway may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
		              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
		   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
		       		  //dialog that tells which elements are present that cannot be translated
		       		  return returnedMultipleParameters;
		            }
		            Spool +="\n"+ xor;
		            //SINTASSI OK
		            }




		        //controllo inclusiveGateway(OR)
		        NodeList inclusiveGateway =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), "./*[local-name()='inclusiveGateway']", XPathConstants.NODESET);
		        for(int i=0;i<inclusiveGateway.getLength();i++){

		            String id = inclusiveGateway.item(i).getAttributes().getNamedItem("id").getNodeValue();
		            String name = "";
		            if (inclusiveGateway.item(i).getAttributes().getNamedItem("name") != null)
		            {
		            name = inclusiveGateway.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
		            name=name.replaceAll("\\n", "");
		            }
		            if ((name == "") || (name == "null"))
		            {
		            	name = id;
		            }
		            String gatewayDirection = inclusiveGateway.item(i).getAttributes().getNamedItem("gatewayDirection").getNodeValue();
		            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(inclusiveGateway.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

		           // List myList = new ArrayList().add(startEventNodeList.item(i));
		            notEmpty = true;
		            String incoming = "";
		            String incominglist = "edges( ";
		            int lenghtincoming = incomingNodeList.getLength();
		            //controllo se la lista ha solo un sequence flow in uscita
		            if (lenghtincoming == 0){
		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	incoming = "incoming" + randomInt ;

		            }else
		            if (lenghtincoming == 1)
		            	incoming =   incomingNodeList.item(0).getTextContent();
		            else
		            {
		            	incoming +=   incomingNodeList.item(0).getTextContent();
		            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";

			            for (int j= 1; j<incomingNodeList.getLength(); j++)
			            {
			            	incoming +=   incomingNodeList.item(j).getTextContent();
			            	incominglist +=" and"+ '"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

			            }
			            incominglist += " ) ";
		            }
		      //COMMENTATO PERCHE' CON IL NONE NON FUNZIONA
		            //incominglist += "none . 0 ) ";

		            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(inclusiveGateway.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

		           // List myList = new ArrayList().add(startEventNodeList.item(i));

		            String outgoinglist = "edges( ";

		            String outgoing = "";

		            int lenghtoutgoing = outgoingNodeList.getLength();

		            //controllo se la lista ha solo un sequence flow in uscita
		            if (lenghtoutgoing == 0){

		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	outgoing = "outgoing" + randomInt ;

		            }else
		            if (lenghtoutgoing == 1)
		            {
		            outgoing =   outgoingNodeList.item(0).getTextContent();
		            }
		            else
		            {
			               outgoing +=   outgoingNodeList.item(0).getTextContent();
			               outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
			            for (int j= 1; j<outgoingNodeList.getLength(); j++)
			            {
			               outgoing +=   outgoingNodeList.item(j).getTextContent();
			               outgoinglist +=" and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

			            }
			            outgoinglist += " ) ";
		            }

		            String elemType = "";
		            String or = "";
		            if ((gatewayDirection.equals("Diverging") && lenghtincoming==1 && lenghtoutgoing>1))
		            	{
		               	CountTotale +=1;
		            	Countorsplit +=1;
		            	elemType = "orSplit";
		            	or = elemType+"( "+'"'+incoming+'"'+" . 0 , "+ outgoinglist+" ) | ";

		            	}
		            else
		            	 if ((gatewayDirection.equals("Diverging") && lenghtincoming==1 && lenghtoutgoing==1))
		                 	{
		                    	CountTotale +=1;
		                 	Countorsplit +=1;
		                 	elemType = "orsplit";
		                 	or = elemType+"( "+'"'+incoming+'"'+" . 0 , "+'"'+outgoing+'"'+" ) | ";

		                 	}
		            if(or.isEmpty()){
			              testResult="element linked to the Inclusive Gateway may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
			              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
			   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
			       		  //dialog that tells which elements are present that cannot be translated
			       		  return returnedMultipleParameters;
			            }
		            Spool +="\n"+ or;
		            //SINTASSI OK
		            }



		        

		        //controllo parallelGateway (AND)
		        NodeList parallelGateway =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), "./*[local-name()='parallelGateway']", XPathConstants.NODESET);
		        for(int i=0;i<parallelGateway.getLength();i++){

		            String id = parallelGateway.item(i).getAttributes().getNamedItem("id").getNodeValue();
		            String name = "";
		            if (parallelGateway.item(i).getAttributes().getNamedItem("name") != null)
		            {
		            name = parallelGateway.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
		            name=name.replaceAll("\\n", "");
		            }
		            if ((name == "") || (name == "null"))
		            {
		            	name = id;
		            }
		            String gatewayDirection = parallelGateway.item(i).getAttributes().getNamedItem("gatewayDirection").getNodeValue();
		            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(parallelGateway.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

		           // List myList = new ArrayList().add(startEventNodeList.item(i));
		            String incominglist = "edges( ";
		            String incoming = "";
		            notEmpty = true;
		            int lenghtincoming = incomingNodeList.getLength();
		            //controllo se la lista ha solo un sequence flow in uscita
		            if (lenghtincoming == 0){
		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	incoming = "incoming" + randomInt ;

		            }else
		            if (lenghtincoming == 1)
		            	incoming =   incomingNodeList.item(0).getTextContent();
		            else
		            {
		            	incoming +=   incomingNodeList.item(0).getTextContent();
		            	incominglist += '"'+incomingNodeList.item(0).getTextContent()+'"'+" . 0";
			            for (int j= 1; j<incomingNodeList.getLength(); j++)
			            {
			            	incoming +=   incomingNodeList.item(j).getTextContent();
			            	incominglist += " and "+'"'+incomingNodeList.item(j).getTextContent()+'"'+" . 0";

			            }
			            incominglist += " ) ";
		            }

		            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(parallelGateway.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);

		           // List myList = new ArrayList().add(startEventNodeList.item(i));
		            String outgoinglist = "edges( ";
		            String outgoing = "";



		            int lenghtoutgoing = outgoingNodeList.getLength();
		            //controllo se la lista ha solo un sequence flow in uscita
		            if (lenghtoutgoing == 0){

		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	outgoing = "outgoing" + randomInt ;

		            }else
		            if (lenghtoutgoing == 1)
		            {
		            outgoing =   outgoingNodeList.item(0).getTextContent();
		            }
		            else
		            {
			               outgoing +=   outgoingNodeList.item(0).getTextContent();
			               outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
			            for (int j= 1; j<outgoingNodeList.getLength(); j++)
			            {
			               outgoing +=   outgoingNodeList.item(j).getTextContent();
			               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

			            }
			            outgoinglist += " ) ";
		            }

		            String elemType = "";
		            String and = "";

		            //se 1 inc e 1 out
		            if ((lenghtincoming == 1 && lenghtoutgoing == 1)){
		            	if (gatewayDirection.equals("Diverging"))
		            	{
		            	elemType = "andSplit";
		               	CountTotale +=1;
		            	Countandsplit +=1;
		            	and = elemType+"( "+'"'+incoming+'"'+" . 0 , "+'"'+outgoing+'"'+" . 0 "+" ) | ";


		            	}
		            else if(gatewayDirection.equals("Converging"))
		            	{
		                	elemType = "andJoin";
		                   	CountTotale +=1;
		                	Countandjoin +=1;
		                    and =elemType+"( "+'"'+incoming+'"'+" . 0 , "+'"'+ outgoing+'"'+" . 0 ) | ";

		            	}

		            }else


		            if ((gatewayDirection.equals("Diverging")&& lenghtincoming==1 && lenghtoutgoing>1))
		            	{
		            	elemType = "andSplit";
		               	CountTotale +=1;
		            	Countandsplit +=1;
		            	and = elemType+"( "+'"'+incoming+'"'+" . 0 , "+ outgoinglist+" ) | ";


		            	}
		            else if((gatewayDirection.equals("Converging")&& lenghtincoming>1 && lenghtoutgoing==1))
		            	{
		                	elemType = "andJoin";
		                   	CountTotale +=1;
		                	Countandjoin +=1;
		                    and =elemType+"( "+incominglist+" , "+'"'+ outgoing+'"'+" . 0 ) | ";

		            	}
		          //PARTE AGGIUNTA PER GESTIRE MIXED GATEWAYS
		            else if((gatewayDirection.equals("Mixed")&& lenghtincoming>1 && lenghtoutgoing>1)){
		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	String linkgat = "andGat"+randomInt;
		            	and = "andJoin( "+incominglist+" , "+'"'+ linkgat+'"'+" . 0 ) | ";
		            	randomInt++;
		            	and+= "andSplit( "+'"'+ linkgat+'"'+" . 0 "+" , "+outgoinglist+" ) | ";
		            	//////System.out.println("andgat: "+and);
		            }
		            if(and.isEmpty()){
			              testResult="element linked to the Parallel Gateway may not be eligible for parsing, or the element is missing incoming or outgoing sequence flows";
			              PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
			   			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
			       		  //dialog that tells which elements are present that cannot be translated
			       		  return returnedMultipleParameters;
			            }
		            Spool +="\n"+ and;
		            //SINTASSI OK
		            }



		        //Fine elementi
		        ////System.out.println("\nFINE ELEMENTI DELLA POOL Z: "+z);
	        	//partecipantNodeList != DA POOLNODELIST
//		        	////System.out.println("\nFINE ELEMENTI DELLA POOL: "+partecipantNodeList.item(z).getAttributes().getNamedItem("name").getNodeValue());
		        	
					}catch(Exception e){
						e.printStackTrace();
						////System.out.println("\nPOOL SENZA PROCESSO");
//						String idpoolAppoggio = PoolNodeList.item(z).getAttributes().getNamedItem("id").getNodeValue();
						

				    	PoolMessageIn = " in: ";
				    	PoolMessageOut = " out: ";
				    	
				    	////System.out.println("z: "+z);
				    	//NodeList partecipantNodeListAppoggio =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), partecipantQuery, XPathConstants.NODESET);
				    	
				    	String idpool = partecipantNodeList.item(z).getAttributes().getNamedItem("id").getNodeValue();

//				    	String idProcess = partecipantDict.get(idpool);
				    	
				    	////System.out.println("idpool: "+idpool);
//				    	////System.out.println("idProcess: "+idProcess);
				    	
				    	//POTREBBERO ESSERCI MESSAGGI MULTIPLI DIRETTI ALLA STESSA POOL
				    	//PER LE POOL NON VA BENE LA MAP
				    	//ArrayList<String> messageflowentrataAppoggio =  new ArrayList<String>() ;

				        NodeList messageFlowNodeListAppoggio =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);

//				        notEmpty = false;
				        ////System.out.println("\nINTO CATCH POOL SENZA PROCESSO");
				        ////System.out.println("MESSAGE LIST SIZE: "+messageFlowNodeListAppoggio.getLength());
				        for(int i=0;i<messageFlowNodeListAppoggio.getLength();i++){
				        	String name = "";
				        	 String id = messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("id").getNodeValue();
				             String sourceRef = messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("sourceRef").getNodeValue();
				             String targetRef = messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("targetRef").getNodeValue();
				             String messageRef = "";
				             //////System.out.println("MSG id: "+id);
				             //////System.out.println("MSG sourceRef: "+sourceRef);
				             //////System.out.println("MSG targetRef: "+targetRef);
				             if (messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("messageRef") != null)
				             {
				            	 messageRef = messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("messageRef").getNodeValue();
				             }
				             /*CREO 3 DIZIONARI (MITTENTI/DESTINATARI/MESSAGGI)
				              *LI RICHIAMERO' PIU' TARDI, PASSANDOGLI UN QUALSIASI ID
				              *SE PRESENTE MI RITORNERA' UN RISULTATO
				             */
				             
				             if(targetRef.equals(idpool)){
				            	 //////System.out.println("MSG HAS A EMPTY POOL AS TARGET");
				            	 PoolMessageIn += '"'+id+'"'+" .msg 0 andmsg ";
				             }
				             
				             if(sourceRef.equals(idpool)){
				            	 //////System.out.println("MSG HAS A EMPTY POOL AS SOURCE");
				            	 PoolMessageOut += '"'+id+'"'+" .msg 0 andmsg ";
				             }
				             

				        }//chiusura for che scorre i messaggi
				        

				    	
				    	
//		                messageflowEntry= messageflowentrata.get(idpool);
//			            messageflowExit= messageflowuscita.get(idpool);
//			            if (messageflowExit!= null)
//			            {
//			                String messageExit= messageflowmessage.get(messageflowExit);
//				            if (messageExit!= null){
//				                String messageName= messageDict.get(messageExit);
//				                if((messageName!=null)){
//				                messageflowExit = messageName;
//				                }
//				            	PoolMessageOut += '"'+messageflowExit+'"'+" .msg 0 andmsg ";
//				            }
//				            else
//				            {
//				            	PoolMessageOut += '"'+messageflowExit+'"'+" .msg 0 andmsg ";
//				            }
//			            }
//			            if (messageflowEntry!= null)
//			            {
//			                String messageEntry= messageflowmessage.get(messageflowEntry);
//				            if (messageEntry!= null){
//				                String messageName= messageDict.get(messageEntry);
//				                if((messageName!=null)){
//				                messageflowEntry = messageName;
//				                }
//				                PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
//				            }
//				            else
//				            {
//			            	PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
//				            }
//			            }
				            
				            PoolMessageIn += " emptyMsgSet ";
					    	PoolMessageOut += " emptyMsgSet ";    
				    	String namepoolAppoggio = partecipantNodeList.item(z).getAttributes().getNamedItem("name").getNodeValue();
				    	////System.out.println("partecipantNodeList.item(z).getAttributes().getNamedItem(name).getNodeValue();"+partecipantNodeList.item(z).getAttributes().getNamedItem("name").getNodeValue());						
						Spool = "\npool( "+'"'+namepoolAppoggio+'"'+" , \nproc( {emptyAction}";				
			    		Spool+= " emptyProcElements ) , ";
			        	Spool += "\n"+PoolMessageIn+", " + PoolMessageOut;
			        	Scollaboration += Spool +") | ";
			        	continue;
			        	
					
				}

			    	PoolMessageIn += " emptyMsgSet ";
			    	PoolMessageOut += " emptyMsgSet ";
			    	if (notEmpty == true)
			    	{
			    		Spool = Spool.substring(0, Spool.length() - 2);
			    		Spool += "| emptyProcElements ) , ";
			    	}
			    	else
			    	{
			    		
			    		////System.out.println("\n PROCESS DELLA POOL e' VUOTO \n");
			    		////System.out.println("\nz: "+z);
			    		PoolMessageIn = " in: ";
				    	PoolMessageOut = " out: ";
//			    		Spool +="emptyProcess ";
			    		Spool += " emptyProcElements ) , ";
			    		
			    		//scorro le pool per prendere quella giusta
			    		for(int ipool = 0; ipool<partecipantNodeList.getLength(); ipool++){
			    			
				    		//IL PROCESSO E' VUOTO MA HA PIU' DI UN MESSAGGIO QUINDI...
				    		String idpool = partecipantNodeList.item(ipool).getAttributes().getNamedItem("id").getNodeValue();
				    		NodeList messageFlowNodeListAppoggio =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);

//					        notEmpty = false;
				    		
				    		////System.out.println("\nPOOL: "+partecipantNodeList.item(ipool).getAttributes().getNamedItem("name").getNodeValue());
				    		////System.out.println("\nINTO ELSE PROCESS VUOTO ");
					        ////System.out.println("MESSAGE LIST SIZE: "+messageFlowNodeListAppoggio.getLength());
					        
					        String idProcRef = partecipantNodeList.item(ipool).getAttributes().getNamedItem("processRef").getNodeValue();
					        
					        //se il processo String idpool = PoolNodeList.item(z).getAttributes().getNamedItem("id").getNodeValue();
					        String idProc = PoolNodeList.item(z).getAttributes().getNamedItem("id").getNodeValue();
					        //è legato a questa poolString idpool = partecipantNodeList.item(ipool).getAttributes().getNamedItem("id").getNodeValue();
					        if(idProc.equals(idProcRef)){
					        
						        for(int i=0;i<messageFlowNodeListAppoggio.getLength();i++){
						        	String name = "";
						        	 String id = messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("id").getNodeValue();
						             String sourceRef = messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("sourceRef").getNodeValue();
						             String targetRef = messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("targetRef").getNodeValue();
						             String messageRef = "";
						             //////System.out.println("MSG id: "+id);
						             //////System.out.println("MSG sourceRef: "+sourceRef);
						             //////System.out.println("MSG targetRef: "+targetRef);
						             if (messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("messageRef") != null)
						             {
						            	 messageRef = messageFlowNodeListAppoggio.item(i).getAttributes().getNamedItem("messageRef").getNodeValue();
						             }
						             
						             
						             if(targetRef.equals(idpool)){
						            	 //////System.out.println("MSG HAS A EMPTY POOL AS TARGET");
						            	 PoolMessageIn += '"'+id+'"'+" .msg 0 andmsg ";
						             }
						             
						             if(sourceRef.equals(idpool)){
						            	 //////System.out.println("MSG HAS A EMPTY POOL AS SOURCE");
						            	 PoolMessageOut += '"'+id+'"'+" .msg 0 andmsg ";
						             }
						             
	
						        }//chiusura for che scorre i messaggi
					        }//chiusura if pool associata al process corretto e viceversa
			    		}//scorro le pool per beccare quella giusta
			    		 PoolMessageIn += " emptyMsgSet ";
					     PoolMessageOut += " emptyMsgSet ";
			    		
			    	}//chiusura else process vuoto

//		        	Spool += "| emptyProcElements ) , ";
		        	Spool += "\n"+PoolMessageIn+", " + PoolMessageOut;
		        	Scollaboration += Spool +") | ";
//chiudo scorrmento processi
		        }

		        Scollaboration = Scollaboration.substring(0, Scollaboration.length() -2);

		        Scollaboration += "| emptyPool )";
			  
		        outputModel=Scollaboration;
		        
		        
//Statistics Part
		        String StatisticAndPerfomance = "";
		        StatisticAndPerfomance+= "Number of total element: "+CountTotale +"\n";
				StatisticAndPerfomance+= "Number of pool:"+Countpoolev +"\n";
				StatisticAndPerfomance+= "Number of start event:"+Countstart +"\n";
				StatisticAndPerfomance+= "Number of message start event:"+Countmsgstart +"\n";
				StatisticAndPerfomance+= "Number of end event:"+Countend +"\n";
				StatisticAndPerfomance+= "Number of message end event:"+Countmsgend +"\n";
				StatisticAndPerfomance+= "Number of terminated  end event:"+Countterminateend +"\n";
				StatisticAndPerfomance+= "Number of task:"+Counttask +"\n";
				StatisticAndPerfomance+= "Number of sendtask:"+Countsendtask +"\n";
				StatisticAndPerfomance+= "Number of receivetask:"+Countreceivetask +"\n";
				StatisticAndPerfomance+= "Number of intermidiate throw event:"+Countintermediatethrow +"\n";
				StatisticAndPerfomance+= "Number of intermidiate catch event:"+Countintermediatecatch +"\n";
				StatisticAndPerfomance+= "Number of xorsplit event:"+Countxorsplit +"\n";
				StatisticAndPerfomance+= "Number of xorjoin event:"+Countxorjoin +"\n";
				StatisticAndPerfomance+= "Number of andsplit event:"+Countandsplit +"\n";
				StatisticAndPerfomance+= "Number of andjoin event:"+Countandjoin +"\n";
				StatisticAndPerfomance+= "Number of orsplit event:"+Countorsplit +"\n";
				StatisticAndPerfomance+= "Number of event based gateway:"+Counteventbased +"\n";
				StatisticAndPerfomance+= "**\n";
			
		        //Ritorno il modello parsato
				PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
				returnedMultipleParameters.setParsedModel(outputModel);
				returnedMultipleParameters.setNumberOfElement(CountTotale);
				returnedMultipleParameters.setNumberOfPool(Countpoolev);
				returnedMultipleParameters.setNumberOfStart(Countstart);
				returnedMultipleParameters.setNumberOfStartMsg(Countmsgstart);
				returnedMultipleParameters.setNumberOfEnd(Countend);
				returnedMultipleParameters.setNumberOfEndMsg(Countmsgend);
				returnedMultipleParameters.setNumberOfTerminate(Countterminateend);
				returnedMultipleParameters.setNumberOfTask(Counttask);
				returnedMultipleParameters.setNumberOfTaskSnd(Countsendtask);
				returnedMultipleParameters.setNumberOfTaskRcv(Countreceivetask);
				returnedMultipleParameters.setNumberOfIntermidiateThrowEvent(Countintermediatethrow);
				returnedMultipleParameters.setNumberOfIntermidiateCatchEvent(Countintermediatecatch);
				returnedMultipleParameters.setNumberOfGatewayXorSplit(Countxorsplit);
				returnedMultipleParameters.setNumberOfGatewayXorJoin(Countxorjoin);
				returnedMultipleParameters.setNumberOfGatewayAndSplit(Countandsplit);
				returnedMultipleParameters.setNumberOfGatewayAndJoin(Countandjoin);
				returnedMultipleParameters.setNumberOfGatewayOrSplit(Countorsplit);
				returnedMultipleParameters.setNumberOfGatewayEventBased(Counteventbased);
				
				long currentTimeend = System.currentTimeMillis();
				long parsingTime = 0;
				parsingTime = currentTimeend - currentTimeSart;
				
				returnedMultipleParameters.setParsingTime(Long.toString(parsingTime));
				returnedMultipleParameters.setResult("Parsing succeded");	
				
		        return returnedMultipleParameters;
			  		  
		  }else{
			  PostMultipleParameters returnedMultipleParameters = new PostMultipleParameters();
			  returnedMultipleParameters.setParsedModel("The model presents the following ineligible BPMN elements\n"+testResult);	
    		  //dialog that tells which elements are present that cannot be translated
			  long currentTimeend = System.currentTimeMillis();
			  long parsingTime = 0;
			  parsingTime = currentTimeend - currentTimeSart;
			  returnedMultipleParameters.setParsingTime(Long.toString(parsingTime));
			  returnedMultipleParameters.setResult("Parsing failed");
			  
    		  return returnedMultipleParameters;
    	  }
		
	}
	
	
	
	
	
	
	
	
	
	
	@SuppressWarnings("unused")
	public static String eventInterRcv( String interRcvInputEdge) throws Exception{

		//////System.out.println("\neventInterRcv\n");
		String result=null;

		//TROVO TUTTI GLI intermediateCatchEvent
        NodeList intermediateCatchEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), "./*[local-name()='intermediateCatchEvent']", XPathConstants.NODESET);
        for(int i=0;i<intermediateCatchEventNodeList.getLength();i++){

            String id = intermediateCatchEventNodeList.item(i).getAttributes().getNamedItem("id").getNodeValue();
            String name = intermediateCatchEventNodeList.item(i).getAttributes().getNamedItem("name").getNodeValue().replaceAll("[^a-zA-Z0-9\\s]", "");
            name=name.replaceAll("\\n", "");
            if ((name == "") || (name == "null"))
            {
            	name = id;
            }
            NodeList incomingNodeList = (NodeList) XMLUtils.execXPath(intermediateCatchEventNodeList.item(i), "./*[local-name()='incoming']", XPathConstants.NODESET);

            notEmpty = true;
            //boolean isMessage = false;
            NodeList intermediateCatchEventMessageNodeList = null;
            intermediateCatchEventMessageNodeList = (NodeList) XMLUtils.execXPath(intermediateCatchEventNodeList.item(i), "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
            int countmessage = intermediateCatchEventMessageNodeList.getLength();
            String intermediateThrowEventMessage = "";
	        for(int c=0;c<countmessage;c++)
            {
            	intermediateThrowEventMessage = intermediateCatchEventMessageNodeList.item(c).getAttributes().getNamedItem("id").getNodeValue();
            	//isMessage = true;
            }
            String incoming = "";

            int lenghtincoming = incomingNodeList.getLength();
            //controllo se la lista ha solo un sequence flow in uscita
            if (lenghtincoming == 0){
            	Random randomGenerator = new Random();
            	int randomInt = randomGenerator.nextInt(1000);
            	incoming = "incoming" + randomInt ;

            }else
            if (lenghtincoming == 1)
            	incoming =   incomingNodeList.item(0).getTextContent();
            	if(incoming.equals(interRcvInputEdge)){

            		//FORMATTAZIONE INTERRCV
		            NodeList outgoingNodeList = (NodeList) XMLUtils.execXPath(intermediateCatchEventNodeList.item(i), "./*[local-name()='outgoing']", XPathConstants.NODESET);
		            String outgoing = "";
		            int lenghtoutgoing = outgoingNodeList.getLength();
		            //controllo se la lista ha solo un sequence flow in uscita
		            if (lenghtoutgoing == 0){

		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	outgoing = "outgoing" + randomInt ;

		            }else
		            if (lenghtoutgoing == 1)
		            		outgoing =   outgoingNodeList.item(0).getTextContent();
		            else
		            {
		            	//SE MULTIPLE OUTGOING
		            	Random randomGenerator = new Random();
		            	int randomInt = randomGenerator.nextInt(1000);
		            	outgoing = "outgoing" + randomInt ;
		            	//Devo creare un and, dagrli come incoming l'outgoing random precedentemente definito e come outgoing gli outgoing dello start

		            	String andsplitname = '"'+"andSplit"+randomInt+'"';
		            	String outgoinglist = "";


			            outgoinglist += '"'+outgoingNodeList.item(0).getTextContent()+'"'+" . 0";
			            for (int j= 1; j<outgoingNodeList.getLength(); j++)
			            {
			               outgoinglist += " and "+'"'+outgoingNodeList.item(j).getTextContent()+'"'+" . 0";

			            }

		                 String additionalAndSplit = "andSplit"+"("+'"'+ outgoing+'"'+" . 0 , "+ "edges("+  outgoinglist+" )) | ";
		                 Spool +="\n"+ additionalAndSplit;
		            }
		            messageflowEntry= messageflowentrata.get(id);
		            messageflowExit= messageflowuscita.get(id);
		            
		            if (messageflowEntry!= null)
		            {

		            	String messageIn= messageflowmessage.get(messageflowEntry);
		            	if (messageIn!= null){
			                String messageName= messageDict.get(messageIn);
			                if((messageName!=null))messageflowEntry =messageName;
			                PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
			                
			            }else{
			            	PoolMessageIn += '"'+messageflowEntry+'"'+" .msg 0 andmsg ";
			            }
		            }
		            CountTotale +=1;
		            Countintermediatecatch += 1;
		            String elemType = "eventInterRcv";
		            result = elemType+"( disabled , "+ '"'+outgoing+'"'+" . 0 , " + '"' +messageflowEntry+'"'+" .msg 0 "+ ") " ;

            	}
        	}
        return result;
	}
	
	
	
	
		@SuppressWarnings("unused")
		public static String evaluateModel(Document inputModel) throws Exception{
			   
			////System.out.println("\nevaluateModel\n");
			   String result = "";
			   
			   try{

			   inputModel.getDocumentElement().normalize();

		       String startQuery = "./*[local-name()='startEvent']";
		       String endQuery = "./*[local-name()='endEvent']";
		       String simpleTaskQuery = "./*[local-name()='task']";
		       String manualTaskQuery = "./*[local-name()='manualTask']";
		       String serviceTaskQuery = "./*[local-name()='serviceTask']";
		       String businessRuleTaskQuery = "./*[local-name()='businessRuleTask']";
		       String scriptTaskQuery = "./*[local-name()='scriptTask']";
		       String userTaskQuery = "./*[local-name()='userTask']";
		       String sendTaskQuery = "./*[local-name()='sendTask']";
		       String receiveTaskQuery = "./*[local-name()='receiveTask']";
		       String sequenceFlowQuery = "//*[local-name()='sequenceFlow']";
		       String messageFlowQuery = "//*[local-name()='messageFlow']";
		       String partecipantQuery = "//*[local-name()='participant']"; 
		       String exclusiveGatewayQuery =  "//*[local-name()='exclusiveGateway']"; 
		       String inclusiveGatewayQuery =  "//*[local-name()='inclusiveGateway']"; 
		       String parallelGatewayQuery =  "//*[local-name()='parallelGateway']"; 
		       String eventBasedGatewayQuery =  "//*[local-name()='eventBasedGateway']"; 
		       String complexGatewayQuery =  "//*[local-name()='complexGateway']"; 
		       String subProcessQuery =  "//*[local-name()='subProcess']"; 
		       String adHocSubProcessQuery =  "//*[local-name()='adHocSubProcess']"; 
		       String transactionQuery =  "//*[local-name()='transaction']"; 
		       String intermediateThrowEventQuery =  "//*[local-name()='intermediateThrowEvent']"; 
		       String intermediateCatchEventQuery =  "//*[local-name()='intermediateCatchEvent']";  
		       String boundaryEventQuery =  "//*[local-name()='boundaryEvent']"; 
		       String dataObjectQuery =  "//*[local-name()='dataObject']"; 
		       String dataInputQuery =  "//*[local-name()='dataInput']"; 
		       String dataOutputQuery =  "//*[local-name()='dataOutput']"; 
		       String dataStoreReferenceQuery =  "//*[local-name()='dataStoreReference']"; 
		       String messageQuery =  "//*[local-name()='message']";
		       	       
			   String multiInstanceQuery = "//*[local-name()='multiInstanceLoopCharacteristics']";
			   String loopQuery = "//*[local-name()='standardLoopCharacteristics']";			   
	       
		       int CountTotale = 0; 
		       int CountPoolev = 0; 
		       int CountStart= 0; 
		       int CountMsgstart= 0; 
		       
		       int CountMessage = 0;
		       
		       int CountEnd= 0; 
		   	   int CountEndMessageEvent = 0;
		   	   int CountEndEscalationEvent = 0;
		   	   int CountEndSignalEvent = 0;
		   	   int CountEndErrorEvent = 0;
		   	   int CountEndCancelEvent = 0;
		   	   int CountEndCompensationEvent = 0;
		       int CountEndMultiple = 0;
		   	   int CountEndTerminateEvent = 0;
		       		       
		       int CountMsgend= 0; 
		       int CountTask= 0; 
		       int CountManualTask= 0;
		       int CountServiceTask= 0; 
		       int CountBusinessRuleTask = 0;
		       int CountScriptTask = 0;
		       int CountUserTask = 0;
		       int CountSendTask= 0; 
		       int CountReceiveTask= 0; 
		       int CountTerminateEnd = 0;

		       int CountXorSplit = 0;
		       int CountXorJoin = 0;
		       int CountXorUnspecified = 0;
		       int CountXorMixed = 0;
		       int CountAndSplit = 0;
		       int CountAndJoin = 0;
		       int CountAndUnspecified = 0;
		       int CountAndMixed = 0;
		       int CountOrSplit = 0;
		       int CountOrJoin = 0;
		       int CountOrUnspecified = 0;
		       int CountOrMixed = 0;
		       int CountEventBasedSplit = 0;
		       int CountEventBasedJoin = 0;
		       int CountEventBasedUnspecified = 0;
		       int CountEventBasedMixed = 0;
		       int CountComplexSplit = 0;
		       int CountComplexJoin = 0;
		       int CountComplexUnspecified = 0;
		       int CountComplexMixed = 0;
		       int CountProc = 0;
		       int CountSequenceFlow = 0;
		       int CountMessageFlow = 0;
		       int CountExclusiveEventBased = 0;
		       int CountParallelEventBased = 0;
		       
			   int CountParallelEventBasedSplit = 0;
			   int CountParallelEventBasedJoin = 0;
		       int CountParallelEventBasedMixed = 0;
		       int CountExclusiveEventBasedSplit = 0;
		       int CountExclusiveEventBasedJoin = 0;
		       int CountExclusiveEventBasedMixed = 0;

			   int CountParallelEventBasedUnspecified = 0;
			   int CountExclusiveEventBasedUnspecified = 0;
					 
			   int CountStartMessageEvent = 0;
			   int CountStartTimerEvent = 0;
			   int CountStartConditionalEvent = 0;
			   int CountStartSignalEvent = 0;
			   int CountStartParallelMultiple = 0;
			   int CountStartMultiple = 0;
		   	 
		       int CountSubProcess = 0; 
		       int CountAdHocSubProcess = 0; 
		       int CountTransaction = 0; 
		   	 
		       int CountIntermediateThrowEvent = 0;
			   int CountIntermediateThrowMultiple = 0;
			   int CountIntermediateThrowMessageEvent = 0;
			   int CountIntermediateThrowSignalEvent = 0;
			   int CountIntermediateThrowEscalationEvent = 0;
			   int CountIntermediateThrowLinkEvent = 0;
			   int CountIntermediateThrowCompensationEvent = 0;

			   int CountIntermediateCatchEvent = 0;
			   int CountIntermediateCatchMultiple = 0;
			   int CountIntermediateCatchParallelMultiple = 0;
			   int CountIntermediateCatchMessageEvent = 0;
			   int CountIntermediateCatchTimerEvent = 0;
			   int CountIntermediateCatchLinkEvent = 0;
			   int CountIntermediateCatchConditionalEvent = 0;
			   int CountIntermediateCatchSignalEvent = 0;
		       		       
		       int CountBundaryEvent = 0;
		       
		       int CountBoundaryInterruptingEvent = 0;
		       int CountBoundaryInterruptingParallelMultiple = 0;
		       int CountBoundaryInterruptingMultiple = 0;
		       int CountBoundaryInterruptingMessageEvent = 0;
		       int CountBoundaryInterruptingTimerEvent = 0;
		       int CountBoundaryInterruptingLinkEvent = 0;
		       int CountBoundaryInterruptingConditionalEvent = 0;
		       int CountBoundaryInterruptingErrorEvent = 0;
		       int CountBoundaryInterruptingCancelEvent = 0;
		       int CountBoundaryInterruptingCompensationEvent = 0;
		       int CountBoundaryInterruptingSignalEvent = 0;
		       int CountBoundaryInterruptingEscalationEvent = 0;
		   	 
		       int CountBoundaryNonInterruptingEvent = 0;
		       int CountBoundaryNonInterruptingMessageEvent = 0;
		       int CountBoundaryNonInterruptingTimerEvent = 0;
		       int CountBoundaryNonInterruptingConditionalEvent = 0;
		       int CountBoundaryNonInterruptingEscalationEvent = 0;
		       int CountBoundaryNonInterruptingSignalEvent = 0;
		       int CountBoundaryNonInterruptingMultiple = 0;
		       int CountBoundaryNonInterruptingParallelMultiple = 0; 
		   	 
			   int CountDataObject = 0;
			   int CountDataInput = 0;
			   int CountDataOutput = 0;
			   int CountDataStore = 0;
			   
			   int CountMultiInstanceParallel = 0;
			   int CountMultiInstanceSequential = 0;
			   int CountLoop = 0;
			   int CountEmptyPool = 0;

		       
		       NodeList partecipantNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), partecipantQuery, XPathConstants.NODESET);
		       CountPoolev=partecipantNodeList.getLength();
		       
		       NodeList PoolNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(),"//*[local-name()='process']" , XPathConstants.NODESET);
		       CountProc = PoolNodeList.getLength();
		       
		       ////System.out.println("FALLO...: "+CountProc);
		       
		       for(int z=0;z<CountProc;z++){
		    	   
		    	 //CHECK IF POOL IS EMPTY	
		    	   if(CountProc<1){
		    		   CountEmptyPool++;
		    		}
		      	
		      	 //STARTEVENTS
		      	NodeList startEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), startQuery, XPathConstants.NODESET);
		      	for(int j=0; j<startEventNodeList.getLength();j++){
		      	
		      		Element startElement = (Element) startEventNodeList.item(j);
		      		
		      		
		      		
		      		//IN REALTA' NON E' UN ATTRIBUTO MA UN ELEMENTO FIGLIO!!!!!!!!!!!
		      		//dovrei per ogni start scorrere tutti gli elementi figli; se ne è 1 solo allora testo quale è, se ne è più di uno allora lo start è 
		      		//o un multiple o un parallel multiple
//		      		NodeList message= (NodeList) XMLUtils.execXPath(startElement, "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
//		  			NodeList timer= (NodeList) XMLUtils.execXPath(startElement, "./*[local-name()='timerEventDefinition']", XPathConstants.NODESET);
//		  			NodeList conditional= (NodeList) XMLUtils.execXPath(startElement, "./*[local-name()='conditionalEventDefinition']", XPathConstants.NODESET);
//		  			NodeList signal= (NodeList) XMLUtils.execXPath(startElement, "./*[local-name()='signalEventDefinition']", XPathConstants.NODESET);
//		  			 
		      		NodeList message= null;
		  			NodeList timer= null;
		  			NodeList conditional= null;
		  			NodeList signal= null;
		  			 
		      		
					//bpmn2:messageEventDefinition in eclipse
		  			if(message==null||message.getLength()==0)message = startElement.getElementsByTagName("bpmn2:messageEventDefinition");
		  			if(timer==null||timer.getLength()==0)timer = startElement.getElementsByTagName("bpmn2:timerEventDefinition");
		  			if(conditional==null||conditional.getLength()==0)conditional = startElement.getElementsByTagName("bpmn2:conditionalEventDefinition");
		  			if(signal==null||signal.getLength()==0)signal = startElement.getElementsByTagName("bpmn2:signalEventDefinition");
		  			
//		  			
//
//		  			//in Camunda Here
		  			if(message==null||message.getLength()==0)message = startElement.getElementsByTagName("bpmn:messageEventDefinition");
		  			if(timer==null||timer.getLength()==0)timer = startElement.getElementsByTagName("bpmn:timerEventDefinition");
		  			if(conditional==null||conditional.getLength()==0)conditional = startElement.getElementsByTagName("bpmn:conditionalEventDefinition");
		  			if(signal==null||signal.getLength()==0)signal = startElement.getElementsByTagName("bpmn:signalEventDefinition");
//		  			
//		  			
////		  			//in Signavio HERE
		  			if(message==null||message.getLength()==0)message = startElement.getElementsByTagName("messageEventDefinition");
		  			if(timer==null||timer.getLength()==0)timer = startElement.getElementsByTagName("timerEventDefinition");
		  			if(conditional==null||conditional.getLength()==0)conditional = startElement.getElementsByTagName("conditionalEventDefinition");
		  			if(signal==null||signal.getLength()==0)signal = startElement.getElementsByTagName("signalEventDefinition");
////		  			
////		  			//in Apromore
		  			if(message==null||message.getLength()==0)message = startElement.getElementsByTagName("ns4:messageEventDefinition");
		  			if(timer==null||timer.getLength()==0)timer = startElement.getElementsByTagName("ns4:timerEventDefinition");
		  			if(conditional==null||conditional.getLength()==0)conditional = startElement.getElementsByTagName("ns4:conditionalEventDefinition");
		  			if(signal==null||signal.getLength()==0)signal = startElement.getElementsByTagName("ns4:signalEventDefinition");
		  			
		  			if(message.getLength()+timer.getLength()+conditional.getLength()+signal.getLength()>1){
			    			if( (startElement.hasAttribute("parallelMultiple")) & (startElement.getAttribute("parallelMultiple").equals("true"))){
								CountStartParallelMultiple++;
							}else{
								CountStartMultiple++;
							}
		  			}else{
			    			if(message.getLength()>0){
			    					CountStartMessageEvent++;
			    				}else{
			    					if(timer.getLength()>0){CountStartTimerEvent++;
			    					}else{
			    						if(conditional.getLength()>0){CountStartConditionalEvent++; 
			    						}else{	
			    							if(signal.getLength()>0){CountStartSignalEvent++;	
			    							}else{
			    								CountStart++;
			    							}
			    						}
			        				}
			    			}
		  			}
		      	}
		      	
		      	//ENDEVENTS
		         	NodeList endEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), endQuery, XPathConstants.NODESET);
		      	for(int j=0; j<endEventNodeList.getLength();j++){
		      	
		      		Element endElement = (Element) endEventNodeList.item(j);
		      		
		      		//IN REALTA' NON E' UN ATTRIBUTO MA UN ELEMENTO FIGLIO!!!!!!!!!!!
		      		//dovrei per ogni end scorrere tutti gli elementi figli; se ne è 1 solo allora testo quale è, se ne è più di uno allora lo end è 
		      		//o un multiple o un parallel multiple
		      		
//		      		NodeList message = (NodeList) XMLUtils.execXPath(endElement, "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
//		      		NodeList signal = (NodeList) XMLUtils.execXPath(endElement, "./*[local-name()='signalEventDefinition']", XPathConstants.NODESET);
//		      		NodeList escalation = (NodeList) XMLUtils.execXPath(endElement, "./*[local-name()='escalationEventDefinition']", XPathConstants.NODESET);
//		      		NodeList error = (NodeList) XMLUtils.execXPath(endElement, "./*[local-name()='errorEventDefinition']", XPathConstants.NODESET);
//		      		NodeList cancel = (NodeList) XMLUtils.execXPath(endElement, "./*[local-name()='cancelEventDefinition']", XPathConstants.NODESET);
//		      		NodeList compensation = (NodeList) XMLUtils.execXPath(endElement, "./*[local-name()='compensateEventDefinition']", XPathConstants.NODESET);
//		      		NodeList terminate = (NodeList) XMLUtils.execXPath(endElement, "./*[local-name()='terminateEventDefinition']", XPathConstants.NODESET);
//					
		      		NodeList message = null;
		      		NodeList signal = null;
		      		NodeList escalation = null;
		      		NodeList error = null;
		      		NodeList cancel = null;
		      		NodeList compensation = null;
		      		NodeList terminate = null;
					
		      		
		      		//per eclipse
		      		if(message==null||message.getLength()==0)message = endElement.getElementsByTagName("bpmn2:messageEventDefinition");
		  			//removed conditional for ed
		      		if(signal==null||signal.getLength()==0)signal = endElement.getElementsByTagName("bpmn2:signalEventDefinition");
		  			//added for end
		      		if(escalation==null||escalation.getLength()==0)escalation = endElement.getElementsByTagName("bpmn2:escalationEventDefinition");
		      		if(error==null||error.getLength()==0)error = endElement.getElementsByTagName("bpmn2:errorEventDefinition");
		      		if(cancel==null||cancel.getLength()==0)cancel = endElement.getElementsByTagName("bpmn2:cancelEventDefinition");
		      		if(compensation==null||compensation.getLength()==0)compensation = endElement.getElementsByTagName("bpmn2:compensateEventDefinition");
		      		if(terminate==null||terminate.getLength()==0)terminate = endElement.getElementsByTagName("bpmn2:terminateEventDefinition");
		    			
		    		//per camunda
		      		if(message==null||message.getLength()==0)message = endElement.getElementsByTagName("bpmn:messageEventDefinition");
			  		//removed conditional for ed
		      		if(signal==null||signal.getLength()==0)signal = endElement.getElementsByTagName("bpmn:signalEventDefinition");
			  		//added for end
		      		if(escalation==null||escalation.getLength()==0)escalation = endElement.getElementsByTagName("bpmn:escalationEventDefinition");
		      		if(error==null||error.getLength()==0)error = endElement.getElementsByTagName("bpmn:errorEventDefinition");
		      		if(cancel==null||cancel.getLength()==0)cancel = endElement.getElementsByTagName("bpmn:cancelEventDefinition");
		      		if(compensation==null||compensation.getLength()==0)compensation = endElement.getElementsByTagName("bpmn:compensateEventDefinition");
		      		if(terminate==null||terminate.getLength()==0)terminate = endElement.getElementsByTagName("bpmn:terminateEventDefinition");
			    			
		    			
//		    		//per signavio
		      		if(message==null||message.getLength()==0)message = endElement.getElementsByTagName("messageEventDefinition");
			  		//removed conditional for ed
		      		if(signal==null||signal.getLength()==0)signal = endElement.getElementsByTagName("signalEventDefinition");
			  		//added for end
		      		if(escalation==null||escalation.getLength()==0)escalation = endElement.getElementsByTagName("escalationEventDefinition");
		      		if(error==null||error.getLength()==0)error = endElement.getElementsByTagName("errorEventDefinition");
		      		if(cancel==null||cancel.getLength()==0)cancel = endElement.getElementsByTagName("cancelEventDefinition");
		      		if(compensation==null||compensation.getLength()==0)compensation = endElement.getElementsByTagName("compensateEventDefinition");
		      		if(terminate==null||terminate.getLength()==0)terminate = endElement.getElementsByTagName("terminateEventDefinition");
//			    	
//		    		//per apromore
		      		if(message==null||message.getLength()==0)message = endElement.getElementsByTagName("ns4:messageEventDefinition");
			  		//removed conditional for ed
		      		if(signal==null||signal.getLength()==0)signal = endElement.getElementsByTagName("ns4:signalEventDefinition");
			  		//added for end
		      		if(escalation==null||escalation.getLength()==0)escalation = endElement.getElementsByTagName("ns4:escalationEventDefinition");
		      		if(error==null||error.getLength()==0)error = endElement.getElementsByTagName("ns4:errorEventDefinition");
		      		if(cancel==null||cancel.getLength()==0)cancel = endElement.getElementsByTagName("ns4:cancelEventDefinition");
		      		if(compensation==null||compensation.getLength()==0)compensation = endElement.getElementsByTagName("ns4:compensateEventDefinition");
		      		if(terminate==null||terminate.getLength()==0)terminate = endElement.getElementsByTagName("ns4:terminateEventDefinition");
		     			
		  			if(message.getLength()+signal.getLength()+escalation.getLength()+error.getLength()+cancel.getLength()+compensation.getLength()+terminate.getLength()>1){
								CountEndMultiple++;
		  			}else{
			    			if(message.getLength()>0){
			    					CountEndMessageEvent++;
			    			}else{

			    					if(signal.getLength()>0){CountEndSignalEvent++;	
			    					}else{
			    						if(escalation.getLength()>0){CountEndEscalationEvent++;	
			    						}else{
			    							if(error.getLength()>0){CountEndErrorEvent++;	
				    						}else{
				    							if(cancel.getLength()>0){CountEndCancelEvent++;	
					    						}else{
					    							if(compensation.getLength()>0){CountEndCompensationEvent++;	
						    						}else{
						    							if(terminate.getLength()>0){CountEndTerminateEvent++;	
						    							}else{
						    								CountEnd++;
						    							}
						    						}
					    						}
				    						}
			    						}   		
			    				}
			    			}
		  			}
		      	}
		      	
		    				       
		      	NodeList taskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), simpleTaskQuery, XPathConstants.NODESET);
			    CountTask+=taskEventNodeList.getLength();  
			    NodeList manualtaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), manualTaskQuery, XPathConstants.NODESET);
			    CountManualTask+=manualtaskEventNodeList.getLength();  
			    NodeList servicetaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), serviceTaskQuery, XPathConstants.NODESET);
			    CountServiceTask+=servicetaskEventNodeList.getLength();  
			    NodeList businessRuleTaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), businessRuleTaskQuery, XPathConstants.NODESET);
			    CountBusinessRuleTask+=businessRuleTaskEventNodeList.getLength();  
			    NodeList scriptTaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), scriptTaskQuery, XPathConstants.NODESET);
			    CountScriptTask+=scriptTaskEventNodeList.getLength();  
			    NodeList userTaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), userTaskQuery, XPathConstants.NODESET);
			    CountUserTask+=userTaskEventNodeList.getLength();  
			    NodeList sendTaskEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), sendTaskQuery, XPathConstants.NODESET);
			    CountSendTask+=sendTaskEventNodeList.getLength(); 
			    NodeList receiveEventNodeList =  (NodeList) XMLUtils.execXPath(PoolNodeList.item(z), receiveTaskQuery, XPathConstants.NODESET);
			    CountReceiveTask+=receiveEventNodeList.getLength(); 
		       
		       } 
			        
		       //SEQUENCE FLOW
			   NodeList sequenceFlowEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), sequenceFlowQuery, XPathConstants.NODESET);
			   CountSequenceFlow+=sequenceFlowEventNodeList.getLength(); 
			   //MESSAGE FLOW
			   NodeList messageFlowEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageFlowQuery, XPathConstants.NODESET);
			   CountMessageFlow+=messageFlowEventNodeList.getLength(); 
			        
			   //DATA OBJECT
			   NodeList dataObjectNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), dataObjectQuery, XPathConstants.NODESET);
			   CountDataObject+=dataObjectNodeList.getLength(); 	   
			   //DATA INPUT
			   NodeList dataInputNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), dataInputQuery, XPathConstants.NODESET);
			   CountDataInput+=dataInputNodeList.getLength(); 
			   //DATA OUTPUT
			   NodeList dataOutputNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), dataOutputQuery, XPathConstants.NODESET);
			   CountDataOutput+=dataOutputNodeList.getLength(); 
			   //DATA STORE REFERENCE
			   NodeList dataStoreNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), dataStoreReferenceQuery, XPathConstants.NODESET);
			   CountDataStore+=dataStoreNodeList.getLength(); 
			        
			   //MESSAGE messageQuery
			   NodeList messageNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), messageQuery, XPathConstants.NODESET);
			   CountMessage+=messageNodeList.getLength(); 
			        
			   //SUBPROCESSES
			   NodeList subProcessNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), subProcessQuery, XPathConstants.NODESET);
			   CountSubProcess+=subProcessNodeList.getLength(); 
			   NodeList adHocSubProcessNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), adHocSubProcessQuery, XPathConstants.NODESET);
			   CountAdHocSubProcess+=adHocSubProcessNodeList.getLength(); 
			   NodeList transactionNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), transactionQuery, XPathConstants.NODESET);
			   CountTransaction+=transactionNodeList.getLength(); 
			        	        
			   //LOOP  
			   NodeList loopNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), loopQuery, XPathConstants.NODESET);
			   CountLoop+=loopNodeList.getLength(); 
			        
			   //MULTIINSTANCE
			   NodeList multiInstanceNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), multiInstanceQuery, XPathConstants.NODESET); 
			   for(int j=0; j<multiInstanceNodeList.getLength();j++){
				   Element multiInstanceChild = (Element) multiInstanceNodeList.item(j);				  
				    	if((multiInstanceChild.hasAttribute("isSequential")) && (multiInstanceChild.getAttribute("isSequential").equals("true"))){	    		
				    		CountMultiInstanceSequential++; 
				    	}else{
				    		CountMultiInstanceParallel++;
				    	}
				    }
				    
			   //INTERMEDIATE THROW EVENT   
			   NodeList intermediateThrowEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), intermediateThrowEventQuery, XPathConstants.NODESET);
			                
			   for(int j=0; j<intermediateThrowEventNodeList.getLength();j++){
			        	
		       Element intermediateThrowElement = (Element) intermediateThrowEventNodeList.item(j);
		      		
		      		//per Eclipse Modeler
//		       NodeList message = (NodeList) XMLUtils.execXPath(intermediateThrowElement, "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
//		       NodeList escalation = (NodeList) XMLUtils.execXPath(intermediateThrowElement, "./*[local-name()='escalationEventDefinition']", XPathConstants.NODESET);
//		       NodeList link = (NodeList) XMLUtils.execXPath(intermediateThrowElement, "./*[local-name()='linkEventDefinition']", XPathConstants.NODESET);
//		       NodeList compensation = (NodeList) XMLUtils.execXPath(intermediateThrowElement, "./*[local-name()='compensateEventDefinition']", XPathConstants.NODESET);
//		       NodeList signal = (NodeList) XMLUtils.execXPath(intermediateThrowElement, "./*[local-name()='signalEventDefinition']", XPathConstants.NODESET);
//		        
		       
		       NodeList message = null;
		       NodeList escalation = null;
		       NodeList link = null;
		       NodeList compensation = null;
		       NodeList signal = null;
		       
		       if(message==null||message.getLength()==0)  message = intermediateThrowElement.getElementsByTagName("bpmn2:messageEventDefinition");
		       if(escalation==null||escalation.getLength()==0)  escalation = intermediateThrowElement.getElementsByTagName("bpmn2:escalationEventDefinition");
		       if(link==null||link.getLength()==0)  link = intermediateThrowElement.getElementsByTagName("bpmn2:linkEventDefinition");
		       if(compensation==null||compensation.getLength()==0)  compensation = intermediateThrowElement.getElementsByTagName("bpmn2:compensateEventDefinition");
		       if(signal==null||signal.getLength()==0)  signal = intermediateThrowElement.getElementsByTagName("bpmn2:signalEventDefinition");
		  			
		  			//per Camunda
		       if(message==null||message.getLength()==0)message = intermediateThrowElement.getElementsByTagName("bpmn:messageEventDefinition");
		       if(escalation==null||escalation.getLength()==0)escalation = intermediateThrowElement.getElementsByTagName("bpmn:escalationEventDefinition");
		       if(link==null||link.getLength()==0)link = intermediateThrowElement.getElementsByTagName("bpmn:linkEventDefinition");
		       if(compensation==null||compensation.getLength()==0)compensation = intermediateThrowElement.getElementsByTagName("bpmn:compensateEventDefinition");
		       if(signal==null||signal.getLength()==0)signal = intermediateThrowElement.getElementsByTagName("bpmn:signalEventDefinition");
		  			
//		  			//per Signavio
		      		 if(message==null||message.getLength()==0)  message = intermediateThrowElement.getElementsByTagName("messageEventDefinition");
		  			 if(escalation==null||escalation.getLength()==0)  escalation = intermediateThrowElement.getElementsByTagName("escalationEventDefinition");
		  			 if(link==null||link.getLength()==0)  link = intermediateThrowElement.getElementsByTagName("linkEventDefinition");
		     		 if(compensation==null||compensation.getLength()==0)  compensation = intermediateThrowElement.getElementsByTagName("compensateEventDefinition");
		  			 if(signal==null||signal.getLength()==0)  signal = intermediateThrowElement.getElementsByTagName("signalEventDefinition");
		  			
//		  			//per Apromore
		      		 if(message==null||message.getLength()==0)  message = intermediateThrowElement.getElementsByTagName("ns4:messageEventDefinition");
		  			 if(escalation==null||escalation.getLength()==0)  escalation = intermediateThrowElement.getElementsByTagName("ns4:escalationEventDefinition");
		  			 if(link==null||link.getLength()==0)  link = intermediateThrowElement.getElementsByTagName("ns4:linkEventDefinition");
		     		 if(compensation==null||compensation.getLength()==0)  compensation = intermediateThrowElement.getElementsByTagName("ns4:compensateEventDefinition");
		  			 if(signal==null||signal.getLength()==0)  signal = intermediateThrowElement.getElementsByTagName("ns4:signalEventDefinition");
		  			
		  					      		
		  			if(message.getLength()+signal.getLength()+escalation.getLength()+link.getLength()+compensation.getLength()>1){
								CountIntermediateThrowMultiple++;
		  			}else{
			    			if(message.getLength()>0){
			    					CountIntermediateThrowMessageEvent++;
			    			}else{

			    					if(signal.getLength()>0){CountIntermediateThrowSignalEvent++;	
			    					}else{
			    						if(escalation.getLength()>0){CountIntermediateThrowEscalationEvent++;	
			    						}else{
			    							
				    							if(link.getLength()>0){CountIntermediateThrowLinkEvent++;	
					    						}else{
					    							if(compensation.getLength()>0){CountIntermediateThrowCompensationEvent++;	
						    						}else{
						    							
						    								CountIntermediateThrowEvent++;				    							
						    						}
					    						}
			    						}   		
			    				}
			    			}
		  			}
		  			
			        }
			        
		  	    //INTERMEDIATE CATCH EVENT
			        NodeList intermediateCatchEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), intermediateCatchEventQuery, XPathConstants.NODESET);
			       
			        for(int j=0; j<intermediateCatchEventNodeList.getLength();j++){
			        	
		      		Element intermediateCatchElement = (Element) intermediateCatchEventNodeList.item(j);
		      		
		      		//IN REALTA' NON E' UN ATTRIBUTO MA UN ELEMENTO FIGLIO!!!!!!!!!!!
		      		//dovrei per ogni start scorrere tutti gli elementi figli; se ne è 1 solo allora testo quale è, se ne è più di uno allora lo start è 
		      		//o un multiple o un parallel multiple
//		      		NodeList message = (NodeList) XMLUtils.execXPath(intermediateCatchElement, "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
//		      		NodeList timer = (NodeList) XMLUtils.execXPath(intermediateCatchElement, "./*[local-name()='timerEventDefinition']", XPathConstants.NODESET);
//		      		NodeList conditional = (NodeList) XMLUtils.execXPath(intermediateCatchElement, "./*[local-name()='conditionalEventDefinition']", XPathConstants.NODESET);
//		      		NodeList link = (NodeList) XMLUtils.execXPath(intermediateCatchElement, "./*[local-name()='linkEventDefinition']", XPathConstants.NODESET);
//		      		NodeList signal = (NodeList) XMLUtils.execXPath(intermediateCatchElement, "./*[local-name()='signalEventDefinition']", XPathConstants.NODESET);
//		   
		      		NodeList message = null;
		      		NodeList timer = null;
		      		NodeList conditional = null;
		      		NodeList link = null;
		      		NodeList signal = null;
		   
		      		 
//					//per eclipse	modeler
		      		if(message==null||message.getLength()==0) message = intermediateCatchElement.getElementsByTagName("bpmn2:messageEventDefinition");
		      		if(timer==null||timer.getLength()==0) timer = intermediateCatchElement.getElementsByTagName("bpmn2:timerEventDefinition");
		  			if(conditional==null||conditional.getLength()==0) conditional = intermediateCatchElement.getElementsByTagName("bpmn2:conditionalEventDefinition");
		  			if(link==null||link.getLength()==0) link = intermediateCatchElement.getElementsByTagName("bpmn2:linkEventDefinition");
		  			if(signal==null||signal.getLength()==0) signal = intermediateCatchElement.getElementsByTagName("bpmn2:signalEventDefinition");
		  			
//		  			//per Camunda
		  			if(message==null||message.getLength()==0)message = intermediateCatchElement.getElementsByTagName("bpmn:messageEventDefinition");
		  			if(timer==null||timer.getLength()==0)timer = intermediateCatchElement.getElementsByTagName("bpmn:timerEventDefinition");
		  			if(conditional==null||conditional.getLength()==0)conditional = intermediateCatchElement.getElementsByTagName("bpmn:conditionalEventDefinition");
		  			if(link==null||link.getLength()==0)link = intermediateCatchElement.getElementsByTagName("bpmn:linkEventDefinition");
		  			if(signal==null||signal.getLength()==0)signal = intermediateCatchElement.getElementsByTagName("bpmn:signalEventDefinition");
		  			
//		  			//per Signavio
		  			if(message==null||message.getLength()==0) message = intermediateCatchElement.getElementsByTagName("messageEventDefinition");
		  			if(timer==null||timer.getLength()==0) timer = intermediateCatchElement.getElementsByTagName("timerEventDefinition");
		  			if(conditional==null||conditional.getLength()==0) conditional = intermediateCatchElement.getElementsByTagName("conditionalEventDefinition");
		  			if(link==null||link.getLength()==0) link = intermediateCatchElement.getElementsByTagName("linkEventDefinition");
		  			if(signal==null||signal.getLength()==0) signal = intermediateCatchElement.getElementsByTagName("signalEventDefinition");
//		  			
//		  			//per Apromore
		  			if(message==null||message.getLength()==0) message = intermediateCatchElement.getElementsByTagName("ns4:messageEventDefinition");
		  			if(timer==null||timer.getLength()==0) timer = intermediateCatchElement.getElementsByTagName("ns4:timerEventDefinition");
		  			if(conditional==null||conditional.getLength()==0) conditional = intermediateCatchElement.getElementsByTagName("ns4:conditionalEventDefinition");
		  			if(link==null||link.getLength()==0) link = intermediateCatchElement.getElementsByTagName("ns4:linkEventDefinition");
		  			if(signal==null||signal.getLength()==0) signal = intermediateCatchElement.getElementsByTagName("ns4:signalEventDefinition");
		  				  			
		  			/*		  			
		  			CAMBIARE GLI IF ELSE CON 		  			
		  			if(message.getLength()>0){	
			    					CountIntermediateCatchMessageEvent++;
			    		}else if(timer.getLength()>0){CountIntermediateCatchTimerEvent++;			    			
						}else if(link.getLength()>0){CountIntermediateCatchLinkEvent++;		
						}else if(conditional.getLength()>0){CountIntermediateCatchConditionalEvent++; 	
						}else if(signal.getLength()>0){CountIntermediateCatchSignalEvent++;		
						}else CountIntermediateCatchEvent++;		
		  			*/
		  			
		  			if(message.getLength()+timer.getLength()+conditional.getLength()+signal.getLength()+link.getLength()>1){
			    			if( (intermediateCatchElement.hasAttribute("parallelMultiple")) & (intermediateCatchElement.getAttribute("parallelMultiple").equals("true"))){
								CountIntermediateCatchParallelMultiple++;
							}else{
								CountIntermediateCatchMultiple++;
							}
		  			}else{		    			
		  				if(message.getLength()>0){			    					
		  					CountIntermediateCatchMessageEvent++;
			    			}else{
			    				if(timer.getLength()>0){CountIntermediateCatchTimerEvent++;
			    				}else{
			    					if(link.getLength()>0){CountIntermediateCatchLinkEvent++;	
			    					}else{
			    						if(conditional.getLength()>0){CountIntermediateCatchConditionalEvent++; 
			    						}else{	
			    							if(signal.getLength()>0){CountIntermediateCatchSignalEvent++;	
			    							}else{
			    								CountIntermediateCatchEvent++;
			    							}	
			    						}
			    					}
			    				}
			    			}
		  			}

			        }

			        //BOUNDARY EVENTS boundaryEventQuery
			        NodeList boundaryEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), boundaryEventQuery, XPathConstants.NODESET);
				       
			        for(int j=0; j<boundaryEventNodeList.getLength();j++){
			        	
		      		Element boundaryEventElement = (Element) boundaryEventNodeList.item(j);
		      		
		      		if( (boundaryEventElement.hasAttribute("cancelActivity")) & (boundaryEventElement.getAttribute("cancelActivity").equals("false"))){
							
		      //NON INTERRUPTING
//		      			NodeList message = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
//		      			NodeList timer = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='timerEventDefinition']", XPathConstants.NODESET);
//		      			NodeList escalation = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='escalationEventDefinition']", XPathConstants.NODESET);
//		      			NodeList conditional = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='conditionalEventDefinition']", XPathConstants.NODESET);
//		      			NodeList signal = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='signalEventDefinition']", XPathConstants.NODESET);
//		      			
		      			NodeList message = null;
		      			NodeList timer = null;
		      			NodeList escalation = null;
		      			NodeList conditional = null;
		      			NodeList signal = null;
		      			
		      			//per eclipse modeler
							if(message==null||message.getLength()==0)message = boundaryEventElement.getElementsByTagName("bpmn2:messageEventDefinition");
							if(timer==null||timer.getLength()==0)timer = boundaryEventElement.getElementsByTagName("bpmn2:timerEventDefinition");
							if(escalation==null||escalation.getLength()==0)escalation = boundaryEventElement.getElementsByTagName("bpmn2:escalationEventDefinition");
							if(conditional==null||conditional.getLength()==0)conditional = boundaryEventElement.getElementsByTagName("bpmn2:conditionalEventDefinition");
							if(signal==null||signal.getLength()==0)signal = boundaryEventElement.getElementsByTagName("bpmn2:signalEventDefinition");
			    			
			    			//per camunda
							if(message==null||message.getLength()==0)message = boundaryEventElement.getElementsByTagName("bpmn:messageEventDefinition");
							if(timer==null||timer.getLength()==0)timer = boundaryEventElement.getElementsByTagName("bpmn:timerEventDefinition");
							if(escalation==null||escalation.getLength()==0)escalation = boundaryEventElement.getElementsByTagName("bpmn:escalationEventDefinition");
							if(conditional==null||conditional.getLength()==0)conditional = boundaryEventElement.getElementsByTagName("bpmn:conditionalEventDefinition");
							if(signal==null||signal.getLength()==0)signal = boundaryEventElement.getElementsByTagName("bpmn:signalEventDefinition");
		      			
			    			//per signavio
							if(message==null||message.getLength()==0)message = boundaryEventElement.getElementsByTagName("messageEventDefinition");
							if(timer==null||timer.getLength()==0)timer = boundaryEventElement.getElementsByTagName("timerEventDefinition");
							if(escalation==null||escalation.getLength()==0)escalation = boundaryEventElement.getElementsByTagName("escalationEventDefinition");
							if(conditional==null||conditional.getLength()==0)conditional = boundaryEventElement.getElementsByTagName("conditionalEventDefinition");
							if(signal==null||signal.getLength()==0)signal = boundaryEventElement.getElementsByTagName("signalEventDefinition");
			    			
			    			//per Apromore
							if(message==null||message.getLength()==0)message = boundaryEventElement.getElementsByTagName("ns4:messageEventDefinition");
							if(timer==null||timer.getLength()==0)timer = boundaryEventElement.getElementsByTagName("ns4:timerEventDefinition");
							if(escalation==null||escalation.getLength()==0)escalation = boundaryEventElement.getElementsByTagName("ns4:escalationEventDefinition");
							if(conditional==null||conditional.getLength()==0)conditional = boundaryEventElement.getElementsByTagName("ns4:conditionalEventDefinition");
							if(signal==null||signal.getLength()==0)signal = boundaryEventElement.getElementsByTagName("ns4:signalEventDefinition");
//		      			
			    			if(message.getLength()+timer.getLength()+escalation.getLength()+conditional.getLength()+signal.getLength()>1){
				    			if((boundaryEventElement.hasAttribute("parallelMultiple")) & (boundaryEventElement.getAttribute("parallelMultiple").equals("true"))){
									CountBoundaryNonInterruptingParallelMultiple++;
								}else{
									CountBoundaryNonInterruptingMultiple++;
								}
			    			}else{
				    			if(message.getLength()>0){
				    					CountBoundaryNonInterruptingMessageEvent++;
				    			}else{
				    				if(timer.getLength()>0){CountBoundaryNonInterruptingTimerEvent++;
				    				}else{
				    					if(escalation.getLength()>0){CountBoundaryNonInterruptingEscalationEvent++;	
				    					}else{
				    						if(conditional.getLength()>0){CountBoundaryNonInterruptingConditionalEvent++; 
				    						}else{	
							    				if(signal.getLength()>0){CountBoundaryNonInterruptingSignalEvent++;	
							    				}else{
							    					CountBoundaryNonInterruptingEvent++;
							    				}	
							    						
				    						}
				    					}
				    				}
				    			}
			    			}
		      			
		      			
		      			
		      			
		      			
						}else{
							//INTERRUPTING
//							NodeList message = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='messageEventDefinition']", XPathConstants.NODESET);
//			      			NodeList timer = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='timerEventDefinition']", XPathConstants.NODESET);
//			      			NodeList escalation = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='escalationEventDefinition']", XPathConstants.NODESET);
//			      			NodeList conditional = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='conditionalEventDefinition']", XPathConstants.NODESET);
//			      			NodeList signal = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='signalEventDefinition']", XPathConstants.NODESET);
//			      			NodeList error = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='errorEventDefinition']", XPathConstants.NODESET);
//			      			NodeList cancel = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='cancelEventDefinition']", XPathConstants.NODESET);
//			      			NodeList compensation = (NodeList) XMLUtils.execXPath(boundaryEventElement, "./*[local-name()='compensationEventDefinition']", XPathConstants.NODESET);
//							       	
			      			NodeList message = null;
			      			NodeList timer = null;
			      			NodeList escalation = null;
			      			NodeList conditional = null;
			      			NodeList signal = null;
			      			NodeList error = null;
			      			NodeList cancel = null;
			      			NodeList compensation = null;
							 
							//per eclipse modeler
							if(message==null||message.getLength()==0)message = boundaryEventElement.getElementsByTagName("bpmn2:messageEventDefinition");
							if(timer==null||timer.getLength()==0)timer = boundaryEventElement.getElementsByTagName("bpmn2:timerEventDefinition");
							if(escalation==null||escalation.getLength()==0)escalation = boundaryEventElement.getElementsByTagName("bpmn2:escalationEventDefinition");
							if(conditional==null||conditional.getLength()==0)conditional = boundaryEventElement.getElementsByTagName("bpmn2:conditionalEventDefinition");
							if(error==null||error.getLength()==0)error = boundaryEventElement.getElementsByTagName("bpmn2:errorEventDefinition");
							if(cancel==null||cancel.getLength()==0)cancel = boundaryEventElement.getElementsByTagName("bpmn2:cancelEventDefinition");
							if(compensation==null||compensation.getLength()==0)compensation = boundaryEventElement.getElementsByTagName("bpmn2:compensationEventDefinition");
							if(signal==null||signal.getLength()==0)signal = boundaryEventElement.getElementsByTagName("bpmn2:signalEventDefinition");
			    			
			    			//per Camunda
							if(message==null||message.getLength()==0)message = boundaryEventElement.getElementsByTagName("bpmn:messageEventDefinition");
							if(timer==null||timer.getLength()==0)timer = boundaryEventElement.getElementsByTagName("bpmn:timerEventDefinition");
							if(escalation==null||escalation.getLength()==0)escalation = boundaryEventElement.getElementsByTagName("bpmn:escalationEventDefinition");
							if(conditional==null||conditional.getLength()==0)conditional = boundaryEventElement.getElementsByTagName("bpmn:conditionalEventDefinition");
							if(error==null||error.getLength()==0)error = boundaryEventElement.getElementsByTagName("bpmn:errorEventDefinition");
							if(cancel==null||cancel.getLength()==0)cancel = boundaryEventElement.getElementsByTagName("bpmn:cancelEventDefinition");
							if(compensation==null||compensation.getLength()==0)compensation = boundaryEventElement.getElementsByTagName("bpmn:compensationEventDefinition");
							if(signal==null||signal.getLength()==0)signal = boundaryEventElement.getElementsByTagName("bpmn:signalEventDefinition");
			    			
			    			//per signavio modeler
							if(message==null||message.getLength()==0)message = boundaryEventElement.getElementsByTagName("messageEventDefinition");
							if(timer==null||timer.getLength()==0)timer = boundaryEventElement.getElementsByTagName("timerEventDefinition");
							if(escalation==null||escalation.getLength()==0)escalation = boundaryEventElement.getElementsByTagName("escalationEventDefinition");
							if(conditional==null||conditional.getLength()==0)conditional = boundaryEventElement.getElementsByTagName("conditionalEventDefinition");
							if(error==null||error.getLength()==0)error = boundaryEventElement.getElementsByTagName("errorEventDefinition");
							if(cancel==null||cancel.getLength()==0)cancel = boundaryEventElement.getElementsByTagName("cancelEventDefinition");
							if(compensation==null||compensation.getLength()==0)compensation = boundaryEventElement.getElementsByTagName("compensationEventDefinition");
							if(signal==null||signal.getLength()==0)signal = boundaryEventElement.getElementsByTagName("signalEventDefinition");
			    			
			    			//per Apromore
							if(message==null||message.getLength()==0)message = boundaryEventElement.getElementsByTagName("ns4:messageEventDefinition");
							if(timer==null||timer.getLength()==0)timer = boundaryEventElement.getElementsByTagName("ns4:timerEventDefinition");
							if(escalation==null||escalation.getLength()==0)escalation = boundaryEventElement.getElementsByTagName("ns4:escalationEventDefinition");
							if(conditional==null||conditional.getLength()==0)conditional = boundaryEventElement.getElementsByTagName("ns4:conditionalEventDefinition");
							if(error==null||error.getLength()==0)error = boundaryEventElement.getElementsByTagName("ns4:errorEventDefinition");
							if(cancel==null||cancel.getLength()==0)cancel = boundaryEventElement.getElementsByTagName("ns4:cancelEventDefinition");
							if(compensation==null||compensation.getLength()==0)compensation = boundaryEventElement.getElementsByTagName("ns4:compensationEventDefinition");
							if(signal==null||signal.getLength()==0)signal = boundaryEventElement.getElementsByTagName("ns4:signalEventDefinition");
			    			
			    			if(message.getLength()+timer.getLength()+escalation.getLength()+conditional.getLength()+error.getLength()+cancel.getLength()+compensation.getLength()+signal.getLength()>1){
				    			if((boundaryEventElement.hasAttribute("parallelMultiple")) & (boundaryEventElement.getAttribute("parallelMultiple").equals("true"))){
									CountBoundaryInterruptingParallelMultiple++;
								}else{
									CountBoundaryInterruptingMultiple++;
								}
			    			}else{
				    			if(message.getLength()>0){
				    					CountBoundaryInterruptingMessageEvent++;
				    			}else{
				    				if(timer.getLength()>0){CountBoundaryInterruptingTimerEvent++;
				    				}else{
				    					if(escalation.getLength()>0){CountBoundaryInterruptingEscalationEvent++;	
				    					}else{
				    						if(conditional.getLength()>0){CountBoundaryInterruptingConditionalEvent++; 
				    						}else{	
				    							if(error.getLength()>0){CountBoundaryInterruptingErrorEvent++; 
					    						}else{
					    							if(cancel.getLength()>0){CountBoundaryInterruptingCancelEvent++; 
						    						}else{
						    							if(compensation.getLength()>0){CountBoundaryInterruptingCompensationEvent++; 
							    						}else{
							    							if(signal.getLength()>0){CountBoundaryInterruptingSignalEvent++;	
							    							}else{
							    								CountBoundaryInterruptingEvent++;
							    							}	
							    						}
						    							
						    						}
					    						}
				    						}
				    					}
				    				}
				    			}
			    			}
			
				        }
				        
				        }        
				        
			        
			        //GATEWAYS
			        //XOR
			        NodeList exclusiveGatewayEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), exclusiveGatewayQuery, XPathConstants.NODESET);
			        for(int i=0; i<exclusiveGatewayEventNodeList.getLength(); i++){
			        	Element eElement = (Element) exclusiveGatewayEventNodeList.item(i);
			        	
			        	if (eElement.hasAttribute("gatewayDirection")){
				        	if (eElement.getAttribute("gatewayDirection").equals("Diverging")){
				        		CountXorSplit++;
				        	}else{
				        		if (eElement.getAttribute("gatewayDirection").equals("Converging")){
					        		CountXorJoin++;
				        		}else{
					        		if (eElement.getAttribute("gatewayDirection").equals("Mixed")){
						        		CountXorMixed++;
					        		}
				        		}
				        	}
			        	}else{
				        	CountXorUnspecified++;	
			        	 ////System.out.println("NO SPECIFIED");
			        	} 	
			        	
			        	if ((eElement.hasAttribute("gatewayDirection"))&&(eElement.getAttribute("gatewayDirection").equals("Unspecified")))CountXorUnspecified++;	
			        }

			        
			        //AND			        
			        NodeList parallelGatewayEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), parallelGatewayQuery, XPathConstants.NODESET);
			        for(int i=0; i<parallelGatewayEventNodeList.getLength(); i++){
			        	Element eElement = (Element) parallelGatewayEventNodeList.item(i);
			        	

			        	if (eElement.hasAttribute("gatewayDirection")){
			        		
			        		if (eElement.getAttribute("gatewayDirection").equals("Diverging")){
			        			CountAndSplit++;
			        		}else{
			        			if (eElement.getAttribute("gatewayDirection").equals("Converging")){
			        				CountAndJoin++;
			        			}else{
			        				if (eElement.getAttribute("gatewayDirection").equals("Mixed")){
			        					CountAndMixed++;
			        				}
			        			}
			        		}
			        	}else{
			        		CountAndUnspecified++;
			        	}
			        	if ((eElement.hasAttribute("gatewayDirection"))&&(eElement.getAttribute("gatewayDirection").equals("Unspecified")))CountAndUnspecified++;
			        }
			        
			        NodeList inclusiveGatewayEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), inclusiveGatewayQuery, XPathConstants.NODESET);
			        for(int i=0; i<inclusiveGatewayEventNodeList.getLength(); i++){
			        	Element eElement = (Element) inclusiveGatewayEventNodeList.item(i);
			        	if (eElement.hasAttribute("gatewayDirection")){
			        		if (eElement.getAttribute("gatewayDirection").equals("Diverging")){
			        			CountOrSplit++;
			        		}else{
			        			if (eElement.getAttribute("gatewayDirection").equals("Converging")){
			        				CountOrJoin++;
		      			}else{
		      				if (eElement.getAttribute("gatewayDirection").equals("Mixed")){
		      					CountOrMixed++;
		      				}
		      			}
			        	}
			        	}else{
			        		CountOrUnspecified++;
			        	} 
			        	if ((eElement.hasAttribute("gatewayDirection"))&&(eElement.getAttribute("gatewayDirection").equals("Unspecified")))CountOrUnspecified++;
			        }
			        
			        NodeList eventBasedGatewayEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), eventBasedGatewayQuery, XPathConstants.NODESET);
			        for(int i=0; i<eventBasedGatewayEventNodeList.getLength(); i++){
			        	Element eElement = (Element) eventBasedGatewayEventNodeList.item(i);
			        
			        	
			        	//EVENT BASED SE NO INSTANTIEATE
			        	if (eElement.hasAttribute("gatewayDirection")&& (! eElement.hasAttribute("instantiate")) ){      
			        		if (eElement.getAttribute("gatewayDirection").equals("Diverging")){
			        			CountEventBasedSplit++;
			        		}else{
			        			if (eElement.getAttribute("gatewayDirection").equals("Converging")){
			        				CountEventBasedJoin++;
			        			}else{
			        				if (eElement.getAttribute("gatewayDirection").equals("Mixed")){
			        					CountEventBasedMixed++;
			        				}
			        			}
			        		}
			        	}
			        	
			        	if (eElement.hasAttribute("gatewayDirection")&& (eElement.hasAttribute("instantiate")) && (eElement.getAttribute("instantiate").equals("false")) ){      
			        		if (eElement.getAttribute("gatewayDirection").equals("Diverging")){
			        			CountEventBasedSplit++;
			        		}else{
			        			if (eElement.getAttribute("gatewayDirection").equals("Converging")){
			        				CountEventBasedJoin++;
			        			}else{
			        				if (eElement.getAttribute("gatewayDirection").equals("Mixed")){
			        					CountEventBasedMixed++;
			        				}
			        			}
			        		}
			        	}
			        	

			        	if (!(eElement.hasAttribute("gatewayDirection")) && (! eElement.hasAttribute("instantiate"))){
			        			CountEventBasedUnspecified++;
			        	}else{
			        		if ((eElement.hasAttribute("gatewayDirection"))&&(eElement.getAttribute("gatewayDirection").equals("Unspecified")))CountEventBasedUnspecified++;
			        	}
			        	
			        	if ((eElement.hasAttribute("gatewayDirection")) & (eElement.hasAttribute("instantiate")) & ((eElement.hasAttribute("eventGatewayType")))){
				        	if ((eElement.getAttribute("gatewayDirection").equals("Diverging")) && eElement.getAttribute("eventGatewayType").equals("Parallel")){
				        		CountParallelEventBasedSplit++;
				        	}
				        	if ((eElement.getAttribute("gatewayDirection").equals("Merging")) && eElement.getAttribute("eventGatewayType").equals("Parallel")){
				        		CountParallelEventBasedJoin++;
				        	}
				        	if ((eElement.getAttribute("gatewayDirection").equals("Mixed")) && eElement.getAttribute("eventGatewayType").equals("Parallel")){
				        		CountParallelEventBasedMixed++;
				        	}
				        	
			        	}

	
			        	if ((eElement.hasAttribute("gatewayDirection")) & ((eElement.hasAttribute("instantiate"))) & ((!eElement.hasAttribute("eventGatewayType")))){
				        	if ((eElement.getAttribute("gatewayDirection").equals("Diverging"))){
				        		CountExclusiveEventBasedSplit++;
				        	}
				        	if ((eElement.getAttribute("gatewayDirection").equals("Merging"))){
				        		CountExclusiveEventBasedJoin++;
				        	}
				        	if ((eElement.getAttribute("gatewayDirection").equals("Mixed"))){
				        		CountExclusiveEventBasedMixed++;
				        	}
			        	}
			        	
			        	if (!(eElement.hasAttribute("gatewayDirection")) && ( eElement.hasAttribute("instantiate"))){
		      			if (eElement.hasAttribute("eventGatewayType") && eElement.getAttribute("eventGatewayType").equals("Parallel")){
		      					CountParallelEventBasedUnspecified++;
		      			}
		      			if (!eElement.hasAttribute("eventGatewayType")){
		  					CountExclusiveEventBasedUnspecified++;
		      			}
		  			}
			        		
			        } 
			        
			        //DA ERRORE PERCHE?
			        NodeList complexGatewayEventNodeList =  (NodeList) XMLUtils.execXPath(inputModel.getDocumentElement(), complexGatewayQuery, XPathConstants.NODESET);
			        for(int i=0; i<complexGatewayEventNodeList.getLength(); i++){
			        	Element eElement = (Element) complexGatewayEventNodeList.item(i);
			        	if (!eElement.hasAttribute("gatewayDirection")){
			        		CountComplexUnspecified++;
			        	}else{
			        		if ((eElement.hasAttribute("gatewayDirection")) && (eElement.getAttribute("gatewayDirection").equals("Diverging"))){
			        			CountComplexSplit++;
			        		}else{
			        			if ((eElement.hasAttribute("gatewayDirection")) && (eElement.getAttribute("gatewayDirection").equals("Converging"))){
			        				CountComplexJoin++;
			        			}else{
			        				if ((eElement.hasAttribute("gatewayDirection")) && (eElement.getAttribute("gatewayDirection").equals("Mixed"))){
			        					CountComplexMixed++;       					        	
			        				}
			        			}
			        		}
			        	}
			        	if ((eElement.hasAttribute("gatewayDirection"))&&(eElement.getAttribute("gatewayDirection").equals("Unspecified")))CountComplexUnspecified++;
			        	
			        }
			        
			        if(CountEmptyPool!=0)result=result + "Empty Pool: "+CountEmptyPool+"\n";
//ENABLED BECAUSE WE DO NOT CONSIDER DATA IN OUR SEMANTICS SO THEY DO NOT AFFLICT CONTROL FLOW, JUST SKIP THEM			        
//			        if(CountDataObject!=0)result=result + "Data Object: "+CountDataObject+"\n";
//			        if(CountDataInput!=0)result=result + "Data Input: "+CountDataInput+"\n";
//			        if(CountDataOutput!=0)result=result + "Data Output: "+CountDataOutput+"\n";
//			        if(CountDataStore!=0)result=result + "Data Store: "+CountDataStore+"\n";
			        
			        if(CountStartTimerEvent!=0)result=result + "Start Timer Event: "+CountStartTimerEvent+"\n";
			        if(CountStartConditionalEvent!=0)result=result + "Start Conditional Event: "+CountStartConditionalEvent+"\n";
			        if(CountStartSignalEvent!=0)result=result + "Start Signal Event: "+CountStartSignalEvent+"\n";
			        if(CountStartMultiple!=0)result=result + "Start Multiple Event: "+CountStartMultiple+"\n";
			        if(CountStartParallelMultiple!=0)result=result + "Start Parallel Multiple Event: "+CountStartParallelMultiple+"\n";
     
			        if(CountEndEscalationEvent!=0)result=result + "End Escalation Event: "+CountEndEscalationEvent+"\n";
			        if(CountEndSignalEvent!=0)result=result + "End Signal Event: "+CountEndSignalEvent+"\n";
			        if(CountEndErrorEvent!=0)result=result + "End Error Event: "+CountEndErrorEvent+"\n";
			        if(CountEndCancelEvent!=0)result=result + "End Cancel Event: "+CountEndCancelEvent+"\n";
			        if(CountEndCompensationEvent!=0)result=result + "End Compensation Event: "+CountEndCompensationEvent+"\n";
			        if(CountEndMultiple!=0)result=result + "End Multiple Event: "+CountEndMultiple+"\n";
			        
//ENABLED BECAUSE WE MAP THEM TO SIMPLE TASKS			        
//			        if(CountManualTask!=0)result=result + "Manual Task: "+CountManualTask+"\n";
//			        if(CountServiceTask!=0)result=result + "Service Task: "+CountServiceTask+"\n";
//			        if(CountBusinessRuleTask!=0)result=result + "Business Task: "+CountBusinessRuleTask+"\n";
//			        if(CountScriptTask!=0)result=result + "Script Task: "+CountScriptTask+"\n";
//			        if(CountUserTask!=0)result=result + "User Task: "+CountUserTask+"\n";

			        if(CountSubProcess!=0)result=result + "Sub Process: "+CountSubProcess+"\n";
			        if(CountAdHocSubProcess!=0)result=result + "Ad Hoc Sub Process: "+CountAdHocSubProcess+"\n";
			        if(CountTransaction!=0)result=result + "Transaction: "+CountTransaction+"\n";
			        //if(CountIntermediateThrowEvent!=0)result=result + "Intermediate Throw Event: "+CountIntermediateThrowEvent+"\n";
			        if(CountIntermediateThrowMultiple!=0)result=result + "Intermediate Throw Multiple Event: "+CountIntermediateThrowMultiple+"\n";

			        if(CountIntermediateThrowSignalEvent!=0)result=result + "Intermediate Throw Signal Event: "+CountIntermediateThrowSignalEvent+"\n";
			        if(CountIntermediateThrowEscalationEvent!=0)result=result + "Intermediate Throw Escalation Event: "+CountIntermediateThrowEscalationEvent+"\n";
			        if(CountIntermediateThrowLinkEvent!=0)result=result + "Intermediate Throw Link Event: "+CountIntermediateThrowLinkEvent+"\n";
			        if(CountIntermediateThrowCompensationEvent!=0)result=result + "Intermediate Throw Compensation Event: "+CountIntermediateThrowCompensationEvent+"\n";
			        
			        //if(CountIntermediateCatchEvent!=0)result=result + "Intermediate Catch Event: "+CountIntermediateCatchEvent+"\n";
			        if(CountIntermediateCatchMultiple!=0)result=result + "Intermediate Catch Multiple Event: "+CountIntermediateCatchMultiple+"\n";
			        if(CountIntermediateCatchParallelMultiple!=0)result=result + "Intermediate Catch Parallel Event: "+CountIntermediateCatchParallelMultiple+"\n";

			        if(CountIntermediateCatchConditionalEvent!=0)result=result + "Intermediate Catch Conditional Event: "+CountIntermediateCatchConditionalEvent+"\n";
			        if(CountIntermediateCatchLinkEvent!=0)result=result + "Intermediate Catch Link Event: "+CountIntermediateCatchLinkEvent+"\n";
			        if(CountIntermediateCatchTimerEvent!=0)result=result + "Intermediate Catch Timer Event: "+CountIntermediateCatchTimerEvent+"\n";
			        if(CountIntermediateCatchSignalEvent!=0)result=result + "Intermediate Catch Signal Event: "+CountIntermediateCatchSignalEvent+"\n";		        
			        
			        if(CountBoundaryInterruptingMessageEvent!=0)result=result + "Intermediate Boundary Interrupting Message Event: "+CountBoundaryInterruptingMessageEvent+"\n";
			        if(CountBoundaryInterruptingTimerEvent!=0)result=result + "Intermediate Boundary Interrupting Timer Event: "+CountBoundaryInterruptingTimerEvent+"\n";
			        if(CountBoundaryInterruptingConditionalEvent!=0)result=result + "Intermediate Boundary Interrupting Conditional Event: "+CountBoundaryInterruptingConditionalEvent+"\n";
			        if(CountBoundaryInterruptingEscalationEvent!=0)result=result + "Intermediate Boundary Interrupting Escalation Event: "+CountBoundaryInterruptingEscalationEvent+"\n";
			        if(CountBoundaryInterruptingErrorEvent!=0)result=result + "Intermediate Boundary Interrupting Error Event: "+CountBoundaryInterruptingErrorEvent+"\n";
			        if(CountBoundaryInterruptingCancelEvent!=0)result=result + "Intermediate Boundary Interrupting Cancel Event: "+CountBoundaryInterruptingCancelEvent+"\n";
			        if(CountBoundaryInterruptingCompensationEvent!=0)result=result + "Intermediate Boundary Interrupting Compensation Event: "+CountBoundaryInterruptingCompensationEvent+"\n";
			        if(CountBoundaryInterruptingSignalEvent!=0)result=result + "Intermediate Boundary Interrupting Signal Event: "+CountBoundaryInterruptingSignalEvent+"\n";
			        if(CountBoundaryInterruptingMultiple!=0)result=result + "Intermediate Boundary Interrupting Multiple Event: "+CountBoundaryInterruptingMultiple+"\n";
			        if(CountBoundaryInterruptingParallelMultiple!=0)result=result + "Intermediate Boundary Interrupting Parallel Multiple Event: "+CountBoundaryInterruptingParallelMultiple+"\n";	        
			        
			        if(CountBoundaryNonInterruptingEvent!=0)result=result + "Intermediate Boundary Non Interrupting Event: "+CountBoundaryNonInterruptingEvent+"\n";
			        if(CountBoundaryNonInterruptingMessageEvent!=0)result=result + "Intermediate Boundary Non Interrupting Message Event: "+CountBoundaryNonInterruptingMessageEvent+"\n";
			        if(CountBoundaryNonInterruptingTimerEvent!=0)result=result + "Intermediate Boundary Non Interrupting Timer Event: "+CountBoundaryNonInterruptingTimerEvent+"\n";
			        if(CountBoundaryNonInterruptingConditionalEvent!=0)result=result + "Intermediate Boundary Non Interrupting Conditional Event: "+CountBoundaryNonInterruptingConditionalEvent+"\n";
			        if(CountBoundaryNonInterruptingEscalationEvent!=0)result=result + "Intermediate Boundary Non Interrupting Escalation Event: "+CountBoundaryNonInterruptingEscalationEvent+"\n";
			        if(CountBoundaryNonInterruptingSignalEvent!=0)result=result + "Intermediate Boundary Non Interrupting Signal Event: "+CountBoundaryNonInterruptingSignalEvent+"\n";
			        if(CountBoundaryNonInterruptingMultiple!=0)result=result + "Intermediate Boundary Non Interrupting Multiple Event: "+CountBoundaryNonInterruptingMultiple+"\n";
			        if(CountBoundaryNonInterruptingParallelMultiple!=0)result=result + "Intermediate Boundary Non Interrupting Parallel Event: "+CountBoundaryNonInterruptingParallelMultiple+"\n";
			       			        
			        if(CountXorUnspecified!=0)result=result + "Exclusive Unspecified Gateway : "+CountXorUnspecified+"\n";
			        //if(CountXorMixed!=0)result=result + "Exclusive Mixed Gateway : "+CountXorMixed+"\n";
			        
			        if(CountAndUnspecified!=0)result=result + "Parallel Unspecified Gateway : "+CountAndUnspecified+"\n";
			        //if(CountAndMixed!=0)result=result + "Parallel Mixed Gateway : "+CountAndMixed+"\n";

			        if(CountOrJoin!=0)result=result + "Inclusive Join Gateway : "+CountOrJoin+"\n";
			        if(CountOrUnspecified!=0)result=result + "Inclusive Unsecified Gateway : "+CountOrUnspecified+"\n";
			        if(CountOrMixed!=0)result=result + "Inclusive Mixed Gateway : "+CountOrMixed+"\n";

			        if(CountEventBasedJoin!=0)result=result + "Event Based Join Gateway : "+CountEventBasedJoin+"\n";
			        if(CountEventBasedUnspecified!=0)result=result + "Event Based Unspecified Gateway : "+CountEventBasedUnspecified+"\n";
			        if(CountEventBasedMixed!=0)result=result + "Event Based Mixed Gateway : "+CountEventBasedMixed+"\n";
			        
			        if(CountExclusiveEventBasedSplit!=0)result=result + "Exclusive Event Based Split Gateway : "+CountExclusiveEventBasedSplit+"\n";	        
			        if(CountExclusiveEventBasedJoin!=0)result=result + "Exclusive Event Based Join Gateway : "+CountExclusiveEventBasedJoin+"\n";
			        if(CountExclusiveEventBasedMixed!=0)result=result + "Exclusive Event Based Mixed Gateway : "+CountExclusiveEventBasedMixed+"\n";
			        if(CountExclusiveEventBasedUnspecified!=0)result=result + "Exclusive Event Based Unspecified Gateway : "+CountExclusiveEventBasedUnspecified+"\n";
			        
			        if(CountParallelEventBasedSplit!=0)result=result + "Parallel Event Based Split Gateway : "+CountParallelEventBasedSplit+"\n";
			        if(CountParallelEventBasedJoin!=0)result=result + "Parallel Event Based Join Gateway : "+CountParallelEventBasedJoin+"\n";
			        if(CountParallelEventBasedMixed!=0)result=result + "Parallel Event Based Mixed Gateway : "+CountParallelEventBasedMixed+"\n";
			        if(CountParallelEventBasedUnspecified!=0)result=result + "Parallel Event Based Unspecified Gateway : "+CountParallelEventBasedUnspecified+"\n";

			        if(CountComplexSplit!=0)result=result + "Complex Split Gateway : "+CountComplexSplit+"\n";
			        if(CountComplexJoin!=0)result=result + "Complex Join Gateway : "+CountComplexJoin+"\n";
			        if(CountComplexUnspecified!=0)result=result + "Complex Unspecified Gateway : "+CountComplexUnspecified+"\n";
			        if(CountComplexMixed!=0)result=result + "Complex Mixed Gateway : "+CountComplexMixed+"\n";

			        if(CountMultiInstanceParallel!=0)result=result + "Multi Instance Parallel Task/Sub Process : "+CountMultiInstanceParallel+"\n";
			        if(CountMultiInstanceSequential!=0)result=result + "Multi Instance Sequential Task/Sub Process : "+CountMultiInstanceSequential+"\n";
			        if(CountLoop!=0)result=result + "Loop Task/Sub Process: "+CountLoop+"\n";
		      
			        		
		  /*   DEBUG       
		       System.out.println("N° Pool: "+CountPoolev);
		       System.out.println("N° Sequence Flow: "+CountSequenceFlow);
		       System.out.println("N° Message Flow: "+CountMessageFlow);
		       
		       System.out.println("N° Data Object: "+CountDataObject);
		       System.out.println("N° Data Input: "+CountDataInput);
		       System.out.println("N° Data Output: "+CountDataOutput);
		       System.out.println("N° Data Store: "+CountDataStore);
		       
		       System.out.println("N° Message: "+CountMessage);
			        	       
		       System.out.println("N° Start None: "+CountStart);
		       System.out.println("N° Start Message: "+CountStartMessageEvent);
		       System.out.println("N° Start Timer: "+CountStartTimerEvent);
		       System.out.println("N° Start Conditional: "+CountStartConditionalEvent);
		       System.out.println("N° Start Signal: "+CountStartSignalEvent);
		       System.out.println("N° Start Multiple: "+CountStartMultiple);
		       System.out.println("N° Start Parallel Multiple: "+CountStartParallelMultiple);

		       System.out.println("N° End: "+CountEnd);
		       System.out.println("N° End Message Event: "+CountEndMessageEvent);
		       System.out.println("N° End Escalation Event: "+CountEndEscalationEvent);
		       System.out.println("N° End Signale Event: "+CountEndSignalEvent);
		       System.out.println("N° End Error Event: "+CountEndErrorEvent);
		       System.out.println("N° End Cancel Event: "+CountEndCancelEvent);
		       System.out.println("N° End Compensation Event: "+CountEndCompensationEvent);
		       System.out.println("N° End Multiple Event: "+CountEndMultiple);
		       System.out.println("N° End Terminate Event: "+CountEndTerminateEvent);		      
		       
		       System.out.println("N° Simple Task: "+CountTask);
		       System.out.println("N° Manual Task: "+CountManualTask);
		       System.out.println("N° Service Task: "+CountServiceTask);
		       System.out.println("N° Business Rule Task: "+CountBusinessRuleTask);
		       System.out.println("N° Script Task: "+CountScriptTask);
		       System.out.println("N° User Task: "+CountUserTask);
		       System.out.println("N° Send Task: "+CountSendTask);
		       System.out.println("N° Receive Task: "+CountReceiveTask);
		       
		       System.out.println("N° Sub Process: "+CountSubProcess);
		       System.out.println("N° Ad Hoc Sub Process: "+CountAdHocSubProcess);
		       System.out.println("N° Trnsaction: "+CountTransaction);
			       		       
		       System.out.println("N° Intermediate Throw Event: "+CountIntermediateThrowEvent);
		       System.out.println("N° Intermediate Throw Multiple Event: "+CountIntermediateThrowMultiple);
		       System.out.println("N° Intermediate Throw Message Event: "+CountIntermediateThrowMessageEvent);
		       System.out.println("N° Intermediate Throw Signal Event: "+CountIntermediateThrowSignalEvent);
		       System.out.println("N° Intermediate Throw Esclation Event: "+CountIntermediateThrowEscalationEvent);
		       System.out.println("N° Intermediate Throw Link Event: "+CountIntermediateThrowLinkEvent);
		       System.out.println("N° Intermediate Throw Compensation Event: "+CountIntermediateThrowCompensationEvent);

		       System.out.println("N° Intermediate Catch Event: "+CountIntermediateCatchEvent);
		       System.out.println("N° Intermediate Catch Multiple Event: "+CountIntermediateCatchMultiple);
		       System.out.println("N° Intermediate Catch Parallel Multiple Event: "+CountIntermediateCatchParallelMultiple); 
		       System.out.println("N° Intermediate Catch Message Event: "+CountIntermediateCatchMessageEvent);
		       System.out.println("N° Intermediate Catch Signal Event: "+CountIntermediateCatchSignalEvent);
		       System.out.println("N° Intermediate Catch Conditional Event: "+CountIntermediateCatchConditionalEvent);
		       System.out.println("N° Intermediate Catch Link Event: "+CountIntermediateThrowLinkEvent);
		       System.out.println("N° Intermediate Catch Compensation Event: "+CountIntermediateCatchTimerEvent);					
				
		       System.out.println("N° Boundary Interrupting Event: "+CountBoundaryInterruptingEvent);
		       System.out.println("N° Boundary Interrupting Message Event: "+CountBoundaryInterruptingMessageEvent);
		       System.out.println("N° Boundary Interrupting Time Event: "+CountBoundaryInterruptingTimerEvent);
		       System.out.println("N° Boundary Interrupting Conditional Event: "+CountBoundaryInterruptingConditionalEvent);
		       System.out.println("N° Boundary Interrupting Escalation Event: "+CountBoundaryInterruptingEscalationEvent);
		       System.out.println("N° Boundary Interrupting Error Event: "+CountBoundaryInterruptingErrorEvent);
		       System.out.println("N° Boundary Interrupting Cancel Event: "+CountBoundaryInterruptingCancelEvent);
		       System.out.println("N° Boundary Interrupting Compensation Event: "+CountBoundaryInterruptingCompensationEvent);
		       System.out.println("N° Boundary Interrupting Signal Event: "+CountBoundaryInterruptingSignalEvent);
		       System.out.println("N° Boundary Interrupting Multiple Event: "+CountBoundaryInterruptingMultiple);
		       System.out.println("N° Boundary Interrupting Parallel Multiple Event: "+CountBoundaryInterruptingParallelMultiple); 
		       			        
		       System.out.println("N° Boundary Non Interrupting Event: "+CountBoundaryNonInterruptingEvent);
		       System.out.println("N° Boundary Non Interrupting Message Event: "+CountBoundaryNonInterruptingMessageEvent);
		       System.out.println("N° Boundary Non Interrupting Time Event: "+CountBoundaryNonInterruptingTimerEvent);
		       System.out.println("N° Boundary Non Interrupting Conditional Event: "+CountBoundaryNonInterruptingConditionalEvent);
		       System.out.println("N° Boundary Non Interrupting Escalation Event: "+CountBoundaryNonInterruptingEscalationEvent);
		       System.out.println("N° Boundary Non Interrupting Signal Event: "+CountBoundaryNonInterruptingSignalEvent);
		       System.out.println("N° Boundary Non Interrupting Multiple Event: "+CountBoundaryNonInterruptingMultiple);
		       System.out.println("N° Boundary Non Interrupting Parallel Multiple Event: "+CountBoundaryNonInterruptingParallelMultiple); 	       

		       System.out.println("N° Exclusive Split Gateway: "+CountXorSplit);
		       System.out.println("N° Exclusive Join Gateway: "+CountXorJoin);
		       System.out.println("N° Exclusive Unspecified Gateway: "+CountXorUnspecified);
		       System.out.println("N° Exclusive Mixed Gateway: "+CountXorMixed);
		       System.out.println("N° Parallel Split Gateway: "+CountAndSplit);
		       System.out.println("N° Parallel Join Gateway: "+CountAndJoin);
		       System.out.println("N° Parallel Unspecified Gateway: "+CountAndUnspecified);
		       System.out.println("N° Parallel Mixed Gateway: "+CountAndMixed);
		       System.out.println("N° Inclusive Split Gateway: "+CountOrSplit);
		       System.out.println("N° Inclusive Join Gateway: "+CountOrJoin);
		       System.out.println("N° Inclusive Unspecified Gateway: "+CountOrUnspecified);
		       System.out.println("N° Inclusive Mixed Gateway: "+CountOrMixed);
		       System.out.println("N° Event Based Split Gateway: "+CountEventBasedSplit);
		       System.out.println("N° Event Based Join Gateway: "+CountEventBasedJoin);
		       System.out.println("N° Event Based Unspecified Gateway: "+CountEventBasedUnspecified);
		       System.out.println("N° Event Based Mixed Gateway: "+CountEventBasedMixed);
		       
		       System.out.println("N° Exclusive Event Based Split Gateway: "+CountParallelEventBasedSplit);
		       System.out.println("N° Exclusive Event Based Join Gateway: "+CountExclusiveEventBasedJoin);
		       System.out.println("N° Exclusive Event Based Mixed Gateway: "+CountExclusiveEventBasedMixed);
		       System.out.println("N° Exclusive Event Based Unspecified Gateway: "+CountParallelEventBasedUnspecified);
		       
		       System.out.println("N° Parallel Event Based Split Gateway: "+CountParallelEventBasedSplit);
		       System.out.println("N° Parallel Event Based Join Gateway: "+CountParallelEventBasedJoin);
		       System.out.println("N° Parallel Event Based Mixed Gateway: "+CountParallelEventBasedMixed);
		       System.out.println("N° Parallel Event Based Unspecified Gateway: "+CountParallelEventBasedUnspecified);
		            
		       System.out.println("N° Complex Split Gateway: "+CountComplexSplit);
		       System.out.println("N° Complex Join Gateway: "+CountComplexJoin);
		       System.out.println("N° Complex Unspecified Gateway: "+CountComplexUnspecified);
		       System.out.println("N° Complex Mixed Gateway: "+CountComplexMixed);
		       
		       System.out.println("N° Multi Instance Parallel: "+CountMultiInstanceParallel);
		       System.out.println("N° Multi Instance Sequential: "+CountMultiInstanceSequential);
		       System.out.println("N° Loop: "+CountLoop);
		 */      
			        
		 	   
			} catch (ParserConfigurationException e) {
			    e.printStackTrace();
			 } catch (SAXException e) {
			    e.printStackTrace();
			 } catch (IOException e) {
			    e.printStackTrace();
			 } catch (XPathExpressionException e) {
			    e.printStackTrace();
			 } 
			   return result;
		   }
		   

		//FORMATTAZIONE COMANDO MAUDE	
		private String aBPoolstarts (String parsedModel) throws Exception
		{
			//DEBUG 
			//////System.out.println("\nEseguo Operazione 2\n");
			String command = "red modelCheck("+parsedModel+", <> (aBPoolstarts)) . \n";
			return command;
		}
		
		
		private String aBPoolends(String parsedModel) {
			//DEBUG 
			//////System.out.println("\nEseguo Operazione 1\n");
			String command = "red modelCheck("+parsedModel+", <> (aBPoolends)) . \n";
			return command;
		}
		
		@SuppressWarnings("unused")
		private String allBPoolend(String parsedModel) throws Exception {
			String result = "";
			String command = "";
			String command1 = "";
			String poolName = "poolNameVuoto";
			
			Collaboration C = new Collaboration();
			//if(true)return parsedModel;
			
			//ArrayList<Pool> arrayPool = new ArrayList<Pool>();
			ArrayList<String> poolList = C.SplitPool(parsedModel);
			ArrayList<String> poolListNames =  new ArrayList<String>() ;
			
			//if(true)return "poolList.size()"+poolList.size();
				
			for(int i = 0; i<poolList.size(); i++)
			{
				
				//if(true)return "poolList.get(i)"+poolList.get(i);
				//////System.out.println("\nContenuto Array<String> pool: "+pool.get(2));
				try{
//					 String[] elements = poolList.get(i).split(",");
//					 poolName=elements[0];
//					 poolListNames.add(poolName);
				    Pool pool1 = new Pool();
				    
				    poolName=pool1.extractName(poolList.get(i));
				    
				   // if(true)return "\n poolName"+poolName+"\n poolList.get(i)"+poolList.get(i)+"\n i:"+i;

				    poolListNames.add(poolName);
				    
				}catch(Exception e){
					return "Exception"+e+"\n poolName"+poolName+"i: "+i+"Vera pool: "+poolList.get(i);
					//e.printStackTrace();
				}
				
				
//				poolName=pool1.getOrganizationName();
//				//if(true)return "poolName"+poolName;
//				
//				poolListNames.add(poolName);
//				//arrayPool.add(pool1);
				
			}	
			//DOPO AVER CREATO LA LISTA DEI NOMI			
	        
	        int num = poolListNames.size();
			command ="red modelCheck("+parsedModel+", (";
			
			if (num == 1)
				command1 += "(<> (aBPoolendsParameterized("+'"'+poolListNames.get(0)+'"'+") ) )";
			else
			{
				command1 += "(<> (aBPoolendsParameterized("+'"'+poolListNames.get(0)+'"'+") ) )";
				for(int i = 1; i<num; i++)
					command1 += "/\\"+"(<> (aBPoolendsParameterized("+'"'+poolListNames.get(i)+'"'+") ) )";

			}
			command1 += " ) ) . \n";
			command+=command1;
			
			////System.out.println("COMMAND "+command);
			
			return command;
		}
		
		//PARAMETERIZED COMMANDS
		//private String aBPoolOptionToCompleteParameterized(String parsedModel, String poolName1) {
			//String command ="red modelCheck("+parsedModel+", []";
			//command += "(aBPoolstartsParameterized("+'"'+poolName1+'"'+")";
			//command +=  "=> <> aBPoolendsParameterized("+'"'+poolName1+'"'+"))";			
			//command += " ) . \n";
			//SU RESULT TROVI LA STRINGA GENERATA DA MAUDE SIA SE VERA CHE CHECKRESULT
			//return command;
		//}
		
		private String aBPoolstartsParameterized(String parsedModel, String poolName1) {
			String command = "red modelCheck("+parsedModel+", <> (aBPoolstartsParameterized("+'"'+poolName1+'"'+"))) . \n";
			//SU RESULT TROVI LA STRINGA GENERATA DA MAUDE SIA SE VERA CHE CHECKRESULT
			return command;
		}
		
		private String aBPoolendsParameterized(String parsedModel, String poolName1) {
			String command = "red modelCheck("+parsedModel+", <> (aBPoolendsParameterized("+'"'+poolName1+'"'+"))) . \n";
			//SU RESULT TROVI LA STRINGA GENERATA DA MAUDE SIA SE VERA CHE CHECKRESULT
			return command;
		}
	
		private String aTaskEnabledParameterized(String parsedModel,String taskName1) {
			String command = "red modelCheck("+parsedModel+", <> (aTaskEnabledParameterized("+'"'+taskName1+'"'+"))) . \n";
			//SU RESULT TROVI LA STRINGA GENERATA DA MAUDE SIA SE VERA CHE CHECKRESULT
			return command;
		}
		
		private String aTaskRunningParameterized(String parsedModel,String taskName1) {
			String command = "red modelCheck("+parsedModel+", <> (aTaskRunningParameterized("+'"'+taskName1+'"'+"))) . \n";
			//SU RESULT TROVI LA STRINGA GENERATA DA MAUDE SIA SE VERA CHE CHECKRESULT
			return command;
		}
		
		private String aTaskCompleteParameterized(String parsedModel,String taskName1) {
			String command = "red modelCheck("+parsedModel+", <> (aTaskCompleteParameterized("+'"'+taskName1+'"'+"))) . \n";
			//SU RESULT TROVI LA STRINGA GENERATA DA MAUDE SIA SE VERA CHE CHECKRESULT
			return command;
		}

		private String Task1ImpliesTask2(String parsedModel, String taskName1, String taskName2) {
			String command = "red modelCheck("+parsedModel+", (aTaskRunningParameterized("+'"'+taskName1+'"'+")"+"|->"+ "(aTaskRunningParameterized("+'"'+taskName2+'"'+")))) . \n";
			//DEBUG ////System.out.println("PROPIET� OP 8: "+property);
			//SU RESULT TROVI LA STRINGA GENERATA DA MAUDE SIA SE VERA CHE CHECKRESULT
			return command;
		}
		
		private String aBPoolSndMsg(String parsedModel, String poolName1,String sndMsgName) {
			String command = "red modelCheck("+parsedModel+", <> (aBPoolSndMsg("+'"'+poolName1+'"'+","+'"'+sndMsgName+'"'+" ) ) ) . \n";
			//SU RESULT TROVI LA STRINGA GENERATA DA MAUDE SIA SE VERA CHE CHECKRESULT
			System.out.println("COMMAND "+command);	
			return command;
		}

		private String aBPoolRcvMsg(String parsedModel, String poolName1,String rcvMsgName) {
			String command = "red modelCheck("+parsedModel+", <> (aBPoolRcvMsg("+'"'+poolName1+'"'+","+'"'+rcvMsgName+'"'+" ) ) ) . \n";
			System.out.println("COMMAND "+command);	
			//SU RESULT TROVI LA STRINGA GENERATA DA MAUDE SIA SE VERA CHE CHECKRESULT
			return command;
		}

		//private String noDeadActivities(String parsedModel , String taskName) throws Exception {
			//String command ="red modelCheck("+parsedModel+", [] ~ ";
			//command += "(aTaskRunningParameterized("+'"'+taskName+'"'+")" +")";
			//command += " ) . \n";
			//////System.out.println("COMMAND "+command);			
			//return command;
		//}
		
		private String noDeadActivitiesBySearch(String parsedModel , String taskName) throws Exception {
			String command ="search[1] "+parsedModel+" =>* ";
			command += "collaboration( {collab(ON:OrgName,running("+'"'+taskName+'"'+"))}C:Collaboration) . \n";
			////System.out.println("COMMAND "+command);		
			return command;
		}

		private String optionToComplete(String parsedModel, String poolName) throws Exception {

			String command ="red modelCheck("+parsedModel+", []";
			command += "(aBPoolstartsParameterized("+'"'+poolName+'"'+")";
			command +=  "-> <> aBPoolendsParameterized("+'"'+poolName+'"'+"))";
			command += " ) . \n";
			
			////System.out.println("COMMAND "+command);
			
			return command;
		}
		
		private String optionToCompleteCollaboration(String parsedModel, ArrayList<String> poolListNames) throws Exception {
					
			String command ="red modelCheck("+parsedModel+", [] ( ";

    		for(int i = 0; i<poolListNames.size(); i++){	
    			command += "(aBPoolstartsParameterized("+'"'+poolListNames.get(i)+'"'+")";
    			command +=  "-> <> aBPoolendsParameterized("+'"'+poolListNames.get(i)+'"'+")) /\\ ";	
    		}
    			command = command.substring(0, command.length() - 3);
    			command += ") ) . \n";
    			System.out.println("COMMAND "+command);
    			
			return command;
		}
		
		private String properCompletionCollaboration(String parsedModel , ArrayList<String> poolListNames ) throws Exception {
			String command ="red modelCheck("+parsedModel+", [] ( ";

    		for(int i = 0; i<poolListNames.size(); i++){	
    			command += " (aBPoolendsParameterized("+'"'+poolListNames.get(i)+'"'+")";
    			command +=  "-> noMultipleTokenAround("+'"'+poolListNames.get(i)+'"'+")) /\\ ";
    		
    		}
    			command = command.substring(0, command.length() - 3);
    			command += ") ) . \n";

		//System.out.println("COMMAND "+command);

			return command;
		}
		
		private String properCompletionMFCollaboration(String parsedModel , ArrayList<String> poolListNames ) throws Exception {
			String command ="red modelCheck("+parsedModel+", [] ( ";

    		for(int i = 0; i<poolListNames.size(); i++){	
    			command += " (aBPoolendsParameterized("+'"'+poolListNames.get(i)+'"'+")";
    			command +=  "-> noMultipleTokenAroundOnlyMsgsAreNOk("+'"'+poolListNames.get(i)+'"'+")) /\\ ";
    		
    		}
    			command = command.substring(0, command.length() - 3);
    			command += ") ) . \n";

		System.out.println("COMMAND "+command);

			return command;
		}
		
		private String properCompletionCFMFCollaboration(String parsedModel , ArrayList<String> poolListNames ) throws Exception {
			String command ="red modelCheck("+parsedModel+", [] ( ";

    		for(int i = 0; i<poolListNames.size(); i++){	
    			command += " (aBPoolendsParameterized("+'"'+poolListNames.get(i)+'"'+")";
    			command +=  "-> noMultipleTokenAroundMsgsAreNOk("+'"'+poolListNames.get(i)+'"'+")) /\\ ";
    		
    		}
    			command = command.substring(0, command.length() - 3);
    			command += ") ) . \n";

		System.out.println("COMMAND "+command);

			return command;
		}
		
		private String properCompletion(String parsedModel , String poolName ) throws Exception {
			String command ="red modelCheck("+parsedModel+", [] ";
			command += " (aBPoolendsParameterized("+'"'+poolName+'"'+")";
			command +=  "-> noMultipleTokenAround("+'"'+poolName+'"'+"))";
			command += " ) . \n";

			////System.out.println("COMMAND "+command);
			
			return command;
		}
		
		private String properCompletionMF(String parsedModel , String poolName ) throws Exception {
			String command ="red modelCheck("+parsedModel+", [] ";
			command += " (aBPoolendsParameterized("+'"'+poolName+'"'+")";
			command +=  "-> noMultipleTokenAroundOnlyMsgsAreNOk("+'"'+poolName+'"'+"))";
			command += " ) . \n";

			////System.out.println("COMMAND "+command);
			
			return command;
		}
		
		private String properCompletionCFMF(String parsedModel , String poolName ) throws Exception {
			String command ="red modelCheck("+parsedModel+", [] ";
			command += " (aBPoolendsParameterized("+'"'+poolName+'"'+")";
			command +=  "-> noMultipleTokenAroundMsgsAreNOk("+'"'+poolName+'"'+"))";
			command += " ) . \n";

			////System.out.println("COMMAND "+command);
			
			return command;
		}
		

		
		private String safenessCollaboration(String parsedModel , ArrayList<String>  poolListNames) throws Exception {

			String command ="red modelCheck("+parsedModel+", []";
			
	    		for(int i = 0; i<poolListNames.size(); i++){	
					command += "safeness("+'"'+poolListNames.get(i)+'"'+") /\\ ";
	    		}
	    			command = command.substring(0, command.length() - 3);
					command += " ) . \n";

			System.out.println("COMMAND "+command);
			
			return command;
		}
		
		
		private String safenessParam(String parsedModel , String poolName) throws Exception {
					
					String command ="red modelCheck("+parsedModel+", [] ";
					command += "safeness("+'"'+poolName+'"'+")";
					command += " ) . \n";
		
					////System.out.println("COMMAND "+command);
					
					return command;
				}
		
		
		//getVerificationResultPostMultipleParameters
		public PostMultipleParameters getVerificationResultPostMultipleParameters(PostMultipleParameters postMultiple) {
			String parsedModel=postMultiple.getParsedModel();
			String property=postMultiple.getProperty();
			String poolName=postMultiple.getPoolName();
			String taskName1=postMultiple.getTaskName1();
			String taskName2=postMultiple.getTaskName2();
			String sndMsgName=postMultiple.getSndMsgName();
			String rcvMsgName=postMultiple.getRcvMsgName();
		    ////System.out.println("\ngetVerificationResult\n");
						
		    String maudeCommand="";						
		    PostMultipleParameters result=new PostMultipleParameters();						
						
		    if(parsedModel=="null") {postMultiple.setResult("THE MODEL IS MISSING\n");return postMultiple;}		
		    if(property=="null"){postMultiple.setResult("THE PROPERTY TO VERIFY IS MISSING \n");return postMultiple;} 							
			
		    switch (property){
												
		    case "aBPoolstarts": {			
		    	result=new PostMultipleParameters();
				
		    	try {			
		    		maudeCommand=aBPoolstarts(parsedModel);					
		    		////System.out.println("\n MaudeCommand: "+maudeCommand);				
		    	} catch (Exception e) {
				
		    		e.printStackTrace();
				
		    	}
				
		    	result = getMaudeResult(maudeCommand,postMultiple);						
		    	////System.out.println("\nresult in case: "+result+"\n");						
		    	break;

		    }
			
		    case "aBPoolends": {			
		    	result=new PostMultipleParameters();				

		    	try {				
		    		maudeCommand=aBPoolends(parsedModel);				
		    		////System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}			
		    	result = getMaudeResult(maudeCommand,postMultiple);			
		    	break;
				
		    }
						
						
		    case "allBPoolend": {
			
		    	result=new PostMultipleParameters(); 							
				
		    	try {
				
		    		maudeCommand=allBPoolend(parsedModel);					
		    		////System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {		    							
		    		e.printStackTrace();					
		    	}					    	
				
		    	result = getMaudeResult(maudeCommand,postMultiple);		    	
		    	//DEBUG
		    	//////System.out.println("\nRESULT ABPOOLEND: "+result.getResult());					
		    	break;						
		    }
						
			//PARAMETERIZED
						
		    case "aBPoolstartsParameterized": {
			
		    	result=new PostMultipleParameters();
				
		    	try {
				
		    		maudeCommand=aBPoolstartsParameterized(parsedModel , poolName);					
		    		System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {
		    		e.printStackTrace();					
		    	}
		    	
		    	result = getMaudeResult(maudeCommand,postMultiple);				
		    	break;
				
		    }
												
		    case "aBPoolendsParameterized": {
			
		    	result=new PostMultipleParameters();
				
		    	try {
		    		
		    		maudeCommand=aBPoolendsParameterized(parsedModel , poolName);					
		    		////System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}
				
		    	result = getMaudeResult(maudeCommand,postMultiple);				
		    	break;
				
		    }		
		    case "aTaskEnabledParameterized": {
				
		    	result=new PostMultipleParameters();
				
		    	try {
				
		    		maudeCommand=aTaskEnabledParameterized(parsedModel , taskName1 );					
		    		//System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {			
		    		e.printStackTrace();						
		    	}
				
		    	result = getMaudeResult(maudeCommand,postMultiple);				
		    	break;
				
		    }
		    case "aTaskRunningParameterized": {
				
		    	result=new PostMultipleParameters();
				
		    	try {
				
		    		maudeCommand=aTaskRunningParameterized(parsedModel , taskName1 );					
		    		//System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {			
		    		e.printStackTrace();						
		    	}
				
		    	result = getMaudeResult(maudeCommand,postMultiple);				
		    	break;
				
		    }
					
		    case "aTaskCompleteParameterized": {
			
		    	result=new PostMultipleParameters();
				
		    	try {
				
		    		maudeCommand=aTaskCompleteParameterized(parsedModel , taskName1 );					
		    		//System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {			
		    		e.printStackTrace();						
		    	}
				
		    	result = getMaudeResult(maudeCommand,postMultiple);				
		    	break;
				
		    }
						
						
		    case "Task1ImpliesTask2": {
			
		    	result=new PostMultipleParameters();
			
		    	try {
				
		    		maudeCommand=Task1ImpliesTask2(parsedModel , taskName1 , taskName2);					
		    		////System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}
				
		    	result = getMaudeResult(maudeCommand,postMultiple);				
		    	break;
			
		    }					    
		    
						
		    case "aBPoolSndMsg": {
			
		    	result=new PostMultipleParameters();							
				
		    	try {
							
		    		maudeCommand=aBPoolSndMsg(parsedModel , poolName , sndMsgName);				
		    		////System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}
				
		    	result = getMaudeResult(maudeCommand,postMultiple);			
		    	break;
				
		    }
						
						
		    case "aBPoolRcvMsg": {
			
		    	result=new PostMultipleParameters();
			
		    	try {
				
		    		maudeCommand=aBPoolRcvMsg(parsedModel , poolName , rcvMsgName);					
		    		////System.out.println("\n MaudeCommand: "+maudeCommand);
					
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}
				
		    	result = getMaudeResult(maudeCommand,postMultiple);				
		    	break;
				
		    }
						
						
		    case "noDeadActivities": {
			
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
														
		    		////System.out.println("\n noDeadActivities \n");					
		    		Collaboration C = new Collaboration();					
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);										
		    		ArrayList<String> tasksNames = new ArrayList<String>() ;						
		    		ArrayList<String> procElements = new ArrayList<String>();								
					
		    		for(int i = 0; i<poolList.size(); i++){					
					
		    			try{																			    
									   
		    				Pool pool1 = new Pool();								    							
		    				poolName=pool1.extractName(poolList.get(i));							
		    				//DEVO ESTRARRE TUTTI I TASK, QUELLI SEMPLICI, I SEND, E I RECEIVE ... 							
		    				// POI NE METTO IN UNA LISTA DI STRING I NOMI E POI POSSO PROCEDERE CON LA STESURA							
		    				//DEL MAUDE COMMAND													
		    				//DEBUG
		    				//////System.out.println("\n\nPool getOrganizationName: "+poolName);							
		    				Proc ProcessElements = new Proc();							
		    				procElements=ProcessElements.extractProcElement(poolList.get(i));							
		    				//DEBUG
		    				//////System.out.println("\n\nEstraggo i processElements ");						
		    				ProcessElements.analizeProc(procElements);							
		    				//DEBUG
		    				//////System.out.println("\n\nGenero gli oggetti corrispondenti agli elementi ");						
		    				ArrayList<Task> tasks=ProcessElements.getTasks();									    
		    				//DEBUG
		    				//////System.out.println("\n\nEstraggo i Task ");							
		    				for(Iterator<Task> iTask = tasks.iterator(); iTask.hasNext(); ) {						
		    					tasksNames.add(iTask.next().name);							
		    				}							
		    				//DEBUG							
		    				//////System.out.println(tasksNames);							
		    				//SndTasksNames							
		    				ArrayList<SendTask> sndTasks=ProcessElements.getSendTask();							
		    				for(Iterator<SendTask> iTask = sndTasks.iterator(); iTask.hasNext(); ) {						
		    					tasksNames.add(iTask.next().name);							
		    				}
							
		    				//DEBUG							
		    				//////System.out.println(tasksNames);							
		    				//ReceiveTasksNames							
		    				ArrayList<ReceiveTask> rcvTasks=ProcessElements.getReceiveTask();					
							
		    				for(Iterator<ReceiveTask> iTask = rcvTasks.iterator(); iTask.hasNext(); ) {							
		    					tasksNames.add(iTask.next().name);								
		    				}							
		    				//DEBUG							
		    				//////System.out.println(tasksNames);									    											
		    				//Stamapare lista tasksnames							
		    				//////System.out.println("\n Stampo Nomi dei Task\n");						
		    				//for(Iterator<String> iTask = tasksNames.iterator(); iTask.hasNext(); ) {							
		    				//	////System.out.println("\n TaskName: "+ iTask.next());							
		    				//}									    				
							
		    			}catch(Exception e){
						
		    				StringWriter sw = new StringWriter();						
		    				PrintWriter pw = new PrintWriter(sw);							
		    				e.printStackTrace(pw);							
		    				sw.toString();	
		    				//DEBUG
		    				//////System.out.println("\n Exception: "+ sw);							
		    				result.setResult( "Exception"+e+"\n poolName "+poolName+"i: "+i+"Vera pool: "+poolList.get(i));									
		    				return result;						
		    			}															
		    		}	
					
		    		//DOPO AVER CREATO LA LISTA DEI NOMI			  				
		    		//DEBUG					
		    		//////System.out.println("\nLista Nomi Task Creata: \n");					
		    		//////System.out.println("\ntasksNames.size(): "+tasksNames.size()+"\n");					
		    		int num = tasksNames.size();					
		    		//DEBUG
		    		//PostMultipleParameters flagResult = null;										
		    		int realTimeMaudeInt=0;					
		    		if (num == 1){
					
		    			////System.out.println("\nOnly one task: "+tasksNames.get(0)+"\n");						
		    			result=getMaudeResultNoCounterexampleBySearch(noDeadActivitiesBySearch(parsedModel , tasksNames.get(0) ),postMultiple);															
		    			if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){						
		    				break;							
		    			}																		
						
		    			realTimeMaudeInt=Integer.parseInt(result.getPropertyVerificationTime());						
		    			realTimeMaudeIntTot+=realTimeMaudeInt;
					
		    			if(result.getResult().contains("No solution")){
						
		    				result.setResult("The Task: "+tasksNames.get(0)+" does not respect the property No Dead Activities");							
		    				result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));							
		    				break;
									
		    			}
										
		    		}else{

		    			for(int i = 0; i<num; i++){
		    				////System.out.println("\nnoDeadActivities n: "+i+"\n");						
		    				long noDeadTime = System.currentTimeMillis();							
		    				result=getMaudeResultNoCounterexampleBySearch(noDeadActivitiesBySearch(parsedModel, tasksNames.get(i)),postMultiple);
		    											
		    				if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
							
		    					result.setResult(result.getResult());							
		    					break;
								
		    				}
										
	
		    				//DEBUG							
		    				//////System.out.println("\nflagResult: "+flagResult);						
		    				//////System.out.println("\nHEREEE realTimeMaudeString"+result.getPropertyVerificationTime());							
		    				realTimeMaudeInt=Integer.parseInt(result.getPropertyVerificationTime());							
		    				realTimeMaudeIntTot+=realTimeMaudeInt;																	
		    				noDeadTime = System.currentTimeMillis()-noDeadTime;							
		    				//////System.out.println("\nn: "+i+" noDeadTime: "+noDeadTime+"\n tasksNames.get(i): "+tasksNames.get(i)+"\n");						
		    				//////System.out.println("\n result getCounterexample: "+result.getCounterexample());							
		    				//////System.out.println("\n result getResult: "+result.getResult());								
		    				if(result.getResult().contains("false")){
		    					result.setResult("The Task: "+tasksNames.get(i)+" does not respect the property No Dead Activities");
		    					result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
		    					break;	
		    				}
		    			}
		    		}						
		    	} catch (Exception e) {
		    		e.printStackTrace();	
		    	}
				
		    	//DEBUG
		    	//////System.out.println("FUORI DAL TRY CATCH");				
		    	//////System.out.println("\n Result: "+result.getResult());				
		    	if(result.getResult()==null){				
		    		result.setResult("Error Occurred or Time Exceeded result Bool: false No Dead Activities");					
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));												
		    	}else{
				
		    		if(result.getResult().equals("")){					
		    			result.setResult("Error Occurred or Time Exceeded result Bool: false No Dead Activities");					
		    			result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));					
		    		}				
		    	}				
		    	//DEBUG
		    	//////System.out.println("\n Result: "+result.getResult());				
		    	break;				
		    }
						
		    case "noDeadActivitiesParam": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
														
		    		//System.out.println("\n noDeadActivitiesParam \n");					
		    		Collaboration C = new Collaboration();					

		    		ArrayList<String> poolList = C.SplitPool(parsedModel);		
		    		
		    		////System.out.println("parsedModel: \n"+parsedModel);
    				////System.out.println("\n\npostMultiple.getPoolName(): "+postMultiple.getPoolName());
		    		
		    		ArrayList<String> tasksNames = new ArrayList<String>() ;						
		    		ArrayList<String> procElements = new ArrayList<String>();								
					
		    		for(int i = 0; i<poolList.size(); i++){					
					
		    			try{																			    								   
		    				
		    				Pool pool1 = new Pool();								    							
		    				poolName=pool1.extractName(poolList.get(i));	
		    				//System.out.println("\n\nPool getOrganizationName: "+poolName);
		    				//System.out.println("\n\npostMultiple.getPoolName(): "+postMultiple.getPoolName());
		    				
		    				if(poolName.equals(postMultiple.getPoolName())){

			    				//////System.out.println("\n\nPool getOrganizationName: "+poolName);							
			    				Proc ProcessElements = new Proc();							
			    				procElements=ProcessElements.extractProcElement(poolList.get(i));							
			    				//DEBUG
			    				//////System.out.println("\n\nEstraggo i processElements ");						
			    				ProcessElements.analizeProc(procElements);							
			    				//DEBUG
			    				//////System.out.println("\n\nGenero gli oggetti corrispondenti agli elementi ");						
			    				ArrayList<Task> tasks=ProcessElements.getTasks();									    
			    				//DEBUG
			    				//////System.out.println("\n\nEstraggo i Task ");							
			    				for(Iterator<Task> iTask = tasks.iterator(); iTask.hasNext(); ) {						
			    					tasksNames.add(iTask.next().name);							
			    				}							
			    				//DEBUG							
			    				////System.out.println(tasksNames);							
			    				//SndTasksNames							
			    				ArrayList<SendTask> sndTasks=ProcessElements.getSendTask();							
			    				for(Iterator<SendTask> iTask = sndTasks.iterator(); iTask.hasNext(); ) {						
			    					tasksNames.add(iTask.next().name);							
			    				}
								
			    				//DEBUG							
			    				////System.out.println(tasksNames);							
			    				//ReceiveTasksNames							
			    				ArrayList<ReceiveTask> rcvTasks=ProcessElements.getReceiveTask();													
			    				for(Iterator<ReceiveTask> iTask = rcvTasks.iterator(); iTask.hasNext(); ) {							
			    					tasksNames.add(iTask.next().name);								
			    				}							
			    				//[TODO] Dovrei inserire controllo se la pool non è stata trovata
			    				break;
		    				}
								
		    			}catch(Exception e){
						
		    				StringWriter sw = new StringWriter();						
		    				PrintWriter pw = new PrintWriter(sw);							
		    				e.printStackTrace(pw);							
		    				sw.toString();	
		    				//DEBUG
		    				//////System.out.println("\n Exception: "+ sw);							
		    				result.setResult( "Exception"+e+"\n poolName "+poolName+"i: "+i+"Vera pool: "+poolList.get(i));									
		    				return result;						
		    			}															
		    		}	
		    		//DOPO AVER CREATO LA LISTA DEI NOMI			  				
		    		//DEBUG					
		    		//////System.out.println("\nLista Nomi Task Creata: \n");					
		    		//////System.out.println("\ntasksNames.size(): "+tasksNames.size()+"\n");					
		    		int num = tasksNames.size();					
		    		//DEBUG
		    		//PostMultipleParameters flagResult = null;										
		    		int realTimeMaudeInt=0;					
		    		if (num == 1){
					
		    			////System.out.println("\nOnly one task: "+tasksNames.get(0)+"\n");						
		    			result=getMaudeResultNoCounterexampleBySearch(noDeadActivitiesBySearch(parsedModel , tasksNames.get(0) ),postMultiple);															
		    			if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){						
		    				break;							
		    			}																		
						
		    			realTimeMaudeInt=Integer.parseInt(result.getPropertyVerificationTime());						
		    			realTimeMaudeIntTot+=realTimeMaudeInt;
					
		    			if(result.getResult().contains("No solution")){
						
		    				result.setResult("The Task: "+tasksNames.get(0)+" does not respect the property No Dead Activities");							
		    				result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));							
		    				break;
									
		    			}
										
		    		}else{

		    			for(int i = 0; i<num; i++){
		    				////System.out.println("\nnoDeadActivities n: "+i+"\n");						
		    				long noDeadTime = System.currentTimeMillis();							
		    				result=getMaudeResultNoCounterexampleBySearch(noDeadActivitiesBySearch(parsedModel, tasksNames.get(i)),postMultiple);
		    											
		    				if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
							
		    					result.setResult(result.getResult());							
		    					break;
								
		    				}
										
	
		    				//DEBUG							
		    				//////System.out.println("\nflagResult: "+flagResult);						
		    				////System.out.println("\nHEREEE realTimeMaudeString"+result.getPropertyVerificationTime());							
		    				realTimeMaudeInt=Integer.parseInt(result.getPropertyVerificationTime());							
		    				realTimeMaudeIntTot+=realTimeMaudeInt;																	
		    				noDeadTime = System.currentTimeMillis()-noDeadTime;							
		    				//////System.out.println("\nn: "+i+" noDeadTime: "+noDeadTime+"\n tasksNames.get(i): "+tasksNames.get(i)+"\n");						
		    				////System.out.println("\n result getCounterexample: "+result.getCounterexample());							
		    				////System.out.println("\n result getResult: "+result.getResult());								
		    				if(result.getResult().contains("false")){
		    					result.setResult("The Task: "+tasksNames.get(i)+" does not respect the property No Dead Activities");
		    					result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
		    					break;	
		    				}
		    			}
		    		}						
		    	} catch (Exception e) {
		    		e.printStackTrace();	
		    	}
				
		    	//DEBUG
		    	//////System.out.println("FUORI DAL TRY CATCH");				
		    	//////System.out.println("\n Result: "+result.getResult());				
		    	if(result.getResult()==null){				
		    		result.setResult("Error Occurred or Time Exceeded result Bool: false No Dead Activities");					
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));												
		    	}else{
				
		    		if(result.getResult().equals("")){					
		    			result.setResult("Error Occurred or Time Exceeded result Bool: false No Dead Activities");					
		    			result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));					
		    		}				
		    	}				
		    	//DEBUG
		    	//////System.out.println("\n Result: "+result.getResult());				
		    	break;				
		    }					
		    
/*COLLABORATION RESULT AFTER VERIFICATION OF PROPERTY PER EACH SINGLE POOL*/
/*
		    case "optionToComplete": {
			
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
							
		    	try {
				
		    		Collaboration C = new Collaboration();
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);					
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;											
		    		int realTimeMaudeInt=0;					
													
		    		for(int i = 0; i<poolList.size(); i++){

		    			try{
			
		    				Pool pool1 = new Pool();    							
		    				poolName=pool1.extractName(poolList.get(i));							
		    				poolListNames.add(poolName);						
		    				result = getMaudeResult(optionToComplete(parsedModel , poolName),postMultiple);							
		    				if(result.getResult()!=null){							
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){									
		    						break;									
		    					}								
		    				}
									    
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){				    
							
		    					realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());		
		    					realTimeMaudeIntTot+=realTimeMaudeInt;							
		    					if(result.getResult()==null){
									    					
		    						result.setResult("Pool: "+poolName+" does not respect the Option to Complete property");		
		    						result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
		    						//DEBUG
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);			
		    						break;
									   
		    					}else{
								
		    						if(!result.getResult().contains("result Bool: true")){
									
		    							result.setResult("Pool: "+poolName+" does not respect the Option to Complete property");			
		    							result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
		    							//DEBUG
		    							//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
		    							//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);									
		    							break;
								    
		    						}									
		    					}								
		    				}
									
		    			}catch(Exception e){
								    				
		    				e.printStackTrace();						
		    				result.setResult("Exception"+e+"\n poolName"+poolName+"i: "+i+"Vera pool: "+poolList.get(i));						
		    				return result;												
		    			}						
		    		}	//Chiusura For scorre PoolList								
					
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}
				
		    	if(result.getResult().equals("")){				
		    		result.setResult("result Bool: true All the Pools respect the property Option To Complete");					
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));				
		    	}
				
		    	//DEBUG
		    	//////System.out.println("\n Result: "+result);			
		    	break;				
		    }*/
		    
		    case "optionToComplete": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
							
		    	try {
				
		    		Collaboration C = new Collaboration();
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);					
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;											
		    		int realTimeMaudeInt=0;					
													
		    		for(int i = 0; i<poolList.size(); i++){

		    				Pool pool1 = new Pool();    							
		    				poolName=pool1.extractName(poolList.get(i));							
		    				poolListNames.add(poolName);						
		    				
		    		}	//Chiusura For scorre PoolList								
		    		result = getMaudeResult(optionToCompleteCollaboration(parsedModel , poolListNames),postMultiple);							
    				if(result.getResult()!=null){							
    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){									
    						break;									
    					}								
    				}
							    
    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){				    
					
    					realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());		
    					realTimeMaudeIntTot+=realTimeMaudeInt;							
    					if(result.getResult()==null){
							    					
    						result.setResult("The collaboration does not respect the Option to Complete property");		
    						result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
    						//DEBUG
							//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
							//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);			
    						break;
							   
    					}else{
						
    						if(!result.getResult().contains("result Bool: true")){
							
    							result.setResult("The collaboration does not respect the Option to Complete property");			
    							result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
    							//DEBUG
    							//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
    							//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);									
    							break;
						    
    						}									
    					}								
    				}
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}
				
		    	if(result.getResult().equals("")){				
		    		result.setResult("result Bool: true All the Pools respect the property Option To Complete");					
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));				
		    	}
				
		    	//DEBUG
		    	//////System.out.println("\n Result: "+result);			
		    	break;				
		    }
	
		    case "optionToCompleteParam": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
							
		    	try {
														
		    		int realTimeMaudeInt=0;					
													
		    				result = getMaudeResult(optionToComplete(parsedModel , postMultiple.getPoolName()),postMultiple);							
		    				if(result.getResult()!=null){							
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){									
		    						break;									
		    					}								
		    				}
									    
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){				    
							
		    					realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());		
		    					realTimeMaudeIntTot+=realTimeMaudeInt;	
		    					result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
		    					if(result.getResult()==null){
									    					
		    						result.setResult("Pool: "+poolName+" does not respect the Option to Complete property");		
		    						result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
		    						//DEBUG
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);			
									   
		    					}else{
								
		    						if(!result.getResult().contains("result Bool: true")){
									
		    							result.setResult("Pool: "+poolName+" does not respect the Option to Complete property");			
		    							result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
		    							//DEBUG
		    							//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
		    							//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);									
								    
		    						}									
		    					}								
		    				}
																									
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}
				
		    	if(result.getResult().equals("")){				
		    		result.setResult("result Bool: true All the Pools respect the property Option To Complete");					
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));				
		    	}
				
		    	//DEBUG
		    	//////System.out.println("\n Result: "+result);			
		    	break;				
		    }
		    
/*COLLABORATION RESULT AFTER VERIFICATION OF PROPERTY PER EACH SINGLE POOL						
		    case "properCompletionCF": {
			
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
				
		    		Collaboration C = new Collaboration();					
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);					
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;													
		    		int realTimeMaudeInt=0;												
					
		    		for(int i = 0; i<poolList.size(); i++){			
					
		    			try{
						
		    				Pool pool1 = new Pool();    									    
		    				poolName=pool1.extractName(poolList.get(i));		  
		    				poolListNames.add(poolName);	
		    				result = getMaudeResult(properCompletion(parsedModel , poolName),postMultiple);
	
		    				if(result.getResult()!=null){
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
		    						break;
		    					}
		    				}
							
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
								realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());											
								realTimeMaudeIntTot+=realTimeMaudeInt;		
								result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
								System.out.println("\npoolName: "+poolName);
								System.out.println("\nrealTimeMaudeInt: "+realTimeMaudeInt);
								System.out.println("\nrealTimeMaudeIntTot: "+realTimeMaudeIntTot);
								if(result.getResult()==null){										    	
									    	
									result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CF property");																	
									result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
									//DEBUG									
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);									
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);								
									break;									
								}else{								
									if(!result.getResult().contains("result Bool: true")){								
										result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CF property");								
										result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
	    	
										//DEBUG
										//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
										//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);										
										break;									
									}									
								}								
		    				}							
		    			}catch(Exception e){
											
		    				e.printStackTrace();					
		    				result.setResult("Exception"+e+"\n poolName"+poolName+"i: "+i+"Vera pool: "+poolList.get(i));						
		    				return result;								    										
		    			}
					
		    		}	//Chiusura For scorre PoolList
																
		    	} catch (Exception e) {			
		    		e.printStackTrace();				
		    	}			
		    	if(result.getResult().contains("result Bool: true")){
			
		    		result.setResult("All the Pools respect the property Proper Completion CF");				
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
				
		    	}
			
		    	////System.out.println("\n Result: "+result);			
		    	break;			
		    }	*/	
	
		    case "properCompletionCF": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
				
		    		Collaboration C = new Collaboration();					
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);					
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;													
		    		int realTimeMaudeInt=0;												
					
		    		for(int i = 0; i<poolList.size(); i++){			
					
		    			Pool pool1 = new Pool();    									    
	    				poolName=pool1.extractName(poolList.get(i));		  
	    				poolListNames.add(poolName);	
		
		    		}	//Chiusura For scorre PoolList
		    		try{
	    				result = getMaudeResult(properCompletionCollaboration(parsedModel , poolListNames),postMultiple);
	    				if(result.getResult()!=null){
	    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
	    						break;
	    					}
	    				}
						
	    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
							realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());											
							realTimeMaudeIntTot+=realTimeMaudeInt;		
							result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
							System.out.println("\npoolName: "+poolName);
							System.out.println("\nrealTimeMaudeInt: "+realTimeMaudeInt);
							System.out.println("\nrealTimeMaudeIntTot: "+realTimeMaudeIntTot);
							if(result.getResult()==null){										    	
								    	
								result.setResult("\nThe collaboration does not respect the Proper Completion CF property");																	
								result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
								//DEBUG									
								//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);									
								//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);								
								break;									
							}else{								
								if(!result.getResult().contains("result Bool: true")){								
									result.setResult("\nThe collaboration does not respect the Proper Completion CF property");								
									result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
    	
									//DEBUG
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);										
									break;									
								}									
							}								
	    				}							
	    			}catch(Exception e){
										
	    				e.printStackTrace();					
	    				result.setResult("Exception"+e+"\n");						
	    				return result;								    										
	    			}											
		    	} catch (Exception e) {			
		    		e.printStackTrace();				
		    	}			
		    	if(result.getResult().contains("result Bool: true")){
			
		    		result.setResult("All the Pools respect the property Proper Completion CF");				
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
				
		    	}
			
		    	////System.out.println("\n Result: "+result);			
		    	break;			
		    }		
		    
/*COLLABORATION RESULT AFTER VERIFICATION OF PROPERTY PER EACH SINGLE POOL
/*	    
		    case "properCompletionMF": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
				
		    		Collaboration C = new Collaboration();					
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);					
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;													
		    		int realTimeMaudeInt=0;												
					
		    		for(int i = 0; i<poolList.size(); i++){			
					
		    			try{
						
		    				Pool pool1 = new Pool();    									    
		    				poolName=pool1.extractName(poolList.get(i));		  
		    				poolListNames.add(poolName);	
		    				result = getMaudeResult(properCompletionMF(parsedModel , poolName),postMultiple);
	
		    				if(result.getResult()!=null){
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
		    						break;
		    					}
		    				}
							
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
								realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());											
								realTimeMaudeIntTot+=realTimeMaudeInt;								
								if(result.getResult()==null){										    	
									    	
									result.setResult("\nPool: "+poolName+" does not respect the Proper Completion MF property");																	
									result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
									//DEBUG									
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);									
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);								
									break;									
								}else{								
									if(!result.getResult().contains("result Bool: true")){								
										result.setResult("\nPool: "+poolName+" does not respect the Proper Completion MF property");								
										result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
	    	
										//DEBUG
										//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
										//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);										
										break;									
									}									
								}								
		    				}							
		    			}catch(Exception e){
											
		    				e.printStackTrace();					
		    				result.setResult("Exception"+e+"\n poolName"+poolName+"i: "+i+"Vera pool: "+poolList.get(i));						
		    				return result;								    										
		    			}
					
		    		}	//Chiusura For scorre PoolList
																
		    	} catch (Exception e) {			
		    		e.printStackTrace();				
		    	}			
		    	if(result.getResult().contains("result Bool: true")){
			
		    		result.setResult("All the Pools respect the property Proper Completion MF");				
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
				
		    	}
			
		    	////System.out.println("\n Result: "+result);			
		    	break;			
		    }	*/

		    
		    case "properCompletionMF": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
				
		    		Collaboration C = new Collaboration();					
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);					
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;													
		    		int realTimeMaudeInt=0;												
					
		    		for(int i = 0; i<poolList.size(); i++){			
					
		    			Pool pool1 = new Pool();    									    
	    				poolName=pool1.extractName(poolList.get(i));		  
	    				poolListNames.add(poolName);	
		
		    		}	//Chiusura For scorre PoolList
		    		try{
	    				result = getMaudeResult(properCompletionMFCollaboration(parsedModel , poolListNames),postMultiple);
	    				if(result.getResult()!=null){
	    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
	    						break;
	    					}
	    				}
						
	    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
							realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());											
							realTimeMaudeIntTot+=realTimeMaudeInt;		
							result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
							System.out.println("\npoolName: "+poolName);
							System.out.println("\nrealTimeMaudeInt: "+realTimeMaudeInt);
							System.out.println("\nrealTimeMaudeIntTot: "+realTimeMaudeIntTot);
							if(result.getResult()==null){										    	
								    	
								result.setResult("\nPool: "+poolName+" does not respect the Proper Completion MF property");																	
								result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
								//DEBUG									
								//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);									
								//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);								
								break;									
							}else{								
								if(!result.getResult().contains("result Bool: true")){								
									result.setResult("\nPool: "+poolName+" does not respect the Proper Completion MF property");								
									result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
    	
									//DEBUG
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);										
									break;									
								}									
							}								
	    				}							
	    			}catch(Exception e){
										
	    				e.printStackTrace();					
	    				result.setResult("Exception"+e+"\n");						
	    				return result;								    										
	    			}											
		    	} catch (Exception e) {			
		    		e.printStackTrace();				
		    	}			
		    	if(result.getResult().contains("result Bool: true")){
			
		    		result.setResult("All the Pools respect the property Proper Completion MF");				
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
				
		    	}
			
		    	////System.out.println("\n Result: "+result);			
		    	break;	
		    }	

/*COLLABORATION RESULT AFTER VERIFICATION OF PROPERTY PER EACH SINGLE POOL
/*		    
		    case "properCompletionCFMF": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
				
		    		Collaboration C = new Collaboration();					
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);					
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;													
		    		int realTimeMaudeInt=0;												
					
		    		for(int i = 0; i<poolList.size(); i++){			
					
		    			try{
						
		    				Pool pool1 = new Pool();    									    
		    				poolName=pool1.extractName(poolList.get(i));		  
		    				poolListNames.add(poolName);	
		    				result = getMaudeResult(properCompletionCFMF(parsedModel , poolName),postMultiple);
	
		    				if(result.getResult()!=null){
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
		    						break;
		    					}
		    				}
							
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
								realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());											
								realTimeMaudeIntTot+=realTimeMaudeInt;								
								if(result.getResult()==null){										    	
									    	
									result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CFMF property");																	
									result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
									//DEBUG									
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);									
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);								
									break;									
								}else{								
									if(!result.getResult().contains("result Bool: true")){								
										result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CFMF property");								
										result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
	    	
										//DEBUG
										//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
										//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);										
										break;									
									}									
								}								
		    				}							
		    			}catch(Exception e){
											
		    				e.printStackTrace();					
		    				result.setResult("Exception"+e+"\n poolName"+poolName+"i: "+i+"Vera pool: "+poolList.get(i));						
		    				return result;								    										
		    			}
					
		    		}	//Chiusura For scorre PoolList
																
		    	} catch (Exception e) {			
		    		e.printStackTrace();				
		    	}			
		    	if(result.getResult().contains("result Bool: true")){
			
		    		result.setResult("All the Pools respect the property Proper Completion CFMF");				
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
				
		    	}
			
		    	////System.out.println("\n Result: "+result);			
		    	break;			
		    }		    
	*/
		    
		    case "properCompletionCFMF": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
				
		    		Collaboration C = new Collaboration();					
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);					
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;													
		    		int realTimeMaudeInt=0;												
					
		    		for(int i = 0; i<poolList.size(); i++){			
					
		    			Pool pool1 = new Pool();    									    
	    				poolName=pool1.extractName(poolList.get(i));		  
	    				poolListNames.add(poolName);	
		
		    		}	//Chiusura For scorre PoolList
		    		try{
	    				result = getMaudeResult(properCompletionCFMFCollaboration(parsedModel , poolListNames),postMultiple);
	    				if(result.getResult()!=null){
	    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
	    						break;
	    					}
	    				}
						
	    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
							realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());											
							realTimeMaudeIntTot+=realTimeMaudeInt;		
							result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
							System.out.println("\npoolName: "+poolName);
							System.out.println("\nrealTimeMaudeInt: "+realTimeMaudeInt);
							System.out.println("\nrealTimeMaudeIntTot: "+realTimeMaudeIntTot);
							if(result.getResult()==null){										    	
								    	
								result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CFMF property");																	
								result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
								//DEBUG									
								//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);									
								//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);								
								break;									
							}else{								
								if(!result.getResult().contains("result Bool: true")){								
									result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CFMF property");								
									result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
    	
									//DEBUG
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);										
									break;									
								}									
							}								
	    				}							
	    			}catch(Exception e){
										
	    				e.printStackTrace();					
	    				result.setResult("Exception"+e+"\n");						
	    				return result;								    										
	    			}											
		    	} catch (Exception e) {			
		    		e.printStackTrace();				
		    	}			
		    	if(result.getResult().contains("result Bool: true")){
			
		    		result.setResult("All the Pools respect the property Proper Completion CFMF");				
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
				
		    	}
			
		    	////System.out.println("\n Result: "+result);			
		    	break;				
		    }	
		    
		    case "properCompletionCFParam": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
																
		    		int realTimeMaudeInt=0;												
					
		    				result = getMaudeResult(properCompletion(parsedModel , postMultiple.getPoolName()),postMultiple);
	
		    				if(result.getResult()!=null){
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
		    						break;
		    					}
		    				}
							
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
								realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());											
								realTimeMaudeIntTot+=realTimeMaudeInt;								
								if(result.getResult()==null){										    	
									    	
									result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CF property");																	
									result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
									//DEBUG									
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);									
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);																	
								}else{								
									if(!result.getResult().contains("result Bool: true")){								
										result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CF property");								
										result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
	    	
										//DEBUG
										//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
										//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);																			
									}									
								}								
		    				}							

	
		    	} catch (Exception e) {			
		    		e.printStackTrace();				
		    	}			
		    	if(result.getResult().contains("result Bool: true")){
			
		    		result.setResult("All the Pools respect the property Proper Completion CF");				
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
				
		    	}
			
		    	////System.out.println("\n Result: "+result);			
		    	break;			
		    }		
			
		    case "properCompletionMFParam": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
																
		    		int realTimeMaudeInt=0;												
					
		    				result = getMaudeResult(properCompletionMF(parsedModel , postMultiple.getPoolName()),postMultiple);
	
		    				if(result.getResult()!=null){
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
		    						break;
		    					}
		    				}
							
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
								realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());											
								realTimeMaudeIntTot+=realTimeMaudeInt;								
								if(result.getResult()==null){										    	
									    	
									result.setResult("\nPool: "+poolName+" does not respect the Proper Completion MF property");																	
									result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
									//DEBUG									
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);									
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);																	
								}else{								
									if(!result.getResult().contains("result Bool: true")){								
										result.setResult("\nPool: "+poolName+" does not respect the Proper Completion MF property");								
										result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
	    	
										//DEBUG
										//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
										//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);																			
									}									
								}								
		    				}							

	
		    	} catch (Exception e) {			
		    		e.printStackTrace();				
		    	}			
		    	if(result.getResult().contains("result Bool: true")){
			
		    		result.setResult("All the Pools respect the property Proper Completion MF");				
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
				
		    	}
			
		    	////System.out.println("\n Result: "+result);			
		    	break;			
		    }		
		    
		    case "properCompletionCFMFParam": {
				
		    	result=new PostMultipleParameters();				
		    	int realTimeMaudeIntTot=0;
											
		    	try {
																
		    		int realTimeMaudeInt=0;												
					
		    				result = getMaudeResult(properCompletionCFMF(parsedModel , postMultiple.getPoolName()),postMultiple);
	
		    				if(result.getResult()!=null){
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
		    						break;
		    					}
		    				}
							
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
								realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());											
								realTimeMaudeIntTot+=realTimeMaudeInt;								
								if(result.getResult()==null){										    	
									    	
									result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CFMF property");																	
									result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));									
									//DEBUG									
									//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);									
									//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);																	
								}else{								
									if(!result.getResult().contains("result Bool: true")){								
										result.setResult("\nPool: "+poolName+" does not respect the Proper Completion CFMF property");								
										result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
	    	
										//DEBUG
										//////System.out.println("\n OPTIONTOCOMPLETE RESULT: "+result);
										//////System.out.println("\n OPTIONTOCOMPLETE FLAGRESULT: "+flagResult);																			
									}									
								}								
		    				}							

	
		    	} catch (Exception e) {			
		    		e.printStackTrace();				
		    	}			
		    	if(result.getResult().contains("result Bool: true")){
			
		    		result.setResult("All the Pools respect the property Proper Completion CFMF");				
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
				
		    	}
			
		    	////System.out.println("\n Result: "+result);			
		    	break;			
		    }		

		    
/*COLLABORATION RESULT AFTER VERIFICATION OF PROPERTY PER EACH SINGLE POOL		   */ 
/*		    case "safeness": {
			
		    	result=new PostMultipleParameters();	
		    	int realTimeMaudeIntTot=0;
		    	
		    	try {
				
		    		Collaboration C = new Collaboration();
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;
		    		int realTimeMaudeInt=0;		
		    		for(int i = 0; i<poolList.size(); i++){

		    			try{

		    				Pool pool1 = new Pool();    						
		    				poolName=pool1.extractName(poolList.get(i));
		    				poolListNames.add(poolName);
		    				System.out.println("\nHEREPOOL"+poolName+"\n");
		    				result = getMaudeResult(safenessParam(parsedModel , poolName),postMultiple);						
		    				//DEBUG
		    				//////System.out.println("\nBefore  if(result.getResult()!=null)");
							
		    				if(result.getResult()!=null){
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
		    						break;
		    					}
		    				}
							
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
							
		    					//DEBUG								
		    					//////System.out.println("\nSTAMPO RESULT result.getPropertyVerificationTime(): "+result.getPropertyVerificationTime());								
		    					realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());	
		    					//System.out.println("\nMaude execution Time: "+realTimeMaudeInt);
		    					realTimeMaudeIntTot+=realTimeMaudeInt;	
		    					//System.out.println("\nMaude execution TimeTot: "+realTimeMaudeIntTot);
		    					result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
		    					//DEBUG
		    					//////System.out.println("\nBefore  result.getCounterexample().contains");
		    					if(result.getCounterexample()!=null){								
		    						if(result.getCounterexample().contains("counterexample")){									
		    							result.setResult("The Pool: "+poolName+" does not respect the property Safeness");
		    							result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));										
		    						}									
		    					}								
		    				}							
		    			}catch(Exception e){
						
		    				e.printStackTrace();						
		    				result.setResult("Exception"+e+"\n poolName"+poolName+"i: "+i+"Vera pool: "+poolList.get(i));						
		    				return result;
							
		    			}														
		    		}	
															
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}

		    	if(result.getResult().equals("")){
		    		result.setResult("All the Pools respect the property Safeness");
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
		    	}
				
		    	////System.out.println("\n Result: "+result);		
		    	break;	
		    }
*/
		    
		    case "safeness": {
				
		    	result=new PostMultipleParameters();	
		    	int realTimeMaudeIntTot=0;
		    	
		    	try {
				
		    		Collaboration C = new Collaboration();
		    		ArrayList<String> poolList = C.SplitPool(parsedModel);
		    		ArrayList<String> poolListNames =  new ArrayList<String>() ;
		    		int realTimeMaudeInt=0;		
		    		for(int i = 0; i<poolList.size(); i++){

		    				Pool pool1 = new Pool();    						
		    				poolName=pool1.extractName(poolList.get(i));
		    				poolListNames.add(poolName);	
		    		}
		    		
    				result = getMaudeResult(safenessCollaboration(parsedModel , poolListNames),postMultiple);	
    				
		    				//DEBUG
		    				//////System.out.println("\nBefore  if(result.getResult()!=null)");
							
		    				if(result.getResult()!=null){
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
		    						break;
		    					}
		    				}
		    				
							
	    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
							
		    					//DEBUG								
		    					//////System.out.println("\nSTAMPO RESULT result.getPropertyVerificationTime(): "+result.getPropertyVerificationTime());								
		    					realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());								
		    					realTimeMaudeIntTot+=realTimeMaudeInt;								
		    					//DEBUG
		    					//////System.out.println("\nBefore  result.getCounterexample().contains");
		    					if(result.getCounterexample()!=null){								
		    						if(result.getCounterexample().contains("counterexample")){									
		    							result.setResult("The Pool: "+poolName+" does not respect the property Safeness");
		    							result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));										
		    						}									
		    					}								
		    				}							
		    			//}catch(Exception e){
						
		    				//e.printStackTrace();						
		    				//result.setResult("Exception"+e+"\n poolName"+poolName+"i: "+i+"Vera pool: "+poolList.get(i));						
		    				//return result;							
		    			//}				    						    						    				
		    		//}	

			    	if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){						
						//DEBUG								
						//////System.out.println("\nSTAMPO RESULT result.getPropertyVerificationTime(): "+result.getPropertyVerificationTime());								
						realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());								
						realTimeMaudeIntTot+=realTimeMaudeInt;								
						//DEBUG
						//////System.out.println("\nBefore  result.getCounterexample().contains");
						if(result.getCounterexample()!=null){								
							if(result.getCounterexample().contains("counterexample")){									
								result.setResult("The Collaboration does not respect the property Safeness");
								result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));										
							}									
						}								
					}
			    	if(result.getResult().equals("")){
			    		result.setResult("All the Pools respect the property Safeness");
			    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
			    	}
			    	
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}
				
		    	////System.out.println("\n Result: "+result);		
		    	break;	
		    }
		    
		    
		    case "safenessParam": {
				
		    	result=new PostMultipleParameters();	
		    	int realTimeMaudeIntTot=0;
		    	
		    	try {
				

		    		int realTimeMaudeInt=0;		

		    				result = getMaudeResult(safenessParam(parsedModel , postMultiple.getPoolName()),postMultiple);						
		    				//DEBUG
		    				//////System.out.println("\nBefore  if(result.getResult()!=null)");
							
		    				if(result.getResult()!=null){
		    					if(result.getResult().contains("TimerMaude")&&result.getResult().contains("exceeded")){
		    						break;
		    					}
		    				}
							
		    				if(!result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
							
		    					//DEBUG								
		    					//////System.out.println("\nSTAMPO RESULT result.getPropertyVerificationTime(): "+result.getPropertyVerificationTime());								
		    					realTimeMaudeInt = Integer.parseInt(result.getPropertyVerificationTime());								
		    					realTimeMaudeIntTot+=realTimeMaudeInt;								
		    					//DEBUG
		    					//////System.out.println("\nBefore  result.getCounterexample().contains");
		    					if(result.getCounterexample()!=null){								
		    						if(result.getCounterexample().contains("counterexample")){									
		    							result.setResult("The Pool: "+poolName+" does not respect the property Safeness");
		    							result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));										
		    						}									
		    					}								
		    				}							

															
		    	} catch (Exception e) {				
		    		e.printStackTrace();					
		    	}

		    	if(result.getResult().equals("")){
		    		result.setResult("All the Pools respect the property Safeness");
		    		result.setPropertyVerificationTime(String.valueOf(realTimeMaudeIntTot));
		    	}
				
		    	////System.out.println("\n Result: "+result);		
		    	break;	
		    }		    
		    
		    default: 	
		    	result.setResult("ERROR: the received property cannot be verified"); 
		    	break;												
		    }

		    return result;												
		}
		
		private String extractRealTime(String line) {
			String lineCopy=line;
			String[] result = lineCopy.split("\\(");
			//DEBUG
			//////System.out.println("\nRealTime"+result[1]);
			String[] result2 = result[1].split(" ");
			//DEBUG
			//////System.out.println("\nRealTime"+result2[0]);
			return result2[0].substring(0, result2[0].length() - 2);
			
		}
		
/*getMaudeResultNoCounterexampleBySearch	for No Dead Activities only	*/
		@SuppressWarnings("null")
		public PostMultipleParameters getMaudeResultNoCounterexampleBySearch(String property, PostMultipleParameters postMultiple){
			
			//property=property.replace("&&", "/"+"\\");
			//DEBUG
			//////System.out.println("\ngetMaudeResultNoCounterexampleBySearch");
			//////System.out.println("\nProperty"+property);
			boolean noSolution = false;
			BufferedWriter pbr = null;
			BufferedReader br = null;
			OutputStream os = null;
			OutputStreamWriter osw = null;
			InputStreamReader isr = null;
			InputStream is = null; 
			Process MaudeProcess = null;	
			PostMultipleParameters result = null;
			//DEBUG
			////System.out.println("\nBefore Launching Maude\n");
			//Launch Maude
			try {
				long currentTime = System.currentTimeMillis();
			
				MaudeProcess = new ProcessBuilder(path, "-no-banner", "-no-wrap").start();				
				//DEBUG
				//////System.out.println("\nStart Maude\n");
				os = (OutputStream) MaudeProcess.getOutputStream();
				osw = new OutputStreamWriter(os);
				pbr = new BufferedWriter(osw);
				pbr.write(" -random-seed="+currentTime+ "\n");
				pbr.flush();
				pbr.write("load BPMNOS_MODEL_CHECKER.maude\n");
				pbr.flush();
				pbr.write("set show command off .\n");
				pbr.flush();
				//DEBUG
				//////System.out.println("\nLoad Files\n");
				is = MaudeProcess.getInputStream();
				isr = new InputStreamReader(is);
				br = new BufferedReader(isr);
			   
			    //DEBUG
				//////System.out.println("\nMaude Launched in getMaudeResultNoCounterexampleBySearch\n");

			} catch (IOException e) {
				try {
					pbr.close();			
					os.close();
					osw.close();
					MaudeProcess.destroy();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				e.printStackTrace();
			}
			
			String realTimeMaude = null;

			try {
				
				//here it goes the timerThread call
				//DEBUG
				//////System.out.println("\nTimer Launched...");
				maudeTimer myThread = new maudeTimer(maxTimer,MaudeProcess);
				Thread th = new Thread(myThread);
			    th.start();
			  
			    //DEBUG
				//////System.out.println("\nProperty "+property);
				pbr.write(property+" \n");
				pbr.flush();	
				pbr.write("reduce true and false and true and false . \n");
				pbr.flush();
				//to test only one line
			   	String line = "";

		    	while(true){	
		    		
					line = "";	
					
					try{
					line = br.readLine();
						
					if(line != null){
						
						if(line.equals("\n")){
							result.setResult("result2\n"+line+"\nERROR");
							try {
								pbr.close();			
								br.close();
								os.close();
								osw.close();
								isr.close();
								is.close(); 
								MaudeProcess.destroy();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
				    		////System.out.println("Result: "+result);
				    		try{
								myThread.setStop(true);
							}catch(Exception e){
								e.printStackTrace();
								////System.out.println("result5340: "+result.getResult());
								return result;
							}
				    		////System.out.println("result5344: "+result.getResult());
							return result;
				    	}
						
						if(line.contains("Warning")){
				    		result.setResult("result3\n"+line+"\nERROR");
				    		//////System.out.println("Result: "+result);
				    		try {
								pbr.close();			
								br.close();
								os.close();
								osw.close();
								isr.close();
								is.close(); 
								MaudeProcess.destroy();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
				    		try{
								myThread.setStop(true);
							}catch(Exception e){
								e.printStackTrace();
								////System.out.println("result5368: "+result.getResult());
								return result;
							}
				    		////System.out.println("result5372: "+result.getResult());
							return result;
				    	}

						////System.out.println("\n line: "+line); 
						if(line.contains("No solution")){
							////System.out.println("\n NO SOLUTION");
							noSolution=true;
							continue;
						}
						
						if(line.contains("ms real")&&noSolution){
							realTimeMaude=extractRealTime(line);
							////System.out.println("\n noSolution QUESTOOOrealTimeMaude: "+realTimeMaude);
							break;
						}
						
						if(line.contains("ms real")&&!noSolution){
							realTimeMaude=extractRealTime(line);
							////System.out.println("\n !noSolution QUESTOOOrealTimeMaude: "+realTimeMaude);
							break;
						}

					}else{
						result.setResult("Error, Maude LTL Model Checker could not handle the model");
						////System.out.println("Result: "+result);
						try {
							pbr.close();			
							br.close();
							os.close();
							osw.close();
							isr.close();
							is.close(); 
							MaudeProcess.destroy();
						} catch (IOException e1) {
							e1.printStackTrace();
						}
						try{
							myThread.setStop(true);
						}catch(Exception e){
							e.printStackTrace();
							////System.out.println("result5421: "+result.getResult());
							return result;
						}
						////System.out.println("result5425: "+result.getResult());
						return result;
						}
					}catch(Exception e){
						 
						 e.printStackTrace();
						 result.setResult("TimerMaude of "+maxTimer+" exceeded for processing the verification\n"); 	
						 postMultiple.setPropertyVerificationTime(String.valueOf(maxTimer));	
						 ////System.out.println("\nresult=TimerMaude of exceeded for processing the verification\n");
							try {
								pbr.close();			
								br.close();
								os.close();
								osw.close();
								isr.close();
								is.close(); 
								MaudeProcess.destroy();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
							try{
								myThread.setStop(true);
							}catch(Exception e2){
								e2.printStackTrace();
		
								////System.out.println("result5452: "+result.getResult());
								return result;
							}

							////System.out.println("result: "+result.getResult());
							////System.out.println("result5457: "+result.getResult());
							return result;			 
					 }
				} //Chiusura while(true)
		    	if(noSolution){	
		    		////System.out.println("\nnoSolution: "+noSolution);
   		
		    		try {
						pbr.close();			
						br.close();
						os.close();
						osw.close();
						isr.close();
						is.close(); 
						MaudeProcess.destroy();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
		    		try{
						myThread.setStop(true);
					}catch(Exception e){
						e.printStackTrace();
						//postMultiple.setResult(result);
						////System.out.println("result5481: "+result.getResult());
						return result;
					}
		    		postMultiple.setResult("result Bool: false");
		    		postMultiple.setPropertyVerificationTime(realTimeMaude);
					return postMultiple;
		    	
		    	}else{
		    		try {
						pbr.close();			
						br.close();
						os.close();
						osw.close();
						isr.close();
						is.close(); 
						MaudeProcess.destroy();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
		    		try{
						myThread.setStop(true);
					}catch(Exception e){
						e.printStackTrace();
						return result;
					}
		    		////System.out.println("\nnoSolution: "+noSolution);
		    		postMultiple.setResult("result Bool: true");
		    		postMultiple.setPropertyVerificationTime(realTimeMaude);
		    		return postMultiple;
		    	} 
		    	
				} catch (IOException e) {

					result.setResult("TimerMaude of "+maxTimer+" exceeded for processing the verification\n"); 	
					postMultiple.setPropertyVerificationTime(String.valueOf(maxTimer));
					try {
						pbr.close();			
						br.close();
						os.close();
						osw.close();
						isr.close();
						is.close(); 
						MaudeProcess.destroy();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					e.printStackTrace();
					//DEBUG
					//////System.out.println("Result: "+result);
					//postMultiple.setResult(result);
					return result;
				}
	}
	
}
