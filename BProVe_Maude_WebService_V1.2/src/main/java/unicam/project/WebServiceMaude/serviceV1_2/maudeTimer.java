package unicam.project.WebServiceMaude.serviceV1_2;

public class maudeTimer implements Runnable {

    private long maxTime;
    private Process MaudeProcess;
    private Boolean stop = false;
    
    public maudeTimer(long maxTime, Process MaudeProcess) {
        this.maxTime = maxTime;
        this.MaudeProcess = MaudeProcess;
       // run();
    }

    public void run() {
    	long timer;
        // code in the other thread, can reference "var" variable
    	timer=System.currentTimeMillis();
    	//System.out.println("\n running timer thread (inside thread)");
    	boolean timerNotExceeded=true;
    	
    	while(timerNotExceeded&&!stop){
	    	if(System.currentTimeMillis()-timer>maxTime){
	    		//System.out.println("\nTimer Exceeded");	
	    		timerNotExceeded=false;
	    		try{
	    			
	    		MaudeProcess.destroy();
	    		//System.out.println("\nMaudeProcess: "+MaudeProcess+" has been destroyed"); 
	    		}catch(Exception e){
	    			//System.out.println("\nI can't kill the process");
	    			e.printStackTrace();
	    		}
	    	}
	    	
	    	if (Thread.currentThread().isInterrupted()) {
	    		//System.out.println("\nThread has been interrupted");
	    	    break;
	    	}
    	}
    }
    
    public void setStop(Boolean stop) {
    	//System.out.println("\nThead has been stopped");
        this.stop = stop;
    }      
}
