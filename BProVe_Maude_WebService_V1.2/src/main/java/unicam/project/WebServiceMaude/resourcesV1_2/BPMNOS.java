package unicam.project.WebServiceMaude.resourcesV1_2;


import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

import javax.xml.transform.*;
import javax.xml.transform.dom.*;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

//import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import plugin.bpmn.to.maude.handlers.PostMultipleParameters;
import plugin.bpmn.to.maude.notation.CollaborationCompare;
import plugin.bpmn.to.maude.notation.Counterexample;
import plugin.bpmn.to.maude.notation.Proc;
import unicam.project.WebServiceMaude.serviceV1_2.BProVe_Maude_WebService;

@Path("/BPMNOS")
public class BPMNOS {
	
	BProVe_Maude_WebService webService = new BProVe_Maude_WebService();
	String returnedString=null;
	String parsedModel=null;
	Document counterexample=null;
	PostMultipleParameters resultingPostMultipleParameters=null;
	
	
//1 get parsedModel	
	   @POST
	   @Path("/parseModel")
	   @Consumes(MediaType.APPLICATION_JSON)
	   @Produces(MediaType.APPLICATION_JSON)

		public byte[] getModel( byte[] request ) throws Exception{
		   //I read the request content
			  PostMultipleParameters postMultiple = null;
		   	  
			  String jsonString = new String(request, StandardCharsets.UTF_8);
		   	  postMultiple=json2Class(jsonString);
	   	  
			  PostMultipleParameters outputMultiple = null;
			  outputMultiple=webService.getParsedModel(postMultiple);
			  
			  String modelToJson = class2Json(outputMultiple);
			  byte[] responseBytes = modelToJson.getBytes();
			  
		      return responseBytes;
	  }
  
	   
//2 get verification		  
	  @POST
	   @Path("/model/verification")
	   @Consumes(MediaType.APPLICATION_JSON)
	   @Produces(MediaType.APPLICATION_JSON)

		public byte[] getVerification(byte[] request) throws IOException, ClassNotFoundException {
		  
		  //I read the request content
		  PostMultipleParameters postMultiple = null;
	   	  
		  String jsonString = new String(request, StandardCharsets.UTF_8);
		  System.out.println("jsonString: "+jsonString);
	   	  postMultiple=json2Class(jsonString);
	   	  	   	  
	   	  Counterexample counterEx;
	   	  
	   	  resultingPostMultipleParameters=webService.getVerificationResultPostMultipleParameters(postMultiple);
		  
	   	  ////System.out.println("\nResultOfPropertyVerification getCounterexample: "+resultingPostMultipleParameters.getCounterexample());
	   	  ////System.out.println("\nResultOfPropertyVerification getResult: "+resultingPostMultipleParameters.getResult());
		  
			  ////System.out.println("\nNot A New Property");

			  if(resultingPostMultipleParameters.getCounterexample()!=null){
			  //MANAGE THE COUNTEREXAMPLE AND RETURN A SINGLE COLLABORATION REPRESENTING THE COUNTEREXAMPLE 
				  //System.out.println("\nResult is NOT true");
				  try {
					
					  //DA MODIFICARE
					  String violatingTrace = resultingPostMultipleParameters.getCounterexample();
					  
					  counterEx = CollaborationCompare.getCounterexample( violatingTrace , postMultiple.getOriginalModel());
					  counterexample=counterEx.getViolatingCounterexample();
					  Proc finalprocess=counterEx.getViolatingProcess();
					  ////System.out.println("\n Process containing the violating elements to be colored: \n");
					  //finalprocess.printProcess();
					  
					  resultingPostMultipleParameters.setViolatingTrace(fromProcToByteArray(finalprocess));
					  ////System.out.println("resultingPostMultipleParameters.getViolatingTrace(): "+resultingPostMultipleParameters.getViolatingTrace());
						
					  try{
							
						  //System.out.println("\ncounterexample: "+counterexample);							
						  if(counterexample==null){
								  
							  ////System.out.println("\n COUNTEREXAMPLE IS NULL");							
							  resultingPostMultipleParameters.setResult("Error, the Counterexample could not be generated");						  
							  resultingPostMultipleParameters.setCounterexample("Error, the Counterexample could not be generated");							
							  //resultingPostMultipleParameters.printPostMultipleParameters();							
//							  ByteArrayOutputStream out = new ByteArrayOutputStream();							  
//							  ObjectOutputStream os = new ObjectOutputStream(out);   							  
//							  os.writeObject(resultingPostMultipleParameters);
							  String modelToJson = class2Json(resultingPostMultipleParameters);
							  byte[] responseBytes = modelToJson.getBytes();
							  return responseBytes;

							}
						  
						    DOMSource domSource = new DOMSource(counterexample);
					        StringWriter writer = new StringWriter();
					        StreamResult result = new StreamResult(writer);
					        TransformerFactory tf = TransformerFactory.newInstance();
					        Transformer transformer = tf.newTransformer();
					        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
					        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
					        transformer.transform(domSource, result);
					        StringBuffer sb = writer.getBuffer(); 
					        String finalstring = sb.toString();
					        writer.flush();					        
					        //Closing stream
					        writer.close();
						    resultingPostMultipleParameters.setCounterexample(finalstring);
						
						  } catch(TransformerException ex){
							  //Closing stream
						      ex.printStackTrace();
						      //System.out.println("\n UN ERRORE IS NULL");
						      resultingPostMultipleParameters.setResult("Error, the Counterexample could not be generated");
						      resultingPostMultipleParameters.setCounterexample("Error, the Counterexample could not be generated");							
						  }
						
					} catch (Exception e) {
						//System.out.println("\n counterexample not generated catch getcounterexample\n");
						e.printStackTrace();
						// TODO Auto-generated catch block
						if(true){
							//System.out.println("\n if(true){");
							//Closing stream
					        resultingPostMultipleParameters.setResult("Error, the Counterexample could not be generated");
					        resultingPostMultipleParameters.setCounterexample("Error, the Counterexample could not be generated");
						}
						
					}
				}//If the counterexample is present handle it, otherwise do nothing

		  //resultingPostMultipleParameters.printPostMultipleParameters();
		  
		  String modelToJson = class2Json(resultingPostMultipleParameters);
		  byte[] responseBytes = modelToJson.getBytes();
		  
	      return responseBytes;

	  }
	  	  
	  
	public byte[] fromProcToByteArray(Proc finalprocess){
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		
		try {			  
			ObjectOutputStream os = new ObjectOutputStream(out);
			os.writeObject(finalprocess);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return out.toByteArray();
	}
	
	 private PostMultipleParameters json2Class(String jsonString) {
			ObjectMapper mapper = new ObjectMapper();

			PostMultipleParameters jsonPostMultipleParameters = new PostMultipleParameters();
			
			try {
			
				jsonPostMultipleParameters = mapper.readValue(jsonString, PostMultipleParameters.class);
			
			} catch (JsonGenerationException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return jsonPostMultipleParameters;
		}
	 
	 public static String class2Json(PostMultipleParameters pModel){
			ObjectMapper mapper = new ObjectMapper();
			String jsonInString = null;
			try {
				// Convert object to JSON string and save into a file directly
				//mapper.writeValue(new File("\\Users\\user\\Desktop\\staff.json"),pModel);
				//String temp = mapper.writeValueAsString(pModel);

				// Convert object to JSON string
				jsonInString = mapper.writeValueAsString(pModel);
//				System.out.println(jsonInString);

				// Convert object to JSON string and pretty print
				//jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pModel);
				//System.out.println(jsonInString);

			} catch (JsonGenerationException e123) {
				e123.printStackTrace();
			} catch (JsonMappingException e123) {
				e123.printStackTrace();
			} catch (IOException e123) {
				e123.printStackTrace();
			}
				return jsonInString;
		   }
				

	  
	
}
