# BProVe - Business Process Verifier #

BProVe a novel verification framework for BPMN 2.0. 
The analysis is based on a formal operational semantics, 
implemented using MAUDE, defined for the BPMN 2.0 modelling language, 
and is provided as a freely accessible service that uses open standard formats as input data. 
Furthermore a plug-in for the Eclipse platform has been developed making available 
a tool chain supporting users in modelling and visualising, 
in a friendly manner, the results of the verification.

### BProVe - Website ###
http://pros.unicam.it/bprove/

### BProVe - Main Author ###
Fabrizio Fornari fabrizio.fornari@unicam.it

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact