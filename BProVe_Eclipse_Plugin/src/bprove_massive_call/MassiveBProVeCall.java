package bprove_massive_call;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import plugin.bpmn.to.maude.getService.GetReq;
import plugin.bpmn.to.maude.handlers.PostMultipleParameters;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.FlowLayout;
import java.awt.Component;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MassiveBProVeCall extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	static boolean unix =true;
	
	public JFrame frame;
	public static JTextArea textArea;
	
	public File modelToParse;
    
	static String Loadfilepath = "";
    static String Savefilepath = "";
    static String Maudepath = "";

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MassiveBProVeCall frame = new MassiveBProVeCall();
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}
	
	public MassiveBProVeCall() {
		
		//DEBUG 
		//////System.out.println("Loadfilepath: "+Loadfilepath+"\n");
		//DEBUG JOptionPane.showMessageDialog(null, "LOADPATH "+Loadfilepath);
		
		setTitle("BProVe_Massive_Call");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 700, 500);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
	
		//Massive Translation and Verification Commented Out (set to run only on localhost)
		
		//PANEL Second Label
		JPanel panel_2 = new JPanel();
		JLabel lblTheSecondPart = new JLabel("The second part of the menu is reserved for operation on a set of BPMN models.");
		panel_2.add(lblTheSecondPart);
		contentPane.add(panel_2);
		
		//PANEL MASSIVE TRANSLATION
		JPanel massive_panel = new JPanel();
		JButton btnmassivo = new JButton("MASSIVE TRANSLATION");
		btnmassivo.addActionListener(new massiveActionListener());
		massive_panel.add(btnmassivo);
						
		JButton buttonExit2 = new JButton("EXIT");
		buttonExit2.addActionListener(new exitActionListener());
		massive_panel.add(buttonExit2);
		contentPane.add(massive_panel);
	
		

}
	   //massiveActionListener
	   public class massiveActionListener implements ActionListener{
		      public void actionPerformed(ActionEvent e) {
		    	  
		    	frame = new JFrame("Massive Translation");
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		          	    	  
		        String userDir = System.getProperty("user.home");
		  		JFileChooser fileChooser = new JFileChooser(userDir +"/Desktop");
	        	//choose directory from which reading the .bpmn files
		  		fileChooser.setDialogTitle("Choose bpmn models directory");
		  		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				String path = "";
				ArrayList<File> files = new ArrayList<File>();

				if (fileChooser.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION) {
					path = fileChooser.getSelectedFile().getAbsolutePath();
					File directory = new File(path);
					// get all the files from a directory
					File[] fList = directory.listFiles();
					
					for (File file : fList) {
						if(file.isFile())
						{
							if(file.getName().endsWith(".bpmn"))
							{
								//System.out.println(file.getName());
								files.add(file);
							}
						}
					}
	
					//System.out.println("Selected directory " + path);
				} else {
					//System.out.println("Choice aborted");
				}
				
			  	JFileChooser chooser = new JFileChooser(userDir +"/Desktop");
 	  
		    	chooser = new JFileChooser(); 
		    	chooser.setCurrentDirectory(new java.io.File("."));
		    	chooser.setDialogTitle("Save Path");
		    	chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		    	chooser.setAcceptAllFileFilterUsed(false);
		    	  
		    	////System.out.print("\nNumber of uploaded files: "+files.length+"\n");
		    	   
		    	textArea = new JTextArea(20, 35);
			    textArea.setText("Massive Translation  & Verification IN PROGRESS...\n");
			    textArea.setEditable(false);
			    JScrollPane scrollArea = new JScrollPane(textArea);
			          
			    frame.add(scrollArea);		          
			    frame.pack();
			    frame.setVisible(true);
			    frame.revalidate();
		    	frame.repaint();	
		    	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		    	start(files , chooser, textArea);

		      }
	   }//class massiveActionListener implements ActionListener
	   	   
	class exitActionListener implements ActionListener{
		      public void actionPerformed(ActionEvent e) {	
		    	  MassiveBProVeCall.this.dispose();
		      }
	   }
	
	public static void start(final ArrayList<File> files, final JFileChooser chooser, final JTextArea textArea){
		
		 SwingWorker<Boolean, String> worker = new SwingWorker<Boolean , String>(){

				@Override
				protected Boolean doInBackground() throws Exception {

			    	  JSONArray statisticsList = new JSONArray();
			    	  String toSave=null;
		    		  boolean massiveCheckOk=false;
			    	  int index2=0;
			    	  	    	      
			    	  publish("Massive Translation & Verification IN PROGRESS...");   		 
			    	  	    		  
			    	  for(int i = 0; i < files.size(); i++ ) {
			    		  long parsingTime=0;			    		  
			    		  long totalVTime = System.currentTimeMillis();
			    		  
			    		  Loadfilepath = files.get(i).getAbsolutePath();
			    		     int index;	  
			    			if(!System.getProperty("os.name").startsWith("Windows")){
			    				  unix=true;
			    				  index = Loadfilepath.lastIndexOf("/");	
			    			}else{
			    				  index = Loadfilepath.lastIndexOf("\\");	
			    				  unix=false;
			    			}
	
			    		  String nomefile = Loadfilepath.substring(index);
			    		  ////System.out.print("\nName of the file under parsing: "+nomefile+"\n");
			    		  nomefile = nomefile.substring(0, nomefile.length() - 5);
			    	      
			    		  publish("Massive Translation & Verification IN PROGRESS...\n Analyzing model: "+nomefile);			    		 

			    		  Savefilepath=Loadfilepath.substring(0, index);
			    		  ////System.out.println();
			    		  Savefilepath = Savefilepath +nomefile;
			    		  		    		  			    		  
				    	  File modelToParse = new File(Loadfilepath);
				    	  
				    	  //jsonobject di appoggio
				    	  JSONObject rootObj = new JSONObject();
				    	  
				    	  PostMultipleParameters massiveParsedModel = new PostMultipleParameters();
				    	  PostMultipleParameters massiveM = new PostMultipleParameters();
							String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
					  		//////System.out.println("\noriginalModelUrl"+originalModelUrl+"\n"); 
					  		massiveM.setOriginalModel(originalModelUrl);		
					  		
				          	 
				    	  try {
				    		  	parsingTime=System.currentTimeMillis();
				    		  	massiveParsedModel=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(massiveM);
					    		parsingTime=System.currentTimeMillis() - parsingTime;

				    	  } catch (Exception e1) {
								e1.printStackTrace();
							}
				    	  
				    	  //scrivo comunque su file
				    	     File file = new File(Savefilepath+".txt");
				     		 FileWriter fw = null;
				     		 try {
								fw = new FileWriter(file);
							} catch (IOException e2) {
								e2.printStackTrace();
							}
				     		 try{//check if a parsedModel has been returned, if not signal that there is a problem
				     		//Duplicated label check 
				     		if (massiveParsedModel.getParsedModel().contains("Correct Duplicated Labels")){  
				     			////System.out.println("\nThe parsing encountered some problems.\n"+massiveParsedModel.getParsedModel());
					    		fw.write("\nThe parsing encountered some problems.\n"+massiveParsedModel.getParsedModel());	
					    		fw.flush();
					    		continue;
							}
				     		
				     		//Ineligible BPMN elemenst check
					    	if (massiveParsedModel.getParsedModel().contains("The model presents the following ineligible BPMN elements")){
					    		////System.out.println("\n The model presents the following ineligible BPMN elements \n");
					    		 fw.write(massiveParsedModel.getParsedModel());
					    		 fw.flush();
					    	     
					    	}else{
					    		
					    		////System.out.println("\n Property \n");
					    		  try {
							    	    
							    	    String nomefileTest = nomefile.substring(1, nomefile.length() );
							    	    ////System.out.println("\n modelName: "+nomefileTest);
	 
							    	    File modelToParseAppoggio = modelToParse;
										verifyAndWrite(fw,modelToParseAppoggio,massiveParsedModel,parsingTime,nomefileTest,rootObj,totalVTime,statisticsList,index2,toSave,unix);

							 	     fw.flush();
//						    	     fw.close();
								} catch (IOException e1) {

									e1.printStackTrace();
								}
						   
					    	}//chiude if else Ineligible elements
				     		 				    	
					     }catch(NullPointerException e){
					     	fw.write("Some undefined problem has occured during the parsing or the request to the server. It may be possible that some sequence flow is missing (e.g. task without any outgoing sequence flow)");
							fw.flush();			     			
					     }
			    	  }//for(int i = 0; i < files.size(); i++ )
		    		  
 
			    	  Component parentFrame = null;

			    	  
			    	  //FOR SAVING THE MassiveStatistics AS A JSONARRAY OF JSONOBJECT, EACH JSONOBJECT REFERES TO STATISTICS OF A SINGLE MODEL
			    	  //THE JSONARRAY IS SAVED ONLY AT THE END OF THE MASSIVE TRANSALTION/VERIFICATION
			    	  //TO HAVE INTERMEDIATE RESULTS (IF THE MASSIVE TRANSALTION IS ABORTED)
			    	  //THE BCKPMassiveStatistics IS SAVED EVERY TIME A SINGLE MODEL IS ANALYZED AND 
		    	      //A JSONOBJECT IS POPULATED WITH STATISTICS OF THAT MODEL
		    	      //BCKPMassiveStatistics.json CONTAINS ONLY JSONOBJECTs NOT ARRENGED IN A JSONARRAY
			    	  //MassiveStatistics.json CONTAINS THE JSONARRAY POPULATED WITH ALL THE GENERATED JSONOBJECTs
		    		  if(unix){
		    			  index2 = Savefilepath.toString().lastIndexOf("/");	
		    			  toSave = Savefilepath.toString().substring(0, index2);

			    	  }else{
		    			  index2 = Savefilepath.toString().lastIndexOf("\\");	
		    			  toSave = Savefilepath.toString().substring(0, index2);
			    	  }
		    		  
	  
				      try (FileWriter file2 = new FileWriter(toSave+"/MassiveStatistics.json",true)) {		 					    		 
						   file2.write(statisticsList.toJSONString());
						   file2.flush();
						   massiveCheckOk=true;
	
					  } catch (IOException e3) {
						   e3.printStackTrace();
					  }
			    		   		  
		    		  
		    		if(massiveCheckOk)JOptionPane.showMessageDialog(parentFrame, "MASSIVE TRANSLATION COMPLETED! CHECK RESULTING FILES IN:"+toSave);
		    		else JOptionPane.showMessageDialog(parentFrame, "MASSIVE TRANSLATION ENDED INCORRECTLY! CHECK RESULTING FILES IN:"+toSave);
		    		
					return true;
				}
				
				@SuppressWarnings("unchecked")
				private void verifyAndWrite(FileWriter fw, File modelToParse, PostMultipleParameters massiveParsedModel, long parsingTime, String nomefileTest, JSONObject rootObj, long totalVTime, JSONArray statisticsList, int index2, String toSave, boolean unix) throws IOException {
					String newLine;
					
				     rootObj.put("modelName", nomefileTest);

				     rootObj.put("parsingTime", Long.toString(parsingTime));
				     rootObj.put("numberOfElement", Integer.toString(massiveParsedModel.getNumberOfElement()));
				     rootObj.put("numberOfPool", Integer.toString(massiveParsedModel.getNumberOfPool()));
					 rootObj.put("numberOfStart", Integer.toString(massiveParsedModel.getNumberOfStart()));
					 rootObj.put("numberOfStartMsg", Integer.toString(massiveParsedModel.getNumberOfStartMsg()));
					 rootObj.put("numberOfEnd", Integer.toString(massiveParsedModel.getNumberOfEnd()));
					 rootObj.put("numberOfEndMsg", Integer.toString(massiveParsedModel.getNumberOfEndMsg()));
					 rootObj.put("numberOfTerminate", Integer.toString(massiveParsedModel.getNumberOfTerminate()));
					 rootObj.put("numberOfTask", Integer.toString(massiveParsedModel.getNumberOfTask()));
					 rootObj.put("numberOfTaskSnd", Integer.toString(massiveParsedModel.getNumberOfTaskSnd()));
					 rootObj.put("numberOfTaskRcv", Integer.toString(massiveParsedModel.getNumberOfTaskRcv()));
					 rootObj.put("numberOfIntermidiateThrowEvent", Integer.toString(massiveParsedModel.getNumberOfIntermidiateThrowEvent()));
					 rootObj.put("numberOfIntermidiateCatchEvent", Integer.toString(massiveParsedModel.getNumberOfIntermidiateCatchEvent()));
					 rootObj.put("numberOfGatewayXorSplit", Integer.toString(massiveParsedModel.getNumberOfGatewayXorSplit()));
					 rootObj.put("numberOfGatewayXorJoin", Integer.toString(massiveParsedModel.getNumberOfGatewayXorJoin()));
					 rootObj.put("numberOfGatewayAndSplit", Integer.toString(massiveParsedModel.getNumberOfGatewayAndSplit()));
					 rootObj.put("numberOfGatewayAndJoin", Integer.toString(massiveParsedModel.getNumberOfGatewayAndJoin()));
					 rootObj.put("numberOfGatewayOrSplit", Integer.toString(massiveParsedModel.getNumberOfGatewayOrSplit()));
					 rootObj.put("numberOfGatewayEventBased", Integer.toString(massiveParsedModel.getNumberOfGatewayEventBased()));
									
					if(unix) newLine="\n";
					else newLine="\r\n";
					
					writeElementAndParsingTime(fw,massiveParsedModel,parsingTime,unix);
		    	    
		    	    try {
		    	    	//COMMENTED TO FASTEN THE MASSIVE VERIFICATION, WE VERIFY ONLY NODEADACTIVITIES, OPTIONTOCOMPLETE, PROPERCOMPLETION AND SAFENESS						
						//PostMultipleParameters propertyResult1=null;
						//PostMultipleParameters propertyResult2=null;
						//PostMultipleParameters propertyResult3=null;
						PostMultipleParameters propertyResult4=null;
						PostMultipleParameters propertyResult5=null;
						PostMultipleParameters propertyResult6=null;
						PostMultipleParameters propertyResult62=null;
						PostMultipleParameters propertyResult63=null;
						PostMultipleParameters propertyResult7=null;
						//COMMENTED TO FASTEN THE MASSIVE VERIFICATION, WE VERIFY ONLY NODEADACTIVITIES, OPTIONTOCOMPLETE, PROPERCOMPLETION AND SAFENESS						
						//String propertyMaudeTime1=null;
						//String propertyMaudeTime2=null;
						//String propertyMaudeTime3=null;
						String propertyMaudeTime4=null;
						String propertyMaudeTime5=null;
						String propertyMaudeTime6=null;
						String propertyMaudeTime62=null;
						String propertyMaudeTime63=null;
						String propertyMaudeTime7=null;
						int totalMaudeVerificationTime = 0;
						
						//COMMENTED TO FASTEN THE MASSIVE VERIFICATION, WE VERIFY ONLY NODEADACTIVITIES, OPTIONTOCOMPLETE, PROPERCOMPLETION AND SAFENESS						
						//String propertyCounterexampleTime1="-1";
						//String propertyCounterexampleTime2="-1";
						//String propertyCounterexampleTime3="-1";
						String propertyCounterexampleTime4="-1";
						String propertyCounterexampleTime5="-1";
						String propertyCounterexampleTime6="-1";
						String propertyCounterexampleTime62="-1";
						String propertyCounterexampleTime63="-1";
//09/10/2017 COMMENTED BUT WORKING
//COMMENTED TO FASTEN THE MASSIVE VERIFICATION, WE VERIFY ONLY NODEADACTIVITIES, OPTIONTOCOMPLETE, PROPERCOMPLETION AND SAFENESS	
//Remove comments to verify also the rest of the properties				
////1							
//						long property1VertificationTime=System.currentTimeMillis();
//						////System.out.println("\n Into Property Try \n");
////						byte[] originalModelByte = Files.readAllBytes(Paths.get(modelToParse.toString()));
////						String originalModel = new String(originalModelByte, "UTF-8");				
//						String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
//						PostMultipleParameters postMultiple = new PostMultipleParameters();
//						postMultiple.setOriginalModel(originalModelUrl);
//						postMultiple.setParsedModel(massiveParsedModel.getParsedModel());
//						postMultiple.setProperty("aBPoolstarts");
//						propertyResult1=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
//						property1VertificationTime=System.currentTimeMillis()-property1VertificationTime;
//						propertyMaudeTime1 = "RealMaudeTimeaBPoolstarts: "+propertyResult1.getPropertyVerificationTime();
//						totalMaudeVerificationTime+=Integer.valueOf(propertyResult1.getPropertyVerificationTime());
//						
//						if(propertyResult1.getCounterexample()!=null && !propertyResult1.getCounterexample().contains("Counterexample could not be generated")&&!propertyResult1.getCounterexample().contains("counterexample is null")
//								&&!propertyResult1.getCounterexample().contains("TimerMaude")&&!propertyResult1.getCounterexample().contains("exceeded")){	
//							////System.out.println("\n-----------propertyResult1PostaBPoolstarts----------\n"+propertyResult1.getResult());		
//							rootObj.put("aBPoolStartTime", Long.toString(property1VertificationTime));
//							rootObj.put("aBPoolStartRealMaudeTime", propertyResult1.getPropertyVerificationTime());
//
//							//if(propertyResult1.getCounterexample().contains("counterexample")){
//								propertyResult1.setResult("aBPoolstarts property is not verified");
//								long propertyCounterexampleTime1Long=System.currentTimeMillis();
//								//String [] counterexample=maudeTime[0].split("£");
//								writeBPMNcounterexample(propertyResult1.getCounterexample(),Savefilepath,"aBPoolstarts");
//								propertyCounterexampleTime1Long=System.currentTimeMillis()-propertyCounterexampleTime1Long;
//								propertyCounterexampleTime1=Long.toString(propertyCounterexampleTime1Long);
//								//System.out.println("\naBPoolStartCounterexampleTime1: "+propertyCounterexampleTime1);
//								rootObj.put("aBPoolStartCounterexampleTime", propertyCounterexampleTime1);
//								rootObj.put("aBPoolStartResult", "False");
//								rootObj.put("aBPoolstarts", "aBPoolstarts property is not verified");
//							//}
//							
//							//propertyResult1.setResult(propertyResult1.getResult()+" - "+"VerificationTime: "+property1VertificationTime+"ms - "+"RealMaudeTime: "+propertyResult1.getPropertyVerificationTime()+"ms\n");
//						}else{
//							
//							if(propertyResult1.getResult()!=null && propertyResult1.getResult().contains("TimerMaude")&&propertyResult1.getResult().contains("exceeded")){
//								propertyResult1.setResult("aBPoolstarts property");
//								//+" - "+"VerificationTime: "+property1VertificationTime+"ms - "+"RealMaudeTime: "+propertyResult1.getPropertyVerificationTime()+"ms\n");
//								propertyResult1.setResult(propertyResult1.getResult()+" the timer for the verification has been exceeded");
////								propertyMaudeTime1="-1";
//								propertyMaudeTime1="RealMaudeTimeaBPoolstarts: -1";
//								//System.out.println("\n aBPoolstarts: The timer for the verification has been exceeded");
//								rootObj.put("aBPoolstarts", "aBPoolstarts property is not verified"+propertyResult1.getResult());
//								rootObj.put("aBPoolStartResult", "False");
//								rootObj.put("aBPoolStartCounterexampleTime", propertyCounterexampleTime1);
//								rootObj.put("aBPoolStartTime", Long.toString(property1VertificationTime));
//								rootObj.put("aBPoolStartRealMaudeTime", "-1");
//							}else{
//								if(propertyResult1.getResult()!=null && propertyResult1.getResult().contains("true")){
//									propertyResult1.setResult("aBPoolstarts property is verified");
//									//+" - "+"VerificationTime: "+property1VertificationTime+"ms - "+"RealMaudeTime: "+propertyResult1.getPropertyVerificationTime()+"ms\n");
//									rootObj.put("aBPoolStartResult", "True");
//									rootObj.put("aBPoolstarts", "aBPoolstarts property is verified "+propertyResult1.getResult());
//									rootObj.put("aBPoolStartCounterexampleTime", propertyCounterexampleTime1);
//									rootObj.put("aBPoolStartTime", Long.toString(property1VertificationTime));
//									rootObj.put("aBPoolStartRealMaudeTime", propertyResult1.getPropertyVerificationTime());
//
//								}else{
//								
//								propertyResult1.setResult("aBPoolstarts property is not verified");
//								//+" - "+"VerificationTime: "+property1VertificationTime+"ms - "+"RealMaudeTime: "+propertyResult1.getPropertyVerificationTime()+"ms\n");
//								propertyResult1.setResult(propertyResult1.getResult()+": Error Counterexample could not be generated");	
//								rootObj.put("aBPoolStartCounterexampleTime", propertyCounterexampleTime1);
//								rootObj.put("aBPoolStartResult", "False");
//								rootObj.put("aBPoolStartTime", Long.toString(property1VertificationTime));
//								rootObj.put("aBPoolStartRealMaudeTime", propertyResult1.getPropertyVerificationTime());
//								rootObj.put("aBPoolstarts", "aBPoolstarts property is not verified but Counterexample could not be generated");
//								}
//								//System.out.println("\n-----------propertyResult1PostaBPoolstarts----------\n"+propertyResult1.getResult());
//								
//							}
//						}
//						
////2										
//						long property2VertificationTime=System.currentTimeMillis();
//						String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
//						PostMultipleParameters postMultiple = new PostMultipleParameters();
//						postMultiple.setOriginalModel(originalModelUrl);
//						postMultiple.setParsedModel(massiveParsedModel.getParsedModel());
//						postMultiple.setProperty("aBPoolends");
//						propertyResult2=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
//						String maudeTimeString = null;
//						maudeTimeString=propertyResult2.getPropertyVerificationTime();
//						propertyMaudeTime2 = "RealMaudeTimeaBPoolends: "+maudeTimeString;
//						totalMaudeVerificationTime+=Integer.valueOf(maudeTimeString);
//						property2VertificationTime=System.currentTimeMillis()-property2VertificationTime;
//						if(propertyResult2.getCounterexample()!=null && !propertyResult2.getCounterexample().contains("Counterexample could not be generated")&&!propertyResult2.getCounterexample().contains("counterexample is null")
//								&&!propertyResult2.getCounterexample().contains("TimerMaude")&&!propertyResult2.getCounterexample().contains("exceeded")){													
//							////System.out.println("\n-----------propertyResult2PostaBPoolends----------\n"+propertyResult2.getResult());
////  DEVO GESTIRE Result Code : Error, the Counterexample could not be generated 
////	if(!propertyResult2.contains("Counterexample could not be generated")){						
//							
//							rootObj.put("aBPoolEndTime", Long.toString(property2VertificationTime));
//							rootObj.put("aBPoolEndRealMaudeTime", maudeTimeString);
//							rootObj.put("aBPoolends", "aBPoolends property is not verified");
//							
//							//if(propertyResult2.getCounterexample().contains("counterexample")){
//								propertyResult2.setResult("aBPoolends property is not verified");
//								//+" - "+"VerificationTime: "+property2VertificationTime+"ms - "+"RealMaudeTime: "+propertyResult2.getPropertyVerificationTime()+"ms\n");
//								long propertyCounterexampleTime2Long=System.currentTimeMillis();
//								
//								writeBPMNcounterexample(propertyResult2.getCounterexample(),Savefilepath,"aBPoolEnd");
//								propertyCounterexampleTime2Long=System.currentTimeMillis()-propertyCounterexampleTime2Long;
//								propertyCounterexampleTime2=Long.toString(propertyCounterexampleTime2Long);
//								//System.out.println("\naBPoolStartCounterexampleTime2: "+propertyCounterexampleTime2);
//								rootObj.put("aBPoolEndCounterexampleTime", propertyCounterexampleTime2);
//								rootObj.put("aBPoolEndResult", "False");
//							
//							//}
//							
//							//////System.out.println("\n Property: "+propertyResult2);
//							propertyResult2.setResult(propertyResult2.getResult()+": Verification Time Required: "+property2VertificationTime);
//						}else{
////							//System.out.println("\n-----------propertyResult2PostaBPoolends----------\n"+propertyResult2.getResult());
//							if((propertyResult2.getResult()!=null) && (propertyResult2.getResult().contains("TimerMaude")) && (propertyResult2.getResult().contains("exceeded"))){
//								propertyResult2.setResult("aBPoolends property");
//								//+" - "+"VerificationTime: "+property2VertificationTime+"ms - "+"RealMaudeTime: "+propertyResult2.getPropertyVerificationTime()+"ms\n");
//								propertyResult2.setResult(propertyResult2.getResult()+" the timer for the verification has been exceeded");
//								rootObj.put("aBPoolends", "aBPoolends property is not verified"+propertyResult2.getResult()+" the timer for the verification has been exceeded");
//								////System.out.println("\n aBPoolEndResult: The timer for the verification has been exceeded");
//								rootObj.put("aBPoolEndResult", "False");
////								propertyMaudeTime2="-1";
//								propertyMaudeTime2="RealMaudeTimeaBPoolends: -1";
//								//System.out.println("\n aBPoolends: The timer for the verification has been exceeded");
//								//property2VertificationTime=System.currentTimeMillis()-property2VertificationTime;
//								rootObj.put("aBPoolEndTime", Long.toString(property2VertificationTime));
//								rootObj.put("aBPoolEndCounterexampleTime", propertyCounterexampleTime2);
//								rootObj.put("aBPoolEndRealMaudeTime", "-1");
//							}else{
//								if(propertyResult2.getResult().contains("true")){
//									propertyResult2.setResult("aBPoolends property is verified");
//									//+" - "+"VerificationTime: "+property2VertificationTime+"ms - "+"RealMaudeTime: "+propertyResult2.getPropertyVerificationTime()+"ms\n");
//									rootObj.put("aBPoolEndResult", "True");
//									rootObj.put("aBPoolends", "aBPoolends property is verified "+propertyResult2.getResult());
//									rootObj.put("aBPoolEndTime", Long.toString(property2VertificationTime));
//									rootObj.put("aBPoolEndCounterexampleTime", propertyCounterexampleTime2);
//									rootObj.put("aBPoolEndRealMaudeTime", maudeTimeString);
//								}else{
//							//   DEVO GESTIRE Result Co
//								propertyResult2.setResult("aBPoolends property is not verified");
//								//+" - "+"VerificationTime: "+property2VertificationTime+"ms - "+"RealMaudeTime: "+propertyResult2.getPropertyVerificationTime()+"ms\n");
//								propertyResult2.setResult(propertyResult2.getResult()+": Error Counterexample could not be generated");
//								rootObj.put("aBPoolEndResult", "False");
//								rootObj.put("aBPoolEndTime", Long.toString(property2VertificationTime));
//								rootObj.put("aBPoolEndCounterexampleTime", propertyCounterexampleTime2);
//								rootObj.put("aBPoolEndRealMaudeTime", maudeTimeString);
//								rootObj.put("aBPoolends", "aBPoolends property is not verified but Counterexample could not be generated");
//								}
//								
//							}
//							//System.out.println("\n-----------propertyResult2PostaBPoolends----------\n"+propertyResult2.getResult());
//							//System.out.println("\n-----------propertyResult2PostaBPoolendsCounterexample----------\n"+propertyResult2.getCounterexample());
//							////System.out.println("\n-----------propertyResult2PostaBPoolends NO OTHER OPTIONS----------\n"+propertyResult2.getCounterexample());
//						}
//						
//						
//						
////3										
//						long property3VertificationTime=System.currentTimeMillis();
//						String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
//						PostMultipleParameters postMultiple = new PostMultipleParameters();
//						postMultiple.setOriginalModel(originalModelUrl);
//						postMultiple.setParsedModel(massiveParsedModel.getParsedModel());
//						postMultiple.setProperty("allBPoolend");
//						propertyResult3=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
//						////System.out.println("\npropertyResult3: "+propertyResult3.getCounterexample());
//					//////System.out.println("\npropertyMaudeTime3"+propertyMaudeTime3);
////						String[] maudeTimeMS3=maudeTime[1].split("m");
////						maudeTimeMS3[0]=maudeTimeMS3[0].replaceAll("\\s+","");
//						//////System.out.println("\nmaudeTimeMS3"+maudeTimeMS3[0]);
//						propertyMaudeTime3 = "RealMaudeTimeallBPoolend: "+propertyResult3.getPropertyVerificationTime();
//						totalMaudeVerificationTime+=Integer.valueOf(propertyResult3.getPropertyVerificationTime());
//						property3VertificationTime=System.currentTimeMillis()-property3VertificationTime;
//						if(propertyResult3.getCounterexample()!= null && !propertyResult3.getCounterexample().contains("Counterexample could not be generated")&&!propertyResult3.getCounterexample().contains("counterexample is null")
//								&&!propertyResult3.getCounterexample().contains("TimerMaude")&&!propertyResult3.getCounterexample().contains("exceeded")){	
//							
////							maudeTime=propertyResult3.split("\\*");
////							propertyMaudeTime3 = "RealMaudeTimeallBPoolend: "+propertyResult3.getPropertyVerificationTime();
//							
//							rootObj.put("allBPoolendTime", Long.toString(property3VertificationTime));
//							rootObj.put("allBPoolendRealMaudeTime", propertyResult3.getPropertyVerificationTime());
//							//if(propertyResult3.getCounterexample().contains("counterexample")){
//								propertyResult3.setResult("allBPoolend property is not verified");
//								long propertyCounterexampleTime3Long=System.currentTimeMillis();
////								String [] counterexample=maudeTime[0].split("£");
//								writeBPMNcounterexample(propertyResult3.getCounterexample(),Savefilepath,"allBPoolEnd");
//								propertyCounterexampleTime3Long=System.currentTimeMillis()-propertyCounterexampleTime3Long;
//								propertyCounterexampleTime3=Long.toString(propertyCounterexampleTime3Long);
//								//System.out.println("\nallBPoolStartCounterexampleTime3: "+propertyCounterexampleTime3);
//								rootObj.put("allBPoolEnd", "allBPoolEnd is not verified");
//								rootObj.put("allBPoolEndCounterexampleTime", propertyCounterexampleTime3);
//								rootObj.put("allBPoolEndResult", "False");
//							//}
//							
//							//////System.out.println("\n Property: "+propertyResult3);
//							propertyResult3.setResult(propertyResult3.getResult());
//							//System.out.println("\n-----------propertyResult3PostallBPoolends counterexample present----------\n"+propertyResult3.getResult());
//							//System.out.println("\n-----------propertyResult3PostallBPoolends counterexample present----------\n"+propertyResult3.getCounterexample());
//							//+newLine+"Verification Time Required: "+property3VertificationTime);
//						}else{
//							if(propertyResult3.getResult()!=null && propertyResult3.getResult().contains("TimerMaude")&&propertyResult3.getResult().contains("exceeded")){
//								propertyResult3.setResult("allBPoolEnd property");
//								propertyResult3.setResult(propertyResult3.getResult()+" the timer for the verification has been exceeded");	
//								//System.out.println("\n allBPoolEndResult: The timer for the verification has been exceeded");
//								rootObj.put("allBPoolEnd", "allBPoolEnd is not verified "+propertyResult3.getResult());
//								rootObj.put("allBPoolEndResult", "False");
////								propertyMaudeTime3="-1";
//								propertyMaudeTime3="RealMaudeTimeallBPoolend: -1";
//								////System.out.println("\n allBPoolEnd: The timer for the verification has been exceeded");
//								//property3VertificationTime=System.currentTimeMillis()-property3VertificationTime;
//								rootObj.put("allBPoolendTime", Long.toString(property3VertificationTime));
//								rootObj.put("allBPoolendRealMaudeTime", "-1");
//								rootObj.put("allBPoolEndCounterexampleTime", propertyCounterexampleTime3);
//							}else{
//								if(propertyResult3.getResult()!=null && propertyResult3.getResult().contains("true")){
//									propertyResult3.setResult("allBPoolend property is verified");
//									rootObj.put("allBPoolEndResult", "True");
//									rootObj.put("allBPoolEnd", "allBPoolEnd is verified "+propertyResult3.getResult());
//									rootObj.put("allBPoolendTime", Long.toString(property3VertificationTime));
//									rootObj.put("allBPoolendRealMaudeTime", propertyResult3.getPropertyVerificationTime());
//									rootObj.put("allBPoolEndCounterexampleTime", propertyCounterexampleTime3);
//								}
//								else{
//									propertyResult3.setResult("allBPoolEnd property is not verified");
//									propertyResult3.setResult(propertyResult3.getResult()+": Error Counterexample could not be generated");		
//									rootObj.put("allBPoolEnd", "allBPoolEnd property is not verified but Counterexample could not be generated");
//									rootObj.put("allBPoolEndResult", "FALSE");
////									rootObj.put("allBPoolEnd", "allBPoolEnd is not verified "+propertyResult3.getResult());
//									rootObj.put("allBPoolendTime", Long.toString(property3VertificationTime));
//									rootObj.put("allBPoolendRealMaudeTime", propertyResult3.getPropertyVerificationTime());
//									rootObj.put("allBPoolEndCounterexampleTime", propertyCounterexampleTime3);
//								}
//							}
//								//System.out.println("\n-----------propertyResult3PostallBPoolends----------\n"+propertyResult3.getResult());
//								
//						}		
						
//4										
						long property4VertificationTime=System.currentTimeMillis();
						String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
						PostMultipleParameters postMultiple = new PostMultipleParameters();
						postMultiple.setOriginalModel(originalModelUrl);
						postMultiple.setParsedModel(massiveParsedModel.getParsedModel());
						postMultiple.setProperty("noDeadActivities");
						propertyResult4=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
						property4VertificationTime=System.currentTimeMillis()-property4VertificationTime;
						propertyMaudeTime4="RealMaudeTimeNoDeadActivities: "+propertyResult4.getPropertyVerificationTime();
						totalMaudeVerificationTime+=Integer.valueOf(propertyResult4.getPropertyVerificationTime());

						if(propertyResult4.getCounterexample()!=null && !propertyResult4.getCounterexample().contains("Counterexample could not be generated")&&!propertyResult4.getCounterexample().contains("counterexample is null")
								&&!propertyResult4.getCounterexample().contains("TimerMaude")&&!propertyResult4.getCounterexample().contains("exceeded")){	
							//System.out.println("\n-----------propertyResult4 counterexample present----------\n"+propertyResult4.getResult());
							rootObj.put("noDeadActivitiesTime", Long.toString(property4VertificationTime));
							rootObj.put("noDeadActivitiesRealMaudeTime", propertyResult4.getPropertyVerificationTime());

							long propertyCounterexampleTime4Long=System.currentTimeMillis();
							rootObj.put("noDeadActivities", "noDeadActivities property is not verified ");
							writeBPMNcounterexample(propertyResult4.getCounterexample(),Savefilepath,"noDeadActivities");
							propertyCounterexampleTime4Long=System.currentTimeMillis()-propertyCounterexampleTime4Long;
							propertyCounterexampleTime4 = Long.toString(propertyCounterexampleTime4Long);
							//System.out.println("\nanoDeadActivitiesCounterexampleTime4: "+propertyCounterexampleTime4);
							rootObj.put("noDeadActivitiesCounterexampleTime", propertyCounterexampleTime4);
							rootObj.put("noDeadActivitiesResult", "False");								
						
						}else{
							if(propertyResult4.getResult()!=null && propertyResult4.getResult().contains("TimerMaude")&&propertyResult4.getResult().contains("exceeded")){
								propertyResult4.setResult("noDeadActivities property the timer for the verification has been exceeded");
								//System.out.println("\n-----------propertyResult4----------\n"+propertyResult4.getResult());
								//System.out.println("\n noDeadActivities: The timer for the verification has been exceeded");
								rootObj.put("noDeadActivities", "noDeadActivities property is not verified "+propertyResult4.getResult());
								rootObj.put("noDeadActivitiesResult", "False");
								propertyMaudeTime4="RealMaudeTimeNoDeadActivities: -1";

								property4VertificationTime=System.currentTimeMillis()-property4VertificationTime;
								rootObj.put("noDeadActivitiesCounterexampleTime", propertyCounterexampleTime4);
								rootObj.put("noDeadActivitiesTime", Long.toString(property4VertificationTime));
								rootObj.put("noDeadActivitiesRealMaudeTime", "-1");
							}else{
								if(propertyResult4.getResult()!=null && propertyResult4.getResult().contains("true")){
									propertyResult4.setResult("noDeadActivities property is verified");
									rootObj.put("noDeadActivities", "noDeadActivities property is verified "+propertyResult4.getResult());
									rootObj.put("noDeadActivitiesResult", "True");
									rootObj.put("noDeadActivitiesTime", Long.toString(property4VertificationTime));
									rootObj.put("noDeadActivitiesCounterexampleTime", propertyCounterexampleTime4);
									rootObj.put("noDeadActivitiesRealMaudeTime", propertyResult4.getPropertyVerificationTime());
								}
								else{
							
									//System.out.println("\n-----------propertyResult4----------\n"+propertyResult4.getResult());
									propertyResult4.setResult("noDeadActivities property is not verified: "+propertyResult4.getResult());	
									
									rootObj.put("noDeadActivities", "noDeadActivities property is not verified "+propertyResult4.getResult());
									rootObj.put("noDeadActivitiesResult", "False");
									rootObj.put("noDeadActivitiesTime", Long.toString(property4VertificationTime));
									rootObj.put("noDeadActivitiesCounterexampleTime", propertyCounterexampleTime4);
									rootObj.put("noDeadActivitiesRealMaudeTime", propertyResult4.getPropertyVerificationTime());
								}
							}
							//System.out.println("\n-----------propertyResult4----------\n"+propertyResult4.getResult());
							
						}				
						
//5										
						long property5VertificationTime=System.currentTimeMillis();
						originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
						postMultiple = new PostMultipleParameters();
						postMultiple.setOriginalModel(originalModelUrl);
						postMultiple.setParsedModel(massiveParsedModel.getParsedModel());
						postMultiple.setProperty("optionToComplete");
						propertyResult5=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
						
						property5VertificationTime=System.currentTimeMillis()-property5VertificationTime;
						propertyMaudeTime5="RealMaudeTimeOptionToComplete: "+propertyResult5.getPropertyVerificationTime();
						totalMaudeVerificationTime+=Integer.valueOf(propertyResult5.getPropertyVerificationTime());
						if(propertyResult5.getCounterexample()!=null&&!propertyResult5.getCounterexample().contains("Counterexample could not be generated")&&!propertyResult5.getCounterexample().contains("counterexample is null")
								&&!propertyResult5.getCounterexample().contains("TimerMaude")&&!propertyResult5.getCounterexample().contains("exceeded")){	
							////System.out.println("\n-----------propertyResult5PostOptionToComplete counterexample present----------\n"+propertyResult5.getResult());

							long propertyCounterexampleTime5Long = System.currentTimeMillis();
							writeBPMNcounterexample(propertyResult5.getCounterexample(),Savefilepath,"optionToComplete");
							propertyCounterexampleTime5Long = System.currentTimeMillis()-propertyCounterexampleTime5Long;
							propertyCounterexampleTime5 = Long.toString(propertyCounterexampleTime5Long);
							//System.out.println("\noptionToCompleteCounterexampleTime5: "+propertyCounterexampleTime5);
							rootObj.put("optionToCompleteCounterexampleTime", propertyCounterexampleTime5);

							rootObj.put("optionToComplete", "optionToComplete property is not verified");

							rootObj.put("optionToCompleteTime", Long.toString(property5VertificationTime));
							rootObj.put("optionToCompleteRealMaudeTime", propertyResult5.getPropertyVerificationTime());
							////System.out.println("\n Property: "+propertyResult5.getResult());
							rootObj.put("optionToCompleteResult", "False");								

						}else{
							if(propertyResult5.getResult()!=null && propertyResult5.getResult().contains("TimerMaude")&&propertyResult5.getResult().contains("exceeded")){
								propertyResult5.setResult("optionToComplete property the timer for the verification has been exceeded");	
								//System.out.println("\n optionToCompleteResult: The timer for the verification has been exceeded");
								propertyMaudeTime5="RealMaudeTimeOptionToComplete: -1";
								rootObj.put("optionToCompleteResult", "False");
								rootObj.put("optionToComplete", "optionToComplete property is not verified"+propertyResult5.getResult());
								rootObj.put("optionToCompleteTime", Long.toString(property5VertificationTime));
								rootObj.put("optionToCompleteCounterexampleTime", propertyCounterexampleTime5);
								rootObj.put("optionToCompleteRealMaudeTime", "-1");
								rootObj.put("optionToCompleteCounterexampleTime", propertyCounterexampleTime5);
							}else{
								if(propertyResult5.getResult()!=null && propertyResult5.getResult().contains("true")){
									propertyResult5.setResult("optionToComplete property is verified");
									rootObj.put("optionToComplete", "optionToComplete property is verified "+propertyResult5.getResult());
									rootObj.put("optionToCompleteResult", "True");
									rootObj.put("optionToCompleteTime", Long.toString(property5VertificationTime));
									rootObj.put("optionToCompleteRealMaudeTime", propertyResult5.getPropertyVerificationTime());
									rootObj.put("optionToCompleteCounterexampleTime", propertyCounterexampleTime5);
								}else{
							
								
							propertyResult5.setResult("optionToComplete property is not verified"+"Error Counterexample could not be generated");		
							rootObj.put("optionToCompleteResult", "False");
							rootObj.put("optionToComplete", "optionToComplete property is not verified but Counterexample could not be generated");
							rootObj.put("optionToCompleteTime", Long.toString(property5VertificationTime));
							rootObj.put("optionToCompleteRealMaudeTime", propertyResult5.getPropertyVerificationTime());
							rootObj.put("optionToCompleteCounterexampleTime", propertyCounterexampleTime5);
								}
								
							}
							//System.out.println("\n-----------propertyResult5PostOptionToComplete----------\n"+propertyResult5.getResult());
							//System.out.println("\n-----------propertyCounterexample5PostOptionToComplete----------\n"+propertyResult5.getCounterexample());
							
						}
						
					
//6.1 Control Flow									
						long property6VertificationTime=System.currentTimeMillis();
						
						originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
						postMultiple = new PostMultipleParameters();
						postMultiple.setOriginalModel(originalModelUrl);
						postMultiple.setParsedModel(massiveParsedModel.getParsedModel());
						postMultiple.setProperty("properCompletionCF");
						propertyResult6=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
						
						property6VertificationTime=System.currentTimeMillis()-property6VertificationTime;
						propertyMaudeTime6="RealMaudeTimeproperCompletionCF: "+propertyResult6.getPropertyVerificationTime();
						totalMaudeVerificationTime+=Integer.valueOf(propertyResult6.getPropertyVerificationTime());
						
						if(propertyResult6.getCounterexample()!=null&&!propertyResult6.getCounterexample().contains("Counterexample could not be generated")&&!propertyResult6.getCounterexample().contains("counterexample is null")
								&&!propertyResult6.getCounterexample().contains("TimerMaude")&&!propertyResult6.getCounterexample().contains("exceeded")){	
						
							//System.out.println("\n-----------propertyResult6PostproperCompletionCF counterexample present----------\n"+propertyResult6.getResult());

							long propertyCounterexampleTime6Long = System.currentTimeMillis();
							writeBPMNcounterexample(propertyResult6.getCounterexample(),Savefilepath,"properComplition");
							propertyCounterexampleTime6Long = System.currentTimeMillis()-propertyCounterexampleTime6Long;
							propertyCounterexampleTime6 = Long.toString(propertyCounterexampleTime6Long);
							//System.out.println("\nproperCompletionCFCounterexampleTime6: "+propertyCounterexampleTime6);
							rootObj.put("properCompletionCFCounterexampleTime", propertyCounterexampleTime6);

							rootObj.put("properCompletionCFTime", Long.toString(property6VertificationTime));
							rootObj.put("properCompletionCFRealMaudeTime",propertyResult6.getPropertyVerificationTime());
							//////System.out.println("\n Property: "+propertyResult6);
							rootObj.put("properCompletionCFResult", "False");
							rootObj.put("properCompletionCF", "properCompletionCF property is not verified");
								
						}else{
							//System.out.println("\n-----------propertyResult6PostproperCompletionCF ----------\n"+propertyResult6.getResult());
//							
							if(propertyResult6.getResult()!=null && propertyResult6.getResult().contains("TimerMaude")&&propertyResult6.getResult().contains("exceeded")){
								propertyResult6.setResult("properCompletionCF property the timer for the verification has been exceeded");
								//System.out.println("\n properCompletionCFResult: The timer for the verification has been exceeded");
								propertyMaudeTime6="RealMaudeTimeproperCompletionCF: -1";
								rootObj.put("properCompletionCF", "properCompletionCF property is not verified"+propertyResult6.getResult());
								rootObj.put("properCompletionCFResult", "False");
//								//System.out.println("\n properCompletionCFResult: The timer for the verification has been exceeded");
								rootObj.put("properCompletionCFCounterexampleTime", propertyCounterexampleTime6);
								rootObj.put("properCompletionCFTime", Long.toString(property6VertificationTime));
								rootObj.put("properCompletionCFRealMaudeTime", "-1");
							}else{
								if(propertyResult6.getResult()!=null && propertyResult6.getResult().contains("All the Pools respect")){
									rootObj.put("properCompletionCF", "properCompletionCF property is verified "+propertyResult6.getResult());
									rootObj.put("properCompletionCFResult", "True");
									rootObj.put("properCompletionCFCounterexampleTime", propertyCounterexampleTime6);
									rootObj.put("properCompletionCFTime", Long.toString(property6VertificationTime));
									rootObj.put("properCompletionCFRealMaudeTime",propertyResult6.getPropertyVerificationTime());
								}else{			
									propertyResult6.setResult("properCompletionCF property is not verified: "+"Error Counterexample could not be generated");		
									rootObj.put("properCompletionCFResult", "False");
									rootObj.put("properCompletionCF", "properCompletionCF property is not verified but Counterexample could not be generated");
									rootObj.put("properCompletionCFCounterexampleTime", propertyCounterexampleTime6);
									rootObj.put("properCompletionCFTime", Long.toString(property6VertificationTime));
									rootObj.put("properCompletionCFRealMaudeTime",propertyResult6.getPropertyVerificationTime());
								}
								
							}
							//System.out.println("\n \n-----------propertyResult6PostproperCompletionCF----------\n"+propertyResult6.getResult());
							
						}
						
//6.2 Message FLow								
						long property62VertificationTime=System.currentTimeMillis();
						
						originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
						postMultiple = new PostMultipleParameters();
						postMultiple.setOriginalModel(originalModelUrl);
						postMultiple.setParsedModel(massiveParsedModel.getParsedModel());
						postMultiple.setProperty("properCompletionMF");
						propertyResult62=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
						
						property62VertificationTime=System.currentTimeMillis()-property62VertificationTime;
						propertyMaudeTime62="RealMaudeTimeproperCompletionMF: "+propertyResult62.getPropertyVerificationTime();
						totalMaudeVerificationTime+=Integer.valueOf(propertyResult62.getPropertyVerificationTime());
						
						if(propertyResult62.getCounterexample()!=null&&!propertyResult62.getCounterexample().contains("Counterexample could not be generated")&&!propertyResult62.getCounterexample().contains("counterexample is null")
								&&!propertyResult62.getCounterexample().contains("TimerMaude")&&!propertyResult62.getCounterexample().contains("exceeded")){	
						
							//System.out.println("\n-----------propertyResult62PostproperCompletionMF counterexample present----------\n"+propertyResult62.getResult());

							long propertyCounterexampleTime62Long = System.currentTimeMillis();
							writeBPMNcounterexample(propertyResult62.getCounterexample(),Savefilepath,"properComplition");
							propertyCounterexampleTime62Long = System.currentTimeMillis()-propertyCounterexampleTime62Long;
							propertyCounterexampleTime62 = Long.toString(propertyCounterexampleTime62Long);
							//System.out.println("\nproperCompletionMFCounterexampleTime62: "+propertyCounterexampleTime62);
							rootObj.put("properCompletionMFCounterexampleTime", propertyCounterexampleTime62);

							rootObj.put("properCompletionMFTime", Long.toString(property62VertificationTime));
							rootObj.put("properCompletionMFRealMaudeTime",propertyResult62.getPropertyVerificationTime());
							//////System.out.println("\n Property: "+propertyResult62);
							rootObj.put("properCompletionMFResult", "False");
							rootObj.put("properCompletionMF", "properCompletionMF property is not verified");
								
						}else{
							//System.out.println("\n-----------propertyResult62PostproperCompletionMF ----------\n"+propertyResult62.getResult());
//							
							if(propertyResult62.getResult()!=null && propertyResult62.getResult().contains("TimerMaude")&&propertyResult62.getResult().contains("exceeded")){
								propertyResult62.setResult("properCompletionMF property the timer for the verification has been exceeded");
								//System.out.println("\n properCompletionMFResult: The timer for the verification has been exceeded");
								propertyMaudeTime62="RealMaudeTimeproperCompletionMF: -1";
								rootObj.put("properCompletionMF", "properCompletionMF property is not verified"+propertyResult62.getResult());
								rootObj.put("properCompletionMFResult", "False");
//								//System.out.println("\n properCompletionMFResult: The timer for the verification has been exceeded");
								rootObj.put("properCompletionMFCounterexampleTime", propertyCounterexampleTime62);
								rootObj.put("properCompletionMFTime", Long.toString(property62VertificationTime));
								rootObj.put("properCompletionMFRealMaudeTime", "-1");
							}else{
								if(propertyResult62.getResult()!=null && propertyResult62.getResult().contains("All the Pools respect")){
									rootObj.put("properCompletionMF", "properCompletionMF property is verified "+propertyResult62.getResult());
									rootObj.put("properCompletionMFResult", "True");
									rootObj.put("properCompletionMFCounterexampleTime", propertyCounterexampleTime62);
									rootObj.put("properCompletionMFTime", Long.toString(property62VertificationTime));
									rootObj.put("properCompletionMFRealMaudeTime",propertyResult62.getPropertyVerificationTime());
								}else{			
									propertyResult62.setResult("properCompletionMF property is not verified: "+"Error Counterexample could not be generated");		
									rootObj.put("properCompletionMFResult", "False");
									rootObj.put("properCompletionMF", "properCompletionMF property is not verified but Counterexample could not be generated");
									rootObj.put("properCompletionMFCounterexampleTime", propertyCounterexampleTime62);
									rootObj.put("properCompletionMFTime", Long.toString(property62VertificationTime));
									rootObj.put("properCompletionMFRealMaudeTime",propertyResult62.getPropertyVerificationTime());
								}
								
							}
							//System.out.println("\n \n-----------propertyResult62PostproperCompletionMF----------\n"+propertyResult62.getResult());
							
						}

//6.3 Control and Message FLow								
						long property63VertificationTime=System.currentTimeMillis();
						
						originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
						postMultiple = new PostMultipleParameters();
						postMultiple.setOriginalModel(originalModelUrl);
						postMultiple.setParsedModel(massiveParsedModel.getParsedModel());
						postMultiple.setProperty("properCompletionCFMF");
						propertyResult63=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
						
						property63VertificationTime=System.currentTimeMillis()-property63VertificationTime;
						propertyMaudeTime63="RealMaudeTimeproperCompletionCFMF: "+propertyResult63.getPropertyVerificationTime();
						totalMaudeVerificationTime+=Integer.valueOf(propertyResult63.getPropertyVerificationTime());
						
						if(propertyResult63.getCounterexample()!=null&&!propertyResult63.getCounterexample().contains("Counterexample could not be generated")&&!propertyResult63.getCounterexample().contains("counterexample is null")
								&&!propertyResult63.getCounterexample().contains("TimerMaude")&&!propertyResult63.getCounterexample().contains("exceeded")){	
						
							//System.out.println("\n-----------propertyResult63PostproperCompletionCFMF counterexample present----------\n"+propertyResult63.getResult());

							long propertyCounterexampleTime63Long = System.currentTimeMillis();
							writeBPMNcounterexample(propertyResult63.getCounterexample(),Savefilepath,"properComplition");
							propertyCounterexampleTime63Long = System.currentTimeMillis()-propertyCounterexampleTime63Long;
							propertyCounterexampleTime63 = Long.toString(propertyCounterexampleTime63Long);
							//System.out.println("\nproperCompletionCFMFCounterexampleTime63: "+propertyCounterexampleTime63);
							rootObj.put("properCompletionCFMFCounterexampleTime", propertyCounterexampleTime63);

							rootObj.put("properCompletionCFMFTime", Long.toString(property63VertificationTime));
							rootObj.put("properCompletionCFMFRealMaudeTime",propertyResult63.getPropertyVerificationTime());
							//////System.out.println("\n Property: "+propertyResult63);
							rootObj.put("properCompletionCFMFResult", "False");
							rootObj.put("properCompletionCFMF", "properCompletionCFMF property is not verified");
								
						}else{
							//System.out.println("\n-----------propertyResult63PostproperCompletionCFMF ----------\n"+propertyResult63.getResult());
//							
							if(propertyResult63.getResult()!=null && propertyResult63.getResult().contains("TimerMaude")&&propertyResult63.getResult().contains("exceeded")){
								propertyResult63.setResult("properCompletionCFMF property the timer for the verification has been exceeded");
								//System.out.println("\n properCompletionCFMFResult: The timer for the verification has been exceeded");
								propertyMaudeTime63="RealMaudeTimeproperCompletionCFMF: -1";
								rootObj.put("properCompletionCFMF", "properCompletionCFMF property is not verified"+propertyResult63.getResult());
								rootObj.put("properCompletionCFMFResult", "False");
//								//System.out.println("\n properCompletionCFMFResult: The timer for the verification has been exceeded");
								rootObj.put("properCompletionCFMFCounterexampleTime", propertyCounterexampleTime63);
								rootObj.put("properCompletionCFMFTime", Long.toString(property63VertificationTime));
								rootObj.put("properCompletionCFMFRealMaudeTime", "-1");
							}else{
								if(propertyResult63.getResult()!=null && propertyResult63.getResult().contains("All the Pools respect")){
									rootObj.put("properCompletionCFMF", "properCompletionCFMF property is verified "+propertyResult63.getResult());
									rootObj.put("properCompletionCFMFResult", "True");
									rootObj.put("properCompletionCFMFCounterexampleTime", propertyCounterexampleTime63);
									rootObj.put("properCompletionCFMFTime", Long.toString(property63VertificationTime));
									rootObj.put("properCompletionCFMFRealMaudeTime",propertyResult63.getPropertyVerificationTime());
								}else{			
									propertyResult63.setResult("properCompletionCFMF property is not verified: "+"Error Counterexample could not be generated");		
									rootObj.put("properCompletionCFMFResult", "False");
									rootObj.put("properCompletionCFMF", "properCompletionCFMF property is not verified but Counterexample could not be generated");
									rootObj.put("properCompletionCFMFCounterexampleTime", propertyCounterexampleTime63);
									rootObj.put("properCompletionCFMFTime", Long.toString(property63VertificationTime));
									rootObj.put("properCompletionCFMFRealMaudeTime",propertyResult63.getPropertyVerificationTime());
								}
								
							}
							//System.out.println("\n \n-----------propertyResult63PostproperCompletionCFMF----------\n"+propertyResult63.getResult());
							
						}						
						
//7									
						long property7VertificationTime=System.currentTimeMillis();
						originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
						postMultiple = new PostMultipleParameters();
						postMultiple.setOriginalModel(originalModelUrl);
						postMultiple.setParsedModel(massiveParsedModel.getParsedModel());
						postMultiple.setProperty("safeness");
						propertyResult7=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
						
						property7VertificationTime=System.currentTimeMillis()-property7VertificationTime;
						propertyMaudeTime7="RealMaudeTimeSafeness: "+propertyResult7.getPropertyVerificationTime();
						totalMaudeVerificationTime+=Integer.valueOf(propertyResult7.getPropertyVerificationTime());
						if(propertyResult7.getCounterexample()!=null && !propertyResult7.getCounterexample().contains("Counterexample could not be generated")&&!propertyResult7.getCounterexample().contains("counterexample is null")&&!propertyResult7.getCounterexample().contains("does not respect the property Safeness")
								&&!propertyResult7.getCounterexample().contains("TimerMaude")&&!propertyResult7.getCounterexample().contains("exceeded")){												
						
							rootObj.put("safenessTime", Long.toString(property7VertificationTime));
							rootObj.put("safenessRealMaudeTime", propertyResult7.getPropertyVerificationTime());
							rootObj.put("safenessResult", "False");
							rootObj.put("safeness", "safeness property is not verified");

							
						}else{
							if(propertyResult7.getResult()!=null && propertyResult7.getResult().contains("does not respect the property Safeness")){
								//System.out.println("\npropertyResult7: "+propertyResult7);
							
								rootObj.put("safeness", "safeness property is not verified"+propertyResult7.getResult());
								rootObj.put("safenessTime", Long.toString(property7VertificationTime));
								rootObj.put("safenessRealMaudeTime", propertyResult7.getPropertyVerificationTime());
								rootObj.put("safenessResult", "False");
								
							}
							if(propertyResult7.getResult()!=null && propertyResult7.getResult().contains("TimerMaude")&&propertyResult7.getResult().contains("exceeded")){
								propertyResult7.setResult("False");	
								//System.out.println("\n safenessResult: The timer for the verification has been exceeded");
								propertyMaudeTime7="RealMaudeTimeSafeness: -1";
								rootObj.put("safenessResult", "False");
								rootObj.put("safeness", "safeness property is not verified"+propertyResult7.getResult());
//								//System.out.println("\n safenessResult: The timer for the verification has been exceeded");
								rootObj.put("safenessTime", Long.toString(property7VertificationTime));
								rootObj.put("safenessRealMaudeTime", "-1");
							}else{
								if(propertyResult7.getResult()!=null && propertyResult7.getResult().contains("true")){
									propertyResult7.setResult("safeness property is verified");
									rootObj.put("safeness", "safeness property is verified "+propertyResult7.getResult());
									rootObj.put("safenessResult", "True");
									rootObj.put("safenessTime", Long.toString(property7VertificationTime));
									rootObj.put("safenessRealMaudeTime", propertyResult7.getPropertyVerificationTime());
								}else{
								
									propertyResult7.setResult("safeness property is not verified: "+"Error Counterexample could not be generated");
									rootObj.put("safeness", "safeness property is not verified"+"Error Counterexample could not be generated");
									rootObj.put("safenessResult", "False");
									rootObj.put("safenessTime", Long.toString(property7VertificationTime));
									rootObj.put("safenessRealMaudeTime", propertyResult7.getPropertyVerificationTime());
							}
								//System.out.println("\nproperty7Result: "+propertyResult7.getResult());
								//System.out.println("\nproperty7Result: "+propertyResult7.getPropertyVerificationTime());
							}
						}
									
						totalVTime = System.currentTimeMillis() - totalVTime;
						//////System.out.println("\nTotalVerificationTime: \n"+totalVTime);

//09/10/2017 COMMENTED BUT WORKING
//COMMENTED TO FASTEN THE MASSIVE VERIFICATION, WE VERIFY ONLY NODEADACTIVITIES, OPTIONTOCOMPLETE, PROPERCOMPLETION AND SAFENESS						

//						fw.write(propertyResult1.getResult()+newLine);
//						fw.write("aBPoolStartVerificationTime: "+property1VertificationTime+newLine);
//				    	fw.write(propertyMaudeTime1+newLine);
//				    	fw.write("aBPoolStartCounterexampleTime: ");
//				    	fw.write(propertyCounterexampleTime1+newLine);
//				    	fw.write(propertyResult2.getResult()+newLine);
//				    	fw.write("aBPoolEndVerificationTime: "+property2VertificationTime+newLine);
//				    	fw.write(propertyMaudeTime2+newLine);
//				    	fw.write("aBPoolEndCounterexampleTime: ");
//				    	fw.write(propertyCounterexampleTime2+newLine);
//				    	fw.write("allBPoolEndVerificationTime: "+property3VertificationTime+newLine);
//				    	fw.write(propertyResult3.getResult()+newLine);
//				    	fw.write(propertyMaudeTime3+newLine);
//				    	////System.out.println("\naBPoolStartCounterexampleTime: "+propertyCounterexampleTime1);
////				    	if((propertyCounterexampleTime3!=null)||(propertyCounterexampleTime3!="-1")){
//				    	fw.write("allBPoolEndCounterexampleTime: ");
//				    	fw.write(propertyCounterexampleTime3+newLine);
//					    	//fw.write(propertyResult6);
////				    	}
				    		
				    	fw.write(propertyResult4.getResult()+newLine);
				    	fw.write("noDeadActivitiesTime: "+property4VertificationTime+newLine);
				    	fw.write(propertyMaudeTime4+newLine);
				    	fw.write("noDeadActivitiesCounterexampleTime: ");
				    	fw.write(propertyCounterexampleTime4+newLine);
				    	fw.write(propertyResult5.getResult()+newLine);
				    	fw.write("optionToCompleteTime: "+property5VertificationTime+newLine);
				    	fw.write(propertyMaudeTime5+newLine);
				    	fw.write("optionToCompleteCounterexampleTime: ");
				    	fw.write(propertyCounterexampleTime5+newLine);
				    	fw.write(propertyResult6.getResult()+newLine);
				    	fw.write("properCompletionCFTime: "+property6VertificationTime+newLine);
				    	fw.write(propertyMaudeTime6+newLine);
				    	fw.write("properCompletionCFCounterexampleTime: ");
				    	fw.write(propertyCounterexampleTime6+newLine);
				    	
				    	fw.write(propertyResult62.getResult()+newLine);
				    	fw.write("properCompletionMFTime: "+property62VertificationTime+newLine);
				    	fw.write(propertyMaudeTime62+newLine);
				    	fw.write("properCompletionMFCounterexampleTime: ");
				    	fw.write(propertyCounterexampleTime62+newLine);
				    	
				    	fw.write(propertyResult63.getResult()+newLine);
				    	fw.write("properCompletionCFMFTime: "+property63VertificationTime+newLine);
				    	fw.write(propertyMaudeTime63+newLine);
				    	fw.write("properCompletionCFMFCounterexampleTime: ");
				    	fw.write(propertyCounterexampleTime63+newLine);
				    	
				    	
				    	
				    	fw.write(propertyResult7.getResult()+newLine);
				    	fw.write("safenessTime: "+property7VertificationTime+newLine);
				    	fw.write(propertyMaudeTime7+newLine);
			    	    fw.write(newLine+"TotalVerificationTime: "+totalVTime+" TotalMaudeVerificationTime: "+Integer.toString(totalMaudeVerificationTime)+newLine);
			    	    fw.flush();
			    	    //fw.close();
			    	     						    	     
			    	    statisticsList.add(rootObj);

				    	  //FOR SAVING THE INTERMEDIATE RESULTS (IF THE MASSIVE TRANSALTION IS ABORTED)
				    	  //THE BCKPMassiveStatistics IS SAVED EVERY TIME A SINGLE MODEL IS ANALYZED AND 
			    	      //A JSONOBJECT IS POPULATED WITH STATISTICS OF THAT MODEL 
			    	      //BCKPMassiveStatistics.json CONTAINS ONLY JSONOBJECTs NOT ARRENGED IN A JSONARRAY
			    		  if(unix){
			    			  index2 = Savefilepath.toString().lastIndexOf("/");	
			    			  toSave = Savefilepath.toString().substring(0, index2);
			    			  
				    		  try (FileWriter fileStat = new FileWriter(toSave+"/BCKPMassiveStatistics.json",true)) {		 

					    		    fileStat.write(rootObj.toJSONString());
				    			    fileStat.flush();


						        } catch (IOException e3) {
						            e3.printStackTrace();
						        }
				    	  }else{
			    			  index2 = Savefilepath.toString().lastIndexOf("\\");	
			    			  toSave = Savefilepath.toString().substring(0, index2);
					    	  try (FileWriter fileStat = new FileWriter(toSave+"\\BCKPMassiveStatistics.json",true)) {		 
	
					    		    fileStat.write(rootObj.toJSONString());
					    		    fileStat.flush();
	
	
						        } catch (IOException e3) {
						            e3.printStackTrace();
						        }
				    	  }//chiude if unix

					} catch (IOException e1) {
						e1.printStackTrace();
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
				}

				@Override
				protected void process(List<String> chunks) {
					String value = chunks.get(chunks.size() -1);
					textArea.append("\n"+value+"\n");
					super.process(chunks);
				}

				@Override
				protected void done(){
					try {
						@SuppressWarnings("unused")
						Boolean status = get();
					} catch (InterruptedException | ExecutionException e) {
						e.printStackTrace();
					}
					////System.out.println("\n Massive Done \n");
					textArea.append("\n MASSIVE TRANSLATION COMPLETED! \n");
				}
	    	  };
	    	  
	    	  worker.execute();
	}
	
public static void writeBPMNcounterexample( String counterexample, String savepath, String propertyName) {
		
		try{
	
			//System.out.println("\nWrite BPMN counterexample: "+counterexample);
			
			try{
				//from string to xml
				DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
				DocumentBuilder builder = factory.newDocumentBuilder();
				Document doc = builder.parse(new InputSource(new StringReader(counterexample)));

				// Write the parsed document to an xml file
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				DOMSource source = new DOMSource(doc);

				StreamResult result =  new StreamResult(new File(savepath+propertyName+".bpmn"));
				transformer.transform(source, result);
				}catch (IOException | ParserConfigurationException | SAXException | TransformerException e) {
				     e.printStackTrace();
				}
	

			}catch (Exception e) {
			     e.printStackTrace();
			}
		//System.out.println("\nDONE Writing BPMN counterexample");	
		
	}

	private static void writeElementAndParsingTime(FileWriter fw2, PostMultipleParameters parsedModel,
			long parsingTime2, boolean unix) throws IOException {
	
		String newLine;
		if(unix)newLine="\n";
		else newLine="\r\n";
		
		fw2.write(newLine+"Parsed Model: "+parsedModel.getParsedModel());
		    fw2.write(newLine+"Total number of element: "+Integer.toString(parsedModel.getNumberOfElement()));
		    fw2.write(newLine+"Number of pool: "+Integer.toString(parsedModel.getNumberOfPool()));
		    fw2.write(newLine+"Number of start event: "+Integer.toString(parsedModel.getNumberOfStart()));
		    fw2.write(newLine+"Number of message start event: "+Integer.toString(parsedModel.getNumberOfStartMsg()));
		    fw2.write(newLine+"Number of end event: "+Integer.toString(parsedModel.getNumberOfEnd()));
		    fw2.write(newLine+"Number of message end event: "+Integer.toString(parsedModel.getNumberOfEndMsg()));
		    fw2.write(newLine+"Number of terminated end event: "+Integer.toString(parsedModel.getNumberOfTerminate()));
		    fw2.write(newLine+"Number of task: "+Integer.toString(parsedModel.getNumberOfTask()));
		    fw2.write(newLine+"Number of sendtask: "+Integer.toString(parsedModel.getNumberOfTaskSnd()));
		    fw2.write(newLine+"Number of receivetask: "+Integer.toString(parsedModel.getNumberOfTaskRcv()));
		    fw2.write(newLine+"Number of intermidiate throw event: "+Integer.toString(parsedModel.getNumberOfIntermidiateThrowEvent()));
		    fw2.write(newLine+"Number of intermidiate catch event: "+Integer.toString(parsedModel.getNumberOfIntermidiateCatchEvent()));
		    fw2.write(newLine+"Number of xorsplit gateway: "+Integer.toString(parsedModel.getNumberOfGatewayXorSplit()));
		    fw2.write(newLine+"Number of xorjoin gateway: "+Integer.toString(parsedModel.getNumberOfGatewayXorJoin()));
		    fw2.write(newLine+"Number of andsplit gateway: "+Integer.toString(parsedModel.getNumberOfGatewayAndSplit()));
		    fw2.write(newLine+"Number of andjoin gateway: "+Integer.toString(parsedModel.getNumberOfGatewayAndJoin()));				
		    fw2.write(newLine+"Number of orsplit gateway: "+Integer.toString(parsedModel.getNumberOfGatewayOrSplit()));
		    fw2.write(newLine+"Number of event based gateway: "+Integer.toString(parsedModel.getNumberOfGatewayEventBased()));
		    
		    fw2.write(newLine+"ParsingTime: "+Long.toString(parsingTime2)+newLine);	    
		
	}   

}