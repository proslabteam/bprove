package plugin.bpmn.to.maude.handlers;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JComboBox;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.core.filesystem.EFS;
import org.eclipse.core.filesystem.IFileStore;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import plugin.bpmn.to.maude.getService.GetReq;
import plugin.bpmn.to.maude.notation.Proc;
import plugin.bpmn.to.maude.notation.ReceiveTask;
import plugin.bpmn.to.maude.notation.SendTask;
import plugin.bpmn.to.maude.notation.Task;
import plugin.bpmn.to.maude.notation.Collaboration;
import plugin.bpmn.to.maude.notation.Msg;
import plugin.bpmn.to.maude.notation.Pool;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.BorderFactory;
import javax.swing.JButton;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Iterator;

import java.awt.Font;
import java.awt.Color;
import java.awt.Desktop;
import javax.swing.border.LineBorder;



@SuppressWarnings("serial")
public class MaudeOperation extends JFrame {

	private JPanel contentPane;
	private JTextField textOp_11;
	private JTextField textOp_13;
	private JTextArea editorPane;
	
	int count = 0;
	
	static boolean unix = true;
	static boolean ritorno = false;
	static String MaudeCode = "";
	static String Maudepath = "";
	static String operationlogreturn;
	String operationlog = "PROPERTIES VERIFICATION STATISTICS \n";
	static JFrame frame = new JFrame("Maude Operation Message Dialog");	
	
	public String originalModel;
	public String ParsedModel;

	boolean operation_done = false;
	
	private ArrayList<String> poolList;
	private ArrayList<String> taskList;
	private ArrayList<String> inputMsgList;
	private ArrayList<String> outputMsgList;
	
	private String poolFromListOp4;
	private String poolFromListOp5;
	private String poolFromListOp10;
	private String poolFromListOp11;
	private String taskFromListOp6;
	private String taskFromListOp8;
	private String taskFromListOp9;
	private String inputMsg;
	private String outputMsg;
	
	public static void main( final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter ) {
	
		EventQueue.invokeLater(new Runnable() {
			
			public void run() {
				try {
					if(!System.getProperty("os.name").startsWith("Windows")){
						  unix=true;
					  }else{
						  unix=false;
					  }
					MaudeOperation frame = new MaudeOperation(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});
	}

	/**
	 * Create the frame.
	 * @param modelToParse 
	 * @throws IOException 
	 * @throws URISyntaxException 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public MaudeOperation(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter) throws IOException, URISyntaxException {
		
		setAlwaysOnTop(true);
		setTitle("Properties Verification");
		poolList=new  ArrayList<String>();
		taskList=new  ArrayList<String>();
		inputMsgList=new  ArrayList<String>();
		outputMsgList=new  ArrayList<String>();
		
		//Obtain PoolList and TaskList
		//obtainPoolAndTaskList(parsedModel, poolList, taskList);
		
		obtainPoolAndTaskListAndMsgList(parsedModel, poolList, taskList, inputMsgList, outputMsgList);
		
		//System.out.println("\nAfter obtainPoolAndTaskList");

		MaudeOperation.this.addWindowListener(new WindowAdapter() {
		      public void windowClosing(WindowEvent e) {
					try {

						StatisticsFileWriter.write(operationlog);
						StatisticsFileWriter.flush();
						StatisticsFileWriter.close();

					  }
					  catch (IOException e1) {
						  	e1.printStackTrace();
				 	  }  	
					  e.getWindow().setVisible(false); 
					  MaudeOperation.this.dispose();
		      }
		    });
		

		setBounds(100, 50, 562, 620);
		//Color color1 = new Color(205,231,245);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		//contentPane.setBackground(color1);
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel initial_text = new JLabel("Non-Domain dependent properties");
		initial_text.setFont(new Font("Baskerville", Font.BOLD | Font.ITALIC, 15));
		initial_text.setBounds(153, 14, 250, 14);
		contentPane.add(initial_text);
		

//		//TUTTE LE OPZIONI PER LA OPERAZIONE 4
		JLabel initial_text2 = new JLabel("Domain dependent properties");
		initial_text2.setFont(new Font("Baskerville", Font.BOLD | Font.ITALIC, 15));
		initial_text2.setBounds(175, 74, 217, 14);
		contentPane.add(initial_text2);

		

//		
		JLabel lblInsertQualcosa = new JLabel("Will the process of a certain Pool always end?");
		lblInsertQualcosa.setBounds(110, 135, 300, 14);
		contentPane.add(lblInsertQualcosa);
		
		String[] poolArrOp4 = new String[poolList.size()];
		poolArrOp4 = (String[]) poolList.toArray(poolArrOp4);
		final JComboBox poolListBoxOp4 = new JComboBox(poolArrOp4);
		poolListBoxOp4.setBounds(110, 152, 327, 20);
	    contentPane.add(poolListBoxOp4);
		int poolListIndexoperazione4 = poolListBoxOp4.getSelectedIndex();
		poolFromListOp4=(String) poolListBoxOp4.getItemAt(poolListIndexoperazione4);
		final JButton btnOperazione4 = new JButton("CHECK");

		btnOperazione4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				int poolListIndexoperazione4 = poolListBoxOp4.getSelectedIndex();
				poolFromListOp4=(String) poolListBoxOp4.getItemAt(poolListIndexoperazione4);
				
				operation_done=true;
				editorPane.setText("");
				if (!poolFromListOp4.equals(""))
				{
				long propertyVerificationTimer = System.currentTimeMillis();				
				PostMultipleParameters result = new PostMultipleParameters();
				try {
					String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
					PostMultipleParameters postMultiple = new PostMultipleParameters();
					postMultiple.setOriginalModel(originalModelUrl);
					postMultiple.setParsedModel(parsedModel);
					postMultiple.setProperty("aBPoolendsParameterized");
					postMultiple.setPoolName(poolFromListOp4);
					//post vecchia senza json
					//result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
					//post nuova con json
					result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
					
				String output = "";
				String operationlog4="";
				
				if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
					output = "Error, Maude LTL Model Checker could not handle the model";
					operationlog4 = "Error, Maude LTL Model Checker could not handle the model";
			
				}
				//System.out.println("\n\nResult: "+result.getCounterexample());
				propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer;
				
				if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
				{
					output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
					output+="\n However the Counterexample could not be generated";
					operationlog4 = "aBPoolendsParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE However the Counterexample could not be generated";
					operationlog4+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";				
				}
				
				if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
						
						try {
							//System.out.println("\ngenerateXML: ");

							generateXML( result.getCounterexample(), Savepathperformance);
						} catch (Exception e) {

							e.printStackTrace();
						}
						output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
						operationlog4 = "aBPoolendsParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+" - Operation Result: FALSE ";
						operationlog4+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

				}else{
					if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
						{
							output = "TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
							operationlog4 = "aBPoolendsParameterized "+"TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
							operationlog4+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
						}	

				}
				if(unix)operationlog += "\n"+operationlog4;
				else operationlog += "\r\n"+operationlog4;

				editorPane.setText(output);
				
				}
				else
				{
			  		JOptionPane.showMessageDialog(frame,"Write Pool name");	
				}
			}
		});

		btnOperazione4.setBounds(442, 151, 115, 23);
		contentPane.add(btnOperazione4);

//		//TUTTE LE OPZIONI PER LA OPERAZIONE 5

		
		JLabel lblWillACertain = new JLabel("Will the process of a certain Pool always start?");
		lblWillACertain.setBounds(110, 97, 300, 14);
		contentPane.add(lblWillACertain);	
		

		//JCOMBOBOX ADDED
		String[] poolArrOp_5 = new String[poolList.size()];
		poolArrOp_5 = (String[]) poolList.toArray(poolArrOp_5);
		final JComboBox poolListBoxOp_5 = new JComboBox(poolArrOp_5);
		poolListBoxOp_5.setBounds(110, 113, 327, 20);
	    contentPane.add(poolListBoxOp_5);
		int poolListIndexoperazione5 = poolListBoxOp_5.getSelectedIndex();
		poolFromListOp5=(String) poolListBoxOp_5.getItemAt(poolListIndexoperazione5);
				
		final JButton btnOperazione5 = new JButton("CHECK");
		btnOperazione5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				////System.out.println("CHIAMO OPERAZIONE 5");
				int poolListIndexoperazione5 = poolListBoxOp_5.getSelectedIndex();
				poolFromListOp5=(String) poolListBoxOp_5.getItemAt(poolListIndexoperazione5);
				
				operation_done=true;
				editorPane.setText("");

				if (!poolFromListOp5.equals(""))			
				{
				long propertyVerificationTimer = System.currentTimeMillis();
								
				PostMultipleParameters result = new PostMultipleParameters();

				try {

					String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
					PostMultipleParameters postMultiple = new PostMultipleParameters();
					postMultiple.setOriginalModel(originalModelUrl);
					postMultiple.setParsedModel(parsedModel);
					postMultiple.setProperty("aBPoolstartsParameterized");
					postMultiple.setPoolName(poolFromListOp5);
					result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				String output = "";
				String operationlog5="";
				if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
					output = "Error, Maude LTL Model Checker could not handle the model";
					operationlog5 = "Error, Maude LTL Model Checker could not handle the model";
			
				}
				propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer;
				
				if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
				{
					output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
					output+="\n However the Counterexample could not be generated";
					operationlog5 = "aBPoolstartsParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE However the Counterexample could not be generated";
					operationlog5+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}
				
					if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
							
							try {
								generateXML( result.getCounterexample(), Savepathperformance);
							} catch (Exception e) {
								e.printStackTrace();
							}
							output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
							operationlog5 = "aBPoolstartsParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE ";
							operationlog5+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
							//}
					}else{
						if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
							{
								output = "TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
								operationlog5 = "aBPoolstartsParameterized "+"TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
								operationlog5+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
							}	

				}
				if(unix)operationlog += "\n"+operationlog5;
				else operationlog += "\r\n"+operationlog5;

				editorPane.setText(output);
			
				}
				else
				{
			  		JOptionPane.showMessageDialog(frame,"Insert the Pool Name");	
				}
			}
		});

		btnOperazione5.setBounds(442, 113, 115, 23);
		contentPane.add(btnOperazione5);
		
//		//TUTTE LE OPZIONI PER LA OPERAZIONE 6
		
		JLabel lblIllACertain = new JLabel("Will a certain Task complete?");
		lblIllACertain.setBounds(110, 250, 238, 14);
		contentPane.add(lblIllACertain);
	
		String[] taskArr = new String[taskList.size()];
		taskArr = (String[]) taskList.toArray(taskArr);
		final JComboBox taskListBox = new JComboBox(taskArr);
		taskListBox.setBounds(110, 266, 327, 20);
	    contentPane.add(taskListBox);
		int taskListIndexoperazione6 = taskListBox.getSelectedIndex();
		taskFromListOp6=(String) taskListBox.getItemAt(taskListIndexoperazione6);
		
		final JButton btnOperazione6 = new JButton("CHECK");
		btnOperazione6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int taskListIndexoperazione6 = taskListBox.getSelectedIndex();
				taskFromListOp6=(String) taskListBox.getItemAt(taskListIndexoperazione6);
			
				operation_done=true;
				editorPane.setText("");
//				if (!textOp_6.getText().equals(""))
				if (!taskFromListOp6.equals(""))
				{										
					long propertyVerificationTimer = System.currentTimeMillis();				
					PostMultipleParameters result = new PostMultipleParameters();
					try {
						String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
						PostMultipleParameters postMultiple = new PostMultipleParameters();
						postMultiple.setOriginalModel(originalModelUrl);
						postMultiple.setParsedModel(parsedModel);
						postMultiple.setProperty("aTaskCompleteParameterized");
						postMultiple.setTaskName1(taskFromListOp6);
						result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
					String operationlog6=null;
					String output = "";
					
					if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
						output = "Error, Maude LTL Model Checker could not handle the model";
						operationlog6 = "Error, Maude LTL Model Checker could not handle the model";			
					}
					
					propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer; 
					
					if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
					{
						output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
						output+="\n However the Counterexample could not be generated";
						operationlog6 = "aTaskCompleteParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE However the Counterexample could not be generated";
						operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
					}
					
					if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){

							try {
								generateXML( result.getCounterexample(), Savepathperformance);
							} catch (Exception e) {
								e.printStackTrace();
							}
							output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
							operationlog6 = "aTaskCompleteParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE ";
							operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";


					}else{
						if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
							{
								output = "TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
								operationlog6 = "aTaskCompleteParameterized "+"TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
								operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
							}

					}
					
					if(result.getResult()!=null&&result.getResult().contains("exceeded"))
					{
						output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime()+"ms";
						operationlog6 = "aTaskCompleteParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
						operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
					}
					
						if(unix)operationlog += "\n"+operationlog6;
						else operationlog += "\r\n"+operationlog6;
						editorPane.setText(output);

				}
				else
				{
			  		JOptionPane.showMessageDialog(frame,"Insert the Task Name");
				}
				
			}
		});

		btnOperazione6.setBounds(442, 266, 115, 23);
		contentPane.add(btnOperazione6);
	
//		//TUTTE LE OPZIONI PER LA OPERAZIONE 8

		JLabel lblWillTaskEnd = new JLabel("Does task1 implies task2?");		
		//JLabel lblWillTaskEnd = new JLabel("Will task2 end before task1");
		lblWillTaskEnd.setBounds(110, 362, 297, 14);
		contentPane.add(lblWillTaskEnd);
			
		String[] taskArrOp8 = new String[taskList.size()];
		taskArrOp8 = (String[]) taskList.toArray(taskArrOp8);
		final JComboBox taskListBoxOp8 = new JComboBox(taskArrOp8);
		taskListBoxOp8.setBounds(110, 378, 119, 20);
	    contentPane.add(taskListBoxOp8);
		int taskListIndexoperazione8 = taskListBoxOp8.getSelectedIndex();
		taskFromListOp8=(String) taskListBoxOp8.getItemAt(taskListIndexoperazione8);

		
		String[] taskArrOp9 = new String[taskList.size()];
		taskArrOp9 = (String[]) taskList.toArray(taskArrOp9);
		final JComboBox taskListBoxOp9 = new JComboBox(taskArrOp9);
		taskListBoxOp9.setBounds(317, 378, 119, 20);
	    contentPane.add(taskListBoxOp9);
		int taskListIndexoperazione9 = taskListBoxOp9.getSelectedIndex();
		taskFromListOp9=(String) taskListBoxOp9.getItemAt(taskListIndexoperazione9);
		
		final JButton btnOperazione8 = new JButton("CHECK");
		btnOperazione8.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int taskListIndexoperazione8 = taskListBoxOp8.getSelectedIndex();
				taskFromListOp8=(String) taskListBoxOp8.getItemAt(taskListIndexoperazione8);

				int taskListIndexoperazione9 = taskListBoxOp9.getSelectedIndex();
				taskFromListOp9=(String) taskListBoxOp9.getItemAt(taskListIndexoperazione9);
			
				
				operation_done=true;
				editorPane.setText("");

				if (!taskFromListOp8.equals("") && !taskFromListOp9.equals(""))
				{
					
				long propertyVerificationTimer = System.currentTimeMillis();				
				PostMultipleParameters result = new PostMultipleParameters();

				try {
					String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
					PostMultipleParameters postMultiple = new PostMultipleParameters();
					postMultiple.setOriginalModel(originalModelUrl);
					postMultiple.setParsedModel(parsedModel);
					postMultiple.setProperty("Task1ImpliesTask2");
					postMultiple.setTaskName1(taskFromListOp8);
					postMultiple.setTaskName2(taskFromListOp9);
					result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
				} catch (Exception e1) {

					e1.printStackTrace();
				}
				
				String operationlog8="";
				String output = "";
				
				if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
					output = "Error, Maude LTL Model Checker could not handle the model";
					operationlog8 = "Error, Maude LTL Model Checker could not handle the model";		
				}
				
				propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer; 
				if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
				{
					output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
					output+="\n However the Counterexample could not be generated";
					operationlog8 = "Task1ImpliesTask2 "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE However the Counterexample could not be generated";
					operationlog8+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}
				
				if (result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){

						try {
							generateXML( result.getCounterexample(), Savepathperformance);
						} catch (Exception e) {
							e.printStackTrace();
						}
						output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
						operationlog8 = "Task1ImpliesTask2 "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+" - Operation Result: FALSE ";
						operationlog8+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

				}else{
					if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
						{
							output = "TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
							operationlog8 = "Task1ImpliesTask2 "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+" - Operation Result: TRUE ";
							operationlog8+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

						}

				}

				if(unix)operationlog += "\n"+operationlog8;
				else operationlog += "\r\n"+operationlog8;
				editorPane.setText(output);

				}
				else
				{
			  		JOptionPane.showMessageDialog(frame,"Insert the Task Name");	
				}
			}
		});		

		btnOperazione8.setBounds(442, 378, 115, 23);
		contentPane.add(btnOperazione8);
		

//		
//		//TUTTE LE OPZIONI PER LA OPERAZIONE 9
//				

		JLabel lblAPoolAlways = new JLabel("Will a Pool always receive this message?");
		lblAPoolAlways.setBounds(110, 325, 294, 14);
		contentPane.add(lblAPoolAlways);
		
		String[] poolArrOp_10 = new String[poolList.size()];
		poolArrOp_10 = (String[]) poolList.toArray(poolArrOp_10);
		final JComboBox poolListBoxOp_10 = new JComboBox(poolArrOp_10);
		poolListBoxOp_10.setBounds(110, 342, 119, 20);
	    contentPane.add(poolListBoxOp_10);
		int poolListIndexoperazione10 = poolListBoxOp_10.getSelectedIndex();
		poolFromListOp10=(String) poolListBoxOp_10.getItemAt(poolListIndexoperazione10);
		
//		textOp_11 = new JTextField("");
//		textOp_11.setColumns(10);
//		textOp_11.setBounds(320, 342, 115, 20);
//		contentPane.add(textOp_11);
		
		
		String[] inputMsgArr = new String[inputMsgList.size()];
		inputMsgArr = (String[]) inputMsgList.toArray(inputMsgArr);
		final JComboBox inputMsgArrcomboBox = new JComboBox(inputMsgArr);
		inputMsgArrcomboBox.setBounds(321, 342, 116, 27);
	    contentPane.add(inputMsgArrcomboBox);
		int inputMsgArrIndex = inputMsgArrcomboBox.getSelectedIndex();
		inputMsg=(String) inputMsgArrcomboBox.getItemAt(inputMsgArrIndex);
		
		final JButton btnOperazione9 = new JButton("CHECK");
		btnOperazione9.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int poolListIndexoperazione10 = poolListBoxOp_10.getSelectedIndex();
				poolFromListOp10=(String) poolListBoxOp_10.getItemAt(poolListIndexoperazione10);
				int inputMsgoperazione10 = inputMsgArrcomboBox.getSelectedIndex();
				inputMsg=(String) inputMsgArrcomboBox.getItemAt(inputMsgoperazione10);
				
				
				
				operation_done=true;
				editorPane.setText("");

				if (!poolFromListOp10.equals("") && !inputMsg.equals(""))
				{
				long propertyVerificationTimer = System.currentTimeMillis();				
				PostMultipleParameters result = new PostMultipleParameters();
				try {
					String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
					PostMultipleParameters postMultiple = new PostMultipleParameters();
					postMultiple.setOriginalModel(originalModelUrl);
					postMultiple.setParsedModel(parsedModel);
					postMultiple.setProperty("aBPoolRcvMsg");
					postMultiple.setPoolName(poolFromListOp10);
					postMultiple.setRcvMsgName(inputMsg);
					result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				
				String output = "";
				String operationlog9="";
				
				if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
					output = "Error, Maude LTL Model Checker could not handle the model";
					operationlog9 = "Error, Maude LTL Model Checker could not handle the model";		
				}
				
				propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer; 
				
				if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
				{
					output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
					output+="\n However the Counterexample could not be generated";
					operationlog9 = "aBPoolRcvMsg "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE However the Counterexample could not be generated";
					operationlog9+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}
				
				if (result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){

						try {
							generateXML( result.getCounterexample(), Savepathperformance);
						} catch (Exception e) {
							e.printStackTrace();
						}
						output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
						operationlog9 = "aBPoolRcvMsg "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE ";
						operationlog9+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

				}else{
					if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
					{
						output = "TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
						operationlog9 = "aBPoolRcvMsg "+"TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
						operationlog9+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

					}

				}
				if(unix)operationlog += "\n"+operationlog9;
				else operationlog += "\r\n"+operationlog9;

				editorPane.setText(output);

				}
				else
				{
			  		JOptionPane.showMessageDialog(frame,"Riempire i campi con il nome della Pool e del messaggio");	
				}
			}
		});	

		btnOperazione9.setBounds(442, 342, 115, 23);
		contentPane.add(btnOperazione9);
		
		
//		
//		//TUTTE LE OPZIONI PER L'OPERAZIONE 11
		
		JLabel lblAPoolSnd = new JLabel("Will a Pool always send this message?");
		lblAPoolSnd.setBounds(110, 288, 297, 14);
		contentPane.add(lblAPoolSnd);
		
		String[] poolArrOp_11 = new String[poolList.size()];
		poolArrOp_11 = (String[]) poolList.toArray(poolArrOp_11);
		final JComboBox poolListBoxOp_11 = new JComboBox(poolArrOp_11);
		poolListBoxOp_11.setBounds(110, 304, 119, 20);
	    contentPane.add(poolListBoxOp_11);
		int poolListIndexoperazione11 = poolListBoxOp_11.getSelectedIndex();
		poolFromListOp11=(String) poolListBoxOp_11.getItemAt(poolListIndexoperazione11);
		
//		textOp_13 = new JTextField("");
//		textOp_13.setColumns(10);
//		textOp_13.setBounds(320, 304 , 115, 20);
//		contentPane.add(textOp_13);
		String[] outputMsgArr = new String[outputMsgList.size()];
		outputMsgArr = (String[]) outputMsgList.toArray(outputMsgArr);
		final JComboBox outputMsgArrcomboBox = new JComboBox(outputMsgArr);
		outputMsgArrcomboBox.setBounds(321, 304, 116, 27);
	    contentPane.add(outputMsgArrcomboBox);
		int outputMsgArrIndex = outputMsgArrcomboBox.getSelectedIndex();
		outputMsg=(String) outputMsgArrcomboBox.getItemAt(outputMsgArrIndex);


		
		final JButton btnOperazione11 = new JButton("CHECK");
		btnOperazione11.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int poolListIndexoperazione11 = poolListBoxOp_11.getSelectedIndex();
				int aBPoolSndMsgoperazione11 = outputMsgArrcomboBox.getSelectedIndex();
				poolFromListOp11=(String) poolListBoxOp_11.getItemAt(poolListIndexoperazione11);
				outputMsg=(String) outputMsgArrcomboBox.getItemAt(aBPoolSndMsgoperazione11);
				
				operation_done=true;
				editorPane.setText("");

				if (!poolFromListOp11.equals("") && !outputMsg.equals(""))
				{				
				long propertyVerificationTimer = System.currentTimeMillis();				
				PostMultipleParameters result = new PostMultipleParameters();
				try {
					String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
					PostMultipleParameters postMultiple = new PostMultipleParameters();
					postMultiple.setOriginalModel(originalModelUrl);
					postMultiple.setParsedModel(parsedModel);
					postMultiple.setProperty("aBPoolSndMsg");
					postMultiple.setPoolName(poolFromListOp11);
					postMultiple.setSndMsgName(outputMsg);
					result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
				} catch (Exception e1) {

					e1.printStackTrace();
				}

				String output = "";
				String operationlog11="";
				
				if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
					output = "Error, Maude LTL Model Checker could not handle the model";
					operationlog11 = "Error, Maude LTL Model Checker could not handle the model";		
				}
				
				propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer; 
				
				if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
				{
					output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
					output+="\n However the Counterexample could not be generated";
					operationlog11 = "aBPoolSndMsg "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE However the Counterexample could not be generated";
					operationlog11+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}
				if (result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){

						try {
							generateXML( result.getCounterexample(), Savepathperformance);
						} catch (Exception e) {
							e.printStackTrace();
						}
						output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
						operationlog11 += "aBPoolSndMsg "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE ";
						operationlog11+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

				}else{
					if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
					{
						output = "TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
						operationlog11 += "aBPoolSndMsg "+"TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
						operationlog11+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

					}

				}
				if(unix)operationlog += "\n"+operationlog11;
				else operationlog += "\r\n"+operationlog11;
				editorPane.setText(output);

				}else{
			  		JOptionPane.showMessageDialog(frame,"Insert the Pool Name");	
				}
			}
		});

		btnOperazione11.setBounds(442, 304, 115, 23);
		contentPane.add(btnOperazione11);	

		editorPane = new JTextArea();
		editorPane.setLineWrap(true);
		editorPane.setWrapStyleWord(true);
		editorPane.setEnabled(false);
		editorPane.setBackground(Color.WHITE);
		editorPane.setForeground(Color.WHITE);
		editorPane.setBounds(10, 495, 543, 57);
		contentPane.add(editorPane);
			
		JLabel lblOperationResult = new JLabel("Operation Result");
		lblOperationResult.setFont(new Font("Baskerville", Font.BOLD | Font.ITALIC, 15));
		lblOperationResult.setBounds(10, 477, 126, 14);
		contentPane.add(lblOperationResult);
		
		JLabel lbltaskname = new JLabel("(Task1Name)");
		lbltaskname.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lbltaskname.setBounds(33, 378, 86, 14);
		contentPane.add(lbltaskname);
		
		JLabel lbltaskname_1 = new JLabel("(Task2Name)");
		lbltaskname_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lbltaskname_1.setBounds(241, 378, 86, 14);
		contentPane.add(lbltaskname_1);
		
		JLabel lblmessage = new JLabel("(MessageName)");
		lblmessage.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblmessage.setBounds(228, 342, 94, 14);
		contentPane.add(lblmessage);
		
		JLabel lblpoolname = new JLabel("(PoolName)");
		lblpoolname.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblpoolname.setBounds(35, 342, 73, 14);
		contentPane.add(lblpoolname);
		
		JLabel lblpoolname_1 = new JLabel("(PoolName)");
		lblpoolname_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		//lblpoolname_1.setBounds(13, 293, 118, 14);
		lblpoolname_1.setBounds(35, 151, 73, 14);
		contentPane.add(lblpoolname_1);
		
		JLabel lblpoolname_2 = new JLabel("(PoolName)");
		lblpoolname_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblpoolname_2.setBounds(35, 113, 73, 14);
		contentPane.add(lblpoolname_2);
		
		JLabel label_10 = new JLabel("(TaskName)");
		label_10.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_10.setBounds(35, 266, 80, 14);
		contentPane.add(label_10);
		
		JLabel lblpoolname_3 = new JLabel("(PoolName)");
		lblpoolname_3.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblpoolname_3.setBounds(35, 304, 73, 14);
		contentPane.add(lblpoolname_3);

		//TUTTE LE OPZIONI PER IL PULSANTE EXIT, CON LE SUE FUNZIONI
		JButton btnExit = new JButton("EXIT");
		
		//EXIT handler
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
			if(operation_done){	
			try {
				   	//I write the response to the original fileWriter generated in MainFrame
				   	StatisticsFileWriter.write(operationlog);
				   	StatisticsFileWriter.flush();
				   	StatisticsFileWriter.close();
			     JOptionPane.showMessageDialog(frame,"Property Verification Response: SUCCEDED");
			}
			catch (IOException e) {
			     JOptionPane.showMessageDialog(frame,"Property Verification Response: ERROR!");
			     e.printStackTrace();
			}  	 
			}
			MaudeOperation.this.dispose();
		}
		});
		btnExit.setBounds(445, 559, 115, 23);
		contentPane.add(btnExit);
		
//CHANGING ALL NON-DOMAIN PROPERTIES IN A JCOMBOBOX
		String[] propertyStrings = { "Can a Process Start?", "Can a Process End?", "Can All the Process End?", 
								"No Dead Activities", "Option to Complete", "Proper Completion [Control Flow Tokens Only]",
								"Proper Completion [Message Flow Tokens Only]", "Proper Completion [Control Flow and Message Flow Tokens]",
								"Safeness"};
		final JComboBox propertyList = new JComboBox(propertyStrings);
		propertyList.setBounds(83, 40, 300, 20);
		contentPane.add(propertyList);
		
		JButton button = new JButton("CHECK");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {								
				String propertyToVerify=(String) propertyList.getSelectedItem();
				 switch (propertyToVerify) {
		         case "Can a Process Start?":
		             aProcessStart(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
		             break;
		         case "Can a Process End?":
		        	 aProcessEnd(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
		        	 break;
		         case "Can All the Process End?":
		        	 allProcessesEnd(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
		        	 break;
		         case "No Dead Activities":
		        	 noDeadActivities(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
		             break;
		         case "Option to Complete":
		        	 optionToComplete(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
		             break;
		         case "Proper Completion [Control Flow Tokens Only]":
		        	 properCompletionCF(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
		        	 break;
		         case "Proper Completion [Message Flow Tokens Only]":
		        	 properCompletionMF(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
		             break;
		         case "Proper Completion [Control Flow and Message Flow Tokens]":
		        	 properCompletionCFMF(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
		             break;
		         case "Safeness":
		        	 safeness(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter);
		             break;
		         default:
		             throw new IllegalArgumentException("Invalid property: " + propertyToVerify);
		     }
			}
		});
		button.setBounds(395, 39, 133, 23);
		contentPane.add(button);
		
		JLabel label = new JLabel("(TaskName)");
		label.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label.setBounds(35, 189, 80, 14);
		contentPane.add(label);
		
		JLabel lblWillACertain_1 = new JLabel("Will a certain Task be enabled?");
		lblWillACertain_1.setBounds(110, 172, 238, 14);
		contentPane.add(lblWillACertain_1);

		String[] taskEnabledArr = new String[taskList.size()];
		taskEnabledArr = (String[]) taskList.toArray(taskEnabledArr);
		final JComboBox taskEnabledList = new JComboBox(taskEnabledArr);
		taskEnabledList.setBounds(110, 189, 327, 20);
	    contentPane.add(taskEnabledList);
		/*int taskEnabledListIndexoperazione6 = taskListBox.getSelectedIndex();
		taskFromListOp6=(String) taskListBox.getItemAt(taskEnabledListIndexoperazione6);*/
		
		JButton button_taskEnabled = new JButton("CHECK");
		button_taskEnabled.setBounds(442, 189, 115, 23);
		contentPane.add(button_taskEnabled);
		
		button_taskEnabled.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int taskEnabledListIndexoperazione6 = taskEnabledList.getSelectedIndex();
				taskFromListOp6=(String) taskEnabledList.getItemAt(taskEnabledListIndexoperazione6);
			
				operation_done=true;
				editorPane.setText("");
//				if (!textOp_6.getText().equals(""))
				if (!taskFromListOp6.equals(""))
				{										
					long propertyVerificationTimer = System.currentTimeMillis();				
					PostMultipleParameters result = new PostMultipleParameters();
					try {
						String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
						PostMultipleParameters postMultiple = new PostMultipleParameters();
						postMultiple.setOriginalModel(originalModelUrl);
						postMultiple.setParsedModel(parsedModel);
						postMultiple.setProperty("aTaskEnabledParameterized");
						postMultiple.setTaskName1(taskFromListOp6);
						result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
					String operationlog6=null;
					String output = "";
					
					if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
						output = "Error, Maude LTL Model Checker could not handle the model";
						operationlog6 = "Error, Maude LTL Model Checker could not handle the model";			
					}
					
					propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer; 
					
					if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
					{
						output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
						output+="\n However the Counterexample could not be generated";
						operationlog6 = "aTaskCompleteParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE However the Counterexample could not be generated";
						operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
					}
					
					if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){

							try {
								generateXML( result.getCounterexample(), Savepathperformance);
							} catch (Exception e) {
								e.printStackTrace();
							}
							output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
							operationlog6 = "aTaskCompleteParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE ";
							operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";


					}else{
						if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
							{
								output = "TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
								operationlog6 = "aTaskCompleteParameterized "+"TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
								operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
							}

					}
					
					if(result.getResult()!=null&&result.getResult().contains("exceeded"))
					{
						output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime()+"ms";
						operationlog6 = "aTaskCompleteParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
						operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
					}
					
						if(unix)operationlog += "\n"+operationlog6;
						else operationlog += "\r\n"+operationlog6;
						editorPane.setText(output);

				}
				else
				{
			  		JOptionPane.showMessageDialog(frame,"Insert the Task Name");
				}
				
			}
		});
		
		
		

		
		JLabel label_2 = new JLabel("(TaskName)");
		label_2.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_2.setBounds(35, 227, 73, 14);
		contentPane.add(label_2);
		
		JLabel lblWillACertain_2 = new JLabel("Will a certain Task run?");
		lblWillACertain_2.setBounds(110, 211, 238, 14);
		contentPane.add(lblWillACertain_2);
		
		String[] taskRunningArr = new String[taskList.size()];
		taskRunningArr = (String[]) taskList.toArray(taskRunningArr);
		final JComboBox taskRunningList = new JComboBox(taskRunningArr);
		taskRunningList.setBounds(110, 227, 327, 20);
	    contentPane.add(taskRunningList);
		
		JButton button_taskRunning = new JButton("CHECK");
		button_taskRunning.setBounds(442, 227, 115, 23);
		contentPane.add(button_taskRunning);
		
		String[] poolPropArr = new String[poolList.size()];
		poolPropArr = (String[]) poolList.toArray(poolPropArr);
		final JComboBox poolPropListBox = new JComboBox(poolPropArr);
		poolPropListBox.setBounds(10, 442, 142, 20);
	    contentPane.add(poolPropListBox);
	    
		String[] propListStrings = { "No Dead Activities", "Option to Complete", "Proper Completion [Control Flow Tokens Only]",
				"Proper Completion [Message Flow Tokens Only]", "Proper Completion [Control Flow and Message Flow Tokens]",
				"Safeness"};
		final JComboBox propListBox = new JComboBox(propListStrings);
		propListBox.setBounds(155, 442, 285, 20);
		contentPane.add(propListBox);
		
		final JButton btnPoolProp = new JButton("CHECK");	
		btnPoolProp.setBounds(442, 442, 115, 23);
		contentPane.add(btnPoolProp);
		
		btnPoolProp.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {								
				String poolToVerify=(String) poolPropListBox.getSelectedItem();
				String propToVerify=(String) propListBox.getSelectedItem();
				propToVerify=propToVerify.replaceAll("\\s+", "");
				 switch (propToVerify) {
		         case "NoDeadActivities":
		        	 noDeadActivitiesParam(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter, poolToVerify);
		        	 break;
		         case "OptiontoComplete":
		             optionToCompleteParam(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter, poolToVerify);
		             break;
		         case "ProperCompletion[ControlFlowTokensOnly]":
		        	 properCompletionCFParam(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter, poolToVerify);
		        	 break;
		         case "ProperCompletion[MessageFlowTokensOnly]":
		        	 properCompletionMFParam(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter, poolToVerify);
		             break;
		         case "ProperCompletion[ControlFlowandMessageFlowTokens]":
		        	 properCompletionCFMFParam(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter, poolToVerify);
		             break;
		         case "Safeness":
		        	 safenessParam(modelToParse , parsedModel, Savepathperformance, Loadpath, StatisticsFileWriter, poolToVerify);
		             break;
		         default:
		             throw new IllegalArgumentException("Invalid property: " + propToVerify);
		     }
			}
		});
		
		JLabel lblSelectAPool = new JLabel("Select a Pool");
		lblSelectAPool.setBounds(12, 424, 86, 14);
		contentPane.add(lblSelectAPool);
		
		JLabel lblNoDeadActivities_1 = new JLabel("Choose the property to check over the Pool");
		lblNoDeadActivities_1.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		lblNoDeadActivities_1.setBounds(162, 424, 273, 14);
		contentPane.add(lblNoDeadActivities_1);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBounds(8, 35, 548, 30);
		panel_2.setBorder(BorderFactory.createLineBorder(Color.black));
		contentPane.add(panel_2);
		
		JPanel panel_3 = new JPanel();
		panel_3.setBounds(8, 493, 548, 60);
		panel_3.setBorder(BorderFactory.createLineBorder(Color.black));
		contentPane.add(panel_3);
		
		JLabel label_1 = new JLabel("(MessageName)");
		label_1.setFont(new Font("Tahoma", Font.PLAIN, 13));
		label_1.setBounds(228, 305, 94, 14);
		contentPane.add(label_1);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(8, 413, 548, 56);
		panel_1.setBorder(BorderFactory.createLineBorder(Color.black));
		contentPane.add(panel_1);
		
		
		JPanel panel = new JPanel();
		panel.setBounds(10, 564, 338, 34);
		contentPane.add(panel);
		
		//Powered By pros.unicam.it
		final URI uri = new URI("http://pros.unicam.it/");

		
		class OpenUrlAction implements ActionListener {
			  @Override public void actionPerformed(ActionEvent e) {
			      open(uri);
			  }
		}
		JButton button_1 = new JButton();
		button_1.setToolTipText("http://pros.unicam.it/");
		button_1.setText("<HTML>Powered by <FONT color=\"#000099\"><U>PROS Lab - University of Camerino, Italy</U></FONT></HTML>");
		button_1.setOpaque(false);
		button_1.setBackground(Color.WHITE);
		button_1.setToolTipText(uri.toString());
		button_1.addActionListener(new OpenUrlAction());
		button_1.setHorizontalAlignment(SwingConstants.LEFT);
		button_1.setBorderPainted(false);
		button_1.setBackground(Color.WHITE);
		panel.add(button_1);
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new LineBorder(new Color(0, 0, 0)));
		panel_4.setBounds(8, 97, 548, 317);
		contentPane.add(panel_4);
		
		
//		String[] taskArrOp9 = new String[taskList.size()];
//		taskArrOp9 = (String[]) taskList.toArray(taskArrOp9);
//		final JComboBox taskListBoxOp9 = new JComboBox(taskArrOp9);
//		taskListBoxOp9.setBounds(317, 378, 119, 20);
//	    contentPane.add(taskListBoxOp9);
//		int taskListIndexoperazione9 = taskListBoxOp9.getSelectedIndex();
//		taskFromListOp9=(String) taskListBoxOp9.getItemAt(taskListIndexoperazione9);
		
		
		
//		JPanel panel = new JPanel();
//		panel.setBorder(BorderFactory.createLineBorder(Color.black));
//		panel.setBounds(8, 74, 548, 344);
//		contentPane.add(panel);
		

		button_taskRunning.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int taskRunningListIndexoperazione6 = taskRunningList.getSelectedIndex();
				taskFromListOp6=(String) taskRunningList.getItemAt(taskRunningListIndexoperazione6);
			
				operation_done=true;
				editorPane.setText("");
//				if (!textOp_6.getText().equals(""))
				if (!taskFromListOp6.equals(""))
				{										
					long propertyVerificationTimer = System.currentTimeMillis();				
					PostMultipleParameters result = new PostMultipleParameters();
					try {
						String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
						PostMultipleParameters postMultiple = new PostMultipleParameters();
						postMultiple.setOriginalModel(originalModelUrl);
						postMultiple.setParsedModel(parsedModel);
						postMultiple.setProperty("aTaskRunningParameterized");
						postMultiple.setTaskName1(taskFromListOp6);
						result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
					} catch (Exception e1) {
						e1.printStackTrace();
					}
					
					String operationlog6=null;
					String output = "";
					
					if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
						output = "Error, Maude LTL Model Checker could not handle the model";
						operationlog6 = "Error, Maude LTL Model Checker could not handle the model";			
					}
					
					propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer; 
					
					if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
					{
						output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
						output+="\n However the Counterexample could not be generated";
						operationlog6 = "aTaskCompleteParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE However the Counterexample could not be generated";
						operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
					}
					
					if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){

							try {
								generateXML( result.getCounterexample(), Savepathperformance);
							} catch (Exception e) {
								e.printStackTrace();
							}
							output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
							operationlog6 = "aTaskCompleteParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: FALSE ";
							operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";


					}else{
						if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
							{
								output = "TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
								operationlog6 = "aTaskCompleteParameterized "+"TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
								operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
							}

					}
					
					if(result.getResult()!=null&&result.getResult().contains("exceeded"))
					{
						output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime()+"ms";
						operationlog6 = "aTaskCompleteParameterized "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+propertyVerificationTimer+"ms"+" - Operation Result: TRUE ";
						operationlog6+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
					}
					
						if(unix)operationlog += "\n"+operationlog6;
						else operationlog += "\r\n"+operationlog6;
						editorPane.setText(output);

				}
				else
				{
			  		JOptionPane.showMessageDialog(frame,"Insert the Task Name");
				}
				
			}
		});
		

		//petList.setSelectedIndex(4);
		//petList.addActionListener(this);
	}
	
	
	//Open URI
	   private static void open(URI uri) {
		    if (Desktop.isDesktopSupported()) {
		      try {
		        Desktop.getDesktop().browse(uri);
		      } catch (IOException e) { /* TODO: error handling */ }
		    } else { /* TODO: error handling */ }
		  }
	   
//aProcessStart
	public void aProcessStart(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter){
		operation_done=true;
		editorPane.setText("");		
		long propertyVerificationTimer = System.currentTimeMillis();				
		PostMultipleParameters result = new PostMultipleParameters();
		try {
			////System.out.println("\nBefore  GetReq.PostaBPoolstarts");
			////System.out.println("\nmodelToParse: "+modelToParse);
			////System.out.println("\nparsedModel: "+parsedModel);
			//result = GetReq.PostaBPoolstarts(modelToParse , parsedModel);
			////System.out.println("\nresult: "+result.getResult());
			String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
			PostMultipleParameters postMultiple = new PostMultipleParameters();
			postMultiple.setOriginalModel(originalModelUrl);
			postMultiple.setParsedModel(parsedModel);
			postMultiple.setProperty("aBPoolstarts");
			result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
		} catch (Exception e1) {
			result.setResult("\nERROR\n");
			editorPane.setText(result.getResult());
			return;
		}

		String output = "";
		String operationlog2 = "";
		
		if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
			output = "Error, Maude LTL Model Checker could not handle the model";
			operationlog2 = "Error, Maude LTL Model Checker could not handle the model";
	
		}
		
		propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer; 
		if (result.getResult()!=null&&result.getResult().contains("Counterexample could not be generated"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			output+="\n However the Counterexample could not be generated";
			operationlog2 += "aBPoolstarts "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
			operationlog2+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			
		}
		
		if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){

			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			operationlog2 = "aBPoolstarts "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+ propertyVerificationTimer+"ms";
			operationlog2+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

			if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
			{

				output = "TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				operationlog2 += "aBPoolstarts "+"TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: TRUE ";
				operationlog2+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			else
			{
				 if ((result.getResult()!=null&&result.getResult().contains("ERROR"))||(result.equals(""))){
                      output = "ERROR; FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
                      operationlog2 += "aBPoolstarts "+"ERROR; FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: FALSE ";
                      operationlog2+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
                  }else{
                      
                    try {
                    	generateXML( result.getCounterexample(), Savepathperformance);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
                    operationlog2 += "aBPoolstarts "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: FALSE ";
                    operationlog2+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
                  }
			}
		}else{
			if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
			{
				output = "TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				operationlog2 += "aBPoolstarts "+"TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: TRUE ";
				operationlog2+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
		}
		if(unix)operationlog += "\n"+operationlog2;
		else operationlog += "\r\n"+operationlog2;	
		editorPane.setText(output);
	}
	
	
//aProcessEnd	
	public void aProcessEnd(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter){
	operation_done=true;
	editorPane.setText("");
	long propertyVerificationTimer = System.currentTimeMillis();				
	PostMultipleParameters result = new PostMultipleParameters();

	try {
		String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
		PostMultipleParameters postMultiple = new PostMultipleParameters();
		postMultiple.setOriginalModel(originalModelUrl);
		postMultiple.setParsedModel(parsedModel);
		postMultiple.setProperty("aBPoolends");
		result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
	} catch (Exception e1) {
		e1.printStackTrace();
	}
		//Chech Error presence
		String operationlog1="";
		String output = "";
		
		if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
			output = "Error, Maude LTL Model Checker could not handle the model";
			operationlog1 = "Error, Maude LTL Model Checker could not handle the model";
	
		}
		propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer; 
		if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			//System.out.println("output: "+output);
			output+="\n However the Counterexample could not be generated";
			operationlog1 += "aBPoolends "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
			operationlog1+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		if((result.getCounterexample()!=null)&&(!result.getCounterexample().contains("Counterexample could not be generated"))&&(!result.getCounterexample().contains("counterexample is null"))){

			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			operationlog1 = "aBPoolends "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: FALSE";
			operationlog1+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";				

			if (result.getCounterexample().contains("result Bool: true"))
			{
				output = "TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				operationlog1 += "aBPoolends "+"TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: TRUE";
				operationlog1+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			else
			{
				//[ToDo] CONTROLLARE SE IL FILE APERTO, SE È APERTO CHIUDERLO!
				try {
					generateXML( result.getCounterexample(), Savepathperformance);
				} catch (Exception e) {
					e.printStackTrace();
				}
				output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - operation time: "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				operationlog1 += "aBPoolends "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - operation time: "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: FALSE ";
				operationlog1+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}
	
		}else{
			if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
			{
				output = "TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				operationlog1 +=  "aBPoolends "+"TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: TRUE ";
				operationlog1+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
		}
	if(unix)operationlog += "\n"+operationlog1;
	else operationlog += "\r\n"+operationlog1;
	
	editorPane.setText(output);
	}

//allProcessesEnd
	public void allProcessesEnd(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter){	
	operation_done=true;
	editorPane.setText("");		
	long propertyVerificationTimer = System.currentTimeMillis();				
	PostMultipleParameters result = new PostMultipleParameters();
	try {
		//result = GetReq.PostallBPoolend(modelToParse , parsedModel);
		String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
		PostMultipleParameters postMultiple = new PostMultipleParameters();
		postMultiple.setOriginalModel(originalModelUrl);
		postMultiple.setParsedModel(parsedModel);
		postMultiple.setProperty("allBPoolend");
		result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
	} catch (Exception e1) {
		e1.printStackTrace();
	}
	
	String output = "";
	String operationlog3="";
	
	if (result.getResult()!=null&result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
		output = "Error, Maude LTL Model Checker could not handle the model";
		operationlog3 = "Error, Maude LTL Model Checker could not handle the model";

	}
	
	propertyVerificationTimer = System.currentTimeMillis() - propertyVerificationTimer; 
	
	if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
	{
		output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
		output+="\n However the Counterexample could not be generated";
		operationlog3 = "allBPoolend "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
		operationlog3+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
	}
	
	if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){

			try {
				generateXML( result.getCounterexample(), Savepathperformance);
			} catch (Exception e) {
				e.printStackTrace();
			}

			output = "FALSE - THE PROPERTY IS NOT VERIFIED  - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			operationlog3 = "allBPoolend "+"FALSE - THE PROPERTY IS NOT VERIFIED  - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: FALSE ";
			operationlog3+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		
	}else{
	
		if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
		{

			output = "TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			operationlog3 = "allBPoolend "+"TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+propertyVerificationTimer+"ms "+" - Operation Result: TRUE ";
			operationlog3+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}

	}
	
	if(unix)operationlog += "\n"+operationlog3;
	else operationlog += "\r\n"+operationlog3;
	
	editorPane.setText(output);			
	}
	
//noDeadActivities
	public void noDeadActivities(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter){		
		operation_done=true;
		editorPane.setText("");		
		long currentTimestart10 = System.currentTimeMillis();
		PostMultipleParameters result = new PostMultipleParameters();
		try {
			//result = GetReq.PostNoDeadActivities(modelToParse , parsedModel);
			String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
			PostMultipleParameters postMultiple = new PostMultipleParameters();
			postMultiple.setOriginalModel(originalModelUrl);
			postMultiple.setParsedModel(parsedModel);
			postMultiple.setProperty("noDeadActivities");
			result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
		} catch (Exception e1) {
			e1.printStackTrace();
			result.setResult("\nERROR\n");
			editorPane.setText(result.getResult());
			editorPane.setForeground(Color.black);
		}
		
		@SuppressWarnings("unused")
		String output = "";
		String operationlog10="";
					
		if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
			output = "Error, Maude LTL Model Checker could not handle the model";
			operationlog10 = "Error, Maude LTL Model Checker could not handle the model";
	
		}
		long currentTimeend10 = System.currentTimeMillis();
		long time10 = 0;
		time10 = currentTimeend10 - currentTimestart10; 
		
		if (result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			output+="\n However the Counterexample could not be generated";
			operationlog10 = "NoDeadActivities "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
			operationlog10+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		
		if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
			//System.out.println("\nPostNoDeadActivitiesResult: "+result+"\n");				
			
			output = "FALSE - THE PROPERTY IS NOT VERIFIED  - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms ";
			operationlog10 = "noDeadActivities " +"FALSE - THE PROPERTY IS NOT VERIFIED  - "+"VerificationTime: "+ time10+"ms ";
			operationlog10+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			
			result.setResult(result.getResult()+" - "+time10+"ms\n");
			
		}
	
		if(unix)operationlog += "\n"+operationlog10;
		else operationlog += "\r\n"+operationlog10;
	
		if(result.getResult()!=null&&result.getResult().contains("result Bool: true")){
			editorPane.setText("TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms");
			//System.out.println("result.getResult():" +result.getResult());
			result.setResult(result.getResult()+" - "+time10+"ms\n");
		}
		else{
			editorPane.setText(result.getResult()+" - "+result.getPropertyVerificationTime()+"ms");				
		}
	}
	

	//OptionToComplete
	public void optionToComplete(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter){	
			operation_done=true;
			editorPane.setText("");		
			long currentTimestart = System.currentTimeMillis();
			PostMultipleParameters result = new PostMultipleParameters();
			String output = null;
			try {
				//result = GetReq.PostOptionToComplete(modelToParse , parsedModel);
				String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
				PostMultipleParameters postMultiple = new PostMultipleParameters();
				postMultiple.setOriginalModel(originalModelUrl);
				postMultiple.setParsedModel(parsedModel);
				postMultiple.setProperty("optionToComplete");
				result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
		
			} catch (Exception e1) {
				e1.printStackTrace();
				result.setResult("\nERROR\n");
				editorPane.setText(result.getResult());
				editorPane.setForeground(Color.black);
			}
			long currentTimeend = System.currentTimeMillis();
			long time = 0;
			time = currentTimeend - currentTimestart; 
			
			String operationlog112="";
						
			if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
				output = "Error, Maude LTL Model Checker could not handle the model";
				operationlog112 = "Error, Maude LTL Model Checker could not handle the model";
		
			}
			
			if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				output+="\n However the Counterexample could not be generated";
				operationlog112 = "OptionToComplete "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
				operationlog112+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
		
				operationlog112 = "OptionToComplete "+"FALSE - THE PROPERTY IS NOT VERIFIED  - "+"VerificationTime: "+ time+"ms" +" - Operation Result: FALSE ";
				operationlog112+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";				
				
				if(unix)operationlog += "\n"+operationlog112;
				else operationlog += "\r\n"+operationlog112;
				
				generateXML( result.getCounterexample(), Savepathperformance);
		
				output=result.getResult()+"\nVerificationTime: "+result.getPropertyVerificationTime()+"ms";
				editorPane.setText(output);
		
			}else{
				
				if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
				{
					output = "TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
					operationlog112 ="OptionToComplete "+"TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+time+"ms"+ " - Operation Result: TRUE ";
					operationlog112+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}
				editorPane.setText(output);
		
			}
			
			if(result.getResult()!=null&&result.getResult().contains("exceeded"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime()+"ms";
				operationlog112 = "OptionToComplete "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+time+"ms"+" - Operation Result: TRUE ";
				operationlog112+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
		}
		
	//properCompletionCF
		public void properCompletionCF(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter){
			operation_done=true;
			long time = 0;
			editorPane.setText("");		
			long currentTimestart = System.currentTimeMillis();
			PostMultipleParameters result=null;
			String output = null;
			
			try {
				String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
				PostMultipleParameters postMultiple = new PostMultipleParameters();
				postMultiple.setOriginalModel(originalModelUrl);
				postMultiple.setParsedModel(parsedModel);
				postMultiple.setProperty("properCompletionCF");
				result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
			} catch (Exception e1) {
				e1.printStackTrace();
				result.setResult("\nERROR\n");
				editorPane.setText(result.getResult());
				editorPane.setForeground(Color.black);
			}
			
			String operationlog113 = "";
			
			if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
				output = "Error, Maude LTL Model Checker could not handle the model";
				operationlog113 = "Error, Maude LTL Model Checker could not handle the model";
			
			}
			
			
			time = System.currentTimeMillis() - currentTimestart; 
			if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				output+="\n However the Counterexample could not be generated";
				operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
			
				output=result.getResult()+" - VerificationTime: "+result.getPropertyVerificationTime()+"ms"+"\n";
				operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: FALSE ";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				
				//NON FACCIAMO GENERARE L'XML DATO CHE NON SI NOTA IL FATTO CHE UN TOKEN E' PRESENTE IN GIRO, SAREBBE TUTTO IL MODELLO COLORATO
				//generateXML( result.getCounterexample(), Savepathperformance);
					
				editorPane.setText(output);
				//System.out.println("\n HERE2 \n");
				
			}else{
				//System.out.println("\n HERE3 \n");
				if(result.getResult()!=null){
					if(result.getResult()!=null&&result.getResult().contains("All the Pools respect")){
					output=result.getResult()+" - VerificationTime: "+result.getPropertyVerificationTime()+"\n";
					operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: TRUE ";
					operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
					}					
				}		
			}
			if(result.getResult()!=null&&result.getResult().contains("exceeded"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime();
				operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+time+"ms"+" - Operation Result: TRUE ";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			
			
			if(unix)operationlog += "\n"+operationlog113;
			else operationlog += "\r\n"+operationlog113;
			
			editorPane.setText(output);
		}

		//properCompletionMF
		public void properCompletionMF(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter){
			operation_done=true;
			long time = 0;
			editorPane.setText("");		
			long currentTimestart = System.currentTimeMillis();
			PostMultipleParameters result=null;
			String output = null;
			
			try {
				//result = GetReq.PostProperCompletion(modelToParse , parsedModel);
				String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
				PostMultipleParameters postMultiple = new PostMultipleParameters();
				postMultiple.setOriginalModel(originalModelUrl);
				postMultiple.setParsedModel(parsedModel);
				postMultiple.setProperty("properCompletionMF");
				result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
			} catch (Exception e1) {
				e1.printStackTrace();
				result.setResult("\nERROR\n");
				editorPane.setText(result.getResult());
				editorPane.setForeground(Color.black);
			}
			
			String operationlog113 = "";
			
			if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
				output = "Error, Maude LTL Model Checker could not handle the model";
				operationlog113 = "Error, Maude LTL Model Checker could not handle the model";
			
			}
			
			
			time = System.currentTimeMillis() - currentTimestart; 
			if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				output+="\n However the Counterexample could not be generated";
				operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
			
				output=result.getResult()+" - VerificationTime: "+result.getPropertyVerificationTime()+"\n";
				operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: FALSE ";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				
				//NON FACCIAMO GENERARE L'XML DATO CHE NON SI NOTA IL FATTO CHE UN TOKEN E' PRESENTE IN GIRO, SAREBBE TUTTO IL MODELLO COLORATO
				//generateXML( result.getCounterexample(), Savepathperformance);
					
				editorPane.setText(output);
				//System.out.println("\n HERE2 \n");
				
			}else{
				//System.out.println("\n HERE3 \n");
				if(result.getResult()!=null){
					if(result.getResult()!=null&&result.getResult().contains("All the Pools respect")){
					output=result.getResult()+" - VerificationTime: "+result.getPropertyVerificationTime()+"ms"+"\n";
					operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: TRUE ";
					operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
					}					
				}		
			}
			if(result.getResult()!=null&&result.getResult().contains("exceeded"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime()+"ms";
				operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+time+"ms"+" - Operation Result: TRUE ";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			
			
			if(unix)operationlog += "\n"+operationlog113;
			else operationlog += "\r\n"+operationlog113;
			
			editorPane.setText(output);
		}

		//properCompletionCFMF
		public void properCompletionCFMF(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter){
			operation_done=true;
			long time = 0;
			editorPane.setText("");		
			long currentTimestart = System.currentTimeMillis();
			PostMultipleParameters result=null;
			String output = null;
			
			try {
				//result = GetReq.PostProperCompletion(modelToParse , parsedModel);
				String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
				PostMultipleParameters postMultiple = new PostMultipleParameters();
				postMultiple.setOriginalModel(originalModelUrl);
				postMultiple.setParsedModel(parsedModel);
				postMultiple.setProperty("properCompletionCFMF");
				result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
			} catch (Exception e1) {
				e1.printStackTrace();
				result.setResult("\nERROR\n");
				editorPane.setText(result.getResult());
				editorPane.setForeground(Color.black);
			}
			
			String operationlog113 = "";
			
			if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
				output = "Error, Maude LTL Model Checker could not handle the model";
				operationlog113 = "Error, Maude LTL Model Checker could not handle the model";
			
			}
			
			
			time = System.currentTimeMillis() - currentTimestart; 
			if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				output+="\n However the Counterexample could not be generated";
				operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
			
				output=result.getResult()+" - VerificationTime: "+result.getPropertyVerificationTime()+"ms"+"\n";
				operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: FALSE ";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				
				//NON FACCIAMO GENERARE L'XML DATO CHE NON SI NOTA IL FATTO CHE UN TOKEN E' PRESENTE IN GIRO, SAREBBE TUTTO IL MODELLO COLORATO
				//generateXML( result.getCounterexample(), Savepathperformance);
					
				editorPane.setText(output);
				//System.out.println("\n HERE2 \n");
				
			}else{
				//System.out.println("\n HERE3 \n");
				if(result.getResult()!=null){
					if(result.getResult()!=null&&result.getResult().contains("All the Pools respect")){
					output=result.getResult()+" - VerificationTime: "+result.getPropertyVerificationTime()+"ms"+"\n";
					operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: TRUE ";
					operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
					}					
				}		
			}
			if(result.getResult()!=null&&result.getResult().contains("exceeded"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime()+"ms";
				operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+time+"ms"+" - Operation Result: TRUE ";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			
			
			if(unix)operationlog += "\n"+operationlog113;
			else operationlog += "\r\n"+operationlog113;
			
			editorPane.setText(output);
		}
		
	//safeness
		public void safeness(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter){
			long time = 0;
			String output=null;
			operation_done=true;
			editorPane.setText("");		
			long currentTimestart = System.currentTimeMillis();
			PostMultipleParameters result = new PostMultipleParameters();
			try {
				String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
				PostMultipleParameters postMultiple = new PostMultipleParameters();
				postMultiple.setOriginalModel(originalModelUrl);
				postMultiple.setParsedModel(parsedModel);
				postMultiple.setProperty("safeness");
				result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
				////System.out.println("\nresult: "+result.getResult());
			} catch (Exception e1) {
				e1.printStackTrace();
				result.setResult("\nERROR\n");
				editorPane.setText(result.getResult());
				editorPane.setForeground(Color.black);
			}
			String operationlog114="";
			
			if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
				output = "Error, Maude LTL Model Checker could not handle the model";
				operationlog114 = "Error, Maude LTL Model Checker could not handle the model";
		
			}
			
			time = System.currentTimeMillis() - currentTimestart; 
			if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				output+="\n However the Counterexample could not be generated";
				operationlog114 = "Safeness "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
				operationlog114+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			
			if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
				//System.out.println("\n inside safeness result handler");

				output=result.getResult()+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				operationlog114 = "Safeness "+result.getResult() +" - "+"VerificationTime: "+ time+"ms";
				operationlog114+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

			}else{
				if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
				{
					output = "TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
					operationlog114 += "Safeness "+"TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+time+"ms"+" - Operation Result: TRUE ";
					operationlog114+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}
				editorPane.setText(output);			

			}
			editorPane.setText(output);
			if(unix)operationlog += "\n"+operationlog114;
			else operationlog += "\r\n"+operationlog114;			
	}

//NEW PARAMETERIZED PROPERTIES
//noDeadActivitiesParam
		public void noDeadActivitiesParam(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter, String poolName){		
			operation_done=true;
			editorPane.setText("");		
			long currentTimestart10 = System.currentTimeMillis();
			PostMultipleParameters result = new PostMultipleParameters();
			try {
				//result = GetReq.PostNoDeadActivities(modelToParse , parsedModel);
				String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
				PostMultipleParameters postMultiple = new PostMultipleParameters();
				postMultiple.setOriginalModel(originalModelUrl);
				postMultiple.setParsedModel(parsedModel);
				postMultiple.setPoolName(poolName);
				postMultiple.setProperty("noDeadActivitiesParam");
				result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
			} catch (Exception e1) {
				e1.printStackTrace();
				result.setResult("\nERROR\n");
				editorPane.setText(result.getResult());
				editorPane.setForeground(Color.black);
			}
			
			@SuppressWarnings("unused")
			String output = "";
			String operationlog10="";
						
			if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
				output = "Error, Maude LTL Model Checker could not handle the model";
				operationlog10 = "Error, Maude LTL Model Checker could not handle the model";
		
			}
			long currentTimeend10 = System.currentTimeMillis();
			long time10 = 0;
			time10 = currentTimeend10 - currentTimestart10; 
			
			if (result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
			{
				output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				output+="\n However the Counterexample could not be generated";
				operationlog10 = "NoDeadActivities "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
				operationlog10+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			
			if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
				//System.out.println("\nPostNoDeadActivitiesResult: "+result+"\n");				
				
				output = "FALSE - THE PROPERTY IS NOT VERIFIED  - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms ";
				operationlog10 = "noDeadActivities " +"FALSE - THE PROPERTY IS NOT VERIFIED  - "+"VerificationTime: "+ time10+"ms ";
				operationlog10+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				
				result.setResult(result.getResult()+" - "+time10+"ms\n");
				
			}
		
			if(unix)operationlog += "\n"+operationlog10;
			else operationlog += "\r\n"+operationlog10;
		
			if(result.getResult()!=null&&result.getResult().contains("result Bool: true")){
				editorPane.setText("TRUE - THE PROPERTY IS VERIFIED - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms");
				//System.out.println("result.getResult():" +result.getResult());
				result.setResult(result.getResult()+" - "+time10+"ms\n");
			}
			else{
				editorPane.setText(result.getResult()+" - "+result.getPropertyVerificationTime()+"ms");				
			}
		}	
	
	
//OptionToCompleteParam
public void optionToCompleteParam(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter, String poolName){	
		operation_done=true;
		editorPane.setText("");		
		long currentTimestart = System.currentTimeMillis();
		PostMultipleParameters result = new PostMultipleParameters();
		String output = null;
		try {
			//result = GetReq.PostOptionToComplete(modelToParse , parsedModel);
			String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
			PostMultipleParameters postMultiple = new PostMultipleParameters();
			postMultiple.setOriginalModel(originalModelUrl);
			postMultiple.setParsedModel(parsedModel);
			postMultiple.setPoolName(poolName);
			postMultiple.setProperty("optionToCompleteParam");
			result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
	
		} catch (Exception e1) {
			e1.printStackTrace();
			result.setResult("\nERROR\n");
			editorPane.setText(result.getResult());
			editorPane.setForeground(Color.black);
		}
		long currentTimeend = System.currentTimeMillis();
		long time = 0;
		time = currentTimeend - currentTimestart; 
		
		String operationlog112="";
					
		if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
			output = "Error, Maude LTL Model Checker could not handle the model";
			operationlog112 = "Error, Maude LTL Model Checker could not handle the model";
	
		}
		
		if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			output+="\n However the Counterexample could not be generated";
			operationlog112 = "OptionToComplete "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
			operationlog112+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
	
			operationlog112 = "OptionToComplete "+"FALSE - THE PROPERTY IS NOT VERIFIED  - "+"VerificationTime: "+ time+"ms" +" - Operation Result: FALSE ";
			operationlog112+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";				
			
			if(unix)operationlog += "\n"+operationlog112;
			else operationlog += "\r\n"+operationlog112;
			
			generateXML( result.getCounterexample(), Savepathperformance);
	
			output=result.getResult()+"\nVerificationTime: "+result.getPropertyVerificationTime()+"ms";
			editorPane.setText(output);
	
		}else{
			
			if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
			{
				output = "TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				operationlog112 ="OptionToComplete "+"TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+time+"ms"+ " - Operation Result: TRUE ";
				operationlog112+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			editorPane.setText(output);
	
		}
		
		if(result.getResult()!=null&&result.getResult().contains("exceeded"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime()+"ms";
			operationlog112 = "OptionToComplete "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+time+"ms"+" - Operation Result: TRUE ";
			operationlog112+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
	}
	
//properCompletionCFParam
	public void properCompletionCFParam(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter, String poolName){
		operation_done=true;
		long time = 0;
		editorPane.setText("");		
		long currentTimestart = System.currentTimeMillis();
		PostMultipleParameters result=null;
		String output = null;
		
		try {
			String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
			PostMultipleParameters postMultiple = new PostMultipleParameters();
			postMultiple.setOriginalModel(originalModelUrl);
			postMultiple.setParsedModel(parsedModel);
			postMultiple.setPoolName(poolName);
			postMultiple.setProperty("properCompletionCFParam");
			result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
		} catch (Exception e1) {
			e1.printStackTrace();
			result.setResult("\nERROR\n");
			editorPane.setText(result.getResult());
			editorPane.setForeground(Color.black);
		}
		
		String operationlog113 = "";
		
		if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
			output = "Error, Maude LTL Model Checker could not handle the model";
			operationlog113 = "Error, Maude LTL Model Checker could not handle the model";
		
		}
		
		
		time = System.currentTimeMillis() - currentTimestart; 
		if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			output+="\n However the Counterexample could not be generated";
			operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
			operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
		
			output=result.getResult()+" - VerificationTime: "+result.getPropertyVerificationTime()+"ms"+"\n";
			operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: FALSE ";
			operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			
			//NON FACCIAMO GENERARE L'XML DATO CHE NON SI NOTA IL FATTO CHE UN TOKEN E' PRESENTE IN GIRO, SAREBBE TUTTO IL MODELLO COLORATO
			//generateXML( result.getCounterexample(), Savepathperformance);
				
			editorPane.setText(output);
			//System.out.println("\n HERE2 \n");
			
		}else{
			//System.out.println("\n HERE3 \n");
			if(result.getResult()!=null){
				if(result.getResult()!=null&&result.getResult().contains("All the Pools respect")){
				String propOk=result.getResult().replaceAll("All the Pools respect", "The pool respects");
				output=propOk+" - VerificationTime: "+result.getPropertyVerificationTime()+"ms"+"\n";
				operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: TRUE ";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}					
			}		
		}
		if(result.getResult()!=null&&result.getResult().contains("exceeded"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime()+"ms";
			operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+time+"ms"+" - Operation Result: TRUE ";
			operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		
		
		if(unix)operationlog += "\n"+operationlog113;
		else operationlog += "\r\n"+operationlog113;
		
		editorPane.setText(output);
	}

//properCompletionMFParam
	public void properCompletionMFParam(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter, String poolName){
		operation_done=true;
		long time = 0;
		editorPane.setText("");		
		long currentTimestart = System.currentTimeMillis();
		PostMultipleParameters result=null;
		String output = null;
		
		try {
			//result = GetReq.PostProperCompletion(modelToParse , parsedModel);
			String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
			PostMultipleParameters postMultiple = new PostMultipleParameters();
			postMultiple.setOriginalModel(originalModelUrl);
			postMultiple.setParsedModel(parsedModel);
			postMultiple.setPoolName(poolName);
			postMultiple.setProperty("properCompletionMFParam");
			result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
		} catch (Exception e1) {
			e1.printStackTrace();
			result.setResult("\nERROR\n");
			editorPane.setText(result.getResult());
			editorPane.setForeground(Color.black);
		}
		
		String operationlog113 = "";
		
		if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
			output = "Error, Maude LTL Model Checker could not handle the model";
			operationlog113 = "Error, Maude LTL Model Checker could not handle the model";
		
		}
		
		
		time = System.currentTimeMillis() - currentTimestart; 
		if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			output+="\n However the Counterexample could not be generated";
			operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
			operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
		
			output=result.getResult()+" - VerificationTime: "+result.getPropertyVerificationTime()+"\n";
			operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: FALSE ";
			operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			
			//NON FACCIAMO GENERARE L'XML DATO CHE NON SI NOTA IL FATTO CHE UN TOKEN E' PRESENTE IN GIRO, SAREBBE TUTTO IL MODELLO COLORATO
			//generateXML( result.getCounterexample(), Savepathperformance);
				
			editorPane.setText(output);
			//System.out.println("\n HERE2 \n");
			
		}else{
			//System.out.println("\n HERE3 \n");
			if(result.getResult()!=null){
				if(result.getResult()!=null&&result.getResult().contains("All the Pools respect")){
				String propOk=result.getResult().replaceAll("All the Pools respect", "The pool respects");
				output=propOk+" - VerificationTime: "+result.getPropertyVerificationTime()+"ms"+"\n";
				operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: TRUE ";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}					
			}		
		}
		if(result.getResult()!=null&&result.getResult().contains("exceeded"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime();
			operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+time+"ms"+" - Operation Result: TRUE ";
			operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		
		
		if(unix)operationlog += "\n"+operationlog113;
		else operationlog += "\r\n"+operationlog113;
		
		editorPane.setText(output);
	}

//properCompletionCFMFParam
	public void properCompletionCFMFParam(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter, String poolName){
		operation_done=true;
		long time = 0;
		editorPane.setText("");		
		long currentTimestart = System.currentTimeMillis();
		PostMultipleParameters result=null;
		String output = null;
		
		try {
			//result = GetReq.PostProperCompletion(modelToParse , parsedModel);
			String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
			PostMultipleParameters postMultiple = new PostMultipleParameters();
			postMultiple.setOriginalModel(originalModelUrl);
			postMultiple.setParsedModel(parsedModel);
			postMultiple.setPoolName(poolName);
			postMultiple.setProperty("properCompletionCFMFParam");
			result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
		} catch (Exception e1) {
			e1.printStackTrace();
			result.setResult("\nERROR\n");
			editorPane.setText(result.getResult());
			editorPane.setForeground(Color.black);
		}
		
		String operationlog113 = "";
		
		if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
			output = "Error, Maude LTL Model Checker could not handle the model";
			operationlog113 = "Error, Maude LTL Model Checker could not handle the model";
		
		}
		
		
		time = System.currentTimeMillis() - currentTimestart; 
		if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			output+="\n However the Counterexample could not be generated";
			operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
			operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
		
			output=result.getResult()+" - VerificationTime: "+result.getPropertyVerificationTime()+"\n";
			operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: FALSE ";
			operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			
			//NON FACCIAMO GENERARE L'XML DATO CHE NON SI NOTA IL FATTO CHE UN TOKEN E' PRESENTE IN GIRO, SAREBBE TUTTO IL MODELLO COLORATO
			//generateXML( result.getCounterexample(), Savepathperformance);
				
			editorPane.setText(output);
			//System.out.println("\n HERE2 \n");
			
		}else{
			//System.out.println("\n HERE3 \n");
			if(result.getResult()!=null){
				if(result.getResult()!=null&&result.getResult().contains("All the Pools respect")){
				String propOk=result.getResult().replaceAll("All the Pools respect", "The pool respects");
				output=propOk+" - VerificationTime: "+result.getPropertyVerificationTime()+"ms"+"\n";
				operationlog113="ProperCompletion "+result.getResult()+" - VerificationTime: "+time+"ms"+ " - Operation Result: TRUE ";
				operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
				}					
			}		
		}
		if(result.getResult()!=null&&result.getResult().contains("exceeded"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+result.getPropertyVerificationTime()+"ms";
			operationlog113 = "ProperCompletion "+"FALSE - THE PROPERTY IS NOT VERIFIED - "+"VerificationTime: EXCEEDED "+time+"ms"+" - Operation Result: TRUE ";
			operationlog113+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		
		
		if(unix)operationlog += "\n"+operationlog113;
		else operationlog += "\r\n"+operationlog113;
		
		editorPane.setText(output);
	}
	
//safenessParam
	public void safenessParam(final File modelToParse, final String parsedModel, final String Savepathperformance, final String Loadpath, final FileWriter StatisticsFileWriter, String poolName){
		long time = 0;
		String output=null;
		operation_done=true;
		editorPane.setText("");		
		long currentTimestart = System.currentTimeMillis();
		PostMultipleParameters result = new PostMultipleParameters();
		try {
			String originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
			PostMultipleParameters postMultiple = new PostMultipleParameters();
			postMultiple.setOriginalModel(originalModelUrl);
			postMultiple.setParsedModel(parsedModel);
			postMultiple.setPoolName(poolName);
			postMultiple.setProperty("safenessParam");
			result=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(postMultiple);
			////System.out.println("\nresult: "+result.getResult());
		} catch (Exception e1) {
			e1.printStackTrace();
			result.setResult("\nERROR\n");
			editorPane.setText(result.getResult());
			editorPane.setForeground(Color.black);
		}
		String operationlog114="";
		
		if (result.getResult().contains("Error, Maude LTL Model Checker could not handle the model")){
			output = "Error, Maude LTL Model Checker could not handle the model";
			operationlog114 = "Error, Maude LTL Model Checker could not handle the model";
	
		}
		
		time = System.currentTimeMillis() - currentTimestart; 
		if(result.getCounterexample()!=null && result.getCounterexample().contains("Counterexample could not be generated"))
		{
			output = "FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			output+="\n However the Counterexample could not be generated";
			operationlog114 = "Safeness "+"FALSE - THE PROPERTY IS NOT VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms "+" - Operation Result: FALSE However the Counterexample could not be generated";
			operationlog114+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
		}
		
		if(result.getCounterexample()!=null && !result.getCounterexample().contains("Counterexample could not be generated")&&!result.getCounterexample().contains("counterexample is null")){
			//System.out.println("\n inside safeness result handler");

			output=result.getResult()+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
			operationlog114 = "Safeness "+result.getResult() +" - "+"VerificationTime: "+ time+"ms";
			operationlog114+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";

		}else{
			if (result.getResult()!=null&&result.getResult().contains("result Bool: true"))
			{
				output = "TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+result.getPropertyVerificationTime()+"ms";
				operationlog114 += "Safeness "+"TRUE - THE PROPERTY IS VERIFIED"+ " - "+"VerificationTime: "+time+"ms"+" - Operation Result: TRUE ";
				operationlog114+=" Real Maude Time: "+result.getPropertyVerificationTime()+"ms";
			}
			editorPane.setText(output);			

		}
		editorPane.setText(output);
		if(unix)operationlog += "\n"+operationlog114;
		else operationlog += "\r\n"+operationlog114;			

	}
	

	

	public void generateXML( String counterexampleOriginal, String savepath) {
		int index;
		boolean unix;
		//System.out.println("\ncounterexampleOriginal: "+counterexampleOriginal+"\n");
		String counterexample= counterexampleOriginal;
		
		if (counterexample.contains("counterexample")&&counterexample.contains("£")){
			String[] counterexampleArray = counterexample.split("£");
			counterexample=counterexampleArray[1];
		}
		//System.out.println("\ncounterexample: "+counterexample);
		
		String[] appoggio=counterexample.split("\\*");
		counterexample=appoggio[0];
		if(!System.getProperty("os.name").startsWith("Windows")){
			index = savepath.toString().lastIndexOf("/");	
			unix=true;
		}else{
			index = savepath.toString().lastIndexOf("\\");
			unix=false;
		}
		
		String copypath = savepath.toString().substring(0, index);
		String filecopy = null;
		
		if(unix)filecopy = copypath+"/copia1.bpmn";
		else filecopy = copypath+"\\copia1.bpmn";
			
		try{
		//from string to xml
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document doc = builder.parse(new InputSource(new StringReader(counterexample)));

		// Write the parsed document to an xml file
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(doc);

		StreamResult result =  new StreamResult(new File(filecopy));
		transformer.transform(source, result);
		}catch (IOException | ParserConfigurationException | SAXException | TransformerException e) {
		     e.printStackTrace();
		}
		
		try{		
			openFile(filecopy);
		}catch (Exception e) {
		     e.printStackTrace();
		}  	
		
		return;
	
	}
	
	public void openFile(final String filecopy) throws Exception
	{
		Display.getDefault().asyncExec(new Runnable() {
		@SuppressWarnings("deprecation")
		public void run() {
		   
		IWorkbenchPage page = null;
		String TitleEditor = null;
		
		int startindex = filecopy.lastIndexOf("/");
		int endindex = filecopy.length()-5;
		TitleEditor = filecopy.substring(startindex+1, endindex);
		
		File fileToOpen = new File(filecopy);
		if (fileToOpen.exists() && fileToOpen.isFile()) {  
			IFileStore fileStore = EFS.getLocalFileSystem().getStore(fileToOpen.toURI());
			IWorkbenchWindow[] windows = PlatformUI.getWorkbench().getWorkbenchWindows();
			for(int i=0; i<windows.length; i++) { 
				IWorkbenchPage[] pages = windows[i].getPages();
				for(int j=0; i<pages.length; i++){
					windows[i].setActivePage(pages[j]);
					page = windows[i].getActivePage();

					IEditorPart[] editors= page.getEditors();
					for(int k=0; k<page.getEditors().length; k++){
									
						if(!System.getProperty("os.name").startsWith("Windows")){
							if(editors[k].getTitle().toString().equals(TitleEditor)){
								page.closeEditor(editors[k], false);
							} 
						}else{
						
							String[] parts = TitleEditor.split("\\\\");
							String editorTitle = parts[parts.length-1];

							if(editors[k].getTitle().toString().equals(editorTitle)){
								page.closeEditor(editors[k], false);
							} 
						}
															
					}
				}
			}
			
		    try {
		    	IDE.openEditorOnFileStore( page, fileStore );
		    } catch ( PartInitException e ) {
		        //Put your exception handler here if you wish to
		    }
		} else {
		    //[ToDo] Do something if the file does not exist
		}
				
	}
		});
	}	
	
	
//	obtainTaskList
	public void obtainPoolAndTaskList(String parsedModel, ArrayList<String> poolListNames, ArrayList<String> tasksNames){
		
		////System.out.println("\nparsedModel: "+parsedModel);	
		////System.out.println("\nIn obtainPoolAndTaskList");
		Collaboration C = new Collaboration();
		ArrayList<String> poolList = C.SplitPool(parsedModel);
		ArrayList<String> procElements = new ArrayList<String>();		
		////System.out.println("\npoolList.size(): "+poolList.size());
		for(int i = 0; i<poolList.size(); i++)
		{			
			////System.out.println("\nPoolN: "+i);
			String poolName;
			try{
				
			    //Pool pool1 = new Pool(poolList.get(i));
			    
			    Pool pool1 = new Pool();
			    
			    poolName=pool1.extractName(poolList.get(i));
			    poolListNames.add(poolName);
			    //DEVO ESTRARRE TUTTI I TASK, QUELLI SEMPLICI, I SEND, E I RECEIVE ... 
			    // POI NE METTO IN UNA LISTA DI STRING I NOMI E POI POSSO PROCEDERE CON LA STESURA
			    //DEL MAUDE COMMAND
	//TasksNames
			    ////System.out.println("\npoolName: "+poolName);
			    //poolName=pool1.getOrganizationName();
			    ////System.out.println("\n\nPool getOrganizationName: "+poolName);
			    Proc ProcessElements = new Proc();
			    procElements=ProcessElements.extractProcElement(poolList.get(i));
			    ////System.out.println("\n\nEstraggo i processElements ");
			    ////System.out.println("\nBefore  ProcessElements.analizeProc(procElements)");
			    ProcessElements.analizeProc(procElements);
			    ////System.out.println("\nAfter  ProcessElements.analizeProc(procElements)");
			    ////System.out.println("\n\nGenero gli oggetti corrispondenti agli elementi ");
			    ArrayList<Task> tasks=ProcessElements.getTasks();//);
			    ////System.out.println("\n\nEstraggo i Task ");
			    for(Iterator<Task> iTask = tasks.iterator(); iTask.hasNext(); ) {
			    	tasksNames.add(iTask.next().name);
			    }

	//SndTasksNames
			    ArrayList<SendTask> sndTasks=ProcessElements.getSendTask();//);
			    //poolListNames.add(poolName);
			    for(Iterator<SendTask> iTask = sndTasks.iterator(); iTask.hasNext(); ) {
			    	tasksNames.add(iTask.next().name);
			    }
			    
	//ReceiveTasksNames
			    ArrayList<ReceiveTask> rcvTasks=ProcessElements.getReceiveTask();//);
			    //poolListNames.add(poolName);
			    for(Iterator<ReceiveTask> iTask = rcvTasks.iterator(); iTask.hasNext(); ) {
			    	tasksNames.add(iTask.next().name);
			    }

	//Print TaskName List
			    ////System.out.println("\n Stampo Nomi dei Task\n");
			    //for(Iterator<String> iTask = tasksNames.iterator(); iTask.hasNext(); ) {
			    //	//System.out.println("\n TaskName: "+ iTask.next());
			    //}
			    
			}catch(Exception e){
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				sw.toString();
				////System.out.println("\n Exception: "+ sw);				
			}
			
		}	
		

	}
	
	
//	obtainTaskList
	public void obtainPoolAndTaskListAndMsgList(String parsedModel, ArrayList<String> poolListNames, ArrayList<String> tasksNames, ArrayList<String> inputMsgString, ArrayList<String> outputMsgString){
		
		////System.out.println("\nparsedModel: "+parsedModel);	
		////System.out.println("\nIn obtainPoolAndTaskList");
		Collaboration C = new Collaboration();
		ArrayList<String> poolList = C.SplitPool(parsedModel);
		ArrayList<String> procElements = new ArrayList<String>();	
		ArrayList<Msg> inputMsg = new ArrayList<Msg>();
		ArrayList<Msg> outputMsg = new ArrayList<Msg>();
		////System.out.println("\npoolList.size(): "+poolList.size());
		for(int i = 0; i<poolList.size(); i++)
		{			
			////System.out.println("\nPoolN: "+i);
			String poolName;
			try{
				
			    //Pool pool1 = new Pool(poolList.get(i));
			    
			    Pool pool1 = new Pool();
			    
			    poolName=pool1.extractName(poolList.get(i));
			    poolListNames.add(poolName);
			    
			    //Estraggo i messaggi
			    inputMsg=pool1.extractInputMsg(poolList.get(i));
			    outputMsg=pool1.extractOutputMsg(poolList.get(i));
			    for(int i2=0; i2<inputMsg.size();i2++){
			    	inputMsgString.add(inputMsg.get(i2).getMsgName());
			    	//System.out.println("\ninputMsg.get(i).getMsgName()\n: index: "+i2+"msg: "+inputMsg.get(i2).getMsgName());
			    }
			    
			    for(int i3=0; i3<outputMsg.size();i3++){
			    	outputMsgString.add(outputMsg.get(i3).getMsgName());
			    	//System.out.println("\noutputMsg.get(i).getMsgName()\n: index: "+i3+"msg: "+outputMsg.get(i3).getMsgName());
			    }
			    
			    
			    
			    //DEVO ESTRARRE TUTTI I TASK, QUELLI SEMPLICI, I SEND, E I RECEIVE ... 
			    // POI NE METTO IN UNA LISTA DI STRING I NOMI E POI POSSO PROCEDERE CON LA STESURA
			    //DEL MAUDE COMMAND
	//TasksNames
			    ////System.out.println("\npoolName: "+poolName);
			    //poolName=pool1.getOrganizationName();
			    ////System.out.println("\n\nPool getOrganizationName: "+poolName);
			    Proc ProcessElements = new Proc();
			    procElements=ProcessElements.extractProcElement(poolList.get(i));
			    ////System.out.println("\n\nEstraggo i processElements ");
			    ////System.out.println("\nBefore  ProcessElements.analizeProc(procElements)");
			    ProcessElements.analizeProc(procElements);
			    ////System.out.println("\nAfter  ProcessElements.analizeProc(procElements)");
			    ////System.out.println("\n\nGenero gli oggetti corrispondenti agli elementi ");
			    ArrayList<Task> tasks=ProcessElements.getTasks();//);
			    ////System.out.println("\n\nEstraggo i Task ");
			    for(Iterator<Task> iTask = tasks.iterator(); iTask.hasNext(); ) {
			    	tasksNames.add(iTask.next().name);
			    }

	//SndTasksNames
			    ArrayList<SendTask> sndTasks=ProcessElements.getSendTask();//);
			    //poolListNames.add(poolName);
			    for(Iterator<SendTask> iTask = sndTasks.iterator(); iTask.hasNext(); ) {
			    	tasksNames.add(iTask.next().name);
			    }
			    
	//ReceiveTasksNames
			    ArrayList<ReceiveTask> rcvTasks=ProcessElements.getReceiveTask();//);
			    //poolListNames.add(poolName);
			    for(Iterator<ReceiveTask> iTask = rcvTasks.iterator(); iTask.hasNext(); ) {
			    	tasksNames.add(iTask.next().name);
			    }

	//Print TaskName List
			    ////System.out.println("\n Stampo Nomi dei Task\n");
			    //for(Iterator<String> iTask = tasksNames.iterator(); iTask.hasNext(); ) {
			    //	//System.out.println("\n TaskName: "+ iTask.next());
			    //}
			    
			}catch(Exception e){
				StringWriter sw = new StringWriter();
				PrintWriter pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				sw.toString();
				////System.out.println("\n Exception: "+ sw);				
			}
			
		}	
//		for(int i2=0; i2<inputMsgString.size();i2++){
//	    	//inputMsgString.add(inputMsg.get(i2).getMsgName());
//	    	System.out.println("\ninputMsg.get(i).getMsgName()\n: index: "+i2+"msg: "+inputMsgString.get(i2));
//	    }
//	    
//	    for(int i3=0; i3<outputMsgString.size();i3++){
//	    	//outputMsgString.add(outputMsg.get(i3).getMsgName());
//	    	System.out.println("\noutputMsg.get(i).getMsgName()\n: index: "+i3+"msg: "+outputMsgString.get(i3));
//	    }
		

	}
}
