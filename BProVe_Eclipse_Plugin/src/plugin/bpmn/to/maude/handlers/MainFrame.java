package plugin.bpmn.to.maude.handlers;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import bprove_massive_call.MassiveBProVeCall;
import plugin.bpmn.to.maude.getService.GetReq;
import plugin.bpmn.to.maude.handlers.MainFrame.ConfirmedActionListener;
import plugin.bpmn.to.maude.handlers.MainFrame.LoadActionListener;
import plugin.bpmn.to.maude.handlers.MainFrame.SaveActionListener;
import plugin.bpmn.to.maude.handlers.MainFrame.exitActionListener;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.SwingWorker;
import javax.swing.JTextField;
import javax.swing.JButton;

import java.awt.FlowLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Desktop;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class MainFrame extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JTextField textField_1;

	static boolean unix =true;
	
	public JFrame frame;
	public JTextArea textArea;
	
	public File modelToParse;
    
	static String Loadfilepath = "";
    static String Savefilepath = "";
    static String Maudepath = "";
   
	public static void main(String[] args, String titlepage, String worspacelocation){

		  if(!System.getProperty("os.name").startsWith("Windows")){
			  Loadfilepath= worspacelocation+"/"+titlepage;
			  unix=true;
		  }else{
			  //JOptionPane.showMessageDialog(null, "You are on windows");
			  Loadfilepath= worspacelocation+"/"+titlepage;
			  unix=false;
			  //Loadfilepath=Loadfilepath.replaceAll("/", "\\");
		  }

		  
		  
		   EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainFrame frame = new MainFrame(Loadfilepath, Savefilepath, Maudepath );
					frame.setVisible(true);
					frame.setResizable(false);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}


		public MainFrame(String Loadfilepath, String Savefilepath, String Maude) throws URISyntaxException {

			//DEBUG 
			//////System.out.println("Loadfilepath: "+Loadfilepath+"\n");
			//DEBUG JOptionPane.showMessageDialog(null, "LOADPATH "+Loadfilepath);


			
			setTitle("BProVe");
			setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
			setBounds(100, 100, 700, 500);
			contentPane = new JPanel();
			contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
			setContentPane(contentPane);
			contentPane.setLayout(null);
			
			
			//PANEL First label
			JPanel panel = new JPanel();
			panel.setBounds(105, 10, 489, 26);
			FlowLayout flowLayout_0 = (FlowLayout) panel.getLayout();
			flowLayout_0.setAlignment(FlowLayout.LEFT);
			
			JLabel initial_text = new JLabel("The first part of the menu is reserved for operation on a single BPMN model.");
			panel.add(initial_text);
			contentPane.add(panel);
			
			
			//PANEL UPLOAD
			JPanel panel_upload = new JPanel();
			panel_upload.setBounds(92, 69, 516, 39);
			panel_upload.setToolTipText("");
			FlowLayout flowLayout_1 = (FlowLayout) panel_upload.getLayout();
			flowLayout_1.setAlignment(FlowLayout.LEFT);
			
			JLabel lblUpload = new JLabel("UPLOAD PATH");
			lblUpload.setHorizontalAlignment(SwingConstants.LEFT);
			panel_upload.add(lblUpload);
			
			textField = new JTextField(Loadfilepath);
			textField.setEditable(false);
			panel_upload.add(textField);
			textField.setColumns(25);
			
			JButton btnUpload = new JButton("UPLOAD");
			btnUpload.addActionListener(new LoadActionListener()); 
			panel_upload.add(btnUpload);
			contentPane.add(panel_upload);			
		
			//PANEL Save Path
			JPanel panel_save = new JPanel();
			panel_save.setBounds(90, 132, 518, 39);
			FlowLayout flowLayout = (FlowLayout) panel_save.getLayout();
			flowLayout.setAlignment(FlowLayout.LEFT);
			
			JLabel lblSavepath = new JLabel("SAVE PATH     ");
			lblSavepath.setHorizontalAlignment(SwingConstants.LEFT);
			panel_save.add(lblSavepath);
			
			textField_1 = new JTextField(Savefilepath);
			textField_1.setEditable(false);
			textField_1.setHorizontalAlignment(SwingConstants.LEFT);
			panel_save.add(textField_1);
			textField_1.setColumns(25);
			
			JButton btnSave = new JButton("    SAVE  ");
			btnSave.addActionListener(new SaveActionListener());  
			panel_save.add(btnSave);
			
			contentPane.add(panel_save);
			
			//PANEL Confirm
			JPanel panel_confirm = new JPanel();
			panel_confirm.setBounds(235, 213, 199, 39);
			JButton btnGenerate = new JButton("GENERATE");
			btnGenerate.addActionListener(new ConfirmedActionListener()); 
			panel_confirm.add(btnGenerate);
								
			JButton btnExit = new JButton("EXIT");
			btnExit.addActionListener(new exitActionListener()); 
			panel_confirm.add(btnExit);
			contentPane.add(panel_confirm);
					
			
//			//Massive Translation and Verification Commented Out (set to run only on localhost)
//			
//			JPanel panel_2 = new JPanel();
//			panel_2.setBounds(92, 268, 520, 26);
//			FlowLayout flowLayout_2 = (FlowLayout) panel.getLayout();
//			flowLayout_2.setAlignment(FlowLayout.LEFT);
//			
//			//PANEL Second Label
//			//JPanel panel_2 = new JPanel();
//			JLabel lblTheSecondPart = new JLabel("The second part of the menu is reserved for operation on a set of BPMN models.");
//			panel_2.add(lblTheSecondPart);
//			contentPane.add(panel_2);
//			
//			//PANEL MASSIVE TRANSLATION
//			JPanel massive_panel = new JPanel();
//			massive_panel.setBounds(137, 306, 400, 30);
//			FlowLayout flowLayout_3 = (FlowLayout) panel.getLayout();
//			flowLayout_3.setAlignment(FlowLayout.LEFT);
//			JButton btnmassivo = new JButton("MASSIVE TRANSLATION");
//			btnmassivo.addActionListener(new massiveActionListener());
//			massive_panel.add(btnmassivo);
//							
//			JButton buttonExit2 = new JButton("EXIT");
//			buttonExit2.addActionListener(new exitActionListener());
//			massive_panel.add(buttonExit2);
//			contentPane.add(massive_panel);
			
			
			
			
			
			
			
			//Powered By pros.unicam.it
			final URI uri = new URI("http://pros.unicam.it/");
			
			class OpenUrlAction implements ActionListener {
				  @Override public void actionPerformed(ActionEvent e) {
				      open(uri);
				  }
			}
				
			JPanel panel_poweredby = new JPanel();
			panel_poweredby.setBounds(178, 347, 382, 39);
			 JButton powered_button = new JButton();
			 powered_button.setText("<HTML>Powered by <FONT color=\"#000099\"><U>PROS Lab - University of Camerino, Italy</U></FONT>"
			        + "</HTML>");
			 powered_button.setHorizontalAlignment(SwingConstants.LEFT);
			 powered_button.setBorderPainted(false);
			 powered_button.setOpaque(false);
			 powered_button.setBackground(Color.WHITE);
			 powered_button.setToolTipText(uri.toString());
			 powered_button.addActionListener(new OpenUrlAction());
			panel_poweredby.add(powered_button);
			contentPane.add(panel_poweredby);
		

	}
	
	//Open URI
	   private static void open(URI uri) {
		    if (Desktop.isDesktopSupported()) {
		      try {
		        Desktop.getDesktop().browse(uri);
		      } catch (IOException e) { /* TODO: error handling */ }
		    } else { /* TODO: error handling */ }
		  }
	
	//LoadActionListener
	   class LoadActionListener implements ActionListener{
		      public void actionPerformed(ActionEvent e) {

		    	String userDir = System.getProperty("user.home");
		  		JFileChooser fileChooser = new JFileChooser(userDir +"/Desktop");
				int result = fileChooser.showOpenDialog(fileChooser);
				if (result == JFileChooser.APPROVE_OPTION) {
				    File selectedFile = fileChooser.getSelectedFile();
				    Loadfilepath = selectedFile.getAbsolutePath();
				    textField.setText(Loadfilepath);
				}
		      }
		   }
	   
	   
	   //SaveActionListener
	   class SaveActionListener implements ActionListener{
		      public void actionPerformed(ActionEvent e) {
		    	  JFrame parentFrame = new JFrame();
		    	  
		    	  String userDir = System.getProperty("user.home");
			  	  JFileChooser fileChooser = new JFileChooser(userDir +"/Desktop");
		    	  fileChooser.setDialogTitle("Specify a file to save");   
		    	   
		    	  int userSelection = fileChooser.showSaveDialog(parentFrame);
		    	   
		    	  if (userSelection == JFileChooser.APPROVE_OPTION) {
		    	      File fileToSave = fileChooser.getSelectedFile();
		    	      File txtFile = new File(fileChooser.getSelectedFile()+".txt");
		    	      //////System.out.println("fileToSave.exists(): " + txtFile+"\n");
			    	  if(txtFile.exists()){
			  			  int reply = JOptionPane.showConfirmDialog(null, "File already existing, overwrite?", "Confirm", JOptionPane.YES_NO_OPTION);
			  			  if ( reply == JOptionPane.NO_OPTION ){
			  				JOptionPane.showMessageDialog(parentFrame, "Change Savepath\n");
			  				return;
			  			  }
			  		  }
		    	      ////System.out.println("Save as file: " + fileToSave.getAbsolutePath()+"\n");
					    Savefilepath = fileToSave.getAbsolutePath();
					    textField_1.setText(Savefilepath);
		    	  }
		      	}
			   
			   	}
	   
	   //The Generate Button does the following
	   //ConfirmedActionListener
	   class ConfirmedActionListener implements ActionListener{
		   
		   FileWriter fw;
		   long parsingTime = 0;
		   
		      public void actionPerformed(ActionEvent e) {
		    	  PostMultipleParameters parsedModel = new PostMultipleParameters();	    	  
		    	  
		    	  JFrame parentFrame = new JFrame();
		    	  
		    	  if(Loadfilepath.isEmpty()&&Savefilepath.isEmpty()){
		    		  JOptionPane.showMessageDialog(parentFrame, "Loadpath and Savepath are missing\n");
		    		  return;
		    	  }
		    	  if(Loadfilepath.isEmpty()){
		    		  JOptionPane.showMessageDialog(parentFrame, "Loadpath is missing\n");
		    		  return;
		    	  }
		    	  if(Savefilepath.isEmpty()){
		    		  JOptionPane.showMessageDialog(parentFrame, "Savepath is missing\n");
		    		  return;
		    	  }
		    	  
		    	  modelToParse = new File(Loadfilepath);
		    	  String originalModelUrl;
		    	  
				  try {
				 	
				  	  originalModelUrl = new String(Files.readAllBytes(Paths.get(modelToParse.toString())));
			    	  PostMultipleParameters inputM = new PostMultipleParameters();
			    	  inputM.setOriginalModel(originalModelUrl);
	    		      parsingTime=System.currentTimeMillis();
	    		      //OLD WITHOUT JSON
		    		  //parsedModel=GetReq.PostReq_BProve_Maude_WebServie(inputM);
	    		      //NEW WITH JSON
		    		  parsedModel=GetReq.PostReq_BProve_Maude_WebServie_Property_JSON(inputM);
		    		  
		    		  ////System.out.println("\nparsedModel: "+parsedModel);
		    		  parsingTime=System.currentTimeMillis()-parsingTime;

   				  } catch (IOException e2) {
					  e2.printStackTrace();
				  } catch (Exception e1) {
					  e1.printStackTrace();
				  }
		  		
		    	  //TENTATIVO DI CONVERSIONE AUTOMATICA A JSON DELLA CLASSE POSTMULTIPLEPARAMETERS
//				  String jsonString = class2Json(parsedModel);
//				  System.out.println("\njsonString: "+jsonString);
				  /* TODO: LA STRINGA JSONSTRING è PRONTA PER ESSERE INVIATA AL SERVER VEDERE COME CAMBIARE IL TUTTO*/
				  //PostMultipleParameters jsonPostMultipleParameters =json2Class(jsonString);
				  //jsonPostMultipleParameters.printPostMultipleParameters();
			    					  
		    	  if (parsedModel.getParsedModel().contains("Error")){  
		    		JOptionPane.showMessageDialog(parentFrame, "\nThe parsing encountered some problems. Check if the webservice is still online\n"+parsedModel);
				  }else{
		    	    
			    	  if (parsedModel.getParsedModel().contains("Correct Duplicated Labels")){  
			    		 JOptionPane.showMessageDialog(parentFrame, "\nThe parsing encountered some problems.\n"+parsedModel);
					  }else{
			      
				    	  if (parsedModel.getParsedModel().contains("The model presents the following ineligible BPMN elements")){
				    		  //dialog that tells which elements are present that cannot be translated
				    		  JOptionPane.showMessageDialog(parentFrame, "\n"+parsedModel.parsedModel);			    	
				    	  	}else{
				    		  //
				    	  		File file = new File(Savefilepath+".txt");
					     		
							    try {
									
									fw = new FileWriter(file);
									
									writeElementAndParsingTime(fw,parsedModel,parsingTime,unix);						
												    	     
								} catch (IOException e1) {
									
									e1.printStackTrace();
								}
		

					    		int reply = JOptionPane.showConfirmDialog(null, "Do you want to proceed with Properties Verification?", "Confirm", JOptionPane.YES_NO_OPTION);
					   		  
					    		if (reply == JOptionPane.YES_OPTION)MaudeOperation.main(modelToParse , parsedModel.getParsedModel(), Savefilepath, Loadfilepath, fw );
					    		
					    	}//if(parsedModel.getParsedModel().contains("The model presents the following ineligible BPMN elements")
					  }// if (parsedModel.getParsedModel().contains("Correct Duplicated Labels"))  
				  }//if (parsedModel.getParsedModel().contains("Error"))  	    	  
			  }//public void actionPerformed(ActionEvent e)

	   }
	

//massiveActionListener
	   class massiveActionListener implements ActionListener{
		      public void actionPerformed(ActionEvent e) {
		    	  
		    	JFrame frame;
		    	frame = new JFrame("Massive Translation");
		        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		          	    	  
		        String userDir = System.getProperty("user.home");
		  		JFileChooser fileChooser = new JFileChooser(userDir +"/Desktop");
	        	//choose directory from which reading the .bpmn files
		  		fileChooser.setDialogTitle("Choose bpmn models directory");
		  		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				String path = "";
				ArrayList<File> files = new ArrayList<File>();

				if (fileChooser.showOpenDialog(getParent()) == JFileChooser.APPROVE_OPTION) {
					path = fileChooser.getSelectedFile().getAbsolutePath();
					File directory = new File(path);
					// get all the files from a directory
					File[] fList = directory.listFiles();
					
					for (File file : fList) {
						if(file.isFile())
						{
							if(file.getName().endsWith(".bpmn"))
							{
								//System.out.println(file.getName());
								files.add(file);
							}
						}
					}
	
					//System.out.println("Selected directory " + path);
				} else {
					//System.out.println("Choice aborted");
				}
				
			  	JFileChooser chooser = new JFileChooser(userDir +"/Desktop");
    	  
		    	chooser = new JFileChooser(); 
		    	chooser.setCurrentDirectory(new java.io.File("."));
		    	chooser.setDialogTitle("Save Path");
		    	chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		    	chooser.setAcceptAllFileFilterUsed(false);
		    	  
		    	////System.out.print("\nNumber of uploaded files: "+files.length+"\n");
		    	   
		    	textArea = new JTextArea(20, 35);
			    textArea.setText("Massive Translation  & Verification IN PROGRESS...\n");
			    textArea.setEditable(false);
			    JScrollPane scrollArea = new JScrollPane(textArea);
			          
			    frame.getContentPane().add(scrollArea);		          
			    frame.pack();
			    frame.setVisible(true);
			    frame.revalidate();
		    	frame.repaint();	
		    	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		    	MassiveBProVeCall.start(files , chooser, textArea);

		      }
	   }//class massiveActionListener implements ActionListener
	   	   
	class exitActionListener implements ActionListener{
		      public void actionPerformed(ActionEvent e) {	
		    	  MainFrame.this.dispose();
		      }
	   }


	private void writeElementAndParsingTime(FileWriter fw2, PostMultipleParameters parsedModel,
			long parsingTime2, boolean unix) throws IOException {

		String newLine;
		if(unix)newLine="\n";
		else newLine="\r\n";
		
		fw2.write(newLine+"Parsed Model: "+parsedModel.getParsedModel());
 	    fw2.write(newLine+"Total number of element: "+Integer.toString(parsedModel.getNumberOfElement()));
 	    fw2.write(newLine+"Number of pool: "+Integer.toString(parsedModel.getNumberOfPool()));
 	    fw2.write(newLine+"Number of start event: "+Integer.toString(parsedModel.getNumberOfStart()));
 	    fw2.write(newLine+"Number of message start event: "+Integer.toString(parsedModel.getNumberOfStartMsg()));
 	    fw2.write(newLine+"Number of end event: "+Integer.toString(parsedModel.getNumberOfEnd()));
 	    fw2.write(newLine+"Number of message end event: "+Integer.toString(parsedModel.getNumberOfEndMsg()));
 	    fw2.write(newLine+"Number of terminated end event: "+Integer.toString(parsedModel.getNumberOfTerminate()));
 	    fw2.write(newLine+"Number of task: "+Integer.toString(parsedModel.getNumberOfTask()));
 	    fw2.write(newLine+"Number of sendtask: "+Integer.toString(parsedModel.getNumberOfTaskSnd()));
 	    fw2.write(newLine+"Number of receivetask: "+Integer.toString(parsedModel.getNumberOfTaskRcv()));
 	    fw2.write(newLine+"Number of intermidiate throw event: "+Integer.toString(parsedModel.getNumberOfIntermidiateThrowEvent()));
 	    fw2.write(newLine+"Number of intermidiate catch event: "+Integer.toString(parsedModel.getNumberOfIntermidiateCatchEvent()));
 	    fw2.write(newLine+"Number of xorsplit gateway: "+Integer.toString(parsedModel.getNumberOfGatewayXorSplit()));
 	    fw2.write(newLine+"Number of xorjoin gateway: "+Integer.toString(parsedModel.getNumberOfGatewayXorJoin()));
 	    fw2.write(newLine+"Number of andsplit gateway: "+Integer.toString(parsedModel.getNumberOfGatewayAndSplit()));
 	    fw2.write(newLine+"Number of andjoin gateway: "+Integer.toString(parsedModel.getNumberOfGatewayAndJoin()));				
 	    fw2.write(newLine+"Number of orsplit gateway: "+Integer.toString(parsedModel.getNumberOfGatewayOrSplit()));
 	    fw2.write(newLine+"Number of event based gateway: "+Integer.toString(parsedModel.getNumberOfGatewayEventBased()));
 	    
 	    fw2.write(newLine+"ParsingTime: "+Long.toString(parsingTime2)+newLine);	    
		
	}   
}
	   
