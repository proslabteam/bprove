package plugin.bpmn.to.maude.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.core.resources.ResourcesPlugin;
/**
 * Our sample handler extends AbstractHandler, an IHandler base class.
 * @param <IWorkspace>
 * @see org.eclipse.core.cmmands.IHandler
 * @see org.eclipse.core.commands.AbstractHandler
 */
public class SampleHandler<IWorkspace> extends AbstractHandler {

	public SampleHandler() {
	}

	public Object execute(ExecutionEvent event) throws ExecutionException {	
		
		String pagetitle = "";
		String worspacelocation = "";

		try{
			worspacelocation =ResourcesPlugin.getWorkspace().getRoot().getLocation().toString();
		}
		catch (Exception e)
		{
	  		//JFrame frame = new JFrame("Workspace Location Error");
	     	//JOptionPane.showMessageDialog(frame,"Impossible to determine the workspace location");     
			e.printStackTrace();
		}	
		
		/**getActiveEditor**/
		//try{
			IEditorPart editor = HandlerUtil.getActiveEditor(event);
			if (editor.getTitle() != null)
			{
				pagetitle = editor.getTitleToolTip();   	
			}	
			else
			{
				pagetitle = null;
			}
		//} catch (Exception e){
			//System.out.print("errore:" + e.getMessage());
			//e.printStackTrace();
		//}	
		
		//CALL THE MAIN MENU
		MainFrame.main(null, pagetitle,worspacelocation);		
		return null;
	}

}