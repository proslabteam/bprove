package plugin.bpmn.to.maude.notation;

import org.w3c.dom.Document;

@SuppressWarnings("serial")
public class Counterexample  implements java.io.Serializable {
		
	
	Document violatingCounterexample;	
	Proc violatingProcess;

	
	public Counterexample() {
		this.violatingCounterexample = null;
		this.violatingProcess = null;
	}
	
	public void setViolatingProcess(Proc violatingProcess) {
		this.violatingProcess = violatingProcess;
	}

	public Proc getViolatingProcess() {
		return violatingProcess;
	}


	public Counterexample(Document violatingCounterexample, Proc violatingProcess) {
		this.violatingCounterexample = violatingCounterexample;
		this.violatingProcess = violatingProcess;
	}
	
	public Document getViolatingCounterexample() {
		return violatingCounterexample;
	}
	public void setViolatingCounterexample(Document violatingCounterexample) {
		this.violatingCounterexample = violatingCounterexample;
	}


}
